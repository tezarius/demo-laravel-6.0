<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| ICONS:
|       https://ionicons.com/v2/
|       https://useiconic.com/open#icons
|       https://fontawesome.com/icons
|
*/

///Route::middleware('web')->middleware('tz.init')->group(function()
///Route::group(['middleware'=>['web','tz.init']],function()
Route::middleware('tz.init')->group(function()
{
    ///Route::get('/', function () { return view('welcome'); });
    Route::get('/',['as'=>'home','uses'=>'StoreController@home']);
    Route::get('/offer','StoreController@offer')->name('offer');
    Route::get('/about','StoreController@about')->name('about');
    Route::get('/contacts','StoreController@contacts')->name('contacts');
    Route::get('/delivery','StoreController@delivery')->name('delivery');
    Route::get('/payment','StoreController@payment')->name('payment');
    Route::get('/refund','StoreController@refund')->name('refund');
    Route::get('/certificate','StoreController@certificate')->name('certificate');
    Route::post('/callback','StoreController@callback')->name('callback');
    Route::get('/news/{id?}','StoreController@news')->name('news');


    Route::prefix('basket')->group(function(){
        Route::get('/list','BasketController@main')->name('basketList');
        Route::post('/add','BasketController@add')->name('basketAdd');
        Route::post('/update','BasketController@update')->name('basketUpdate');
        Route::post('/delete','BasketController@remove')->name('basketRemove');
        Route::post('/create','BasketController@create')->name('basketCreate');
        Route::post('/change','BasketController@change')->name('basketChange');
        Route::post('/clear','BasketController@clear')->name('basketClear');
        Route::post('/coupon','BasketController@coupon')->name('basketCoupon');
    });

    Route::prefix('price')->group(function(){
        Route::get('/brands/{number}','PriceController@brands')->name('priceBrands');
        Route::get('/offers/{number}/{brand}','PriceController@offers')->name('priceOffers');
        Route::get('/history','PriceController@history')->name('priceHistory');
        Route::get('/goods/{hash}','PriceController@goods')->name('priceGoods');
        Route::post('/stock/info','PriceController@stockInfo')->name('stockInfo');
        Route::get('/name/{search}','PriceController@name')->name('priceName');
    });

    Route::prefix('goods')->group(function(){
        Route::get('/info/{hash}','GoodsController@info')->name('goodsInfo');
    });

    Route::prefix('office')->group(function(){
        Route::post('/choose/{id}','OfficeController@choose')->name('officeChoose');
        ///Route::post('/contacts','OfficeController@contacts')->name('officeContacts');
    });

    Route::prefix('account')->group(function(){
        Route::get('/logout','AccountController@logout')->name('logout');
        Route::any('/login','AccountController@login')->name('login');
        Route::any('/register','AccountController@register')->name('register');
    });

    /** Под авторизацию */
    Route::middleware('tz.auth')->group(function()
    {
        Route::prefix('account')->group(function()
        {
            Route::get('/','AccountController@main')->name('account');
            Route::any('/edit','AccountController@edit')->name('accountEdit');
            Route::any('/password','AccountController@password')->name('password');
            Route::get('/forgotten','AccountController@forgotten')->name('forgotten');
            Route::get('/wishlist','AccountController@wishlist')->name('wishlist');
            Route::get('/reward','AccountController@reward')->name('reward');
            Route::any('/newsletter','AccountController@newsletter')->name('newsletter');
        });
        Route::prefix('garage')->group(function()
        {
            Route::get('/','GarageController@main')->name('garage');
            Route::any('/add','GarageController@add')->name('garageAdd');
        });
        Route::prefix('balance')->group(function()
        {
            Route::any('/','BalanceController@main')->name('balance');
        });
        Route::prefix('orders')->group(function()
        {
            Route::post('/checkout','OrdersController@checkout')->name('orderCheckout');
            Route::any('/','OrdersController@main')->name('orders');
            Route::post('/history','OrdersController@history')->name('orderHistory');
            Route::post('/withdraw','OrdersController@withdraw')->name('orderWithdraw');
            Route::post('/delivery','OrdersController@delivery')->name('orderDelivery');
            Route::post('/toWork','OrdersController@toWork')->name('orderToWork');
        });
        /** А Д М И Н К А */
        Route::middleware('tz.room')->prefix('rooom')->group(function()
        {
            Route::get('/{url?}',function(){ return Redirect::route('roomNewsList'); })->where('url','|home|dashboard|main');
            Route::get('/','Room\StoreController@dashboard')->name('room');
            /** Новости */
            Route::prefix('news')->group(function()
            {
                Route::get('/','Room\NewsController@main')->name('roomNewsList');
                Route::any('/add','Room\NewsController@action')->name('roomNewsAdd');
                Route::any('/upd/{id}','Room\NewsController@action')->name('roomNewsUpd');
                Route::post('/del/{id}','Room\NewsController@delete')->name('roomNewsDel');
            });
            /** Категории */
            Route::prefix('category')->group(function()
            {
                Route::get('/list','Room\CategoryController@main')->name('roomCategoryList');
                Route::prefix('icon')->group(function()
                {
                    Route::any('/add','Room\CategoryController@iconAdd')->name('roomCatIconAdd');
                    Route::post('/del','Room\CategoryController@iconDel')->name('roomCatIconDel');
                });
                Route::post('/recommended','Room\CategoryController@recommended')->name('roomRecommended');
            });
            /** Другие страницы HTML блоками */
            Route::group(['prefix'=>'page'],function()
            {
                Route::group(['scope'=>'roomPages'],function(){
                    Route::any('/refund','Room\PagesController@refund')->name('roomPageRefund');
                    Route::any('/delivery','Room\PagesController@delivery')->name('roomPageDelivery');
                    Route::any('/payment','Room\PagesController@payment')->name('roomPagePayment');
                    Route::any('/contacts','Room\PagesController@contacts')->name('roomPageContacts');
                    Route::any('/about','Room\PagesController@about')->name('roomPageAbout');
                    Route::any('/offer','Room\PagesController@offer')->name('roomPageOffer');
                });
                Route::group(['scope'=>'roomComponents'],function(){
                    Route::any('/logo','Room\PagesController@logo')->name('roomMainLogo');
                    Route::any('/slider','Room\PagesController@slider')->name('roomMainSlider');
                    Route::any('/schedule','Room\PagesController@schedule')->name('roomSchedule');
                    Route::any('/hello-chat','Room\PagesController@helloChat')->name('roomHelloChat');
                    Route::any('/map','Room\PagesController@map')->name('roomPageMap');
                    Route::any('/counters','Room\PagesController@counters')->name('roomPageCounters');
                });
            });
        });
    });

    /** Общая группа для всех каталогов */
    Route::group([],function()
    {
        /** Общая группа для неоригинальных */
        Route::prefix('aftermarket')->group(function()
        {
            Route::get('/types','Catalogs\AftermarketController@types')->name('amTypes');
            Route::any('/marks/{hash?}','Catalogs\AftermarketController@marks')->name('amMarks');
            Route::any('/models/{hash?}','Catalogs\AftermarketController@models')->name('amModels');
            Route::any('/vehicles/{hash?}','Catalogs\AftermarketController@vehicles')->name('amVehicles');
            Route::any('/tree/{hash?}','Catalogs\AftermarketController@tree')->name('amTree');
            Route::any('/details/{hash?}','Catalogs\AftermarketController@details')->name('amDetails');
        });
        /** Каталог ТО */
        Route::prefix('maintenance')->group(function()
        {
            Route::any('/marks','Catalogs\MaintenanceController@marks')->name('toMarks');
            Route::any('/models/{hash?}','Catalogs\MaintenanceController@models')->name('toModels');
            Route::any('/vehicles/{hash?}','Catalogs\MaintenanceController@vehicles')->name('toVehicles');
            Route::any('/details/{hash?}','Catalogs\MaintenanceController@details')->name('toDetails');
        });
    });

    /** Маршруты для чата */
    Route::group(['prefix'=>'ajax/chat'],function(){
        Route::post('/new','Ajax\ChatController@new');
        Route::post('/last','Ajax\ChatController@last');
        Route::post('/send','Ajax\ChatController@send');
        Route::post('/regsocket','Ajax\ChatController@regsocket');
        Route::post('/callback','Ajax\ChatController@callback')->name('callbackForm');
    });
});
