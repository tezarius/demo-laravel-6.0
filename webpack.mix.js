const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    /// For Room
    .js('resources/js/jquery.treeBuilder.js', 'public/room/js/jquery.treeBuilder.js')
    .js('resources/js/jquery.treeFilter.js', 'public/room/js/jquery.treeFilter.js')
;
