##### Создаем каталог для сайта и переходим в него:
```
mkdir -p /path/to/site/name.ru
cd /path/to/site/name.ru
```
##### Клонируем репозиторий:
```
git clone https://bitbucket.org/tezarius/demo-laravel-6.0.git .
```
##### Проверям установлен ли composer:
```
composer -V
```
>Если установлен покажет версию  
>На момент написания readme была версия 1.2.2  
>Если у вас ниже, обновитесь: `composer self-update`

##### Если не установлен:
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'c5b9b6d368201a9db6f74e2611495f369991b72d9c8cbd3ffbc63edff210eb73d46ffbfce88669ad33695ef77dc76976') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```
##### Устанавливаем необходимые пакеты:
```
composer install
```
##### Выставляем права для системных папок:
```
chmod 777 ./bootstrap/cache
chmod 777 ./storage/
chmod 777 ./storage/logs/
chmod 777 ./storage/framework/sessions
chmod 777 ./storage/framework/views
```
> Аппетит лучше урезать до 775, идеально 755

##### Создаем конфиг файл из примера:
```
cp ./.env.example ./.env
```
##### Генерируем уникальный ключ для сайта:
```
php artisan key:generate
```
##### Создаем ссылку на хранилище, нужно для сохранения файлов:
```
php artisan storage:link
```

### Для лакольной разработки под Windows симлинк создается в той среде (в 10-й еще Ubuntu есть) где крутится сервер !
##### И если на самой Windows, то:  
```
mklink /j "C:\Tezarius\demo.tezarius.ru\laravel\public\storage" "C:\Tezarius\demo.tezarius.ru\laravel\storage\app\public"
```

### Дальше переходим к конфигурации подключения к базе

##### Нужно скорректировать данную секцию:
```
######################################
DB_CONNECTION=mysql
DB_HOST=N.tezarius.ru
DB_PORT=3306
DB_DATABASE=TZRS_XXXX_pTr6L
DB_USERNAME=TZRSXXXX_7
DB_PASSWORD=YYYYYY
TEZ_PROJECT_ID=XXXX
DEFAULT_FIRM=F
STORE_TYPE=auto
######################################
```
> Где:  
> N - номер сервера, можно посмотреть в личном кабинете  
> XXXX - четырехзначное число, номер проекта, можно посмотреть в личном кабинете  
> F - id фирмы по умолчанию, куда будут оформляться заказы у незакрепленных пользователей, можно посмотреть в справочники фирмы  
> STORE_TYPE - это тип интернет-магазина, два значения: `base` или `auto`  

# ВАЖНО !
##### Если у вас HTTPS, то заменяем:
```
APP_ENV=local
```
##### На:
```
APP_ENV=production
```