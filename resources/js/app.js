global.$ = global.jQuery = require('jquery');

require('cleave.js');
require('cleave.js/dist/addons/cleave-phone.ru.js');

global.moment = require('moment');
///import 'moment/locale/ru.js';
require('moment/locale/ru.js');

///require('popper'); /// Popper: Realtime Cross-Browser Automation (for bootstrap 4)
require('popper.js'); /// Popper.js is a positioning engine
require('bootstrap');
require('bootstrap4-toggle');
global.Swal = require('sweetalert2');
global.SVGInject = require('@iconfu/svg-inject');

global.PhotoSwipe = require('photoswipe/dist/photoswipe.min.js');
global.PhotoSwipeUI_Default = require('photoswipe/dist/photoswipe-ui-default.min.js');

///global.lightbox = require('lightbox2');
global.lightbox = require('lightbox2/dist/js/lightbox.min.js');

require('owl.carousel');
require('./bootstrap3-typeahead');
require('./jquery.treeBuilder');
require('./jquery.treeFilter');
require('./common');
require('./chat');