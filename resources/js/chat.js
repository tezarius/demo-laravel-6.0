let
    $chatSidebar,       /// Sibebar где расположен чат
    $chatPanel,         /// Panel\Tab где расположен чат
    $chatContent,       /// Вешается на root всех сообщений
    $chatBTNOpen,       /// Кнопка открытия чата
    $chatBTNClose,      /// Кнопка закрытия чата
    $chatOverlay,       /// Затемнение при открытие чата
    /** Вариант когда в боковой панели не один чат и есть еще табы */
    isVisibleChat,      /// По умолчанию чат не видно
    isVisibleHelpPanel, /// По умолчанию боковую панель не видно
    /** Если из FAB будут переходы */
    $badgeFABChat,      ///
    iBadgeFABChat,      ///
    ///$badgeFABMain,   /// Одиночная кнопка чата
    ///iBadgeFABMain,   /// Ее badge
    chatFileInputID,    ///
    $addedImages,       /// Тут контейнер с превьюшками, пока только IMAGES
    $addedImagesItems,  ///
    ///                 ///
    $chatProgress,      ///
    $chatFileName,      /// Имя загружаемого файла, в юлсан по одному отправляли
    ///                 ///
    $chatFileMsg,       /// Текст сообщения INPUT
    ///chatRouteLang,   ///
    messageIsSent       ///
;
/**
 * FIELDS
 * -------------------------------
 * id: "1064"
 * data: "2020-01-27 10:44:04"
 * mess: "йцукен1111"
 * foto_path: ""
 * isMyMess: "1"
 * SenderName: ""
 * dd: "Сегодня"
 * dt: "10:44"
*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function initDOMChat(){
    $chatSidebar = $('#chat-sidebar');

    $chatContent = $('#chat-messages');
    $chatBTNOpen = $('#chat-authority');
    $chatBTNClose = $('#chat-dismiss');
    $chatOverlay = $('#chat-overlay');

    /** Вариант когда в боковой панели не один чат и есть еще табы */
    isVisibleChat = false;      /// По умолчанию чат не видно
    isVisibleHelpPanel = false; /// По умолчанию боковую панель не видно

    /** Если из FAB будут переходы */
    $badgeFABChat = $chatBTNOpen.find('.badge--chat');
    iBadgeFABChat = $badgeFABChat.data('unread') * 1;
    /// Одиночная кнопка чата
    ///$badgeFABMain = $('.btn-group-fab .kt-badge--main');
    ///iBadgeFABMain = $badgeFABMain.data('unread') * 1;

    chatFileInputID = '#chatImages'; /// Прикрепляемые файлы INPUT
    $addedImages = $('#addedImages'); /// Тут контейнер с превьюшками, пока только IMAGES
    $addedImagesItems = $('#addedImages .message');

    $chatProgress = $('#readProgress');
    $chatFileName = $('#fileName'); /// Имя загружаемого файла, в юлсан по одному отправляли

    $chatFileMsg = $('#chat-sent-message'); /// Текст сообщения INPUT
    ///chatRouteLang;
    messageIsSent = false;

    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: I N I T  D O M  C H A T ::. |',{
        /// $chatContent: $chatContent
    });
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function clearSendData(){
    $chatFileMsg.val('');
    $addedImagesItems.html('');
    $addedImages.hide();
}
function getSendDateFormat(someDate,split){
    split = split||false;
    let date = moment(someDate).format('DD.MM.YYYY');
    let time = moment(someDate).format('HH:mm');
    let dateText = date + ' ' + time;

    let startOfToday = moment().startOf('day');
    let startOfDate = moment(someDate).startOf('day');
    let daysDiff = startOfDate.diff(startOfToday, 'days');
    let days = {
        '-2': 'Позавчера',
        '-1': 'Вчера',
        '0': 'Сегодня',
        '1': 'Завтра',
    };

    if( Math.abs(daysDiff) <= 2 ){
        date = days[daysDiff];
        dateText = date + ' ' + time;
    }

    return ( split ) ? {dd:date,dt:time} : dateText;
}
function createMassageHTML(message,type,date,lastID){
    date = date || new Date();
    lastID = lastID || $('#chat-messages .message:last').data('message-id') * 1 + 1;
    let oDate = getSendDateFormat(date,true);
    let msgHTML = '' +
        '<div class="message ' + type + '" data-message-id="' + lastID + '">' + '\n' +
        '' + message.replace(/(?:\r\n|\r|\n)/g, '<br>') + '</br>' +
        '<span class="metadata"><span class="time">'+ oDate.dd + ' ' + oDate.dt +'</span></span>' +
        '</div>'
    ;
    return msgHTML.toString();
}
/// Добавить сообщение в чат - это как правило в текущий момнет происходит, поэтому без даты передаем, потом она в текущее конвертируется
function addMessage(message,type){
    let msgHTML = createMassageHTML(message,type);
    if( msgHTML )
    {
        let $imgMsg = $addedImages.detach();
        $chatContent.append(msgHTML);
        $chatContent.append($imgMsg);
    }
}
/// Добавить сообщения в чат - как правило это первая прогрузка всех сообщений, решили через JS сделать
function addMessages(messages){
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: CHAT ADD MESSAGES ::. |',messages);
    let msgHTML = '';
    messages.forEach(function(m){
        let type = ( m.isMyMess*1>0 ) ? 'sent' : 'received';
        let message = m.mess;

        msgHTML += createMassageHTML(message,type,m.data,m.id);
    });
    if( msgHTML ){
        let $imgMsg = $addedImages.detach();
        $chatContent.html($chatContent.html()+msgHTML);
        $chatContent.append($imgMsg);
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Отправить пуши
function sendPushes(tokens) {
    let _arr = tokens.split(',');
    _arr.forEach(function(token){
        let _url = 'https://' + TZ_GLOBAL.PUSH_SERVER +
            '/services/in/push/entry.php' +
            '?method=send' +
            '&to=token' +
            '&sender=' + TZ_GLOBAL.USID +
            '&section=chat' +
            '&title=TEZARIUS' +
            '&body=Новое сообщение с сайта' +
            '&token=' + token;
        $.ajax({
            dataType: 'json',
            url: _url,
            beforeSend:function(){
                if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: jPush Before ::. |',_url);
            },
            ///beforeSend:function(){TZMain.loader.show();},
            complete:function(){/*TZMain.loader.hide();*/}
        }).done(function(data){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: jPush Done ::. |',data);
        }).fail(function(err){
            let errCode = err.status;
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: jPush Error ::. |',errCode,err);
            if( errCode===419 ) location.href = '/';
        }).always(function(){
            ///$chatLoader.hide();
            messageIsSent = false;
        });
    });
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Прочитать сообщение
function jvMessage( params ){
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: jvMessage ::. |',params);
    /// expires = new Date(); // получаем текущую дату
    /// expires.setTime(expires.getTime() + (86400*30*6));

    ///let objSend = {};
    let formData = new FormData();

    if( 'send'===params.action ){
        if( (!params.files || params.files.length===0) && (!params.message || params.message.length===0) ) return -1;
        if( params.files && params.files.length>0 ) for( let i = 0; i < params.files.length; i++ ){
            let file = params.files[i];
            formData.append('images[]', file, file.name);
        }
        formData.append( 'message', params.message);
    }
    if( 'new'===params.action ) formData.append( 'lastID', ''+params.lastID);


    $.ajax({
        type: "POST",
        dataType: 'json',
        processData: false,
        contentType : false,
        cache : false,
        ///url: '/' + chatRouteLang + '/ajax/chat/' + params.action,
        url: '/ajax/chat/' + params.action,
        data: formData,
        beforeSend:function(){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: jSend Message Before ::. |',params.action,formData);
        },
        ///beforeSend:function(){TZMain.loader.show();},
        complete:function(){/*TZMain.loader.hide();*/}
    }).done(function(data){
        if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: jSend Message Done ::. |',data);
        if( data ){
            ///$chatContent.html( data );
            if( 'send'===params.action ){
                let __addMsg__ = '';
                if( data.sockets ){
                    let objMsg = {
                        event:data.event,
                        type:data.type,
                        uid:data.uid,
                        senderId:data.senderId,
                        sockets:data.sockets,
                    };
                    ws.send(JSON.stringify(objMsg));
                }
                if( data.tokens ) sendPushes(data.tokens);
                if( data.images ){
                    data.images.forEach(function(img){
                        __addMsg__ += '<img src="'+img.url+'" width="200" onLoad="loadNotification(false,false)" /><br>';
                        ///addMessage(params.message,'user');
                    })
                }
                if( params.message ) __addMsg__ += params.message;
                if( __addMsg__ ){
                    addMessage(__addMsg__,'sent');
                    loadNotification(false,false);
                }
                if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: Add Message ::. |',__addMsg__);
            }
            /// Уведомления считываем при открытие в файле tickets.js
            /// В этот момент не нужна нотификация
            if( 'new'===params.action ){
                if( data.messages ){
                    addMessages(data.messages,'operator');
                    loadNotification(false,false);
                    /// Отправляем сокет, что прочитали
                    let objMsg = {
                        event:data.event,
                        type:data.type,
                        uid:data.uid,
                        senderId:data.senderId,
                        sockets:data.sockets,
                    };
                    ws.send(JSON.stringify(objMsg));
                }
            }

            /// Очищаем форму ввода
            if( params.byClient ) clearSendData();
        }
    }).fail(function(err){
        let errCode = err.status;
        if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: Ajax Error ::. |',errCode,err);
        if( errCode===419 ) location.href = '/';
    }).always(function(){
        ///$chatLoader.hide();
        messageIsSent = false;
    });
}
/// Это для первой загрузки
function jvLoadLast(){ /// Может наложиться на jvLoadRobot, точнее быть избыточным
    ///jvMessage({action:'first'});
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: jLoad Message FIRST ::. |');
}
/// Задумка для робота, что будет бегать и проверять на не прочитанные
function jvLoadRobot(){
    ///jvMessage({action:'ch'});
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: jLoad Message ROBOT ::. |');
}
/// Это для сокета, как придет сообщение будет уведомление
function jvLoadSocket(){
    ///jvMessage({action:'socket'});
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: sLoad Message ::. |');
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Звуковое уведомление
function jvNTFAudio(){
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: NTF Audio ::. |');
    let melody = new Audio();
    melody.src = "/media/chat/newmessget.wav";
    melody.play();
}
/// Уведвление подсвечиванием иконки
function jvNTFBabble(){
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: NTF Babble ::. |');
    ++iBadgeFABChat;
    $badgeFABChat.text(iBadgeFABChat).show();
    ///++iBadgeFABMain;
    ///$badgeFABMain.text(iBadgeFABMain).show();
}
/// Уведомление после загрузки сообщения
function loadNotification(ntfBabble,ntfAudio,time){
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: Chat Load Message ::. |');
    $chatContent.scrollTop( $chatContent[0].scrollHeight );
    ///$chatContent.scrollTop( 1000000000 ); /// MAGIC PEOPLE - WOODOO PEOPLE
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: CHAT HEIGHT 2 ::. |',$chatContent.height(),$chatContent[0].scrollHeight );
    if( ntfBabble || ntfAudio ){
        setTimeout(function(){
            if( ntfBabble ) jvNTFBabble(); /// babble notification
            if( ntfAudio ) jvNTFAudio(); /// audio notification
        },time);
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
let timerRobot, ws, wsClose=true;
/// if( !window.WebSocket ) document.body.innerHTML = 'WebSocket в этом браузере не поддерживается.';
function wsStart(){
    ///let wsURI =  (( 'https:'==location.protocol ) ? 'wss:' : 'ws:') + '//2.tezarius.ru:8009';
    let wsURI = (( 'https:'===location.protocol ) ? 'wss' : 'ws') + '://chat2.tezarius.ru';
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('|.:: WEBSOCKET URL ::.|',wsURI);
    try{
        ws = new WebSocket(wsURI);
        ws.onopen = function(e,d) {
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: WebSocket Open ::. |');

            wsClose = false;
            ///$chatContent.append(SYSTEM+"<p>система: соединение открыто</p>");

            /// Вырубаем робота
            clearInterval(timerRobot);
            timerRobot = null;

            ///$chatSocketIO.show(1000);
        };
        ws.onclose = function() {
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: WebSocket Close ::. |');

            ///$chatContent.append(SYSTEM+"<p>система: соединение закрыто, пытаюсь переподключиться</p>");
            setTimeout(wsStart, 1000);
            if( !wsClose ){
                wsClose = true;
                ///$chatSocketIO.hide(1000);

                clearInterval(timerRobot);
                timerRobot = setInterval( jvLoadRobot, 10000 );
            }
            if( !timerRobot ) timerRobot = setInterval( jvLoadRobot, 10000 );
        };
        ws.onmessage = function(evt) {
            /// Нам приходит СТРОКА!!
            let strMsg = evt.data ,objMsg = JSON.decode(strMsg);
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: Message Arrived ::. |',objMsg);
            /// Всегда оказываемся тут, но так как это очень плохо, оставим такую реализацию, возможно одумаемся!!
            if( objMsg.err ) {
                let _arr = objMsg.str.split(',');
                let _typ = _arr[0].toString().trimChars('"');
                let _sid = _arr[1]*1;
                if( 'socket'===_typ && _sid>0 ){
                    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: First Message ::. |',_typ,_sid);
                    $.ajax({
                        type:"POST",
                        ///url: '/' + chatRouteLang + '/ajax/chat/regsocket',
                        url: '/ajax/chat/regsocket',
                        data:{socket:_sid},
                    }).done(function(data){
                        if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: Register Socket Done ::. |',data.length,data);
                    }).fail(function(err){
                        if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: Register Socket Error ::. |',err);
                    }).always(function(){
                        ///$chatLoader.hide();
                    });
                }
                if( 'type'===_typ ){
                    /// Если события не завелись, можно переопределить
                    isVisibleHelpPanel = true; /// Панели нет, механизм оставим, что бы помнить
                    isVisibleChat = $chatContent.is(":visible");
                    if( isVisibleHelpPanel && isVisibleChat ){
                        jvMessage({action:'new',lastID:$('#chat-messages .message:last').data('message-id'),byClient:false});
                    }
                    else{
                        loadNotification(true,true);
                        /// Тут можно отправить сокет, что сообщение доставленно
                        /// "type,CustomerSess,uid,w2k1xlBfuKrxeFjcFXc0fBFyHQqz0qtLAQvJCPG8,senderId,7,event,newmess"
                        /*/
                        let objMsg = {
                            event:'delivered',
                            type:_arr[1],
                            uid:_arr[3],
                            senderId:_arr[5],
                            sockets:data.sockets,
                        };
                        ws.send(JSON.stringify(objMsg));
                        //*/
                    }
                }
            }
            else{
                /// Надеюсь одумаемся и эта сепкция заработает как надо!!
            }
        };
        ws.onerror = function(evt) {
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: WebSocket Error ::. |',ws.readyState,evt);
        };
        /// отправить сообщение из формы publish
        document.forms.publish.onsubmit = function(e){
            e.preventDefault();
            if( messageIsSent ) return false;
            messageIsSent = true;

            let outgoingMessage = this.message.value;
            ///let files = $(chatFileInputID).prop('files');///При выборе уже загрузили в массив
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: Event Submit Message::. |',{action:'send',message:outgoingMessage,files:uploadImages,byClient:true});
            jvMessage({action:'send',message:outgoingMessage,files:uploadImages,byClient:true});

            return false;
        };
    }
    catch(e){
        if ( TZ_GLOBAL.DEBUG.chat ) console.error('Sorry, the web socket at "%s" is un-available',wsURI,e);
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function initEventsChat(){
    /// Прокручиваем чат в самый низ
    $chatBTNOpen.click(function(){
        if( !isVisibleChat ){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: O P E N  C H A T ::. |');

            if( iBadgeFABChat>0 || !$badgeFABChat.data('fistread') ){
                jvMessage({action:'new',lastID:$chatContent.find('.message:last').data('message-id')*1,byClient:false});
                $badgeFABChat.data('fistread',true);
            }

            ///iBadgeFABMain -= iBadgeFABChat;
            iBadgeFABChat = 0;

            $badgeFABChat.text(iBadgeFABChat).data('unread',iBadgeFABChat).hide();
            ///$badgeFABMain.text(iBadgeFABMain).data('unread',iBadgeFABMain);
            ///if( iBadgeFABMain===0 ) $badgeFABMain.hide();

            loadNotification(false,false,0);

            isVisibleChat = true;
        }
    });
    ///chatRouteLang = $('#content').data('locale');

    /// Биндим состояние
    let $chatTab= $('#help_panel_tab_chat'); /// Оставил такое название, что бы помнить откуда ноги растут
    $chatTab
        .bind('show', function(){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: C H A T  show ::. |');
            isVisibleChat = true;
        })
        .bind('hide', function(){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: C H A T  hide ::. |');
            isVisibleChat = false;
        })
        .bind('toggleClass', function(){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: C H A T  toggleClass ::. |');
        })
        .bind('addClass', function(){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: C H A T  addClass ::. |');
        })
        .bind('removeClass', function(){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: C H A T  removeClass ::. |');
        })
    ;
    let $helpPanel = $('#help_panel'); /// Это если чат в боковой панели
    $helpPanel
        .bind('show', function(){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: HELP PANEL  show ::. |');
            isVisibleHelpPanel = true;
        })
        .bind('hide', function(){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: HELP PANEL  hide ::. |');
            isVisibleHelpPanel = false;
        })
        .bind('toggleClass', function(){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: HELP PANEL  toggleClass ::. |');
        })
        .bind('addClass', function(){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: HELP PANEL  addClass ::. |');
        })
        .bind('removeClass', function(){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: HELP PANEL  removeClass ::. |');
        })
    ;

    /// Открытия\Закрытие чата
    $chatBTNClose.on('click', closeChat);
    $chatOverlay.on('click', closeChat);
    $chatBTNOpen.on('click', openChat);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function openChat(){
    // open sidebar
    $chatSidebar.show().addClass('active');
    // fade in the overlay
    $chatOverlay.addClass('active');
    // scroll off for body
    $('body').addClass('noscroll');
}
function closeChat(){
    // hide sidebar
    $chatSidebar.removeClass('active').delay(800).hide();
    // hide overlay
    $chatOverlay.removeClass('active');
    // scroll on for body
    $('body').removeClass('noscroll');
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///$(function(){ /// Это DOM Ready !! - сюда загрузка JS и CSS не входит
ready(function(){
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: R E A D Y  S C R I P T  C H A T ::. |');

    /// Init jQuery Selectors
    initDOMChat();

    /// Load Last Messages
    jvLoadLast();

    /// Handle Message
    timerRobot = setInterval( jvLoadRobot, 10000 );

    /// Socket Command
    wsStart();

    /// Events
    initEventsChat();
});
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///   U P L O A D  I M A G E   /////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if( !Array.prototype.last ) Array.prototype.last = function(){ return this[this.length - 1] };

let uploadImages = [];
function deleteAddImage(index,name){
    $('#addImage_'+index).remove();
    ///$(chatFileInputID)[0].files.splice(index,1);
    ///uploadImages.splice(index,1);
    uploadImages = uploadImages.filter(function(img){
        return img.name !== name;
    });
    if( uploadImages.length===0 ) $addedImages.hide();
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: Count Files ::. |',index,name,uploadImages);
}
function resizeImage(file, maxWidth, maxHeight, callback) {
    ///let sourceImage = new Image();
    let canvas = document.createElement("canvas");
    let img = document.createElement("img");
    ///img.src = window.URL.createObjectURL(file);
    let reader = new FileReader();
    reader.onload = function(e){
        img.src = e.target.result;

        let MAX_WIDTH = maxWidth;
        let MAX_HEIGHT = maxHeight;
        let width = img.width;
        let height = img.height;
        if ( TZ_GLOBAL.DEBUG.chat ) console.log('IN: ',[width,height,img.src.length],[maxWidth,maxHeight]);

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        }
        else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);

        ///callback(canvas.toDataURL());
        let dataURL = canvas.toDataURL("image/png");
        if ( TZ_GLOBAL.DEBUG.chat ) console.log('OUT: ',width,height,dataURL.length);
        if( callback && 'function'==typeof callback) callback(dataURL);
        else return dataURL;
    };
    reader.readAsDataURL(file);
}
function readImageFiles(e){
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: readImageFiles ::. |',e);
    $addedImagesItems.html('');
    uploadImages = Array.from(e.target.files);
    if( uploadImages && uploadImages.length>0 ) for( let i = 0; i < uploadImages.length; i++ ){
        let index = i;
        let file = uploadImages[index];
        if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: File Load ::. |',index,file);
        if( !file ) return -1;
        let fileSizeB = file.size, fileSizeM = fileSizeB/1024/1024;
        if( fileSizeM>1 ){
            ///let newFile = resizeImage(file,1024,1024);
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: File Resize ::. |',newFile.length);
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('Ошибка :(','Загружаемый файл превышает допустимое значение!','error');
            swal('Ошибка :(','Загружаемый файл превышает допустимое значение!','error');
            return -2;
        }

        let reader = new FileReader();

        $chatProgress.show();
        $chatFileName.hide();

        reader.imgName = file.name;
        reader.imgIndex = index;

        reader.onload = function(e){
            let contents = e.target.result;
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: reader.onload ::. |',file.name,e);

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            /// Resize
            let MAX_WIDTH = 1024;
            let MAX_HEIGHT = 1024;
            let canvas = document.createElement("canvas");

            let img = new Image();
            img.onload = function(env){
                ///alert(this.width + " " + this.height);
                let width = this.width;
                let height = this.height;
                if ( TZ_GLOBAL.DEBUG.chat ) console.log('IMG IN: ',[width,height,contents.length],[MAX_WIDTH,MAX_HEIGHT]);

                if( width > height ){
                    if( width > MAX_WIDTH ){
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                }
                else{
                    if( height > MAX_HEIGHT ){
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }
                }
                canvas.width = width;
                canvas.height = height;
                let ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, width, height);
                contents = canvas.toDataURL("image/png");
                if ( TZ_GLOBAL.DEBUG.chat ) console.log('IMG OUT: ',width,height,contents.length);
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////

                setTimeout(function(){
                    $chatProgress.hide();
                    ///let fileName = $(chatFileInputID).val().split('\\').last();
                    let fileName = env.path[0].name;
                    let fileIndex = env.path[0].index;
                    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: fileName ::. |',fileIndex,env.path[0].name);
                    let imgHTML = '<div id="addImage_'+fileIndex+'">' +
                        '<span class="metadata"><span class="time close" onClick="deleteAddImage('+fileIndex+',\''+fileName+'\')">Х</span></span>' +
                        '<img src="'+contents+'" width="200" onLoad="loadNotification(false,false)" /><br>' +
                        '<span class="metadata">' +
                        '<span class="time">'+ fileName +' прикреплен</span>' +
                        '<span class="tick">' +
                        '<svg xmlns="//www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076">' +
                        '<path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7"/>' +
                        '</svg>' +
                        '</span>' +
                        '</span>' +
                        '<br><br>' +
                        '</div>'
                    ;
                    ///if( uploadImages.length - index == 1 ) imgHTML += '<span class="metadata"><span class="time">ГОТОВО К ОТПОРАВКЕ '+index+'</span></span><br>';
                    $addedImagesItems.append(imgHTML);
                    ///$chatFileName.text(fileName).show();
                    ///jvCheckMess('addmess');
                },0);
                $addedImages.show();
            };
            img.src = contents;
            img.name = e.target.imgName;
            img.index = e.target.imgIndex;
        };

        reader.onprogress = function(event){
            if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: reader.onprogress ::. |',event);
            if( event.lengthComputable ){
                $chatProgress[0].max  = event.total;
                $chatProgress[0].value = event.loaded;
            }
        };
        ///reader.readAsText(file);
        reader.readAsDataURL(file);
    }
    if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: readImageFiles ::. END |');
}

$(chatFileInputID).change(readImageFiles);
$(chatFileInputID).click(function(){
    return ( confirm("Выбрать изображение для отправки оператору в чат?") );
});
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if ( TZ_GLOBAL.DEBUG.chat ) console.log('| .:: L O A D  S C R I P T  C H A T ::. |');
