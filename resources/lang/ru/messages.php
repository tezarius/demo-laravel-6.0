<?php

// resources/lang/ru/messages.php

return [
    'authorization' => 'Авторизация',
    'registration' => 'Регистрация',
    'reset' => 'Сброс пароля',
    ///
    'please___wait' => 'Пожалуйста подождите...',
    'no_records_found' => 'Записей не найдено',
    'showing_of' => 'Показано :start - :end из :total',
    'first_page' => 'Первая',
    'prev_page' => 'Предыдущаяя',
    'next_page' => 'Следующая',
    'last_page' => 'Последняя',
    'more_pages' => 'Больше страниц',
    'page_number' => 'Номер страницы',
    'select_page_size' => 'Выберите размер страницы',
    'all' => 'Все',
    /*
    : 'First',
    : 'Previous',
    : 'Next',
    : '',
    : '',
    */
    /// Тикеты/Пожелания
    'title' => 'Заголовок',
    'topic' => 'Тема',
    'user' => 'Пользователь',
    'ticket_id' => 'Номер тикета',
    'status' => 'Статус',
    'upd_date' => 'Обновлен',
    'vote' => 'Голоса',
    /// Пополнение баланса
    'operation_date' => 'Дата',
    'operation_number' => 'Номер',
    'operation_name' => 'Операция',
    'total' => 'Всего',
    'note' => 'Описание',
];
