<div class="tz_search-wrapper position-relative mb-2 mb-lg-0">
    <div id="tz_search" class="input-group">
        <?php /*/ /// Это если нужно сделать фильтр для поиска - оставлю это здесь?>
        <div class="input-group-prepend">
            <button class="btn btn-lg-tz btn-light border-like-form dropdown-toggle select-button pr-md-2" type="button" data-toggle="dropdown" aria-label="Везде">
                <span class="select-text d-none d-md-block float-left text-truncate">Везде</span>
                <span class="si si-rem si-arrow">
                    <svg fill="#aaa" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"></path></svg>
                </span>
            </button>
            <div class="dropdown-menu select-list">
                <a class="dropdown-item active" href="#" onclick="return false" data-category="0">Везде</a>
                <a class="dropdown-item" href="#" onclick="return false" data-category="20">Компьютеры</a>
                <a class="dropdown-item" href="#" onclick="return false" data-category="18">Ноутбуки</a>
                <a class="dropdown-item" href="#" onclick="return false" data-category="25">Компоненты</a>
                <a class="dropdown-item" href="#" onclick="return false" data-category="57">Планшеты</a>
                <a class="dropdown-item" href="#" onclick="return false" data-category="17">Програмное обеспечение</a>
                <a class="dropdown-item" href="#" onclick="return false" data-category="24">Телефоны и PDA</a>
                <a class="dropdown-item" href="#" onclick="return false" data-category="33">Камеры</a>
                <a class="dropdown-item" href="#" onclick="return false" data-category="34">MP3 Плееры</a>
            </div>
        </div>
        <?php //*/?>
        @if( 'auto'==Store::singleton()->getType() )
            <input type="search" name="search" value="" class="form-control text-center text-md-left"
                   placeholder="Поиск по артикулу или VIN коду"
                   aria-label="Поиск по артикулу или VIN коду"
                   data-search-uri="{{ route('priceBrands',['number'=>'#RPL#']) }}"
                   autocomplete="off"
            />
        @endif
        @if( 'base'==Store::singleton()->getType() )
            <input type="search" name="search" value="" class="form-control text-center text-md-left"
                   placeholder="Поиск в каталоге"
                   aria-label="Поиск в каталоге"
                   data-search-uri="{{ route('priceName',['search'=>'#RPL#']) }}"
                   autocomplete="off"
            />
        @endif
        <?php /*/ /// ХЗ зачем это ?>
        <div class="dropdown-menu"></div>
        <input class="selected-category" type="hidden" name="category_id" value="0">
        <?php //*/?>
        <div class="input-group-append">
            <button type="button" class="btn btn-lg-tz btn-{{ 'base'==Store::singleton()->getType()?'success':'danger' }} border-like-form search-button" aria-label="Поиск в каталоге">
                <span class="si">
                    <svg fill="#fff" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path>
                    </svg>
                </span>
            </button>
        </div>
    </div>
    <div class="tz-search-mask position-fixed"></div>
</div>
