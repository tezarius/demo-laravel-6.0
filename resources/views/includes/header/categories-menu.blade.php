<style>
    #tz_menu .ico-close { display: none; }
    @media ( max-width: 991px ) {
        #tz_menu.dive-menu {
            position: fixed;
            display: block;
            top: 0;
            left: 0;
            right: 0;
            width: 100%;
            height: 100vh;
            overflow: hidden;
            z-index: 15;
        }
        #tz_menu.dive-menu .ico-open { display: none; }
        #tz_menu.dive-menu .ico-close { display: block; }
        #tz_menu.dive-menu > .category-menu-list {
            position: fixed !important;
            display: block;
            top: 3rem !important;;
            width: 100% !important;
            height: calc(100vh - 3rem) !important;;
            overflow-y: scroll !important;
            overflow-x: hidden;
            padding-bottom: 7rem !important;
        }
        #tz_menu.dive-menu .child-list-close {
            position: relative;
        }
        #tz_menu.dive-menu .child-list-open {
            position: fixed !important;
            top: 0;
            left: 0;
            right: 0;
            z-index: 1;
        }
        #tz_menu.dive-menu .third-level-list.collapse.show {
            display: none;
        }
        #tz_menu.dive-menu .child-list-open > .has-child {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            width: 100%;
            height: 3rem;
            padding-left: 2.7rem;
            background: lightgrey;
            z-index: 1;
        }
        #tz_menu.dive-menu .child-list-open > .child-list-toggle {
            position: fixed !important;
            left: 0;
            right: 0;
            z-index: 2;
        }
        #tz_menu.dive-menu .child-list-open > .child-list-toggle > .child-list-hidden { display: none; }
        #tz_menu.dive-menu .child-list-open > .child-list-toggle > .child-list-shown { display: inline-block; vertical-align: middle; }
        #tz_menu.dive-menu .child-list-open > .collapse {
            position: fixed;
            top: 3rem;
            left: 0;
            right: 0;
            /*width: 100%;*/
            display: block !important;
            z-index: 2;
            background: white;
            height: calc(100vh - 6rem);
            overflow-y: scroll;
            overflow-x: hidden;
            padding-bottom: 7rem;
        }
        #tz_menu.dive-menu .child-list-open > .collapse .tree li { line-height: 2rem; }
    }
</style>
<nav id="tz_menu" class="dropdown mb-2 mb-md-3 {{ (@$openedLeftMenu)?'d-show':'' }}">
    <button type="button" class="btn btn-lg-tz btn-block btn-{{ 'base'==Store::singleton()->getType()?'success':'danger' }}" data-toggle="dropdown">
        <span class="row no-gutters">
            <span class="col col-sm-auto">
                <span class="si"><svg height="24" fill="#fff" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path></svg></span>
            </span>
            <span class="col d-none d-sm-inline">
                <strong>Категории</strong>
            </span>
            <span class="col-auto d-none d-md-inline">
                <span class="ico-open si si-tin si-rem si-arrow"><svg fill="#fff" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"></path></svg></span>
                <i class="ico-close icon ion-2-close-round"></i>
            </span>
        </span>
    </button>
    <div class="dropdown-menu category-menu-list p-0 m-0 w-100 {{ (@$openedLeftMenu)?'d-show':'' }}">

        @foreach( $categories AS $c )
            @include("includes.header.categories-menu.lvl".Arr::get($c,'level'),['category'=>$c])
        @endforeach

    </div>
</nav>
<div class="tz-menu-mask d-none d-lg-block position-fixed"></div>
<script>
    // Tezarius Dive Menu
    const TZCategoryMenu = function(){
        // Private Function
        let $root;
        let rootID = 'tz_menu';
        let $btnMenu;
        let common = function(){
            $root = $('#'+rootID);
            $('.child-list-toggle,.has-child').click(function(){
                ///let $ths = $(this);
                let $paren = $(this).parents('.category-menu-item').eq(0);
                if( $paren.hasClass('child-list-open') ){
                    $paren.removeClass('child-list-open').addClass('child-list-close');
                    $paren.siblings('.child-list-close').removeClass('d-none').addClass('d-block');
                }
                else{
                    $paren.siblings('.child-list-close').removeClass('d-block').addClass('d-none');
                    $paren.removeClass('child-list-close').addClass('child-list-open').removeClass('d-none').addClass('d-block');
                }
                return false;
            });
            $btnMenu = $('#tz_menu > button').attr('onClick','TZCategoryMenu.toggle()');
        };
        let open = function(){
            $root.addClass('dive-menu');
            $('body').addClass('blured').addClass('noscroll');
            let $arrSpan = $btnMenu.find('.row > span');
            $arrSpan.eq(0).addClass('col-auto');
            $arrSpan.eq(1).addClass('d-block');
            $arrSpan.eq(2).addClass('d-block');
        };
        let close = function(){
            $root.removeClass('dive-menu');
            $('body').removeClass('blured').removeClass('noscroll');
            let $arrSpan = $btnMenu.find('.row > span');
            $arrSpan.eq(0).removeClass('col-auto');
            $arrSpan.eq(1).removeClass('d-block');
            $arrSpan.eq(2).removeClass('d-block');
        };
        let toggle = function(){
            if( $root.hasClass('dive-menu') ) close();
            else open()
        };
        return {
            // Public Function
            init: function () {
                common();
            }
            ,open: open
            ,close: close
            ,toggle: toggle
        };
    }();
    ready(function(){
        if( window.innerWidth > 991 )
            $('.category-menu-list').hover(function (e) {$('body').addClass('blured').addClass('noscroll')}, function (e) {$('body').removeClass('blured').removeClass('noscroll')});
        else
            TZCategoryMenu.init();
    });
</script>