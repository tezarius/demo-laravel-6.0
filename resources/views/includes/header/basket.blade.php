<style>
    @media ( max-width: 768px ) {
        #cart:not('.empty-basket') .si svg { width: 1.2rem }
        #cart:not('.empty-basket') .btn-lg-tz {
            padding: .3rem .3rem;
            font-size: .8rem;
            line-height: 1.16rem;
        }
    }
    #cart button .empty-basket-text {
        display: none;
    }

    #cart.empty-basket button #cart-total {
        margin: 0;
    }
    #cart.empty-basket button .products ,
    #cart.empty-basket button .prices {
        display: none;
    }
    #cart.empty-basket button .empty-basket-text {
        display: block;
    }
</style>
<div id="cart" class="m-blured mb-2 mb-md-3 {{ $fbCount==0 ? 'empty-basket' : '' }}"
     data-route-basket-add="{{ route('basketAdd') }}"
     data-route-basket-update="{{ route('basketUpdate') }}"
     data-route-basket-remove="{{ route('basketRemove') }}"
     data-route-basket-create="{{ route('basketCreate') }}"
     data-route-basket-change="{{ route('basketChange') }}"
     data-route-basket-clear="{{ route('basketClear') }}"
     data-route-basket-coupon="{{ route('basketCoupon') }}"
>
    <button class="btn btn-lg-tz btn-primary btn-block" @if( 'basketList'!==$routeName )type="button" data-toggle="modal" data-target="#tz-popup-cart" data-loading="Загрузка..."@endif>
        <span id="cart-total" class="row no-gutters">
            <span class="col-auto m-auto">
                <span class="cart-icon-wrapper mr-lg-3">
                    <span class="si loading-icon">
                        <svg class="spinner light" width="24" height="24" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                             <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
                        </svg>
                    </span>
                    <span class="si static-icon">
                        <svg class="" fill="#fff" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M17.21 9l-4.38-6.56c-.19-.28-.51-.42-.83-.42-.32 0-.64.14-.83.43L6.79 9H2c-.55 0-1 .45-1 1 0 .09.01.18.04.27l2.54 9.27c.23.84 1 1.46 1.92 1.46h13c.92 0 1.69-.62 1.93-1.46l2.54-9.27L23 10c0-.55-.45-1-1-1h-4.79zM9 9l3-4.4L15 9H9zm3 8c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"></path>
                        </svg>
                    </span>
                </span>
                <span class="d-md-none">
                    <span class="products"><b>{{ $fbCount }}</b> ед</span>
                </span>
            </span>
            <span class="col-12 col-md">
                <span class="d-none d-md-inline-block text-center text-lg-left">
                    <span class="products"><b>{{ $fbCount }}</b> товар(ов), </span>
                    <span class="prices">на <b>{{ $fbTotal }}</b>р.</span>
                    <span class="empty-basket-text">пусто</span>
                </span>
                <span class="prices d-md-none"><b>{{ $fbTotal }}</b> р</span>
                <span class="empty-basket-text d-md-none">пусто</span>
            </span>
        </span>
    </button>
</div>