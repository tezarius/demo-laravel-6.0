<div class="d-table w-100 h-100">
    <div class="d-table-cell w-100 h-100 align-middle">
        <div id="h-contacts" class="text-center text-lg-left">
            <div class="dropdown-toggle dropdown d-inline-block pointer" data-toggle="dropdown">
                <div class="h5 mb-1 phone-number">
                    <i class="icon ico-custom-phone"></i>
                    {{ Store::singleton()->getMainPhone() }}
                    <i class="icon ion-2-chevron-down"></i>
                </div>
            </div>
            <br>
            <span class="hint text-muted ml-lg-1">{!! $schedule !!}</span>

            <div class="dropdown-menu dropdown-menu-left">
                <a class="dropdown-item" tabindex="1" data-whatever="contacts" data-from="top" onClick="TZCallBack.open('#tz-header-contacts',this)">
                    <i class="icon ico-custom-consult"></i>
                    Перезвоните мне
                </a>
                @if( Store::singleton()->getStockPhones(FALSE) )
                    <div class="dropdown-divider"></div>
                    @foreach( Store::singleton()->getStockPhones(FALSE) AS $phone )
                        <a class="dropdown-item" href="tel:{{ $phone }}">
                            <i class="fa fa-phone-alt fa-fw"></i>
                            {{ $phone }}
                        </a>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>