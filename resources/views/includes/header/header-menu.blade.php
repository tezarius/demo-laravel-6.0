<div class="header-menu text-center text-lg-right h-100">
    <div class="d-table w-100 h-100">
        <div class="d-table-cell w-100 h-100 align-middle">
            <ul class="list-inline d-inline-block m-0">
                <li class="list-inline-item mx-3 mx-sm-3 pt-2 pb-2">
                    <a href="{{ route('refund') }}">Возврат</a>
                </li>
                <li class="list-inline-item mx-3 mx-sm-3 pt-2 pb-2">
                    <a href="{{ route('payment') }}">Оплата</a>
                </li>
                <li class="list-inline-item mx-3 mx-sm-3 pt-2 pb-2">
                    <a href="{{ route('delivery') }}">Доставка</a>
                </li>
                <li class="list-inline-item mx-3 mx-sm-3 pt-2 pb-2">
                    <a href="{{ route('contacts') }}">Контакты</a>
                </li>
            </ul>
        </div>
    </div>
</div>