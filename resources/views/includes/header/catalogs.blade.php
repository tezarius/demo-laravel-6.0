<div id="catalogs" class="m-blured mb-2 mb-md-3">
    <button type="button" class="btn btn-lg-tz btn-success btn-block   dropdown-toggle dropdown" data-toggle="dropdown">
        <span>Автокаталоги</span>&nbsp;<i class="icon icon-xs ion-2-chevron-down"></i>
    </button>
    <div class="dropdown-menu dropdown-13 dropdown-menu-right">
        <a class="dropdown-item" href="{{ route('amTypes') }}">
            <i class="fa-fw icon ico-custom-construction mt-1"></i>
            <span>Неоригинальные</span>
        </a>
        <a class="dropdown-item" href="{{ route('toMarks') }}">
            <i class="fa-fw icon ico-custom-repair mt-1"></i>
            <span>Каталоги ТО</span>
        </a>
    </div>
</div>