<span class="category-menu-item d-block !position-relative child-list-close">

    @php( $icon = Arr::get($category,'icon_url') )

    @if( Arr::get($category,'children') )
        <span class="child-list-toggle collapsed position-absolute d-block d-lg-none" data-toggle="collapse" data-target="#category-{{ Arr::get($category,'id') }}">
            <i class="fas fa-chevron-right  child-list-hidden"></i>
            <i class="fas fa-chevron-left  child-list-shown"></i>
        </span>
        <a class="d-block has-child" data-toggle="collapse" data-target="#category-{{ Arr::get($category,'id') }}"
           href="{{ route('priceGoods',['hash'=>Arr::encryption(['id'=>Arr::get($category,'id'),'name'=>Arr::get($category,'name')])]) }}"
        >
            <span class="row no-gutters">
                <span class="col-auto">
                    @if( $icon )
                        <img src="{{ $icon }}" width="20" class="float-right mr-1" />
                    @endif
                </span>
                <span class="col">
                    {{ Arr::get($category,'name') }}
                </span>
                <span class="col-auto d-none d-lg-block"><i class="icon icon-xs ion-2-chevron-right"></i></span>
            </span>
        </a>
        <span class="mobile-link-blocker d-none d-lg-block position-relative w-lg-100"></span>

        <span id="category-{{ Arr::get($category,'id') }}" class="LVL1MORE second-level-list collapse {{ $secondMenuClass??'one-col' }}">
            <span class="row">

                @foreach( Arr::get($category,'children') AS $c )
                    @include("includes.header.categories-menu.more.lvl".Arr::get($c,'level'),['category'=>$c])
                @endforeach

            </span>
            <?php /*/?>
            <a href="#" class="d-none">Показать все MP3 Плееры <small class="count">4</small></a>
            <?php //*/?>
        </span>
    @else
        <span class="category-menu-item d-block">
            <a href="{{ route('priceGoods',['hash'=>Arr::encryption(['id'=>Arr::get($category,'id'),'name'=>Arr::get($category,'name')])]) }}" class="d-block">
                <span class="row no-gutters">
                    <span class="col-auto">
                        @if( $icon )
                            <img src="{{ $icon }}" width="20" class="float-right mr-1" />
                        @endif
                    </span>
                    <span class="col">
                        {{ Arr::get($category,'name') }}
                    </span>
                </span>
            </a>
        </span>
    @endif
</span>