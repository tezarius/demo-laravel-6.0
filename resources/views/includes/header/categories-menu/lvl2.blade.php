@php( $icon = Arr::get($category,'icon_url') )

@if( Arr::get($category,'children') )
    <span class="cols-100 col-12 LVL1BLADE CHILDREN-YES">
        <span class="category-menu-item d-block position-relative second-level child-list-close">
            <span class="child-list-toggle collapsed position-absolute d-block d-lg-none" data-toggle="collapse" data-target="#category-{{ Arr::get($category,'id') }}">
                <i class="fas fa-chevron-right  child-list-hidden"></i>
                <i class="fas fa-chevron-left  child-list-shown"></i>
            </span>
            <a class="d-block pr-lg-0 pl-lg-0  has-child" data-toggle="collapse" data-target="#category-{{ Arr::get($category,'id') }}"
               href="{{ route('priceGoods',['hash'=>Arr::encryption(['id'=>Arr::get($category,'id'),'name'=>Arr::get($category,'name')])]) }}"
            >
                <span class="row no-gutters">
                    <span class="col-auto">
                        @if( $icon )
                            <img src="{{ $icon }}" width="20" class="float-right mr-1" />
                        @endif
                    </span>
                    <span class="col">
                        {{ Arr::get($category,'name') }}
                    </span>
                </span>
            </a>

            <span id="category-{{ Arr::get($category,'id') }}" class="LVL2BLADE third-level-list collapse">

                @foreach( Arr::get($category,'children') AS $c )
                    @include("includes.header.categories-menu.lvl".Arr::get($c,'level'),['category'=>$c])
                @endforeach

                <?php /*/ /// Это если решим ограничивать по кол-ву в дереве?>
                <span class="category-menu-item d-none d-lg-block third-level">
                    <a href="http://demo.tezarius.lo/index.php?route=product/category&amp;path=34_52" class="d-block p-lg-0">
                        <span class="row no-gutters">
                            <span class="col">
                                ...
                            </span>
                        </span>
                    </a>
                </span>
                <?php //*/?>

            </span>
        </span>
    </span>
@else
    <span class="cols-100 {{ $secondMenuCols??'col-sm-4' }} LVL1BLADE CHILDREN-NO">
        <span class="category-menu-item d-block second-level">
            <a href="{{ route('priceGoods',['hash'=>Arr::encryption(['id'=>Arr::get($category,'id'),'name'=>Arr::get($category,'name')])]) }}" class="d-block pr-lg-0 pl-lg-0 ">
                <span class="row no-gutters">
                    <span class="col-auto">
                        @if( $icon )
                            <img src="{{ $icon }}" width="20" class="float-right mr-1" />
                        @endif
                    </span>
                    <span class="col">
                        {{ Arr::get($category,'name') }}
                    </span>
                </span>
            </a>
        </span>
    </span>
@endif
