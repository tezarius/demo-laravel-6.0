@php( $icon = Arr::get($category,'icon_url') )

@if( Arr::get($category,'children') )
    <a href="{{ route('priceGoods',['hash'=>Arr::encryption(['id'=>Arr::get($category,'id'),'name'=>Arr::get($category,'name')])]) }}">
        @if( $icon )
            <img src="{{ $icon }}" width="20" class="float-right mr-1" />&nbsp;
        @endif
        <span class="{{ count(Arr::get($category,'children',[]))>0?'dashed':'' }}">{{ Arr::get($category,'name') }}</span>
    </a>
    <ul class="more-level tree-submenu">
        @foreach( Arr::get($category,'children') AS $c )
            <li>@include("includes.header.categories-menu.lvln",['category'=>$c])</li>
        @endforeach
    </ul>
@else
    <a href="{{ route('priceGoods',['hash'=>Arr::encryption(['id'=>Arr::get($category,'id'),'name'=>Arr::get($category,'name')])]) }}">
        <span class="row no-gutters">
            <span class="col-auto">
                @if( $icon )
                    <img src="{{ $icon }}" width="20" class="float-right mr-1" />
                @endif
            </span>
            <span class="col">
                {{ Arr::get($category,'name') }}
            </span>
        </span>
    </a>
@endif