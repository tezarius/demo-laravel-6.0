@php( $icon = Arr::get($category,'icon_url') )

@if( Arr::get($category,'children') )
    <span class="category-menu-item !d-block third-level {{ $secondMenuCols??'col-md-4' }}">
        <a class="d-block p-lg-0" href="#collapseMore-{{ Arr::get($category,'id') }}" data-toggle="collapse" aria-expanded="false">
            <span class="row no-gutters">
                <span class="col-auto">
                    @if( $icon )
                        <img src="{{ $icon }}" width="20" class="float-right mr-1" />
                    @else
                        <span class="d-inline-block float-right mr-1">↳</span>
                    @endif
                </span>
                <span class="col">
                    <span class="{{ count(Arr::get($category,'children',[]))>0?'dashed':'' }}">{{ Arr::get($category,'name') }}</span>
                    <span class="d-none"><i class="icon icon-xs ion-2-chevron-right"></i></span>
                </span>
            </span>
        </a>
        <div class="collapse" id="collapseMore-{{ Arr::get($category,'id') }}">
            <div class="card card-body">
                <ul class="more-level tz-tree">
                    @foreach( Arr::get($category,'children') AS $c )
                        <li>@include("includes.header.categories-menu.more.lvln",['category'=>$c])</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </span>
@else
    <span class="category-menu-item !d-block third-level {{ $secondMenuCols??'col-md-4' }}">
        <a href="{{ route('priceGoods',['hash'=>Arr::encryption(['id'=>Arr::get($category,'id'),'name'=>Arr::get($category,'name')])]) }}" class="d-block p-lg-0">
            <span class="row no-gutters">
                <span class="col-auto">
                    @if( $icon )
                        <img src="{{ $icon }}" width="20" class="float-right mr-1" />
                    @else
                        <span class="d-inline-block float-right mr-1">↳</span>
                    @endif
                </span>
                <span class="col">
                    {{ Arr::get($category,'name') }}
                </span>
            </span>
        </a>
    </span>
@endif