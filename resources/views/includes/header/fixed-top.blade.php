<div id="fixed-top">
    <div class="container">
        <div class="row">
            @if( 'auto'==Store::singleton()->getType() )
                <div class="col-12 col-lg order-lg-2 m-blured">
                    @include('includes.header.search-panel')
                </div>
                <div class="col-3 col-md-4 col-lg-3 col-xl-mw260 order-1 order-lg-1  pr-0 pr-md-2">
                    @if( 1==1 || 'home'===Route::currentRouteName() )
                        @include('includes.header.categories-menu')
                    @else
                        @include('includes.header.categories-button')
                    @endif
                </div>
                <div class="col-6 col-md-4 col-lg-auto order-2 order-lg-3  pr-2 pl-2 pr-md-2 pl-md-2">
                    @include('includes.header.catalogs')
                </div>
                <div class="col-3 col-md-4 col-lg-auto order-3 order-lg-4  pl-0 pl-md-2">
                    @include('includes.header.basket')
                </div>
            @endif
            @if( 'base'==Store::singleton()->getType() )
                <div class="col-12 col-lg order-lg-2 m-blured">
                    @include('includes.header.search-panel')
                </div>
                <div class="col-6 col-md-6 col-lg-3 col-xl-mw260 order-1 order-lg-1  pr-1 pr-md-2">
                    @if( 1==1 || 'home'===Route::currentRouteName() )
                        @include('includes.header.categories-menu')
                    @else
                        @include('includes.header.categories-button')
                    @endif
                </div>
                <div class="col-6 col-md-6 col-lg-auto order-3 order-lg-4  pl-1 pl-md-2">
                    @include('includes.header.basket')
                </div>
            @endif
        </div>
    </div>
</div>
<div id="fixed-top-mask"></div>