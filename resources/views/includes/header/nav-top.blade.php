<nav id="top" class="m-blured">
    <div class="container">
        <div class="row no-gutters">
            <div id="top-offices-links" data-current="{{ $officeID }}" class="col-auto">
                @if( count($offices)>1 )
                    <div class="btn-group">
                        <a class="btn btn-sm-tz btn-light dropdown-toggle" href="#" title="{{ $officeName }}" data-toggle="dropdown">
                            <i class="fas fa-map-marker-alt"></i>
                            <span class="d-none d-lg-inline mx-1">{{ $officeName }}</span>
                            <i class="icon ion-2-chevron-down"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @foreach( $offices AS $office )
                                <a class="dropdown-item" data-office-id="{{ $office->id }}" onClick="TZOffices.selected(this)">
                                    @if( $officeID==$office->id )
                                        <i class="fa fa-check text-success"></i>
                                    @else
                                        <i class="fa fa-sign-in-alt"></i>
                                    @endif
                                    <span>{{ $office->address }}</span>
                                </a>
                            @endforeach
                        </div>
                    </div>
                @elseif( count($offices)==1 )
                    <div class="btn-group">
                        <a class="btn btn-sm-tz btn-light pl-1" href="{{ route('contacts') }}">
                            <i class="fas fa-map-marker-alt"></i>
                            <span class="d-none d-md-inline ml-1" id="compare-total">{{ $offices[0]->address }}</span>
                        </a>
                    </div>
                @else
                    <div class="btn-group">
                        <i class="fas fa-map-marker-alt"></i>
                        <span class="d-md-inline ml-1" id="compare-total">Что-то пошло не так :(</span>
                    </div>

                @endif
            </div>
            <div class="col text-center"></div>
            <div class="col-auto">
                @if( Customer::singleton()->isLogged() )
                    <div class="!btn-group">
                        @if( Arr::get($discountInfo,'status') )
                            <div class="btn-group">
                                <a class="btn btn-sm-tz btn-light pl-1" href="{{ route('balance') }}" aria-label="Гараж">
                                    <span class="row no-gutters">
                                        <span class="col-auto">
                                            <i class="far fa-credit-card"></i>
                                        </span>
                                        <span class="col ml-1" style="font-size:.7rem">
                                            <span class="d-block">{{ Arr::get($balanceInfo,'onBalance','00.00')*1 }} руб</span>
                                            <span class="d-block">
                                               @if( $onBonus = (int)Arr::get($discountInfo,'onBonus') )
                                                    +{{ Str::declension($onBonus,'бонус','бонуса','бонусов') }}
                                                @endif
                                                @if( $prcMax = (int)Arr::get($discountInfo,'percent_max') )
                                                    {{ $prcMax }}% скидка
                                                @endif
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        @else
                            <div class="btn-group">
                                <a class="btn btn-sm-tz btn-light pl-1" href="{{ route('balance') }}" aria-label="Гараж">
                                    <i class="far fa-credit-card"></i>
                                    <span class="!d-none d-md-inline ml-1" id="compare-total">{{ Arr::get($balanceInfo,'onBalance','00.00')*1 }} руб</span>
                                </a>
                            </div>
                        @endif

                        @if( 'auto'==Store::singleton()->getType() )
                            <div class="btn-group d-none d-sm-inline-block">
                                <a class="btn btn-sm-tz btn-light pl-1" href="{{ route('garage') }}" aria-label="Гараж">
                                    <i class="fas fa-warehouse"></i>
                                    <span class="d-none d-md-inline ml-1" id="compare-total">Гараж</span>
                                </a>
                            </div>
                        @endif
                        <div class="btn-group">
                            <a class="btn btn-sm-tz btn-light dropdown-toggle" href="#" title="Личный кабинет" data-toggle="dropdown">
                                <i class="fas fa-user-alt"></i>
                                <span class="d-none d-lg-inline mx-1" data-user-id="{{ Customer::singleton()->getID() }}">{{ Customer::singleton()->getFullName() }}</span>
                                <i class="icon ion-2-chevron-down"></i>
                            </a>
                            <div class="dropdown-menu dropdown-13 dropdown-menu-right">
                                @if( Customer::singleton()->isAdminWebsite() )
                                    <a class="dropdown-item" href="{{ route('room') }}">
                                        <i class="icon ion-2-settings mr-2"></i>
                                        Админка
                                    </a>
                                @endif
                                <a class="dropdown-item {{ ( 'account'==$routeName )?'active':'' }}" href="{{ route('account') }}">
                                    <i class="fa fa-chart-area mr-2 {{ ( 'account'==$routeName )?'text-white':'' }}"></i>
                                    Кабинет
                                </a>
                                @if( 'auto'==Store::singleton()->getType() )
                                    <a class="dropdown-item {{ ( 'garage'==$routeName )?'active':'' }}" href="{{ route('garage') }}">
                                        <i class="fas fa-warehouse mr-2 {{ ( 'garage'==$routeName )?'text-white':'' }}"></i>
                                        Гараж
                                    </a>
                                @endif
                                <a class="dropdown-item {{ ( 'orders'==$routeName )?'active':'' }}" href="{{ route('orders') }}">
                                    <i class="fa fa-history mr-2 {{ ( 'orders'==$routeName )?'text-white':'' }}"></i>
                                    Заказы
                                </a>
                                <a class="dropdown-item {{ ( 'basketList'==$routeName )?'active':'' }}" href="{{ route('basketList') }}">
                                    <i class="fa fa-shopping-basket mr-2 {{ ( 'basketList'==$routeName )?'text-white':'' }}"></i>
                                    Корзина
                                </a>
                                <a class="dropdown-item {{ ( 'balance'==$routeName )?'active':'' }}" href="{{ route('balance') }}">
                                    <i class="fa fa-credit-card mr-2 {{ ( 'balance'==$routeName )?'text-white':'' }}"></i>
                                    Баланс
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}">
                                    <i class="fa fa-sign-out-alt mr-2"></i>
                                    Выйти
                                </a>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="!btn-group">
                        <div class="btn-group">
                            <a class="btn btn-sm-tz btn-light pl-1" href="{{ route('register') }}" aria-label="Регистрация">
                                <i class="fas fa-user-plus"></i>
                                <span class="d-none d-md-inline ml-1" id="compare-total">Регистрация</span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a class="btn btn-sm-tz btn-light pl-1" href="{{ route('login') }}" aria-label="Войти">
                                <i class="fas fa-sign-in-alt"></i>
                                <span class="d-none d-md-inline ml-1" id="compare-total">Войти</span>
                            </a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</nav>