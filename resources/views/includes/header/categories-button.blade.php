<button id="categories-authority" type="button" class="btn btn-lg-tz btn-block btn-danger">
    <span class="row no-gutters">
        <span class="col-auto">
            <span class="si"><svg height="24" fill="#fff" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path></svg></span>
        </span>
        <span class="col">
            <strong>Категории</strong>
        </span>
    </span>
</button>