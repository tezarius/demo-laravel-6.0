<?php
use \App\Helpers\Support\Arr;
?>
@isset( $breads )
    <section id="breadcrumbs" class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                @foreach( $breads AS $k=>$b )
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                    @if(  Arr::get($b,'link') )
                        <a href="{{ Arr::get($b,'link') }}" itemprop="item">
                    @else
                        <a itemprop="item">
                    @endif

                            <meta itemprop="name" content="{{ Arr::get($b,'text') }}">
                            @if(  Arr::get($b,'icon') )
                                {!! Arr::get($b,'icon') !!}
                            @endif
                            <?php /*/?>@if(  Arr::get($b,'icon') && !Arr::get($b,'hide') )&thinsp;@endif<?php //*/?>
                            @if( !Arr::get($b,'hide') )
                                <span>{!! Arr::get($b,'text') !!}</span>
                            @endif

                            <?php /* Пока используется в последних крошках для добавление кнопки или других элементов */?>
                            @if( Arr::get($b,'html') )
                                {!! Arr::get($b,'html') !!}
                            @endif


                    @if(  Arr::get($b,'link') )
                        </a>
                    @else
                        </a>
                    @endif
                    <meta itemprop="position" content="{{ ++$k }}">
                </li>
                @endforeach
            </ol>
        </nav>
    </section>
@endisset

