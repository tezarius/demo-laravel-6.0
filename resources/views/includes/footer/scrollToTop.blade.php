<div id="scrll-on-top" class="position-fixed rounded-circle bg-secondary p-1">
    <svg class="d-block" width="36" height="36" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <path fill="#fff" d="M4 12l1.41 1.41L11 7.83V20h2V7.83l5.58 5.59L20 12l-8-8-8 8z"></path>
    </svg>
</div>