<!-- Category Sidebar -->
@php(
$treeHTML = Store::singleton()->buildTreeHTMLCategory($categories,[
    'fields' => [
        'nodeID' => 'id',
        'nodeName' => 'name',
        'nodeIcon' => 'icon_url',
        'iconType' => 'path', /// path||class
        'children' => 'children',

        'rootID' => 'tz-tree',
        'rootClass' => 'tz-tree',
        'subMenuClass' => 'tree-submenu',
        'itemClass' => 'exile',
    ],
    'route' => [
        'params' => function($category){
            return ['id'=>Arr::get($category,'id'),'name'=>Arr::get($category,'name')];
        },
        'name' => 'priceGoods',
    ],
])
)
<nav id="categories-sidebar" class="sidebar sidebar-collapse bg-white text-dark" style="display:none">
    <button id="categories-dismiss" class="dismiss btn btn-secondary text-white border-dark">
        <i class="icon ion-2-close-round"></i>
    </button>
    <div class="sidebar-container">
        <div class="sidebar-header">Категории</div>
        <div class="sidebar-content">
            {!! $treeHTML !!}
        </div>
        <div class="sidebar-footer"></div>
    </div>
</nav>
<!-- Category Overlay -->
<div id="categories-overlay"></div>
<script>
    ready(function(){
        let $categoriesSidebar = $('#categories-sidebar');
        let $categoriesBTNOpen = $('#categories-authority');
        let $categoriesBTNClose = $('#categories-dismiss');
        let $categoriesOverlay = $('#categories-overlay');

        function openCategories(){
            $categoriesSidebar.show().addClass('active');
            $categoriesOverlay.addClass('active');
            $('body').addClass('noscroll');
        }
        function closeCategories(){
            $categoriesSidebar.removeClass('active').delay(800).hide();
            $categoriesOverlay.removeClass('active');
            $('body').removeClass('noscroll');
        }
        
        $categoriesBTNClose.on('click', closeCategories);
        $categoriesOverlay.on('click', closeCategories);
        $categoriesBTNOpen.on('click', openCategories);
    });
</script>