<!-- Chat Open Btn -->
<div id="callback-fab" class="btn-group-fab fab-2" role="group" aria-label="FAB CallBack" data-from="fab" onClick="TZCallBack.open('#tz-header-contacts',this)">
    <button class="btn btn-main btn-primary has-tooltip" data-placement="left" title="CallBack">
        <div class="wave border-primary"></div>
        <i class="icon ico-custom-smartphone"></i>
    </button>
</div>


<div class="modal fade" id="tz-header-contacts" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content text-left">
            <div class="modal-header no-gutters">
                <div class="col-auto">
                    <div class="h5 modal-title modal-h-icon mr-2"></div>
                </div>
                <div class="col">
                    <h5 class="modal-title">Обратный звонок</h5>
                </div>
                <div class="col-auto">
                    <div class="h5 modal-title modal-h-icon ml-2" data-dismiss="modal">
                        <svg height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div>
                    <form id="tz-callback-form">
                        <div class="form-row">
                            <div class="col-12 mb-2">
                                <div class="tz-input-grope-iconic">
                                        <span class="tz-icon-wrapper">
                                            <svg class="tz-icon" fill="#ddd" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"></path>
                                            </svg>
                                        </span>
                                    <input type="text" name="name" class="form-control form-iconic" placeholder="Ваше имя *" value="">
                                </div>
                            </div>
                            <div class="col-12 mb-2">
                                <div class="tz-input-grope-iconic">
                                        <span class="tz-icon-wrapper">
                                            <svg class="tz-icon" fill="#ddd" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M6.62 10.79c1.44 2.83 3.76 5.14 6.59 6.59l2.2-2.2c.27-.27.67-.36 1.02-.24 1.12.37 2.33.57 3.57.57.55 0 1 .45 1 1V20c0 .55-.45 1-1 1-9.39 0-17-7.61-17-17 0-.55.45-1 1-1h3.5c.55 0 1 .45 1 1 0 1.25.2 2.45.57 3.57.11.35.03.74-.25 1.02l-2.2 2.2z"></path>
                                            </svg>
                                        </span>
                                    <input type="tel" name="phone" class="form-control form-iconic input-phone" placeholder="Ваш телефон *" value="">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="comment-show">
                                    <a href="#tz-header-contacts-comment" class="unbordered-link" data-toggle="collapse" onclick="$('#tz-header-contacts .comment-show').slideToggle(350)">Комментарий...</a>
                                </div>
                                <div class="collapse position-relative" id="tz-header-contacts-comment">
                                    <div class="comment-hide position-absolute p-2">
                                        <a data-toggle="collapse" href="#tz-header-contacts-comment" onclick="$('#tz-header-contacts .comment-show').slideToggle(350)">
                                            <svg class="d-block" height="18" width="18" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M12 7c2.76 0 5 2.24 5 5 0 .65-.13 1.26-.36 1.83l2.92 2.92c1.51-1.26 2.7-2.89 3.43-4.75-1.73-4.39-6-7.5-11-7.5-1.4 0-2.74.25-3.98.7l2.16 2.16C10.74 7.13 11.35 7 12 7zM2 4.27l2.28 2.28.46.46C3.08 8.3 1.78 10.02 1 12c1.73 4.39 6 7.5 11 7.5 1.55 0 3.03-.3 4.38-.84l.42.42L19.73 22 21 20.73 3.27 3 2 4.27zM7.53 9.8l1.55 1.55c-.05.21-.08.43-.08.65 0 1.66 1.34 3 3 3 .22 0 .44-.03.65-.08l1.55 1.55c-.67.33-1.41.53-2.2.53-2.76 0-5-2.24-5-5 0-.79.2-1.53.53-2.2zm4.31-.78l3.15 3.15.02-.16c0-1.66-1.34-3-3-3l-.17.01z"></path>
                                            </svg>
                                        </a>
                                    </div>
                                    <textarea name="note" class="form-control pr-5" rows="3" placeholder="Напишите комментарий"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="my-2">

                </div>
                <div class="text-right">
                    <button type="button" class="btn btn-primary" id="tz-callback-send" data-callback="{{ route('callback') }}" onclick="TZCallBack.send(this)">
                        <span class="si si-18 loading-icon mr-2">
                            <svg class="spinner light" width="18" height="18" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                 <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
                            </svg>
                        </span>
                        Жду звонка
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>