<div id="loader-box">
    <div class="container">
        <div class="row">
            <div id="loader-2">
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    </div>
</div>