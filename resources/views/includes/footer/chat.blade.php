<!--
<script src="/js/chat.js"></script>
<link rel="stylesheet" href="">
-->
<style>
    /*FOR DEBUG*/
    .message::after {
        flex-basis:100%;
    }
</style>

<!-- Chat Open Btn -->
<div id="chat-authority" class="btn-group-fab" role="group" aria-label="FAB Menu">
    <button class="btn btn-main btn-success has-tooltip" data-placement="left" title="Menu">
        <div class="wave border-success"></div>
        <span class="badge--chat" data-unread="{{ $countNewMsg }}" style="display:{{ ($countNewMsg>0)?'block':'none' }}">{{ $countNewMsg }}</span>
        <i class="icon ico-custom-messages"></i>
    </button>
</div>

<!-- Chat Sidebar -->
<nav id="chat-sidebar" class="sidebar sidebar-collapse sidebar-30 bg-white text-dark !footer-sticky footer-fix footer-fix-46" data-host="{{ env('DB_HOST','z.tezarius.ru') }}" style="display:none">
    <button id="chat-dismiss" class="dismiss btn btn-secondary text-white border-dark">
        <i class="icon ion-2-close-round"></i>
    </button>
    <div class="sidebar-container">
        <div class="sidebar-header">Чат с менеджером магазина</div>
        <div id="chat-messages" class="sidebar-content">
            <div class="message received" data-message-id="0">
                @if( $helloChat )
                    {!! $helloChat !!}
                @else
                    Здравствуйте! Спасибо, за обращение в Tezarius! Чем мы можем вам помочь?<br>
                @endif
                <span class="metadata"><span class="time">Сегодня</span></span>
            </div>
            <div id="addedImages" style="display:none">
                <progress  id="readProgress" value="" max="" style="display:none"></progress>
                <?php /// Не использовать класс `message`, по нему забирается last message id ?>
                <!--div class="message sent"></div-->
            </div>
        </div>
        <div class="sidebar-footer bg-white">
            <form id="chatFormPublish" name="publish" class="fixed-massage-block_form" enctype="multipart/form-data" method="post">
                <input id="chat-sent-message" class="input-msg" name="message" placeholder="Введите сообщение" autocomplete="off" autofocus />
                <?php /*/?>
                <div class="photo">
                    <input id="chatImages" type="file" name="images[]" multiple style="display:none">
                    <i class="icon ion-2-android-attach" onClick="$('#chatImages').trigger('click')"></i>
                </div>
                <?php //*/?>
                <button type="submit" class="send ml-3">
                    <div class="circle">
                        <i class="fa fa-paper-plane"></i>
                    </div>
                </button>
            </form>
        </div>
    </div>
</nav>
<!-- Dark Overlay element -->
<div id="chat-overlay"></div>