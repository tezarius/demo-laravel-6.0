@if( 'basketList'!==$routeName )
    <div class="modal fade" id="tz-popup-cart" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-gutters">
                    <div class="col-auto">
                        <div class="h5 modal-h-icon modal-title mr-3">
                            <span class="loading-icon d-none">
                                <svg class="spinner" width="24px" height="24px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle></svg>
                            </span>
                            <span class="static-icon">
                                <svg height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M17.21 9l-4.38-6.56c-.19-.28-.51-.42-.83-.42-.32 0-.64.14-.83.43L6.79 9H2c-.55 0-1 .45-1 1 0 .09.01.18.04.27l2.54 9.27c.23.84 1 1.46 1.92 1.46h13c.92 0 1.69-.62 1.93-1.46l2.54-9.27L23 10c0-.55-.45-1-1-1h-4.79zM9 9l3-4.4L15 9H9zm3 8c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"></path></svg>
                            </span>
                        </div>
                    </div>
                    <div class="col">
                        <h5 class="modal-title">
                            Корзина покупок
                        </h5>
                    </div>
                    <div class="col-auto">
                        <div id="fb-btn-close" class="h5 modal-title modal-h-icon" data-dismiss="modal">
                            <svg class="d-block position-absolute" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="cart-list">
                        @php( $emptyBasket = 1 )
                        @if( count($fbProducts)>0 )
                            <div class="cart-list-product">
                                @foreach( $fbProducts AS $p )
                                    @php( $emptyBasket = 0 )
                                    <div id="row-product-id-{{ $p->id }}">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a data-more-href="#" class="ml-1" target="_blank"><img src="/img/no_img_47x47.png" alt="" title="" class="img-thumbnail"></a>
                                            </div>
                                            <div class="col">
                                                <div class="row">
                                                    <div class="col ">
                                                        <div class="mb-2">
                                                            <div class="row no-gutters">
                                                                <div class="col">
                                                                    <a data-more-href="#" target="_blank"><strong class="tz-fb-name">{{ $p->name }}</strong></a>
                                                                    <div></div>
                                                                </div>
                                                                <div class="col-auto">
                                                                    <button type="button" class="close ml-2 d-sm-none" onclick="TZBasket.remove('{{ $p->id }}','#row-product-id-{{ $p->id }}');">×</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-auto ">
                                                        <div class="mr-3 mb-2 text-left text-sm-right">
                                                            <strong><span class="tz-fb-total">{{ $p->total }}</span>р.</strong>
                                                        </div>
                                                        <div class="form-row mr-1">
                                                            <div class="col-auto ml-sm-auto">
                                                                <div id="counter_{{ $p->id }}" class="input-group input-group-sm">
                                                                    <div class="input-group-prepend">
                                                                        <button class="minus btn btn-light border-like-form px-1" type="button"
                                                                                onclick="TZCounter.minus('#counter_{{ $p->id }}',this);">
                                                                            <span class="si si-rem"><svg fill="#aaa" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M19 13H5v-2h14v2z"></path></svg></span>
                                                                        </button>
                                                                    </div>
                                                                    <input type="text" class="counter form-control input-quantity text-center" placeholder="" size="1"
                                                                           name="quantity[{{ $p->id }}]"
                                                                           value="{{ $p->qty*1 }}"
                                                                           data-step="{{ $p->part||1 }}"
                                                                           data-min="{{ $p->part }}"
                                                                           data-max="{{ $p->qty_free*1 }}"
                                                                           data-id="{{ $p->id }}"
                                                                           onChange="TZBasket.quantity(this);"
                                                                           onInput="TZCounter.input('#counter_{{$p->id}}');"
                                                                    />
                                                                    <div class="input-group-append">
                                                                        <button class="plus btn btn-light border-like-form px-1" type="button" onclick="TZCounter.plus('#counter_{{ $p->id }}',this);">
                                                                            <span class="si si-rem"><svg fill="#aaa" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"></path></svg></span>
                                                                        </button>
                                                                        <button class="btn btn-light border-like-form px-1" type="button" title="Удалить"
                                                                                onclick="TZBasket.remove('{{ $p->id }}','#row-product-id-{{ $p->id }}');"
                                                                        >
                                                                            <span class="si si-rem"><svg fill="#aaa" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"></path></svg></span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <div class="cart-empty text-center p-3">
                                <span class="si si-rem loading-icon mr-2"><svg class="spinner" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle></svg>
                                </span>
                                Ваша корзина пуста!
                            </div>
                        @endif
                        <div class="cart-totals" @if( $emptyBasket ) style="display:none" @endif;>
                            <button class="btn btn-light btn-block collapsed" data-toggle="collapse" data-target="#totals-details" aria-expanded="false">
                                <span class="si si-rem loading-icon mr-1"><svg class="spinner" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle></svg></span>
                                <span class="si si-rem static-icon mr-1"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path fill="#bbb" d="M19 2h-4.18C14.4.84 13.3 0 12 0c-1.3 0-2.4.84-2.82 2H5c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm-7 0c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm7 18H5V4h2v3h10V4h2v16z"></path></svg></span>
                                <span class="products"><b>{{ $fbCount }}</b> товар(ов), </span><span class="prices">на <b>{{ $fbTotal }}</b>р.</span>
                                <span class="si si-rem"><svg fill="#aaa" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"></path></svg></span>
                            </button>
                            <div id="totals-details" class="collapse" style="">
                                <hr>
                                <div class="row no-gutters">ТУТ КАЛЬКУЛЯЦИЯ</div>
                                <hr>
                                <div class="row no-gutters">
                                    <div class="col text-right"><span class="mr-2">Итого:</span></div>
                                    <div class="col text-left prices"><strong>{{ $fbTotal }}</strong>р.</div>
                                </div>
                                <div class="row no-gutters">
                                    <div class="col text-right"><span class="mr-2">Всего:</span></div>
                                    <div class="col text-left prices"><strong>{{ $fbTotal }}</strong>р.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cart-mask"></div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-12 col-sm-4 order-sm-3">
                            <a class="btn btn-primary btn-block mb-3 mb-sm-0" tabindex="1" onClick="TZBasket.checkout('{{ route('orderCheckout') }}','{{ route('orders') }}')">Оформить заказ</a>
                        </div>
                        <div class="col-5 col-sm-4 order-sm-1">
                            <a class="btn btn-light btn-block" data-dismiss="modal">Вернуться</a>
                        </div>
                        <div class="col-7 col-sm-4 order-sm-2">
                            <a class="btn btn-light btn-block" href="{{ route('basketList') }}">Перейти в корзину</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif