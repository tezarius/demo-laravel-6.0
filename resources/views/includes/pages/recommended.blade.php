@php( $offersRg = (array) Arr::get($offers,'origin.items',[]) )
@php( $offersNg = (array) Arr::get($offers,'analog.items',[]) )
@if( count($offersRg)>0 || count($offersNg)>0 )
    @php( $i = 1 )
    <h4 class="mt-5 mb-3">Рекомендуем</h4>
    <div id="view-list-grid" class="list-grid ft carousel-wrapper position-relative mb-5">
        <div class="!row offers-items owl-carousel">
            @foreach( $offersRg AS $offer )
                @include('pages.price.offers.includes.row-grid-list',['class'=>'row-root product-layout product-items product-grid mb-0'])
                @php( ++$i )
            @endforeach

            @foreach( $offersNg AS $offer )
                @include('pages.price.offers.includes.row-grid-list',['class'=>'row-root product-layout product-items product-grid mb-0'])
                @php( ++$i )
            @endforeach
        </div>

        <div class="button-next show"><svg class="d-block" fill="#aaa" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"></path></svg></div>
        <div class="button-prev show"><svg class="d-block" fill="#aaa" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"></path></svg></div>

    </div>
    <script>
        ready(function () {

            var owlCarouselOffers = $('#view-list-grid .owl-carousel');

            owlCarouselOffers.owlCarousel({
                loop: false,
                margin: 0,
                lazyLoad: true,
                responsive:{
                    0:		{items: 1},
                    380:	{items: 1},
                    576:	{items: 2},
                    768:	{items: 3},
                    992:	{items: 4},
                    1200:	{items: 5}
                },
                nav: false,
                dots: false,
                autoplay: false,
                autoplayTimeout: 2500,
            });

            owlCarouselOffers.on('loaded.owl.lazy',function(e){e.element.parent().find('.lazy-spinner').remove()});

            $('#view-list-grid .button-prev').click(function() { owlCarouselOffers.trigger('prev.owl.carousel'); });
            $('#view-list-grid .button-next').click(function() { owlCarouselOffers.trigger('next.owl.carousel'); });

        });
    </script>
@endif