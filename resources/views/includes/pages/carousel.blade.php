<div id="carousel-{{ $module }}" class="ft carousel-wrapper carousel-{{ $module }} position-relative mb-3 p-3">

    <div class="owl-carousel">
        @foreach( $banners as $banner )
            <div class="text-center position-relative">
                @if( Arr::get($banner,'link') )
                    <a href="{{ Arr::get($banner,'link') }}">
                        <div class="position-absolute lazy-spinner"><div class="spinner-border"></div></div>
                        <img src="{{ Arr::get($banner,'image','/img/no_img_47x47.png') }}" data-src="{{ Arr::get($banner,'image') }}" alt="{{ Arr::get($banner,'title') }}" class="img-fluid w-auto d-block mx-auto owl-lazy"/>
                    </a>
                @else
                    <div class="position-absolute lazy-spinner"><div class="spinner-border"></div></div>
                    <img src="{{ Arr::get($banner,'image','/img/no_img_47x47.png') }}" data-src="{{ Arr::get($banner,'image') }}" alt="{{ Arr::get($banner,'title') }}" class="img-fluid w-auto d-block mx-auto owl-lazy" />
                @endif
            </div>
        @endforeach
    </div>

    <div class="button-next"><svg class="d-block" fill="#aaa" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"></path></svg></div>
    <div class="button-prev"><svg class="d-block" fill="#aaa" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"></path></svg></div>

</div>
<script>
    ready(function () {

        var owl_carousel_{{ $module }} = $('#carousel-{{ $module }} .owl-carousel');

        owl_carousel_{{ $module }}.owlCarousel({
            loop: true,
            margin: 0,
            lazyLoad: true,
            responsive:{
                0:		{items: 2},
                380:	{items: 3},
                576:	{items: 4},
                768:	{items: 5},
                992:	{items: 6},
                1200:	{items: 6}
            },
            nav: false,
            dots: false,
            autoplay: true,
            autoplayTimeout: 2500,
        });

        owl_carousel_{{ $module }}.on('loaded.owl.lazy',function(e){e.element.parent().find('.lazy-spinner').remove()});

        $('#carousel-{{ $module }} .button-prev').click(function() { owl_carousel_{{ $module }}.trigger('prev.owl.carousel'); });
        $('#carousel-{{ $module }} .button-next').click(function() { owl_carousel_{{ $module }}.trigger('next.owl.carousel'); });

    });
</script>