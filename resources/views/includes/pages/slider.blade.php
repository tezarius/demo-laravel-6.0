<div id="slideshow-{{ $module }}" class="slideshow-{{ $module }} ft slideshow carousel-wrapper position-relative mb-3">

    <div class="owl-carousel">
        @foreach( $sliders as $slide )
            @php( $hash = crc32($slide) )
            <div class="text-center position-relative" style="max-height:20rem">
                @if( ($uri = Arr::get($links,$hash)) )
                    <a href="{{ $uri }}">
                        <div class="position-absolute lazy-spinner"><div class="spinner-border"></div></div>
                        <img src="{{ $slide }}" data-src="{{ $slide }}" class="img-fluid d-block mx-auto owl-lazy" />
                    </a>
                @else
                    <div class="position-absolute lazy-spinner"><div class="spinner-border"></div></div>
                    <img src="{{ $slide }}" data-src="{{ $slide }}" class="img-fluid d-block mx-auto owl-lazy" />
                @endif
            </div>
        @endforeach
    </div>

    <div class="owl-pagination position-absolute text-center px-3 py-2"></div>

    <div class="button-next"><svg class="d-block" fill="#aaa" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"></path></svg></div>
    <div class="button-prev"><svg class="d-block" fill="#aaa" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"></path></svg></div>

</div>

<script>
    ready(function () {

        var owl_carousel_{{ $module }} = $('#slideshow-{{ $module }} .owl-carousel');

        owl_carousel_{{ $module }}.owlCarousel({
            loop: true,
            lazyLoad: true,
            margin: 0,
            items:1,
            nav: false,
            dots: true,
            dotsContainer: '#slideshow-{{ $module }} .owl-pagination',
            autoplay: true,
            autoplayTimeout: 2500,
            autoplayHoverPause: true,
        });

        owl_carousel_{{ $module }}.on('loaded.owl.lazy',function(e){
            e.element.parent().find('.lazy-spinner').remove();
        });

        $('#slideshow-{{ $module }} .button-prev').click(function() { owl_carousel_{{ $module }}.trigger('prev.owl.carousel'); });
        $('#slideshow-{{ $module }} .button-next').click(function() { owl_carousel_{{ $module }}.trigger('next.owl.carousel'); });

    });
</script>
