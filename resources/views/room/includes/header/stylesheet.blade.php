<!--begin::Global Theme Styles(used by all pages) -->
<link href="/room/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
<link href="/room/css/style.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles -->

<!--begin::Layout Skins(used by all pages) -->
<link href="/room/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
<link href="/room/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
<link href="/room/css/skins/brand/light.css" rel="stylesheet" type="text/css" />
<link href="/room/css/skins/aside/light.css" rel="stylesheet" type="text/css" />
<!--end::Layout Skins -->