<?php /// https://getbootstrap.com/docs/4.0/components/alerts/ ?>
<?php /// !! ВНИМАНИЕ: Отличается от frontend !!  ?>
<section id="flashers">
    @foreach( $flashers AS $flash )
        <div class="alert alert-{{ ( ($t = Arr::get($flash,'type'))=='error' )?'danger':$t }} fade show" role="alert">
            <div class="alert-icon"><i class="fa fa-exclamation"></i></div>
            <div class="alert-text">
                @if( Arr::get($flash,'title') )<strong>{{ Arr::get($flash,'title') }}</strong>@endif
                <span>{{ Arr::get($flash,'text') }}</span>
            </div>
            <div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div>
        </div>
    @endforeach

    @if( session('info') )
        <div class="alert alert-success fade show" role="alert">
            <div class="alert-icon"><i class="fa fa-exclamation"></i></div>
            <div class="alert-text">{{ session('status') }}</div>
            <div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div>
        </div>
    @endif
    @if( session('status') )
        <div class="alert alert-success fade show" role="alert">
            <div class="alert-icon"><i class="fa fa-exclamation"></i></div>
            <div class="alert-text">{{ session('status') }}</div>
            <div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div>
        </div>
    @endif
    @if( session('success') )
        <div class="alert alert-success fade show" role="alert">
            <div class="alert-icon"><i class="fa fa-exclamation"></i></div>
            <div class="alert-text">{{ session('success') }}</div>
            <div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div>
        </div>
    @endif
    @if( session('warning') )
        <div class="alert alert-warning fade show" role="alert">
            <div class="alert-icon"><i class="fa fa-exclamation"></i></div>
            <div class="alert-text">{{ session('warning') }}</div>
            <div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div>
        </div>
    @endif
    @if( session('error') )
        <div class="alert alert-danger fade show" role="alert">
            <div class="alert-icon"><i class="fa fa-exclamation"></i></div>
            <div class="alert-text">{{ session('error') }}</div>
            <div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div>
        </div>
    @endif

    <div id="alertTemplate" class="alert fade show" role="alert" style="display:none">
        <strong class="title"></strong><span class="message"></span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button>
    </div>
</section>