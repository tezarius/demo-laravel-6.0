<div id="aside-main" class="{{ $addClasses??'' }}"><?php /// Текущая обертка добавленна, если нужно скрыть основной сайдбар и показать свой ?>
    <!-- begin::Aside Brand -->
    <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
        <div class="kt-aside__brand-logo">
            @if( $headLogo )
                <a href="/"><img alt="Logo" src="{{ $headLogo }}" width="auto" height="50"/></a>
            @endif
        </div>
        <div class="kt-aside__brand-tools">
            <button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
        </div>
    </div>
    <!-- end:: Aside Brand -->

    <!-- begin:: Aside Menu -->
    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
            <ul class="kt-menu__nav ">

                <li class="kt-menu__item {{ in_array($routeName,['roomNewsList','roomNewsAdd','roomNewsUpd']) ? 'kt-menu__item--open' : '' }}" aria-haspopup="true">
                    <a href="{{ route('roomNewsList') }}" class="kt-menu__link ">
                        <i class="kt-menu__link-icon fa fa-newspaper"></i>
                        <span class="kt-menu__link-text">Новости</span>
                    </a>
                </li>

                <li class="kt-menu__item {{ in_array($routeName,['roomCategoryList']) ? 'kt-menu__item--open' : '' }}" aria-haspopup="true">
                    <a href="{{ route('roomCategoryList') }}" class="kt-menu__link ">
                        <i class="kt-menu__link-icon fa fa-stream"></i>
                        <span class="kt-menu__link-text">Категории</span>
                    </a>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu {{ 'roomPages'==$routeScope ? 'kt-menu__item--open' : '' }}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <i class="kt-menu__link-icon flaticon2-layers"></i>
                        <span class="kt-menu__link-text">Страницы</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item {{ 'roomPageRefund'==$routeName ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('roomPageRefund') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                    <span class="kt-menu__link-text">Возврат</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ 'roomPageDelivery'==$routeName ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('roomPageDelivery') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                    <span class="kt-menu__link-text">Доставка</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ 'roomPagePayment'==$routeName ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('roomPagePayment') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                    <span class="kt-menu__link-text">Оплата</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ 'roomPageContacts'==$routeName ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('roomPageContacts') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                    <span class="kt-menu__link-text">Контакты</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ 'roomPageAbout'==$routeName ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('roomPageAbout') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                    <span class="kt-menu__link-text">О нас</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ 'roomPageOffer'==$routeName ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('roomPageOffer') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                    <span class="kt-menu__link-text">Публичная оферта</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu {{ 'roomComponents'==$routeScope ? 'kt-menu__item--open' : '' }}" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <i class="kt-menu__link-icon flaticon-squares"></i>
                        <span class="kt-menu__link-text">Компоненты</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item {{ 'roomMainLogo'==$routeName ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('roomMainLogo') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                    <span class="kt-menu__link-text">Логотип</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ 'roomMainSlider'==$routeName ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('roomMainSlider') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                    <span class="kt-menu__link-text">Слайдер</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ 'roomSchedule'==$routeName ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('roomSchedule') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                    <span class="kt-menu__link-text">График работы</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ 'roomHelloChat'==$routeName ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('roomHelloChat') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                    <span class="kt-menu__link-text">Приветствие в чат</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ 'roomPageMap'==$routeName ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('roomPageMap') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                    <span class="kt-menu__link-text">Карта сайта</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ 'roomPageCounters'==$routeName ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{ route('roomPageCounters') }}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                    <span class="kt-menu__link-text">Счетчики</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div>
    </div>
    <!-- end:: Aside Menu -->

    <!-- begin:: Aside Footer -->
    <?php /*/?>
    <div class="kt-aside__footer kt-grid__item" id="kt_aside_footer">
        <div class="kt-aside__footer-nav">
            <div class="kt-aside__footer-item">
                <a href="#" class="btn btn-icon"><i class="flaticon2-gear"></i></a>
            </div>
            <div class="kt-aside__footer-item">
                <a href="#" class="btn btn-icon"><i class="flaticon2-cube"></i></a>
            </div>
            <div class="kt-aside__footer-item">
                <a href="#" class="btn btn-icon"><i class="flaticon2-bell-alarm-symbol"></i></a>
            </div>
            <div class="kt-aside__footer-item">
                <button type="button" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="flaticon2-add"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-left">
                    <ul class="kt-nav">
                        <li class="kt-nav__section kt-nav__section--first">
                            <span class="kt-nav__section-text">Export Tools</span>
                        </li>
                        <li class="kt-nav__item">
                            <a href="#" class="kt-nav__link">
                                <i class="kt-nav__link-icon la la-print"></i>
                                <span class="kt-nav__link-text">Print</span>
                            </a>
                        </li>
                        <li class="kt-nav__item">
                            <a href="#" class="kt-nav__link">
                                <i class="kt-nav__link-icon la la-copy"></i>
                                <span class="kt-nav__link-text">Copy</span>
                            </a>
                        </li>
                        <li class="kt-nav__item">
                            <a href="#" class="kt-nav__link">
                                <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                <span class="kt-nav__link-text">Excel</span>
                            </a>
                        </li>
                        <li class="kt-nav__item">
                            <a href="#" class="kt-nav__link">
                                <i class="kt-nav__link-icon la la-file-text-o"></i>
                                <span class="kt-nav__link-text">CSV</span>
                            </a>
                        </li>
                        <li class="kt-nav__item">
                            <a href="#" class="kt-nav__link">
                                <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                <span class="kt-nav__link-text">PDF</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="kt-aside__footer-item">
                <a href="#" class="btn btn-icon"><i class="flaticon2-calendar-2"></i></a>
            </div>
        </div>
    </div>
    <?php //*/?>
    <!-- end:: Aside Footer-->
</div>