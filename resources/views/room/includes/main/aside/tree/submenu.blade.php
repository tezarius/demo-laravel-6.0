<li class="kt-menu__item kt-menu__item--submenu kt-show {{ ($item->id==$cid)?'kt-menu__item--open':'' }}" aria-haspopup="true"  data-ktmenu-submenu-toggle="hover">
    <a  href="javascript:;" class="kt-menu__link kt-menu__toggle">
        <i class="kt-menu__link-icon flaticon-folder"></i>
        <span class="kt-menu__link-text">{{ $item->name }}</span>
        <i class="kt-menu__ver-arrow la la-angle-right"></i>
    </a>
    <div class="kt-menu__submenu ">
        <span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            @foreach( $item->{$with} AS $_item )
                @include('includes.page.aside.tree.item',['item'=>$_item,'tid'=>$tid])
            @endforeach

            @foreach( $item->children AS $child )
                <li class="kt-menu__item kt-menu__item--parent" aria-haspopup="true" >
                    <span class="kt-menu__link"><span class="kt-menu__link-text">{{ $item->name }}</span></span>
                </li>
                @include('includes.page.aside.tree.submenu',['item'=>$child,'cid'=>$cid,'tid'=>$tid,'routeShow'=>$routeShow,'with'=>$with])
            @endforeach
        </ul>
    </div>
</li>
