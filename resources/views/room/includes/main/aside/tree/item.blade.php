<li class="kt-menu__item kt-menu__item--item kt-show {{ ($item->id==$tid)?'kt-menu__item--active':'' }}" aria-haspopup="true">
    <a href="{{ route($routeShow,['item_id'=>$item->id]) }}" class="kt-menu__link">
        <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
        <span class="kt-menu__link-text">{{ $item->name }}</span>
    </a>
</li>
