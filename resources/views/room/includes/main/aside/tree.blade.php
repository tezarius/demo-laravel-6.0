<?php /// Сайдбар просто перенесен с лк, скорее всего нужна адаптация под keen 1.4.6 + JS TAsideTree ?>
<div id="aside-tree">
    <!-- begin::Aside Brand -->
    <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
        <div class="kt-aside__brand-logo">
            <?php //*/?>
            <a href="/">
                <img alt="Logo" src="/assets/media/logos/header-logo.svg" />
            </a><?php //*/?>
        </div>
        <div class="kt-aside__brand-tools">
            <button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
        </div>
    </div>
    <!-- end:: Aside Brand -->

    <!-- begin:: Aside Menu -->
    <?php $rrPrefix = request()->route()->getPrefix()?>
    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
            <ul class="kt-menu__nav ">
                <li class="kt-menu__item" aria-haspopup="true">
                    <a class="kt-menu__link" onClick="TAsideTree.hideMenu()">
                        <i class="kt-menu__link-icon la la-long-arrow-left"></i>
                        <span class="kt-menu__link-text">{{ __('Back') }}</span>
                    </a>
                </li>
                <li class="kt-menu__section">
                    <div id="aside-tree-search" class="kt-input-icon">
                        <input type="text" id="aside-tree-search-input" name="search" class="form-control bg-transparent" placeholder="{{ __('What are we looking for?') }}">
                        <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span>
                                <i class="la la-search icon-search"></i>
                                <i class="la la-remove icon-delete" style="display:none" onClick="TAsideTree.clearSearch()"></i>
                            </span>
                        </span>
                    </div>
                </li>
                @foreach( Arr::get($params,'items',[]) AS $item )
                    @include('includes.page.aside.tree.submenu',[
                        'with' => Arr::get($params,'with'),
                        'item' => $item,
                        'cid'  => Arr::get($params,'cid'),
                        'tid'  => Arr::get($params,'tid'),
                        'routeShow' => Arr::get($params,'routeShow'),
                    ])
                @endforeach
                <?php /*/?>
                <li class="kt-menu__section ">
                    <h4 class="kt-menu__section-text">CURRENT ::: {{ request()->route()->getName() }} ::: {{ $rrPrefix }}</h4>
                    <i class="kt-menu__section-icon flaticon-more-v2"></i>
                </li>
                <?php //*/?>
            </ul>
        </div>
    </div>
    <!-- end:: Aside Menu -->
</div>
