<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-footer__copyright">
            {{ date('Y') }}&nbsp;&copy;&nbsp;<a href="//tezarius.ru" target="_blank" class="kt-link">TEZARIUS SHOP</a>
        </div>
        <?php /*/?>
        <div class="kt-footer__menu">
            <a href="/" target="_blank" class="kt-footer__menu-link kt-link">Link 1</a>
            <a href="/" target="_blank" class="kt-footer__menu-link kt-link">Link 2</a>
            <a href="/" target="_blank" class="kt-footer__menu-link kt-link">Link 3</a>
        </div>
        <?php //*/?>
    </div>
</div>