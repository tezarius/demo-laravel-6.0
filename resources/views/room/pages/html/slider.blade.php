@extends('layouts.room')

@section('cssThisPage')
    <link href="/room/plugins/custom/uppy/uppy.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section('jsThisPage')

    <script src="/room/plugins/custom/uppy/uppy.bundle.js" type="text/javascript"></script>
    <script src="/room/plugins/custom/uppy/ru_RU.min.js" type="text/javascript"></script>
    <!--script src="/room/js/pages/components/forms/file-upload/uppy.js" type="text/javascript"></script-->
    <script src="/room/js/pages/html/tz-files.js?1"></script>
    <script src="/room/js/pages/html/pages.js" type="text/javascript"></script>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-xl-6">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">{{ $header }}</h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <form class="kt-form kt-form--label-right">
                        @csrf
                        @if( $arrFilePath )
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label">Текущие слайды:</label>
                                <div class="col-lg-9 col-xl-6">
                                    @foreach( $arrFilePath AS $path )
                                        <div class="kt-avatar kt-avatar--outline kt-avatar--circle-">
                                            <a href="{{ $path }} " data-lightbox="images" target="_blank"><div class="kt-avatar__holder" style="background-image: url('{{ $path }}');"></div></a>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Удалить изображение" style="z-index:1"
                                                   data-hash="{{ Arr::encryption(['code'=>$code,'path'=>$path]) }}"
                                                   onClick="TZFiles.remove(this)"
                                            >
                                                <i class="flaticon2-cancel-music"></i>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        <div class="form-group form-group-last row">
                            <label class="col-lg-3 col-form-label">Загрузить слайды:</label>
                            <div class="col-lg-6">
                                <div class="kt-uppy" id="kt_uppy">
                                    <div class="kt-uppy__wrapper"></div>
                                    <div class="kt-uppy__list"></div>
                                    <div class="kt-uppy__status"></div>
                                    <div class="kt-uppy__informer kt-uppy__informer--min"></div>
                                </div>
                                <span class="form-text text-muted">Максимальный объем 500 КБайт (860x280)</span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="kt-portlet__foot">
                    <button type="button" class="btn btn-sm btn-primary" onClick="TZFiles.upload()">Сохранить</button>
                    <button type="reset" class="btn btn-sm btn-secondary" onClick="TZFiles.cancel()">Отмена</button>
                </div>
            </div>
            <script>
                document.ready(function(){
                    TZFiles.settings({
                        fieldName: '{{ $fieldName }}',
                        elemId: 'kt_uppy',
                        uploadURI: '{{ route($route) }}',
                        removeURI: '{{ route($route) }}',
                        removeMSG: 'Удалить слайд ?',
                        allowedFileTypes: ['image/*', '.jpg', '.jpeg', '.png', '.gif'],
                        maxFileSize: 500000, /// 50Kb
                        minFiles: 1,
                        maxFiles: 100,
                        hideUploadButton: false,
                        allowMultipleFiles: true,
                        formData: true,
                        bundle: true,
                    });
                    lightbox.init();
                });
            </script>
        </div>
        @if( $arrFilePath )
            <div class="col-12 col-xl-6">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Привязать новость, категорию, товар и тд</h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <form id="tz-form-slider-link" class="kt-form kt-form--label-right">
                            @csrf
                            @foreach( $arrFilePath AS $path )
                                @php( $hash = crc32($path) )
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">
                                        <a href="#" class="kt-media">
                                            <img src="{{ $path }}" alt="image" />
                                        </a>
                                    </label>
                                    <div class="col-lg-9 col-xl-6 align-self-center">
                                        <input type="text" name="links[{{ $hash }}]" value="{{ Arr::get($arrLinks,$hash) }}" class="slider-link form-control" aria-describedby="urlHelp" placeholder="Введите адрес страницы">
                                    </div>
                                </div>
                            @endforeach
                        </form>
                    </div>
                    <div class="kt-portlet__foot">
                        <button type="button" class="btn btn-sm btn-primary" name="btnPageSave" data-route="{{ route($route) }}" onClick="TZPage.save(this)">Сохранить</button>
                        <button type="reset" class="btn btn-sm btn-secondary" onClick="TZPage.reset()">Отмена</button>
                    </div>
                </div>
                <script>
                    document.ready(function(){
                        TZPage.editor('form','#tz-form-slider-link');
                    });
                </script>
            </div>
        @endif
    </div>
@endsection
