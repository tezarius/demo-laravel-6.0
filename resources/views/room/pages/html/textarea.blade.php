@extends('layouts.room')

@section('cssThisPage')
    <!--link href="" rel="stylesheet" type="text/css" /-->
@endsection

@section('jsThisPage')
    <!--script src="/room/plugins/custom/ckeditor/ckeditor-balloon-block.bundle.js" type="text/javascript"></script-->
    <script src="/room/js/pages/html/pages.js" type="text/javascript"></script>
@endsection

@section('content')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">{{ $header }}</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <textarea id="pages-editor" rows="10">{!! $content !!}</textarea>
        </div>
        <div class="kt-portlet__foot">
            <button type="button" class="btn btn-sm btn-primary" data-route="{{ route($route) }}" onClick="TZPage.save()">Сохранить</button>
            <button type="reset" class="btn btn-sm btn-secondary" onClick="TZPage.reset()">Отмена</button>
        </div>
    </div>
    <script>
        document.ready(function(){
            TZPage.editor('none','#pages-editor');
        });
    </script>
@endsection
