@extends('layouts.room')

@section('cssThisPage')
    <link href="/room/plugins/custom/uppy/uppy.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section('jsThisPage')

    <script src="/room/plugins/custom/uppy/uppy.bundle.js" type="text/javascript"></script>
    <script src="/room/plugins/custom/uppy/ru_RU.min.js" type="text/javascript"></script>
    <!--script src="/room/js/pages/components/forms/file-upload/uppy.js" type="text/javascript"></script-->
    <script src="/room/js/pages/html/tz-files.js?1"></script>
@endsection

@section('content')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">{{ $header }}</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <form class="kt-form kt-form--label-right">
                @csrf
                @if( $content )
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">Текущий логотип:</label>
                        <div class="col-lg-9 col-xl-6">
                            <div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_user_avatar">
                                <div class="kt-avatar__holder" style="background-image: url('{{ $content }}');"></div>
                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Удалить логотип"
                                       data-hash="{{ Arr::encryption(['code'=>$code,'path'=>$content]) }}"
                                       onClick="TZFiles.remove(this)">
                                    <i class="flaticon2-cancel-music"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="form-group form-group-last row">
                    <label class="col-lg-3 col-form-label">
                        @if( $content )Обновить логотип:@elseЗагрузить логотип:@endif
                    </label>
                    <div class="col-lg-6">
                        <div class="kt-uppy" id="kt_uppy">
                            <div class="kt-uppy__wrapper"></div>
                            <div class="kt-uppy__list"></div>
                            <div class="kt-uppy__status"></div>
                            <div class="kt-uppy__informer kt-uppy__informer--min"></div>
                        </div>
                        <span class="form-text text-muted">Максимальный объем 50КБайт</span>
                    </div>
                </div>
            </form>
        </div>
        <div class="kt-portlet__foot">
            <button type="button" class="btn btn-sm btn-primary" onClick="TZFiles.upload()">Сохранить</button>
            <button type="reset" class="btn btn-sm btn-secondary" onClick="TZFiles.cancel()">Отмена</button>
        </div>
    </div>
    <script>
        document.ready(function(){
            TZFiles.settings({
                fieldName: 'logo',
                elemId: 'kt_uppy',
                uploadURI: '{{ route($route) }}',
                removeURI: '{{ route($route) }}',
                allowedFileTypes: ['image/*', '.jpg', '.jpeg', '.png', '.gif'],
                maxFileSize: 50000, /// 50Kb
                minFiles: 1,
                maxFiles: 1,
            });
        });
    </script>
@endsection
