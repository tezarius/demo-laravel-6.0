@extends('layouts.room')

@section('cssThisPage')
    <link href="/room/plugins/custom/uppy/uppy.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/room/css/pages/categories/tree.css" rel="stylesheet" type="text/css"/>
@endsection

@section('jsThisPage')
    <script src="/room/js/jquery.treeBuilder.js"></script>
    <script src="/room/js/jquery.treeFilter.js"></script>

    <script src="/room/plugins/custom/uppy/uppy.bundle.js" type="text/javascript"></script>
    <script src="/room/plugins/custom/uppy/ru_RU.min.js" type="text/javascript"></script>
    <!--script src="/room/js/pages/components/forms/file-upload/uppy.js" type="text/javascript"></script-->
    <script src="/room/js/pages/categories/tz-categories.js?3"></script>
@endsection

@section('content')
    <!--begin::Row-->
    <div class="row"></div>
    <!--end::Row-->

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Категории товаров</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-portlet__content">
                {!! $treeHTML !!}
                <br>
                <hr>
                <h5>
                    Категория для рекомендуемых товаров: <span class="text-primary font-weight-bolder">{{ $recommended }}</span>
                </h5>
            </div>
        </div>


        <!-- Button trigger modal -->
        <button id="categorySetIconBtn" type="button" class="btn btn-outline-brand kt-hide" data-toggle="modal" data-target="#categorySetIcon"
                data-icon-add="{{ route('roomCatIconAdd') }}"
                data-icon-del="{{ route('roomCatIconDel') }}"
                data-set-recommended="{{ route('roomRecommended') }}"
        >
            Установка иконки для категории
        </button>
        <!-- Modal -->
        <div class="modal fade" id="categorySetIcon" tabindex="-1" role="dialog" aria-labelledby="Установка иконки для категории" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Установка иконки для категории</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть" onClick="TZCategory.clearForm()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="kt-form kt-form--label-right">
                            @csrf
                            <div class="form-group form-group-last row">
                                <label class="col-lg-3 col-form-label">Загрузить файл:</label>
                                <div class="col-lg-6">
                                    <div class="kt-uppy" id="kt_uppy_5">
                                        <div class="kt-uppy__wrapper"></div>
                                        <div class="kt-uppy__list"></div>
                                        <div class="kt-uppy__status"></div>
                                        <div class="kt-uppy__informer kt-uppy__informer--min"></div>
                                    </div>
                                    <span class="form-text text-muted">Максимальный объем 50КБайт</span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-brand" data-dismiss="modal" onClick="TZCategory.clearForm()">Закрыть</button>
                        <button type="button" class="btn btn-outline-brand" onclick="TZCategory.uploadIcon()">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
