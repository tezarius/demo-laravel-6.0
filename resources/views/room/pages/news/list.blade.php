@extends('layouts.room')

@section('cssThisPage')
    <!--link href="" rel="stylesheet" type="text/css" /-->
@endsection

@section('jsThisPage')
    <script src="/room/js/pages/news/list.js" type="text/javascript"></script>
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Все новости, включая архивные
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('roomNewsAdd') }}" class="btn btn-brand btn-bold btn-upper btn-font-sm">
                        <i class="la la-plus"></i>
                        Добавить новость
                    </a>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-fork--label-right kt-margin-t-20 kt-margin-b-10">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-8 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Поиск..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                        <span><i class="la la-search"></i></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label">
                                        <label>Статус:</label>
                                    </div>
                                    <div class="kt-form__control">
                                        <select class="form-control bootstrap-select" id="kt_form_status">
                                            <option value="">Все</option>
                                            <option value="1">В архиве</option>
                                            <option value="0">Активные</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--end: Search Form -->
        </div>
        <div class="kt-portlet__body kt-portlet__body--fit">

            <!--begin: Datatable -->
            <table class="kt-datatable" id="html_table" width="100%" data-url-edit="{{ route('roomNewsUpd',['id'=>'@ID']) }}" data-url-delete="{{ route('roomNewsDel',['id'=>'@ID']) }}">
                <thead>
                <tr>
                    <th title="Subject">Анонс</th>
                    <th title="DateStart">Начало</th>
                    <th title="DateEnd">Окончание</th>
                    <th title="Status">Статус</th>
                    <th title="Действие">Действие</th>
                    <th title="ID">id</th>
                    <th title="Body">body</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $news AS $n )
                    <tr>
                        <td>{{ Arr::get($n,'subject') }}</td>
                        <td>{{ date('d.m.Y',strtotime(Arr::get($n,'data_start'))) }}</td>
                        <td>{{ date('d.m.Y',strtotime(Arr::get($n,'data_end'))) }}</td>
                        <td>{{ Arr::get($n,'isArchive') }}</td>
                        <td></td>
                        <td>{{ Arr::get($n,'id') }}</td>
                        <td>{{ Arr::get($n,'body') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
@endsection
