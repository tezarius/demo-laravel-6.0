@extends('layouts.room')

@section('cssThisPage')
    <!--link href="" rel="stylesheet" type="text/css" /-->
@endsection

@section('jsThisPage')
    <!--script src="" type="text/javascript"></script-->
    <script src="/room/plugins/custom/tinymce/tinymce.bundle.js" type="text/javascript"></script>
    <script src="/room/js/pages/components/forms/editors/tinymce.js" type="text/javascript"></script>
    <script src="/room/js/pages/components/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
@endsection

@section('content')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">{{ $header }}</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('roomNewsList') }}" class="btn btn-brand btn-bold btn-upper btn-font-sm">
                        <i class="la la-list"></i>
                        К списку
                    </a>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form method="post" class="kt-form kt-form--label-right">
            @csrf
            <div class="kt-portlet__body">
                <div class="form-group form-group-last">
                    <div class="alert alert-secondary" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                        <div class="alert-text">
                            Заполните <code>все поля</code> и нажмите <code>сохранить</code>:
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="news-subject" class="col-2 col-form-label">Анонс текст:</label>
                    <div class="col-10">
                        <input class="form-control" type="text" name="subject" id="news-subject" value="{{ $subject }}" required/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="news-img" class="col-2 col-form-label">Анонс изображение:</label>
                    <div class="col-10">
                        <input class="form-control" type="text" name="img" id="news-img" value="{{ $img }}" required/>
                        <span class="form-text text-muted">Без http и https, пример: //site.ru/image.jpg</span>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="kt-tinymce-4" class="col-2 col-form-label">Новость:</label>
                    <div class="col-10">
                        <textarea id="kt-tinymce-4" name="body" class="form-control" required>{!! $body !!}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="kt_datepicker_5" class="col-2 col-form-label">Актуальность:</label>
                    <div class="col-10">
                        <div class="input-daterange input-group" id="kt_datepicker_5">
                            <input type="text" class="form-control" name="dd1" value="{{ $dd1 }}" autocomplete="off" required/>
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                            </div>
                            <input type="text" class="form-control" name="dd2" value="{{ $dd2 }}" autocomplete="off" required/>
                        </div>
                        <span class="form-text text-muted">Выберите начало и окончание показа новости</span>
                    </div>
                </div>


            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" name="btnNewsSave" class="btn btn-success">Сохранить</button>
                            <button type="reset" class="btn btn-secondary">Отменить</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
