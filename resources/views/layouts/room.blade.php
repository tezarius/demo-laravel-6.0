<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<!-- begin::Head -->
<head>

    <!--begin::Base Path (base relative path for assets of this page) -->
    <base href="/">
    <!--end::Base Path -->

    <meta charset="utf-8" />

    <title>@yield('title','Панель упревления магазином | TEZARIUS (ТЕЗАРИУС)')</title>
    <meta name="keywords" content="@yield('keywords','купить запчасти,автозапчасти онлайн,интернет магазин онлайн,оплата картой,купить на сайте')" />
    <meta name="description" content="@yield('description','Интернет-магазин продажи автозапчастей онлайн')" />

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
    <!--end::Fonts -->

    <script src="/room/js/tz-init.js"></script>

    @include('room.includes.header.stylesheet')

    <!--begin::Page Vendors Styles(used by this page) -->
    @yield('cssThisPage')
    <!--end::Page Vendors Styles -->

    <link rel="shortcut icon" href="/img/favicon.ico" />
</head>
<!-- end::Head -->

<!-- begin::Body -->
<body class="
kt-quick-panel--right
kt-demo-panel--right
kt-offcanvas-panel--right
kt-header--fixed
kt-header-mobile--fixed
kt-subheader--enabled
kt-subheader--transparent
kt-aside--enabled
kt-aside--fixed
kt-page--loading
">
@include('room.includes.main.headerMobile')

<!-- begin:: Root -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <!-- begin:: Page -->
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
        <!-- begin:: Aside -->
        <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
            @if( isset($aside) )
                @include("room.includes.main.aside.$aside",['params'=>$params])
                @include('room.includes.main.aside.main',['addClasses'=>'kt-hide'])
            @else
                @include('room.includes.main.aside.main')
            @endif
        </div>
        <!-- end:: Aside -->

        <!-- begin:: Wrapper -->
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
            @include('room.includes.main.header')
            <!-- end:: Header -->

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                <!-- begin:: Subheader -->
                @include('room.includes.main.headerSub')
                <!-- end:: Subheader -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                    @include('room.includes.main.flash')
                    @yield('content')
                </div>
                <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            @include('room.includes.main.footer')
            <!-- end:: Footer -->
        </div>
        <!-- end:: Wrapper -->
    </div>
    <!-- end:: Page -->
</div>
<!-- end:: Root -->

<!-- begin:: Topbar Offcanvas Panels -->
<?php /*/?>
@include('room.includes.footer.quickActions')
<?php //*/?>
<!-- end::Offcanvas Toolbar Quick Actions -->

<!-- begin:: Quick Panel -->
<?php /*/?>
@include('room.includes.footer.quickPanel')
<?php //*/?>
<!-- end:: Quick Panel -->


@include('room.includes.footer.scrollTop')
@include('room.includes.footer.javascript')


<!--begin::Page Scripts(used by this page) -->
@yield('jsThisPage')
<!--end::Page Scripts -->


</body>
<!-- end::Body -->

</html>
