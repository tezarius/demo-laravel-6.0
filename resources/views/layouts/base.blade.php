<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="desktop landscape" dir="ltr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="is-auth" content="{{ Customer::singleton()->isLogged() }}">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#ffffff">

    <title>@yield('title','Автозапчасти онлайн | TEZARIUS (ТЕЗАРИУС)')</title>
    <meta name="keywords" content="@yield('keywords','купить запчасти,автозапчасти онлайн,интернет магазин онлайн,оплата картой,купить на сайте')" />
    <meta name="description" content="@yield('description','Интернет-магазин продажи автозапчастей онлайн')" />


    <script>
        window.storeType = '{{ Store::singleton()->getType() }}';
        JSON.check = function(str){ try { return (JSON.parse(str) && !!str); } catch (e) { return false; } };
        JSON.decode = function(str){ return ( JSON.check(str) ) ? JSON.parse(str) : {err:1,str:str}; };
        JSON.encode = JSON.stringify;
        String.prototype.trimChars = function (c) {
            let re = new RegExp("^[" + c + "]+|[" + c + "]+$", "g");
            return this.replace(re,"");
        };
        String.prototype.declension = function(number,forma1,forma2,forma3,pn){
            let regexp1 = /[056789]$/;
            let match1 = regexp1.exec(number);
            let regexp2 = /[1]$/;
            let match2 = regexp2.exec(number);
            let regexp3 = /[234]$/;
            let match3 = regexp3.exec(number);

            let str = '';
            if( (number == "0") || ((number >= "5") && (number <= "20")) || match1 != null )
                str = forma3;
            if( match2 != null ) str = forma1;
            if( match3 != null ) str = forma2;

            if( pn ) str = number + ' ' + str;

            return this + str;
        };
        const ready = function ( fn ) {
            if ( typeof fn !== 'function' ) return;
            if ( document.readyState === 'complete'  ) return fn();
            document.addEventListener( 'interactive', fn, false );
            document.addEventListener( 'complete', fn, false );
            window.addEventListener( 'load', fn, false );
        };
        let loaderTimer;
        ready(function(){
            /// 'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
            $.ajaxSetup({
                headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
                beforeSend:function(){ if ( loaderTimer ) { clearTimeout(loaderTimer); } loaderTimer = setTimeout(function () { TZMain.loader.show(); }, 500); },
                complete:function(){ if ( loaderTimer ) { clearTimeout(loaderTimer); } TZMain.loader.hide(); } /// Если не нужно отключать лоадер, либо метод переопределяем, либо через setTimeout вешаем новый !!
            });
            $.extend($.expr[":"],{
                "containsIN" : function(elem, i, match, array) {
                    return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                },
                "contains" : function(elem, i, match, array) {
                    return $(elem).text().toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                    ///return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
                }
            });
        });
        var TZ_GLOBAL = {
            DEBUG: {chat:true}, /// Юзаем для включение или отключения логов по секциями, по умолчанию все отключено | access: chat
            PUSH_SERVER: '{{ env('DB_HOST','127.0.0.1') }}',
            USID: '{{ Customer::singleton()->getSID() }}',
        };
    </script>

    <link href="/css/app.css?19" rel="preload" as="style" />
    <link href="/css/app.css?19" rel="stylesheet" />
	@yield('cssThisPage')

    <link rel="shortcut icon" href="/img/favicon.ico" />
</head>
<body class="tz-store-{{ Store::singleton()->getType() }}">

<header>
    @include('includes.header.nav-top',['offices'=>$offices,'officeID'=>$officeID,'officeName'=>$officeName,])
    <div class="container m-blured">
        <div class="row mt-3 mb-3 mt-lg-4 mb-lg-4">
            <div class="col-12 col-lg-3 col-xl-mw260">
                <div id="logo" class="image-center mb-3 mb-lg-0">
                    @if( $headLogo )
                        <a href="/"><img src="{{ $headLogo }}" alt="Tezarius Logo"></a>
                    @endif
                </div>
            </div>
            <!--div class="col-12 col-lg d-lg-none"><hr></div-->
            <div class="col-12 col-lg-auto">
                @include('includes.header.contacts',['officeName'=>$officeName,])
            </div>
            <!--div class="col-12 col-lg d-lg-none"><hr></div-->
            <div class="col-12 col-lg">
                @include('includes.header.header-menu')
            </div>
        </div>
    </div>
    @include('includes.header.fixed-top')
</header>

<main class="content-wrapper m-blured pb-5">
    @if( $breads )
        @include('includes.main.breads',['breads'=>$breads])
    @endif
    @include('includes.main.flash',['flashers'=>$flashers])
    @yield('content')
</main>

<footer class="m-blured">
    <div class="container">
        <div class="card">
            <div class="card-body p-4 bg-white rounded">
                <div class="row mb-4">
                    <a class="col-12 col-sm-6 col-lg-4 text-center" href="{{ route('about') }}">О нас</a>
                    <a class="col-12 col-sm-6 col-lg-4 text-center" href="{{ route('contacts') }}">Контакты</a>
                    <a class="col-12 col-sm-6 col-lg-4 text-center" href="{{ route('delivery') }}">Доставка</a>
                    <a class="col-12 col-sm-6 col-lg-4 text-center" href="{{ route('refund') }}">Возврат товара</a>
                    <a class="col-12 col-sm-6 col-lg-4 text-center" href="{{ route('payment') }}">Оплата</a>
                    <a class="col-12 col-sm-6 col-lg-4 text-center" href="{{ route('offer') }}">Договор оферты</a>
                </div>
                <hr class="d-none d-sm-block">
                <div class="row">
                    <div class="col-12 col-lg-auto">
                        <div class="text-muted text-left">
                            © 2019-2020 <a href="http://tezarius.ru/" target="_blank">Разработано на платформе TEZARIUS.RU</a>
                        </div>
                    </div>
                    <div class="col-12 col-lg">
                        <div class="d-table w-100 h-100">
                            <div class="d-table-cell w-100 h-100 align-middle">
                                <hr class="d-lg-none">
                                <ul class="list-payments list-inline text-left text-lg-right m-0 f-pay-icons">
                                    <li class="list-inline-item mt-2">
                                        <img src="/img/payments/001.png" alt="Disney" class="img-fluid">
                                    </li>
                                    <li class="list-inline-item mt-2">
                                        <img src="/img/payments/002.png" alt="Dell" class="img-fluid">
                                    </li>
                                    <li class="list-inline-item mt-2">
                                        <img src="/img/payments/003.png" alt="Harley Davidson" class="img-fluid">
                                    </li>
                                    <li class="list-inline-item mt-2">
                                        <img src="/img/payments/004.png" alt="Canon" class="img-fluid">
                                    </li>
                                    <li class="list-inline-item mt-2">
                                        <img src="/img/payments/005.png" alt="Burger King" class="img-fluid">
                                    </li>
                                    <li class="list-inline-item mt-2">
                                        <img src="/img/payments/006.png" alt="Coca Cola" class="img-fluid">
                                    </li>
                                    <li class="list-inline-item mt-2">
                                        <img src="/img/payments/007.png" alt="Sony" class="img-fluid">
                                    </li>
                                    <li class="list-inline-item mt-2">
                                        <img src="/img/payments/008.png" alt="RedBull" class="img-fluid">
                                    </li>
                                    <li class="list-inline-item mt-2">
                                        <img src="/img/payments/009.png" alt="NFL" class="img-fluid">
                                    </li>
                                    <li class="list-inline-item mt-2">
                                        <img src="/img/payments/mir.png" alt="Starbucks" class="img-fluid black-white">
                                    </li>
                                    <li class="list-inline-item mt-2">
                                        <img src="/img/payments/paykeeper.png" alt="Nintendo" class="img-fluid black-white">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="tzOfficeChooseModal" data-office-id="{{ $officeID }}" tabindex="-1" style="display: none;" aria-hidden="true"  @if( !$officeID ) data-backdrop="static" @endif>
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header no-gutters">
                    <div class="col">
                        <h5 class="modal-title">Выберите офис</h5>
                    </div>
                    @if( $officeID )
                        <div class="col-auto">
                            <div class="h5 modal-title modal-h-icon ml-2" data-dismiss="modal">
                                <svg height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="modal-body">
                    @foreach( $offices AS $office )
                        <div class="form-check" data-office-id="{{ $office->id }}" onClick="TZOffices.selected(this)">
                            <a tabindex="1" class="pointer">{{ $office->address }}</a>
                        </div>
                    @endforeach
                    <hr>
                </div>
            </div>
        </div>
    </div>
</footer>


@include('includes.footer.scrollToTop')
@include('includes.footer.loader-2')
@include('includes.footer.fastBasket')


<link href="/js/app.js?14" rel="preload" as="script">
<script src="/js/app.js?14"></script>

@include('includes.footer.callback')
@include('includes.footer.chat')
@if( 1==2 && 'home'!==Route::currentRouteName() )
    @include('includes.footer.categories',['categories'=>$categories])
@endif

<script src="/js/global.js?10"></script>

@yield('jsThisPage')

<script>
    ready(function(){
        // https://www.w3schools.com/howto/howto_js_navbar_sticky.asp
        let
            $fixedTop = $("#fixed-top")
            ,$mBtn = $fixedTop.find('#tz_menu')
            ,$mCnt = $fixedTop.find('#tz_menu .category-menu-list')
            ,$menuHolder = $('.menu-holder')
            ,$menuList = $(".category-menu-list")
            ,withDShow = $mBtn.hasClass('d-show')
        ;
        ////console.log('| .:: $menuHolder ::. |',$menuHolder);
        let sticky = $fixedTop[0].offsetTop;
        function stickyTop(){
            if( window.pageYOffset >= sticky ){
                $fixedTop.addClass("sticky");
                $mBtn.removeClass("show");
                $mCnt.removeClass("show");
                if( withDShow ){
                    $mBtn.removeClass("d-show");
                    $mCnt.removeClass("d-show");
                }
                if( $menuHolder.length ) $menuHolder.height(0);
            } else {
                $fixedTop.removeClass("sticky");
                if( withDShow ){
                    $mBtn.addClass("d-show");
                    $mCnt.addClass("d-show");
                }
                ///menu_open_holder();
                if( $menuHolder.length ) $menuHolder.height($menuList.outerHeight() - $menuHolder.offset().top + $menuList.offset().top);
            }
        }
        window.onscroll = function(){
            stickyTop()
        };

        $('.img2svg').each(function(){
            SVGInject(this)
        });

        $('.tz-tree').treeBuilder({
            iconOpened:'fa fa-folder-open',
            iconClosed:'fa fa-folder',
            subMenuRoot:'tree-submenu',
            collapseMenuItems: true,
            hideClass: 'd-none',
            showClass: 'd-block',
            openClass: 'tz-menu__item--open',
            closeClass: 'tz-menu__item--close',
        });

        /// https://nosir.github.io/cleave.js/
        let cleave = new Cleave('.input-phone', {phone: true, phoneRegionCode: 'RU'});

        ///console.log('| .:: B A S E  R E A D Y  F I N I S H ::. |');
    });
    const TZAlert = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-primary btn-lg px-4 mr-2',
            cancelButton: 'btn btn-secondary btn-lg px-4 mr-2'
        },
        buttonsStyling: false
    });
</script>

{!! $jsCounters !!}

</body>
</html>
