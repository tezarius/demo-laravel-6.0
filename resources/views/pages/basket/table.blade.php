@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('content')
    <section id="price-offers" class="container">
        <hr>
        @if( count($products)>0 )
            <div class="row">
                <div class="col-10">
                    <div class="btn-group tz-basket-tabs" role="group">
                        @foreach( $tabsList AS $tab )
                        <button type="button" data-toggle="tooltip"
                                title="{{ ($tab->tabID==$tabActive) ? 'Активная корзина' : 'Переключиться в корзину' }}"
                                class="btn {{ ($tab->tabID==$tabActive) ? 'btn-success active-basket' : 'btn-secondary' }} tz-basket-tab"
                                data-tab-id="{{ $tab->tabID }}"
                                onClick="TZBasket.change(this)"
                        ><i class="icon ion-2-android-cart"></i>&ensp;{{ $tab->total*1 }}&nbsp;р.</button>
                        @endforeach
                        <button type="button" data-toggle="tooltip" style="display:none"
                                data-title-active="Активная корзина"
                                data-title-default="Переключиться в корзину"
                                data-class-current="active-basket"
                                data-class-active="btn-success active-basket"
                                data-class-default="btn-secondary"
                                onClick="TZBasket.change(this)"
                                class="btn tz-basket-tab-tpl">
                            <i class="icon ion-2-android-cart"></i>&ensp;{{ '0' }}&nbsp;р.
                        </button>
                        <button type="button" data-toggle="tooltip" title="Создать еще корзину" class="btn btn-primary" onClick="TZBasket.create(this)"><i class="oi oi-plus"></i></button>
                    </div>
                </div>
                <div class="col-2 text-right">
                    @if( count($products)>0 )
                        <button id="btn-clear-basket" type="button" data-toggle="tooltip" title="Удалить текущую корзину" class="btn btn-danger" onClick="TZBasket.clear('{{ $tabActive }}')"><i class="oi oi-trash"></i></button>
                    @endif
                </div>
            </div>
            <hr>
            <div id="basket-active-content" class="user-cart">
                @php( $totalDiscount = 0 )
                <table class="user-table mobile-table">
                    <thead class="light">
                    <tr>
                        <th>Дата добавлен</th>
                        <th>Описание</th>
                        <th>Артикул / Бренд</th>
                        <th>Цена</th>
                        <th>Количество</th>
                        <th>Сумма</th>
                        <th>Срок</th>
                        <th>Коментарий на стикере</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $products AS $product )
                        <tr id="row-product-id-{{ $product->id }}">
                            <td data-label="Дата добавлен">
                                {{ date('d.m H:i', strtotime($product->data_ins)) }}
                            </td>
                            <td data-label="Описание">
                                {{ $product->name }}
                            </td>
                            <td data-label="Артикул / Бренд">
                                @if( $product->isHideArticleCode ) ******* @else {{ $product->code_display }} @endif / {{ $product->brand }}
                            </td>
                            <td data-label="Цена">
                                <span class="fs-20">{{ $product->cost*1 }} р</span>
                            </td>
                            <td data-label="Количество">
                                <div id="counter_{{ $product->id }}" class="input-group" style="width:80px;">
                                    <div class="input-group-prepend">
                                        <button class="minus btn btn-primary btn-xs" onclick="TZCounter.minus('#counter_{{ $product->id }}',this);">
                                            <i class="icon ion-2-minus"></i>
                                        </button>
                                    </div>
                                    <input type="text" name="quantity[{{ $product->id }}]" size="1" class="counter form-control text-center"
                                           value="{{ $product->qty*1 }}"
                                           data-step="{{ $product->part }}"
                                           data-min="{{ $product->part }}"
                                           data-max="{{ $product->qty_free*1 }}"
                                           data-id="{{ $product->id }}"
                                           onChange="TZBasket.quantity(this);"
                                           onInput="TZCounter.input('#counter_{{$product->id}}');"
                                    />
                                    <div class="input-group-append">
                                        <button class="plus btn btn-primary btn-xs" onclick="TZCounter.plus('#counter_{{ $product->id }}',this);">
                                            <i class="icon ion-2-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                            <td data-label="Сумма">
                                <span class="fs-20">
                                    @php( $discount = $product->discount*1 )
                                    @php( $totalDiscount += $discount )
                                    @if ($discount > 0 )
                                        <del class="text-danger small">{{ $product->total*1 + $product->discount*1 }} р</del>
                                        <br>
                                        <span class="text-success fs-20">{{ $product->total*1 }} р</span>
                                    @else
                                        <span class="fs-20">{{ $product->total*1 }} р</span>
                                    @endif
                                </span>
                            </td>
                            <td data-label="Срок">
                                {{ $product->dlvr_period }}
                            </td>
                            <td data-label="Коментарий на стикере">
                                <input type="text" class="form-control" value="{{ $product->note }}" data-id="{{ $product->id }}"
                                       onChange="TZBasket.note('#counter_{{ $product->id }}',this);"
                                       onKeyPress="TZBasket.note('#counter_{{ $product->id }}',this);"
                                />
                            </td>
                            <td width="0" class="table-buttons">
                                <h4 class="text-danger" onclick="TZBasket.remove('{{ $product->id }}','#row-product-id-{{ $product->id }}');">
                                    <i class="oi oi-circle-x"></i>
                                </h4>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <hr>
            <div class="row">
                <div class="col-12 col-sm-4">
                    ПРОМОКОД
                    <div class="input-group mb-3">
                        <input id="coupon-code" type="text" class="form-control {{ $coupon?'border-success':'border-secondary' }}"
                               placeholder="Действующий промокод" aria-label="Действующий промокод" value="{{ $coupon }}"
                               data-state-default="border-secondary"
                               data-state-success="border-success"
                               data-state-error="border-danger"
                        />
                        <div class="input-group-append">
                            <button class="btn {{ $coupon?'btn-success':'btn-primary' }}" type="button"
                                    data-state-default="btn-primary"
                                    data-state-success="btn-success"
                                    data-state-error="btn-danger"
                                    onClick="TZBasket.coupon('#coupon-code',this)"
                            >
                                <i class="oi oi-check"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-4">
                    БОНУСЫЕ РУБЛИ
                    <div class="input-group mb-3">
                        <input id="reward-code" type="text" class="form-control {{ $reward?'border-success':'border-secondary' }}"
                               placeholder="Сколько использовать ?" aria-label="Сколько использовать ?" value="{{ $reward }}"
                               data-state-default="border-secondary"
                               data-state-success="border-success"
                               data-state-error="border-danger"
                        />
                        <div class="input-group-append">
                            <button class="btn {{ $reward?'btn-success':'btn-primary' }}" type="button"
                                    data-state-default="btn-primary"
                                    data-state-success="btn-success"
                                    data-state-error="btn-danger"
                                    onClick="TZBasket.reward('#reward-code',this)"
                            >
                                <i class="oi oi-check"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-4 text-right">
                    <div>&nbsp;</div>
                    <button type="button" class="btn btn-primary text-uppercase" onClick="TZBasket.checkout('{{ route('orderCheckout') }}','{{ route('orders') }}')">Оформить заказ</button>
                </div>
            </div>
            <hr>
            @if( $totalDiscount > 0 )
                <h4>Скидка: <span class="font-weight-bolder text-success">{{ $totalDiscount }} р</span></h4>
            @endif
        @else
            <h1 class="text-secondary">Корзина пустая!</h1>
        @endif
    </section>
@endsection
