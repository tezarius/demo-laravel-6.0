@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('content')
    <section id="price-offers" class="container">
        <hr>
        <div class="row">
            <div class="col-10">
                <div class="btn-group tz-basket-tabs" role="group">
                    @foreach( $tabsList AS $tab )
                    <button type="button" data-toggle="tooltip"
                            title="{{ ($tab->tabID==$tabActive) ? 'Активная корзина' : 'Переключиться в корзину' }}"
                            class="btn {{ ($tab->tabID==$tabActive) ? 'btn-success active-basket' : 'btn-secondary' }} tz-basket-tab"
                            data-tab-id="{{ $tab->tabID }}"
                            onClick="TZBasket.change(this)"
                    ><i class="oi oi-basket"></i>&ensp;{{ $tab->total*1 }}&nbsp;р.</button>
                    @endforeach
                    <button type="button" data-toggle="tooltip" style="display:none"
                            data-title-active="Активная корзина"
                            data-title-default="Переключиться в корзину"
                            data-class-current="active-basket"
                            data-class-active="btn-success active-basket"
                            data-class-default="btn-secondary"
                            onClick="TZBasket.change(this)"
                            class="btn tz-basket-tab-tpl">
                        <i class="oi oi-basket"></i>&ensp;{{ '0' }}&nbsp;р.
                    </button>
                    <button type="button" data-toggle="tooltip" title="Создать еще корзину" class="btn btn-info" onClick="TZBasket.create(this)"><i class="oi oi-plus"></i></button>
                </div>
            </div>
            <div class="col-2 text-right">
                <button type="button" data-toggle="tooltip" title="Удалить текущую корзину" class="btn btn-danger" onClick="TZBasket.clear('{{ $tabActive }}')"><i class="oi oi-trash"></i></button>
            </div>
        </div>
        <hr>
        <div id="basket-active-content" class="table-grid">
            @if( count($products)>0 )
                @php( $i = 1 )
                @foreach( $products AS $product )
                    <div id="row-product-id-{{ $product->id }}" class="row row-root bg-light !!row-product">

                        <!-- article / brand | date_order | date_delivery | name -->
                        @if( $openedLeftMenu )@php( $class = '' ) @else @php( $class = '' ) @endif
                        @if( $openedLeftMenu )@php( $class = 'col-9 col-sm-6' ) @else @php( $class = 'col-12 col-sm-6' ) @endif
                        <div class="{{ $class }}">
                            <div class="row">
                                <!-- article / brand | date_order | date_delivery -->
                                @if( $openedLeftMenu )@php( $class = 'col-lg-4 col-12' ) @else @php( $class = 'col-lg-4 col-12' ) @endif
                                <div class="{{ $class }}">
                                    <div class="row">
                                        <div class="col-4 col-lg-12">
                                            @if( $product->isHideArticleCode ) ******* @else {{ $product->code_display }} @endif / {{ $product->brand }}
                                        </div>
                                        <div class="col-4 col-lg-12 text-center text-lg-left">
                                            {{ date('d.m H:i', strtotime($product->data_ins)) }}
                                        </div>
                                        <div class="col-4 col-lg-12 text-right text-lg-left">
                                            {{ $product->dlvr_period }}
                                        </div>
                                    </div>
                                </div>
                                <!-- name -->
                                @if( $openedLeftMenu )@php( $class = 'col-lg-8 col-12' ) @else @php( $class = 'col-lg-8 col-12' ) @endif
                                <div class="{{ $class }}">{{ $product->name }}</div>
                            </div>
                        </div>

                        <!-- price_one | counter | price_total | note | delete -->
                        @if( $openedLeftMenu )@php( $class = 'col-9 col-sm-6' ) @else @php( $class = 'col-12 col-sm-6' ) @endif
                        <div class="{{ $class }}">
                            <div class="row">
                                <!-- price_one | counter | price_total -->
                                <div class="col-sm-12 col-md-6 mr-top-10-xs">
                                    <div class="row">
                                        <div class="col-4 col-sm-3">
                                            @if( $product->cost_first*1>$product->cost*1 )
                                            <del class="text-danger small">{{ $product->cost_first*1 }}</del>
                                            <br>
                                            <span class="text-success fs-20">{{ $product->cost*1 }}</span>
                                            @else
                                            <span class="fs-20">{{ $product->cost*1 }}</span>
                                            @endif
                                        </div>
                                        <div class="col-4 col-sm-6 text-xs-center">
                                                <span class="inline-block">
                                                    <div id="counter_{{ $product->id }}" class="input-group" style="width:80px;">
                                                        <div class="input-group-prepend">
                                                            <button class="btn btn-primary btn-xs" onclick="TZCounter.minus('#counter_{{ $product->id }}',this);">
                                                               <i class="icon ion-2-minus"></i>
                                                            </button>
                                                        </div>
                                                        <input type="text" name="quantity[{{ $product->id }}]" size="1" class="form-control text-center counter"
                                                               value="{{ $product->qty*1 }}"
                                                               data-step="{{ $product->part }}"
                                                               data-qty="{{ $product->qty_free*1 }}"
                                                               data-id="{{ $product->id }}"
                                                               onChange="TZBasket.quantity(this);"
                                                        />
                                                        <div class="input-group-append">
                                                            <button class="btn btn-primary btn-xs" onclick="TZCounter.plus('#counter_{{ $product->id }}',this);">
                                                                <i class="icon ion-2-plus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </span>
                                        </div>
                                        <div class="col-4 col-sm-3 text-xs-right">
                                            <span class="fs-20">{{ $product->total*1 }}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- note | delete -->
                                <div class="col-sm-12 col-md-6 mr-top-10-sm mr-top-10-xs">
                                    <div class="row">
                                        <div class="col-10 col-sm-8">
                                            <input type="text" class="form-control" value="{{ $product->note }}" data-id="{{ $product->id }}"
                                                   onChange="TZBasket.note('#counter_{{ $product->id }}',this);"
                                                   onKeyPress="TZBasket.note('#counter_{{ $product->id }}',this);"
                                            />
                                        </div>
                                        <div class="col-2 col-sm-4 text-right">
                                            <button type="button" data-toggle="tooltip" title="Удалить" class="btn btn-danger ui-button-icon-only"
                                                    onclick="TZBasket.remove('{{ $product->id }}','#row-product-id-{{ $product->id }}');">
                                                <i class="oi oi-circle-x"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                @endforeach
            @else
                <h1 class="text-secondary">Корзина пустая!</h1>
            @endif
        </div>
        <hr>
        <div class="row">
            <div class="col-12 col-sm-4">
                ПРОМОКОД
                <div class="input-group mb-3">
                    <input id="coupon-code" type="text" class="form-control {{ $coupon?'border-success':'border-secondary' }}"
                           placeholder="Действующий промокод" aria-label="Действующий промокод" value="{{ $coupon }}"
                           data-state-default="border-secondary"
                           data-state-success="border-success"
                           data-state-error="border-danger"
                    />
                    <div class="input-group-append">
                        <button class="btn {{ $coupon?'btn-success':'btn-primary' }}" type="button"
                                data-state-default="btn-primary"
                                data-state-success="btn-success"
                                data-state-error="btn-danger"
                                onClick="TZBasket.coupon('#coupon-code',this)"
                        >
                            <i class="oi oi-check"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                БОНУСЫЕ РУБЛИ
                <div class="input-group mb-3">
                    <input id="reward-code" type="text" class="form-control {{ $reward?'border-success':'border-secondary' }}"
                           placeholder="Сколько использовать ?" aria-label="Сколько использовать ?" value="{{ $reward }}"
                           data-state-default="border-secondary"
                           data-state-success="border-success"
                           data-state-error="border-danger"
                    />
                    <div class="input-group-append">
                        <button class="btn {{ $reward?'btn-success':'btn-primary' }}" type="button"
                                data-state-default="btn-primary"
                                data-state-success="btn-success"
                                data-state-error="btn-danger"
                                onClick="TZBasket.reward('#reward-code',this)"
                        >
                            <i class="oi oi-check"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4 text-right">
                <div>&nbsp;</div>
                <button type="button" class="btn btn-primary text-uppercase" onClick="TZBasket.checkout('{{ route('orderCheckout') }}','{{ route('orders') }}')">Оформить заказ</button>
            </div>
        </div>
        <hr>
    </section>
@endsection
