@if( sizeof($details) > 0 )
    <h3 class="mb-2">Глобальный запрос цен</h3>
    <div class="table-grid">
        @foreach( $details AS $detail )
            <div class="row row-root bg-light">
                <div class="col-8 col-sm-10">
                    <div class="row">
                        <div class="col-12 col-sm-6 ">
                            <div class="row">
                                <div class="col-6">
                                    {{ $detail->mfrName }}
                                </div>
                                <div class="col-6">
                                    {{ $detail->articleNumber }}
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            {{ $detail->designation }}
                        </div>
                    </div>
                </div>
                <div class="col-4 col-sm-2 text-right">
                    <a href="{{ route('priceOffers',['number'=>$detail->articleNumber,'brand'=>$detail->mfrName]) }}" class="btn btn-primary" aria-label="Узнать цену" target="_blank">
                        <i class="fa fa-search text-white"></i>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endif