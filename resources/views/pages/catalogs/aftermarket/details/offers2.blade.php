@if( sizeof($offers) > 0 )
    <script src="/js/offers.js?3"></script>
    <div id="price-offers" class="container" data-stock-info="{{ route('stockInfo') }}">
        <h3 class="mb-2">Результат по текущем прайсам</h3>
        @php( $i = 1 )
        <div id="view-table-grid" class="table-grid show mb-4">
            <div class="offers-items">
                @foreach( $offers AS $offer )
                    @include('pages.price.offers.includes.row-grid-table')
                    @php( ++$i )
                @endforeach
            </div>
        </div>
    </div>
@endif
