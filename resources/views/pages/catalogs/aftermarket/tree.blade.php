@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
  <section id="aftermarket-tree" class="container mb-5">
    <h1>Дерево узлов и агрегатов</h1>
    <hr>
    <div class="row">
      <div class="col-12 col-md-4 col-lg-3">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text text-primary bg-white border-primary"><i class="fa fa-search"></i></span>
          </div>
          <input type="text" class="input-search-tree form-control border-primary" aria-label="Amount (to the nearest dollar)">
          <div class="input-group-append">
            <button id="btn-search-reset" type="button" class="btn btn-outline-danger"><i class="icon ion-2-close"></i></button>
          </div>
        </div>
        {!! $treeHTML !!}
      </div>
      <div id="details" class="col-12 col-md-8 col-lg-9"></div>
    </div>
  </section>
  <script>
    ready(function(){
      $('.am-tree-nodes').treeBuilder({
        iconOpened:'fa fa-folder-open',
        iconClosed:'fa fa-folder',
        subMenuRoot:'tree-submenu',
        collapseMenuItems: true,
        hideClass: 'd-none',
        showClass: 'd-block',
        openClass: 'tz-menu__item--open',
        closeClass: 'tz-menu__item--close',
      });
      $(".am-tree-nodes").treeFilter({
        menuParentRoot: '.am-tree-nodes',
        menuParentShow: 'd-block',
        menuParentHide: 'd-none',

        menuTitleRoot: '.item__branch',
        menuTitleText: '.item__branch > a',
        menuTitleOpen: 'active',
        menuTitleClose: '',

        menuItemsRoot: '.tree-submenu',
        menuItemsShow: 'd-block',
        menuItemsHide: 'd-none',

        menuItemRoot: '.item__link',
        menuItemText: '.item__link > .pricing',
        menuItemTags: '.item__link > .pricing > .keywords-tags',
        menuItemShow: 'active',
        menuItemHide: 'd-none',

        ///searchWithMenuText: true,
        searcher: $('.input-search-tree'),
        reset: $('#btn-search-reset')
      });
      $('a.pricing').click(function(){
        let
                $ths = $(this)
                ,type = $ths.data('type')
                ,car  = $ths.data('car')
                ,node = $ths.data('node')
        ;
        $.ajax({
          url: '{{ route('amDetails',['hash'=>'inner']) }}',
          type: 'POST',
          data: 'type=' + type + '&car=' + car + '&node=' + node + '&inner=1',
          dataType: 'json',
          beforeSend: function() {
            ///$(ths).button('loading');
          },
          complete: function() {
            ///$(ths).button('reset');
          },
          success: function(json) {
            ///console.log('|.:: AFTERMARKET details JSON ::.|',json);
            ///if( json.status ) location.href = json.redirect;
            if( json['flash'] ) $('.breadcrumb').after(json['flash']);
            if( json.message ){
              Swal.fire({
                type: json.message.type,
                title: json.message.text,
                ///text: json.message.text,
              });
            }
            if( json['details'] ){
              let $details = $('#details');
              $details.html(json['details']);
              $("html, body").animate({
                scrollTop: $details.offset().top - 50
              }, 1000);
            }
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });

        return false;
      });
    })
  </script>
@endsection