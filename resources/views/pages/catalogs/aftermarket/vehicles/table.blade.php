@extends('layouts.base')

@section('cssThisPage')
@endsection

@section('jsThisPage')
    <script src="/js/jquery.localSearch.js"></script>
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="aftermarket-vehicles" class="container">
        <h1>Доступные модификации</h1>
        <row>
            <span class="col-12 col-sm-6 col-md-4 col-lg-3">
                <input type="text" id="local-search" class="form-control"  placeholder="Быстрый поиск..."/>
            </span>
        </row>
        <hr>
        <table class="user-table mobile-table">
            <thead class="dark">
            <tr>
                <th>Двигатель</th>
                <th>Период выпуска</th>
                <th>Мощность л.с.</th>
                <th>Объем куб.см</th>
                <th>Кузов</th>
                <th>Топливо</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $vehicles AS $vehicle )
                <tr class="vehicle-item">
                    <td data-label="Двигатель">
                        <a class="item-search" href="{{ route('amTree',['hash'=>Arr::encryption(['type'=>$type,'mark'=>$mark,'model'=>$model,'car'=>$vehicle->carId])]) }}">
                            {{ $vehicle->typeName }} <small class="text-secondary">- {{ $vehicle->impulsionType }}</small>
                        </a>
                    </td>
                    <td class="item-search" data-label="Период выпуска">{{ $vehicle->constructioninterval }}</td>
                    <td class="item-search" data-label="Мощность л.с.">{{ $vehicle->hp }}</td>
                    <td class="item-search" data-label="Объем куб.см">{{ $vehicle->cylinderCapacityCcm }}</td>
                    <td class="item-search" data-label="Кузов">{{ $vehicle->constructionType }}</td>
                    <td class="item-search" data-label="Топливо">{{ $vehicle->motorType }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div id="localSearchMark"></div>
    </section>
    <script>
        ready(function(){
            $('#localSearchMark').jsLocalSearch({
                "mincaracteres": 1,
                "searchinput": "#local-search",
                "container": "vehicle-item",
                "containersearch": "item-search",
                "action": "Show", /// Mark | Show |
                "actionok": "mark",
                "actionko": "unmark",
                "html_search": true,
                ///"mark_text": "si"
            });
        });
    </script>
@endsection