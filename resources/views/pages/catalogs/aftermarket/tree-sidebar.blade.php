@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
  <section id="aftermarket-tree" class="container mb-5">
    <h1>Дерево узлов</h1>
    <button id="am-tree-btn" class="btn btn-danger border-like-form d-none" onclick="$('#am-tree-sidebar').toggleClass('active')" title="Дерево узлов">
      <i class="fas fa-list-ul mr-2"></i>Дерево узлов
    </button>
    <hr>
    <div class="row">
      <div class="col-auto p-0">
        <nav id="am-tree-sidebar" class="sidebar sidebar-disguised footer-fix footer-fix-46 bg-white text-dark">
          <button class="dismiss btn btn-secondary text-white border-dark" onclick="$('#am-tree-sidebar').toggleClass('active')">
            <i class="icon ion-2-close-round"></i>
          </button>
          <div class="content">
            <div class="header">
              <h3>Дерево категорий</h3>
            </div>
            <div class="body">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary bg-white border-primary"><i class="fa fa-search"></i></span>
                </div>
                <input type="text" class="input-search-tree form-control border-primary" aria-label="Amount (to the nearest dollar)">
                <div class="input-group-append">
                  <button id="btn-search-reset" type="button" class="btn btn-outline-danger"><i class="icon ion-2-close"></i></button>
                </div>
              </div>
              {!! $treeHTML !!}
            </div>
            <div class="footer"></div>
          </div>
          <div class="overlay" onclick="$('#am-tree-sidebar').toggleClass('active')"></div>
        </nav>
      </div>
      <div class="col">
        <div id="tree-nodes" class="col-12 col-sm-6 d-lg-none">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text text-primary bg-white border-primary"><i class="fa fa-search"></i></span>
            </div>
            <input type="text" class="input-search-tree form-control border-primary" aria-label="Amount (to the nearest dollar)">
            <div class="input-group-append">
              <button id="btn-search-reset" type="button" class="btn btn-outline-danger"><i class="icon ion-2-close"></i></button>
            </div>
          </div>
          {!! $treeHTML !!}
        </div>
        <div id="details"></div>
      </div>
    </div>
    </div>
  </section>
  <script>
    ready(function(){
      ///$('#tz-tree').treed();
      $('.am-tree-nodes').treeBuilder({
        iconOpened:'fa fa-folder-open',
        iconClosed:'fa fa-folder',
        subMenuRoot:'tree-submenu',
        collapseMenuItems: true,
        hideClass: 'd-none',
        showClass: 'd-block',
        openClass: 'tz-menu__item--open',
        closeClass: 'tz-menu__item--close',
      });
      $(".am-tree-nodes").treeFilter({
        menuParentRoot: '.am-tree-nodes',
        menuParentShow: 'd-block',
        menuParentHide: 'd-none',

        menuTitleRoot: '.item__branch',
        menuTitleText: '.item__branch > a',
        menuTitleOpen: 'active',
        menuTitleClose: '',

        menuItemsRoot: '.tree-submenu',
        menuItemsShow: 'd-block',
        menuItemsHide: 'd-none',

        menuItemRoot: '.item__link',
        menuItemText: '.item__link > .pricing',
        menuItemTags: '.item__link > .pricing > .keywords-tags',
        menuItemShow: 'active',
        menuItemHide: 'd-none',

        ///searchWithMenuText: true,
        searcher: $('.input-search-tree'),
        reset: $('#btn-search-reset')
      });
      $('a.pricing').click(function(){
        let
                $ths = $(this)
                ,type = $ths.data('type')
                ,car  = $ths.data('car')
                ,node = $ths.data('node')
        ;
        $.ajax({
          url: '{{ route('amDetails',['hash'=>'inner']) }}',
          type: 'POST',
          data: 'type=' + type + '&car=' + car + '&node=' + node + '&inner=1',
          dataType: 'json',
          success: function(json) {
            ///console.log('|.:: AFTERMARKET details JSON ::.|',json);
            ///if( json.status ) location.href = json.redirect;
            if( json['flash'] ) $('.breadcrumb').after(json['flash']);
            if( json.message ){
              Swal.fire({
                type: json.message.type,
                title: json.message.text,
                ///text: json.message.text,
              });
            }
            if( json['details'] ){
              $('#tree-nodes').detach();
              $('#am-tree-btn').removeClass('d-none');
              $('h1').addClass('d-none');

              let $details = $('#details');
              $details.html(json['details']);
              $("html, body").animate({
                scrollTop: $details.offset().top - 50
              }, 1000);
            }
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });

        return false;
      });
    })
  </script>
@endsection