@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
    <script src="/js/jquery.localSearch.js"></script>
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="aftermarket-marks" class="container">
        <h1>Доступные марки</h1>
        <row>
            <span class="col-12 col-sm-6 col-md-4 col-lg-3">
                <input type="text" id="local-search" class="form-control"  placeholder="Быстрый поиск..."/>
            </span>
        </row>
        <hr>
        <div class="row">
            @foreach( $marks AS $mark )
                <div class="mark-item col-xs-6 col-sm-4 col-md-3 col-lg-2">
                    <p class="text-center"><a class="mark-link" href="{{ route('amModels',['hash'=>Arr::encryption(['type'=>$type,'mark'=>$mark->manuId])]) }}">{{ $mark->manuName }}</a></p>
                </div>
            @endforeach
        </div>
        <div id="localSearchMark"></div>
    </section>
    <script>
        ready(function(){
            $('#localSearchMark').jsLocalSearch({
                "mincaracteres": 1,
                "searchinput": "#local-search",
                "container": "mark-item",
                "containersearch": "mark-link",
                "action": "Show", /// Mark | Show |
                "actionok": "mark",
                "actionko": "unmark",
                "html_search": true,
                ///"mark_text": "si"
            });
        });
    </script>
@endsection