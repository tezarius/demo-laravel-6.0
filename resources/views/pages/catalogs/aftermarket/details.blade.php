@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
  <section id="aftermarket-details" class="container">
    <h1>Доступные марки</h1>
    {!! $detailsOffers !!}
    {!! $detailsCatalog !!}
  </section>
@endsection
