@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
  <section id="aftermarket-types" class="container">
    <h1>Типы автомобилей</h1>
    <hr>
    <div class="row">
      @foreach( $types AS $type )
        <div class="col-sm-4 mb-4 pr-sm-1 pl-sm-1">
          <div class="card box-shadow">
            <a class="card-img-top" href="{{ Arr::get($type,'href') }}">
              <img src="{{ Arr::get($type,'image') }}" alt="{{ Arr::get($type,'name') }}" title="{{ Arr::get($type,'name') }}" class="embed-responsive img2svg p-1" data-holder-rendered="true" onLoad="" />
            </a>
            <div class="card-body p-1">
              <h2 class="text-center"><a href="{{ Arr::get($type,'href') }}">{{ Arr::get($type,'name') }}</a></h2>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </section>
@endsection



