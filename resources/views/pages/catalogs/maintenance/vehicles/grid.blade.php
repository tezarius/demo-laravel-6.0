@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
  <script src="/js/jquery.localSearch.js"></script>
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
  <section id="aftermarket-vehicles" class="container">
    <h1>Доступные модификации</h1>
    <row>
      <span class="col-12 col-sm-6 col-md-4 col-lg-3">
          <input type="text" id="local-search" class="form-control"  placeholder="Быстрый поиск..."/>
      </span>
    </row>
    <hr>
    <div class="row">
      @foreach( $vehicles AS $vehicle )
        <a class="ls-root | col-12 btn btn-outline-light border-right-0 border-left-0 border-top-0 rounded-0 pb-2 pt-2 text-left text-dark border-secondary"
           href="{{ route('toDetails',['hash'=>Arr::encryption(['markID'=>$markID,'markName'=>$markName,'modelID'=>$modelID,'modelName'=>$modelName,'vehicleID'=>$vehicle->id,'vehicleName'=>$vehicle->name])]) }}"
        >
          <span class="ls-text">{{ $vehicle->name }}</span>
        </a>
      @endforeach
    </div>
    <div id="localSearchMark"></div>
  </section>
  <script>
    ready(function(){
      $('#localSearchMark').jsLocalSearch({
        "mincaracteres": 1,
        "searchinput": "#local-search",
        "container": "ls-root",
        "containersearch": "ls-text",
        "action": "Show", /// Mark | Show |
        "actionok": "mark",
        "actionko": "unmark",
        "html_search": true,
        ///"mark_text": "si"
      });
    });
  </script>
@endsection