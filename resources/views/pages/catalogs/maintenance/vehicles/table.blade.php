@extends('layouts.base')

@section('cssThisPage')
@endsection

@section('jsThisPage')
    <script src="/js/jquery.localSearch.js"></script>
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="maintenance-vehicles" class="container">
        <h1>Доступные модификации</h1>
        <row>
            <span class="col-12 col-sm-6 col-md-4 col-lg-3">
                <input type="text" id="local-search" class="form-control"  placeholder="Быстрый поиск..."/>
            </span>
        </row>
        <hr>
        <table class="user-table mobile-table">
            <thead class="dark d-none"><tr><th>Двигатель / Период выпуска</th></tr></thead>
            <tbody>
            @foreach( $vehicles AS $vehicle )
                <tr class="vehicle-item">
                    <td data-label="Двигатель / Период выпуска">
                        <a class="vehicle-link" href="{{ route('toDetails',['hash'=>Arr::encryption(['markID'=>$markID,'markName'=>$markName,'modelID'=>$modelID,'modelName'=>$modelName,'vehicleID'=>$vehicle->id,'vehicleName'=>$vehicle->name])]) }}">
                            {{ $vehicle->name }}
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div id="localSearchMark"></div>
    </section>
    <script>
        ready(function(){
            $('#localSearchMark').jsLocalSearch({
                "mincaracteres": 1,
                "searchinput": "#local-search",
                "container": "vehicle-item",
                "containersearch": "vehicle-link",
                "action": "Show", /// Mark | Show |
                "actionok": "mark",
                "actionko": "unmark",
                "html_search": true,
                ///"mark_text": "si"
            });
        });
    </script>
@endsection