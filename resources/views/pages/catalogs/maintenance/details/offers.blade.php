@if( sizeof($offers) > 0 )
    <h3>Результат по текущем прайсам</h3>
    <div class="table-grid">
    @php( $i = 1 )
    @php( $isAjax = request()->ajax() )
        @foreach( $offers AS $offer )
            <div id="row-root-{{$i}}" class="row row-root {{ (int)Arr::get($offer,'GoodsID') > 0 ? 'bg-success-light' : 'bg-light' }} ">

                    <!-- article / brand | name -->
                @if( $isAjax )@php( $class = 'col-sm-4 col-md-5' ) @else @php( $class = 'col-sm-4 col-md-5 col-lg-6' ) @endif
                <div class="{{ $class }}">
                        <div class="row">
                            <!-- article / brand -->
                            @if( $isAjax )@php( $class = 'col-lg-2 col-12' ) @else @php( $class = 'col-lg-2 col-12' ) @endif
                            <div class="{{ $class }}">
                                <div class="row">
                                    <div class="col-6 col-lg-12">
                                        <a href="{{ $offer->more }}" target="_blank">
                                            {{ $offer->code }}
                                        </a>
                                    </div>
                                    <div class="col-6 col-lg-12">{{ $offer->brand }}</div>
                                </div>
                            </div>
                            <!-- name -->
                            @if( $isAjax )@php( $class = 'col-lg-10 col-12' ) @else @php( $class = 'col-lg-10 col-12' ) @endif
                            <div class="{{ $class }}">
                                {{ $offer->name }}
                            </div>
                        </div>
                    </div>

                    <!-- cost | qty / delivery / stock | counter / btn -->
                @if( $isAjax )@php( $class = 'col-sm-8 col-md-7' ) @else @php( $class = 'col-sm-8 col-md-7 col-lg-6' ) @endif
                <div class="{{ $class }}">
                        <div class="row">

                            <!-- qty | delivery + stock -->
                            @if( $isAjax )@php( $class = 'col-sm-4 col-md-4col-lg-4' ) @else @php( $class = 'col-sm-4 col-md-4 col-lg-4' ) @endif
                            <div class="{{ $class }}">
                                <div class="row">
                                    <!-- qty -->
                                    <div class="col-6 col-sm-3">
                                        {{ $offer->qty*1 }} {{ $offer->unitName }}
                                    </div>
                                    <!-- delivery + stock -->
                                    <div class="col-6 col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                @if( $offer->dlvrWeb_max > $offer->dlvrWeb_min )
                                                    @php( $dlvrDispley = $offer->dlvrWeb_min . '-' . $offer->dlvrWeb_max . ' дн' )
                                                @elseif( $offer->dlvrWeb_max == $offer->dlvrWeb_min and $offer->dlvrWeb_min==0 )
                                                    @php( $dlvrDispley = 'На складе' )
                                                @elseif( $offer->dlvrWeb_max == $offer->dlvrWeb_min and $offer->dlvrWeb_min==1 )
                                                    @php( $dlvrDispley = 'Сегодня' )
                                                @elseif( $offer->dlvrWeb_min==0 )
                                                    @php( $dlvrDispley = 'на складе' )
                                                @elseif( $offer->dlvrWeb_min==1 )
                                                    @php( $dlvrDispley = 'сегодня' )
                                                @else
                                                    @php( $dlvrDispley = $offer->dlvrWeb_min . ' дн' )
                                                @endif
                                                {{ $dlvrDispley }}
                                            </div>
                                            @if( $offer->logo )
                                                <div class="col-sm-12">{{ $offer->logo }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- cost -->
                            @if( $isAjax )@php( $class = 'col-sm-3 col-md-3 col-lg-3' ) @else @php( $class = 'col-sm-3 col-md-3 col-lg-3' ) @endif
                            <div class="{{ $class }}">
                                {{ $offer->cost_display }}
                            </div>

                            <!-- counter / btn -->
                            @if( $isAjax )@php( $class = 'col-sm-5 col-md-5 col-lg-5' ) @else @php( $class = 'col-sm-5 col-md-5 col-lg-5' ) @endif
                            <div class="{{ $class }}">
                                <div class="row">
                                    <!-- counter -->
                                    @if( $isAjax )@php( $class = 'col-6 col-sm-8' ) @else @php( $class = 'col-6 col-sm-8' ) @endif
                                    <div class="{{ $class }}">
                                        <div id="counter_{{$i}}" class="input-group counter">
                                            <div class="input-group-prepend">
                                                <button type="submit" data-toggle="tooltip" title="" class="btn btn-default btn-xs" onclick="TZCounter.minus('#counter_{{$i}}',this);"
                                                        data-original-title="Уменьшить">
                                                    <i class="oi oi-minus"></i>
                                                </button>
                                            </div>
                                            <input type="text" name="quantity[{{$i}}]" class="form-control counter"
                                                   value="{{ $offer->cost_for_qty!=null ? $offer->cost_for_qty : 1 }}"
                                                   data-qty="{{ $offer->qty*1 }}"
                                                   data-step="{{ $offer->cost_for_qty!=null ? $offer->cost_for_qty : 1 }}"
                                                   data-rbg="{{ $offer->GoodsID }}"
                                                   data-tzp="{{ $offer->tzp }}"
                                                   data-std="{{ $offer->id_rbStock }}"
                                                   data-pld="{{ $offer->id_rbStockStoragePlace }}"
                                                   data-row-root="{{$i}}"
                                                   size="1"
                                            />
                                            <div class="input-group-append">
                                                <button type="button" data-toggle="tooltip" title="" class="btn btn-default btn-xs" onclick="TZCounter.plus('#counter_{{$i}}',this);"
                                                        data-original-title="Увеличить">
                                                    <i class="oi oi-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- btn: add basket -->
                                    @if( $isAjax )@php( $class = 'col-6 col-sm-4' ) @else @php( $class = 'col-6 col-sm-4' ) @endif
                                    <div class="{{ $class }}">
                                        <button class="btn btn-primary basket-add" onclick="TZBasket.add('#counter_{{$i}}',this);">
                                            <i class="oi oi-basket"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            @php( ++$i )
        @endforeach
  </div>
@endif