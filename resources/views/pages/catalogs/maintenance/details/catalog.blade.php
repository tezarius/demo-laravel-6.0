@if( sizeof($details) > 0 )
    <h3>Глобальный запрос цен</h3>
    <div class="table-grid">
        @foreach( $details AS $detail )
            <div class="row row-root bg-light">
                <div class="col">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            {{ $detail->name }}
                        </div>
                        <div class="col-6 col-sm-3">
                            {{ $detail->code }}
                        </div>
                        <div class="col-6 col-sm-3">
                            {{ $detail->note }}
                        </div>
                    </div>
                </div>
                <div class="col-auto">
                    <a href="{{ route('priceOffers',['number'=>$detail->code,'brand'=>$detail->brand]) }}" class="btn btn-primary" aria-label="Узнать цену" target="_blank">
                        <i class="fa fa-search text-white"></i>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endif