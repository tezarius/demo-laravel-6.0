@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="maintenance-details" class="container">
        <h1 class="mb-3">Список автозапчастей</h1>
        <hr>
        {!! $detailsOffers !!}
        {!! $detailsCatalog !!}
    </section>
@endsection
