@extends('layouts.base')

@section('cssThisPage')
@endsection

@section('jsThisPage')
    <script src="/js/jquery.localSearch.js"></script>
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="maintenance-models" class="container">
        <h1>Доступные модели</h1>
        <row>
            <span class="col-12 col-sm-6 col-md-4 col-lg-3">
                <input type="text" id="local-search" class="form-control"  placeholder="Быстрый поиск..."/>
            </span>
        </row>
        <hr>
        <table class="user-table mobile-table">
            <thead class="dark d-none"><tr><th>Модель / Год</th></tr></thead>
            <tbody>
            @foreach( $models AS $model )
                <tr class="model-item">
                    <td data-label="Модель / Год" width="1px" class="nowrap pr-3">
                        <a class="model-link" href="{{ route('toVehicles',['hash'=>Arr::encryption(['markID'=>$markID,'markName'=>$markName,'modelID'=>$model->id,'modelName'=>$model->name])]) }}">
                            {{ $model->name }}
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div id="localSearchMark"></div>
    </section>
    <script>
        ready(function(){
            $('#localSearchMark').jsLocalSearch({
                "mincaracteres": 1,
                "searchinput": "#local-search",
                "container": "model-item",
                "containersearch": "model-link",
                "action": "Show", /// Mark | Show |
                "actionok": "mark",
                "actionko": "unmark",
                "html_search": true,
                ///"mark_text": "si"
            });
        });
    </script>
@endsection