@extends('layouts.base',['openedLeftMenu'=>0,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('content')
    <style>
        #price-brands tr {
            border-bottom: 1px solid #dfdfdf;

            display: inherit;
            border-top: none;
            word-break: break-all;
        }
        #price-brands tr:hover {
            background-color: #f4f4f4;
        }
        #price-brands tr a.brand {
            font-family: sans-serif;
            font-weight: bold;
            line-height: 2.3rem;
            text-transform: uppercase;
            white-space: nowrap;
        }
        #price-brands tr td.name { width: 100%; }
        #price-brands tr a.name {
            position: absolute;
            left: 1rem;
            right: 0;
            top: .4rem;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }
    </style>
    <section id="price-brands" class="container">
        <hr>
        @if( !empty($brands) )
            <table class="w-100">
                @foreach( $brands AS $brand )
                    <tr class="">
                        <td width="1">
                            <a class="brand text-dark" href="{{ route('priceOffers',['number'=>$number,'brand'=>Arr::get($brand,'name')]) }}">
                                <strong>{{ Arr::get($brand,'name') }}</strong>
                            </a>
                        </td>
                        <td class="name pl-3 position-relative">
                            <a class="name text-dark" href="{{ route('priceOffers',['number'=>$number,'brand'=>Arr::get($brand,'name')]) }}">
                                {{ Arr::get($brand,'goods_name') }}
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <p>По вашему запросу ничего не найдено</p>
        @endif
    </section>
@endsection
