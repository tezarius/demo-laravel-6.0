<?php /// Не зашло, но удалять жаль?>
<div id="row-root-{{$i}}" class="row row-root {{ $type }} {{ $display }}"
     data-filter-price="{{ Arr::get($offer,'cost',0) * 1 }}"
     data-filter-delivery="{{ Arr::get($offer,'dlvrWeb_min',0) * 1 }}"
     data-filter-brand="{{ Arr::get($offer,'brand','unknown') }}"
     data-filter-count="{{ Arr::get($offer,'qty',0) * 1 }}"
     data-filter-properties="{{ Arr::get($offer,'properties_json','{}') }}"
>
    <div class="col-2">
        <div class="row">
            <div class="col-6 col-lg-12">
                <a href="{{ $offer->more }}" target="_blank" class="bold dashed">
                    {{ $offer->code }}
                </a>
            </div>
            <div class="col-6 col-lg-12">{{ $offer->brand }}</div>
        </div>
    </div>
    <div class="col-2">{{ $offer->qty*1 }} {{ $offer->unitName }}</div>
    <div class="col-2">
        <div class="row {{ (int)Arr::get($offer,'GoodsID') > 0 ? 'text-success font-weight-bold' : '' }}">
            <div class="col-sm-12">
                @if( (int)Arr::get($offer,'GoodsID') > 0 )
                    @php( $dlvrDispley = 'В магазине' )
                @elseif( $offer->dlvrWeb_max > $offer->dlvrWeb_min )
                    @php( $dlvrDispley = $offer->dlvrWeb_min . '-' . $offer->dlvrWeb_max . ' дн' )
                @elseif( $offer->dlvrWeb_max == $offer->dlvrWeb_min and $offer->dlvrWeb_min==0 )
                    @php( $dlvrDispley = 'На складе' )
                @elseif( $offer->dlvrWeb_max == $offer->dlvrWeb_min and $offer->dlvrWeb_min==1 )
                    @php( $dlvrDispley = 'Сегодня' )
                @elseif( $offer->dlvrWeb_min==0 )
                    @php( $dlvrDispley = 'на складе' )
                @elseif( $offer->dlvrWeb_min==1 )
                    @php( $dlvrDispley = 'сегодня' )
                @else
                    @php( $dlvrDispley = $offer->dlvrWeb_min . ' дн' )
                @endif
                {{ $dlvrDispley }}
            </div>
            @if( $offer->logo )
                <div class="col-sm-12">{{ $offer->logo }}</div>
            @endif
        </div>
    </div>
    <div class="col-2">{{ $offer->cost_display }}</div>
    <div class="col-2">
        <div id="counter_{{$i}}" class="input-group counter" style="width:80px;">
            <div class="input-group-prepend">
                <button class="btn btn-primary btn-xs" onclick="TZCounter.minus('#counter_{{$i}}',this);">
                    <i class="icon ion-2-minus"></i>
                </button>
            </div>
            <input type="text" name="quantity[{{$i}}]" class="form-control text-center counter"
                   value="{{ $offer->cost_for_qty!=null ? $offer->cost_for_qty : 1 }}"
                   data-qty="{{ $offer->qty*1 }}"
                   data-step="{{ $offer->cost_for_qty!=null ? $offer->cost_for_qty : 1 }}"
                   data-rbg="{{ $offer->GoodsID }}"
                   data-tzp="{{ $offer->tzp }}"
                   data-std="{{ $offer->id_rbStock }}"
                   data-pld="{{ $offer->id_rbStockStoragePlace }}"
                   data-row-root="{{$i}}"
                   size="1"
            />
            <div class="input-group-append">
                <button class="btn btn-primary btn-xs" onclick="TZCounter.plus('#counter_{{$i}}',this);">
                    <i class="icon ion-2-plus"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="col-2">
        <h2 class="text-primary" onclick="TZBasket.add('#counter_{{$i}}',this);"><i class="icon ion-2-android-cart"></i></h2>
    </div>
    <div class="col-12">{{ $offer->name }}</div>
</div>