<div id="row-root-{{$i}}" class="row row-root {{ $type??'' }} {{ $display??'' }}"
     data-filter-price="{{ Arr::get($offer,'cost',0) * 1 }}"
     data-filter-delivery="{{ Arr::get($offer,'delivery_min',0) * 1 }}"
     data-filter-brand="{{ Arr::get($offer,'brand','unknown') }}"
     data-filter-count="{{ Arr::get($offer,'qty',0) * 1 }}"
     data-filter-properties="{{ Arr::get($offer,'properties_json','{}') }}"
>
    <!-- article / brand | name -->
    <div class="col-sm-5 col-md-6 col-lg-6">
        <div class="row">
            <!-- article / brand -->
            <div class="col-12">
                <div class="row">
                    <div class="col-6 col-lg-6 font-weight-bold">
                        {{ $offer->code }}
                    </div>
                    <div class="col-6 col-lg-6 font-weight-bold">{{ $offer->brand }}</div>
                </div>
            </div>
            <!-- name -->
            <div class="col-12 text-break">
                @if( @$offer->more )
                    <a href="{{ $offer->more }}" class="text-primary">
                        {{ $offer->name }}
                    </a>
                @else
                    {{ $offer->name }}
                @endif

            </div>
        </div>
    </div>

    <!-- cost | qty / delivery / stock | counter / btn -->
    <div class="col-sm-7 col-md-6 col-lg-6">
        <div class="row">
            <!-- qty | delivery + stock -->
            <div class="col-sm-4 col-md-4 col-lg-4">
                <div class="row">
                    <!-- qty -->
                    <div class="col-6 col-sm-12">
                        {{ $offer->qty*1 }}&nbsp;{{ $offer->unitName }}
                    </div>
                    <!-- delivery + stock -->
                    <div class="col-6 col-sm-12">
                        <span class="nowrap">
                            @if( $stat = Arr::get($offer,'dlvrPercInx') )
                                <img src="/img/stat/{{ $stat }}.gif" alt="{{ Arr::get($offer,'deliveryProc') }}%" width="16px">
                            @endif
                            <span class="{{ (int)Arr::get($offer,'GoodsID') > 0 ? 'text-success font-weight-bold' : '' }}">
                                @if( (int)Arr::get($offer,'GoodsID') > 0 )
                                    @php( $delivery_display = 'В магазине' )
                                @elseif( $offer->delivery_max > $offer->delivery_min )
                                    @php( $delivery_display = $offer->delivery_min . '-' . $offer->delivery_max . ' дн' )
                                @elseif( $offer->delivery_max == $offer->delivery_min and $offer->delivery_min==0 )
                                    @php( $delivery_display = 'На складе' )
                                @elseif( $offer->delivery_max == $offer->delivery_min and $offer->delivery_min==1 )
                                    @php( $delivery_display = 'Сегодня' )
                                @elseif( $offer->delivery_min==0 )
                                    @php( $delivery_display = 'на складе' )
                                @elseif( $offer->delivery_min==1 )
                                    @php( $delivery_display = 'сегодня' )
                                @else
                                    @php( $delivery_display = $offer->delivery_min . ' дн' )
                                @endif
                                {{ $delivery_display }}
                            </span>
                        </span>

                            @if( $offer->logo )
                                <span class="pointer d-inline-block p-1" data-toggle="dropdown"
                                      data-stock-id="{{ Arr::get($offer,'id_rbStock') }}"
                                      data-delivery-min="{{ Arr::get($offer,'dlvrWeb_min') }}"
                                      data-delivery-max="{{ Arr::get($offer,'dlvrWeb_max') }}"
                                      data-delivery-prc="{{ Arr::get($offer,'deliveryProc') }}"
                                      @if( Agent::isDesktop() )
                                      onMouseEnter="TZOffer.stockInfoEnter(this)" onMouseLeave="TZOffer.stockInfoLeave(this)"
                                      @else
                                      onClick="TZOffer.stockInfoClick(this)"
                                      @endif
                                >
                                    <span class="font-italic text-black-50 nowrap dashed">{{ $offer->logo }}</span>
                                </span>
                                <div class="dropdown-menu stock-info dropdown-13 dropdown-w-20 dropdown-menu-right p-3">
                                    Загрузка...
                                </div>
                            @endif
                    </div>
                </div>
            </div>

            <!-- cost -->
            <div class="col-sm-3 col-md-3 col-lg-3">
                <h4>{{ $offer->cost_display }}</h4>
            </div>

            <!-- counter / btn -->
            <div class="col-sm-5 col-md-5 col-lg-5">
                <div class="row">
                    <!-- counter -->
                    <div class="col-6 col-sm-8">
                        <div id="counter_{{$i}}" class="input-group counter" style="width:80px;">
                            <div class="input-group-prepend">
                                <button class="minus btn btn-primary btn-xs" onclick="TZCounter.minus('#counter_{{$i}}');">
                                    <i class="icon ion-2-minus"></i>
                                </button>
                            </div>
                            @php( $step = $offer->cost_for_qty!=null ? $offer->cost_for_qty : 1 )
                            <input type="text" name="quantity[{{$i}}]" class="form-control text-center counter"
                                   value="{{ $step }}"
                                   data-step="{{ $step }}"
                                   data-min="{{ $step }}"
                                   data-max="{{ $offer->qty*1 }}"
                                   data-rbg="{{ $offer->GoodsID }}"
                                   data-tzp="{{ $offer->tzp }}"
                                   data-std="{{ $offer->id_rbStock }}"
                                   data-pld="{{ $offer->id_rbStockStoragePlace }}"
                                   data-row-root="{{$i}}"
                                   onInput="TZCounter.input('#counter_{{$i}}');"
                                   size="1"
                            />
                            <div class="input-group-append">
                                <button class="plus btn btn-primary btn-xs" onclick="TZCounter.plus('#counter_{{$i}}');">
                                    <i class="icon ion-2-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- btn: add basket -->
                    <div class="col-6 col-sm-4">
                        <h2 class="text-primary" onclick="TZBasket.add('#counter_{{$i}}',this);"><i class="icon ion-2-android-cart"></i></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
