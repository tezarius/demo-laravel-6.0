<div id="row-root-{{$i}}" class="{{ $class??'row-root product-layout product-items product-list col-6' }}"
     data-filter-price="{{ Arr::get($offer,'cost',0) * 1 }}"
     data-filter-delivery="{{ Arr::get($offer,'dlvrWeb_min',0) * 1 }}"
     data-filter-brand="{{ Arr::get($offer,'brand','unknown') }}"
     data-filter-count="{{ Arr::get($offer,'qty',0) * 1 }}"
     data-filter-properties="{{ Arr::get($offer,'properties_json','{}') }}"
>
    <div class="card h-100">
        <a class="image position-relative px-2 px-md-3 pt-2 pb-2" href="{{ Arr::get($offer,'more') }}" target="_blank">
            @php( $image = ( $img = Arr::get($offer,'image') ) ? $img : '/img/no_img_600x600.png' )
            <img src="{{ $image }}" data-src="{{ Arr::get($offer,'image','/img/no_img_600x600.png',true) }}">
        </a>
        <div class="card-body px-2 px-md-3 pt-2 pb-0">
            <div class="caption">
                <h6 class="card-title line-clamp clamp-2">
                    <a href="{{ Arr::get($offer,'more') }}">
                        {{ Arr::get($offer,'name') }}
                    </a>
                </h6>
                <small class="description text-muted mb-3 line-clamp clamp-2">
                    {{ Arr::get($offer,'code') }} / {{ Arr::get($offer,'brand') }}
                </small>
            </div>
        </div>
        <div class="card-footer px-2 px-md-3 pb-2 pb-md-3 pt-0 bg-white border-0">
            <div class="price">
                @if( Arr::get($offer,'special') )
                    <div class="h6 mb-1">
                        <s class="d-inline-block text-danger font-weight-light">{{ Arr::get($offer,'cost_display') }}</s>
                        <span class="d-inline-block">{{ Arr::get($offer,'special') }}</span>
                    </div>
                @else
                    <div class="h3 mb-1">{{ Arr::get($offer,'cost_display') }}</div>
                @endif
            </div>
            <?php /*/?>
            <div class="rating mt-2">
                <div class="d-inline-block mr-1">
                    <span class="si si-rem si-rem-star">
                        <svg fill="#ddd" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"></path>
                        </svg>
                    </span>
                    <span class="si si-rem si-rem-star">
                        <svg fill="#ddd" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"></path>
                        </svg>
                    </span>
                    <span class="si si-rem si-rem-star">
                        <svg fill="#ddd" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"></path>
                        </svg>
                    </span>
                    <span class="si si-rem si-rem-star">
                        <svg fill="#ddd" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"></path>
                        </svg>
                    </span>
                    <span class="si si-rem si-rem-star">
                        <svg fill="#ddd" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"></path>
                        </svg>
                    </span>
                </div>
                <small class="d-inline-block">
                    <span class="text-muted">Нет отзывов</span>
                </small>
            </div>
            <?php //*/?>
            <div class="stok-status mt-3 mb-2">
                <div class="row">
                    <div class="col-auto align-self-center">
                        <span class="text-success">⬤</span>
                    </div>
                    <div class="col pl-0">
                        <small class="d-block">
                            <span class="text-muted">
                                В наличии {{ Arr::get($offer,'qty')*1 }} шт
                                <br>
                                {{ Arr::get($offer,'rbStock_name') }}
                            </span>
                        </small>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-auto mb-1 pr-1">
                    <div id="counter_{{$i}}" class="input-group counter" style="width:80px;">
                        <div class="input-group-prepend">
                            <button class="minus btn btn-primary btn-xs" onclick="TZCounter.minus('#counter_{{$i}}',this);">
                                <i class="icon ion-2-minus"></i>
                            </button>
                        </div>
                        @php( $step = $offer->cost_for_qty!=null ? $offer->cost_for_qty : 1 )
                        <input type="text" name="quantity[{{$i}}]" class="form-control text-center counter"
                               value="{{ $step }}"
                               data-step="{{ $step }}"
                               data-min="{{ $step }}"
                               data-max="{{ $offer->qty*1 }}"
                               data-rbg="{{ $offer->GoodsID }}"
                               data-tzp="{{ $offer->tzp }}"
                               data-std="{{ $offer->id_rbStock }}"
                               data-pld="{{ $offer->id_rbStockStoragePlace }}"
                               data-row-root="{{$i}}"
                               onInput="TZCounter.input('#counter_{{$i}}');"
                               size="1"
                        />
                        <div class="input-group-append">
                            <button class="plus btn btn-primary btn-xs" onclick="TZCounter.plus('#counter_{{$i}}',this);">
                                <i class="icon ion-2-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <button class="btn btn-danger" onclick="TZBasket.add('#counter_{{$i}}',this);">
                        <i class="icon ion-2-android-cart"></i> <span class="btn-text">Купить</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>