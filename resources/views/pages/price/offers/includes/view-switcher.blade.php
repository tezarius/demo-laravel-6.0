<div class="mb-3">
    <div class="row no-gutters">
        <div class="col-auto col-sm-auto order-sm-3">

            @if( $isGoods )
                <div class="btn-group ml-sm-2">
                    <button type="button" id="tz-view-table" class="tz-view btn btn-light px-2 border-like-form active" title="Таблица" onclick="TZOffers.view.table()" aria-label="Таблица">
                        <i class="icon ion-2-android-menu"></i>
                    </button>
                    <button type="button" id="tz-view-list" class="tz-view btn btn-light px-2 border-like-form" title="Список" onclick="TZOffers.view.list()" aria-label="Список">
                        <i class="fa fa-list"></i>
                    </button>
                    <button type="button" id="tz-view-grid" class="tz-view btn btn-light px-2 border-like-form" title="Сетка" onclick="TZOffers.view.grid()" aria-label="Сетка">
                        <i class="icon ion-2-android-apps"></i>
                    </button>
                </div>
            @endif

        </div>
        <div class="col col-sm-auto ml-sm-auto order-sm-2"></div>
        <div class="col-12 col-sm-auto order-sm-1 mt-2 mt-sm-0">
            <div class="form-group input-group input-group-sm mb-0">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-light border-like-form" onclick="$('#offers-filter').toggleClass('active')" title="Фильтр">
                            <i class="fas fa-filter"></i> Фильтр
                        </button>
                    </div>
                    <select class="form-control custom-select" onChange="TZFilter.sort(this)" aria-label="Сортировка">
                        <option value="sort-asc" selected="selected">По возрастанию цены</option>
                        <option value="sort-desc">По убыванию цены</option>
                        @if( !$isGoods )
                            <option value="delivery-asc">Ближайшая дата доставки</option>
                        @endif
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>