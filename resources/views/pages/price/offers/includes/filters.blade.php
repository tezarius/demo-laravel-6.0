<nav id="offers-filter" class="sidebar sidebar-disguised footer-fix footer-fix-46 bg-white text-dark">
    <button class="dismiss btn btn-secondary text-white border-dark" onclick="$('#offers-filter').toggleClass('active')">
        <i class="icon ion-2-close-round"></i>
    </button>

    <div class="content">
        <div class="header">
            <h3>Фильтр</h3>
        </div>
        <div class="body">
            <form>
                @php( $priceMax = Arr::get($offers,'filters.priceMax') )
                @if( $priceMax > 0 )
                    <h4>По Цене</h4>
                    <div class="input-group">
                        <input type="range" class="custom-range" id="rangePrice" min="0" max="{{ $priceMax }}" value="{{ $priceMax }}" onInput="TZFilter.price('range','#inputPrice',this)"/>
                        <div class="input-group-append">
                            <input id="inputPrice" type="text" min="0" max="{{ $priceMax }}" value="{{ $priceMax }}" onInput="TZFilter.price('input','#rangePrice',this)"/>
                        </div>
                    </div>
                    <hr>
                @endif

                @php( $deliveryMax = Arr::get($offers,'filters.deliveryMax') )
                @if(  $deliveryMax > 0 )
                    <h4>По дате доставки</h4>
                    <div class="input-group">
                        <input type="range" class="custom-range" id="rangeDelivery" min="0" max="{{ $deliveryMax }}" value="{{ $deliveryMax }}" onInput="TZFilter.delivery('range','#inputDelivery',this)"/>
                        <div class="input-group-append">
                            <input id="inputDelivery" type="text" min="0" max="{{ $deliveryMax }}" value="{{ $deliveryMax }}" onInput="TZFilter.delivery('input','#rangeDelivery',this)"/>
                        </div>
                    </div>
                    <hr>
                @endif

                @php( $brands = (array) Arr::get($offers,'filters.arrBrands') )
                @if( count($brands) > 1 )
                    <h4>По брендам</h4>
                    <div class="row">
                        @foreach( $brands AS $k=>$brand )
                            <div class="col-6 nowrap ellipsis" style="width:136px">
                                <div class="form-check">
                                    <input type="checkbox" name="brands[]" class="form-check-input filter-brand" id="brandID_{{ $k }}" value="{{ $brand }}" onclick="TZFilter.brands()">
                                    <label class="form-check-label text-uppercase" for="brandID_{{ $k }}">{{ $brand  }}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <hr>
                @endif

                <?php /*/?>
                @php( $qtyMax = Arr::get($offers,'filters.qtyMax') )
                <hr>
                <h4>Желаемое кол-во</h4>
                <div class="input-group">
                    <div class="input-group-prepend"></div>
                    <input type="text" min="0" max="{{ $qtyMax }}" onInput="TZFilter.count('input','',this)"/>
                    <div class="input-group-append"></div>
                </div>
                <?php //*/?>

                @php( $properties = (array)Arr::get($offers,'filters.properties') )
                @foreach( $properties AS $id=>$property )
                @php( $items = (array)Arr::get($property,'items') )
                @if( count($items) > 1 )
                    <h4>{{ Arr::get($property,'name') }}</h4>
                    <div class="row">
                        @foreach( $items AS $k=>$value )
                            <div class="col-6">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input filter-properties-all filter-properties-{{ $id }}"
                                           id="propID_{{ $id }}_{{ $k }}"
                                           name="propID_{{ $id }}_{{ $k }}"
                                           value="{{ $value }}"
                                           onclick="TZFilter.properties('{{ $id }}')"
                                    >
                                    <label class="form-check-label" for="propID_{{ $id }}_{{ $k }}">{{ $value  }}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <hr>
                @endif
            @endforeach
            </form>
        </div>
        <div class="footer">
            <div id="filter-actions">
                <button type="button" class="btn btn-success" onClick="TZFilter.apply()">Применить</button>
                <button type="reset" class="btn btn-secondary" onClick="TZFilter.reset()">Сбросить</button>
            </div>
        </div>
    </div>

    <div class="overlay" onclick="$('#offers-filter').toggleClass('active')"></div>

    <button id="offersFilterApply" class="apply btn btn-success btn-lg text-uppercase d-none" onClick="TZFilter.apply()">
        Применить
    </button>
    <a id="endOffersFilter" data-note="end follow block"></a>
</nav>