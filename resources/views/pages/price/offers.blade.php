@extends('layouts.base')<!-- ,['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,] -->

@section('cssThisPage')
@endsection

@section('jsThisPage')
    <script src="/js/jquery.scrollFollow.js"></script>
    <script src="/js/offers-filter.js?8"></script>
    <script src="/js/offers.js?3"></script>
@endsection

@section('content')
    <section id="price-offers" class="container"
             data-stock-info="{{ route('stockInfo') }}"
    >

        @php( $offersRg = (array) Arr::get($offers,'origin.items',[]) )
        @php( $offersNg = (array) Arr::get($offers,'analog.items',[]) )
        @if( count($offersRg)>0 || count($offersNg)>0 )
            <div class="row">
                <div class="col-auto p-0">
                    @include('pages.price.offers.includes.filters')
                </div>
                <div class="col">
                    @include('pages.price.offers.includes.view-switcher')

                    @php( $i = 1 )
                    <div id="view-table-grid" class="table-grid show">
                        <div id="offers-origin">
                            @php( $n = 1 )
                            <div class="row row-more">
                                <div class="col">
                                    @if( !$isGoods )
                                        <h2 class="text-center text-secondary">Искомый номер</h2>
                                    @endif
                                </div>
                                <div class="col-auto show-more d-none" data-need-show="0">
                                    <h5><a tabindex="1" class="pointer dashed" onClick="TZFilter.more(this)" data-show="0">скрыть</a></h5>
                                </div>
                            </div>

                            @php( $limit = (int)Arr::get($offers,'origin.limit'))
                            <div class="offers-items" data-limit="{{ $limit }}">
                                @foreach( $offersRg AS $offer )
                                    @php( $display = $n>$limit ? 'd-none' : '' )
                                    @include('pages.price.offers.includes.row-grid-table',['type'=>'origin','display'=>$display])
                                    @php( ++$i )
                                    @php( ++$n )
                                @endforeach
                            </div>
                            @if( ($more = (int)Arr::get($offers,'origin.count') - (int)Arr::get($offers,'origin.limit'))>0 )
                                <div class="row row-more" data-need-hide="1">
                                    <div class="col-auto m-auto">
                                        <h5>
                                            <i class="icon ion-2-refresh text-secondary"></i>
                                            <a tabindex="1" class="pointer dashed" onClick="TZFilter.more(this)" data-show="1">
                                                Показать еще {{ Str::declension($more,'позицию','позиции','позиций') }}
                                            </a>
                                        </h5>
                                    </div>
                                </div>
                            @endif
                        </div>

                        @if( !$isGoods )
                            <div id="offers-analog">
                                @php( $n = 1 )
                                <div class="row row-more">
                                    <div class="col">
                                        <h2 class="text-center text-secondary mt-3">Возможные аналоги и замены</h2>
                                    </div>
                                    <div class="col-auto show-more d-none"data-need-show="0">
                                        <h5><a tabindex="1" class="pointer dashed" onClick="TZFilter.more(this)" data-show="0">скрыть</a></h5>
                                    </div>
                                </div>
                                @php( $limit = (int)Arr::get($offers,'analog.limit'))
                                <div class="offers-items" data-limit="{{ $limit }}">
                                    @foreach( $offersNg AS $offer )
                                        @php( $display = $n>$limit ? 'd-none' : '' )
                                        @include('pages.price.offers.includes.row-grid-table',['type'=>'analog','display'=>$display])
                                        @php( ++$i )
                                        @php( ++$n )
                                    @endforeach
                                </div>
                                @if( ($more = (int)Arr::get($offers,'analog.count') - (int)Arr::get($offers,'analog.limit'))>0 )
                                    <div class="row row-more" data-need-hide="1">
                                        <div class="col-auto m-auto">
                                            <h5>
                                                <i class="icon ion-2-refresh text-secondary"></i>
                                                <a tabindex="1" class="pointer dashed" onClick="TZFilter.more(this)" data-show="1">
                                                    Показать еще {{ Str::declension($more,'позицию','позиции','позиций') }}
                                                </a>
                                            </h5>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                    @if( $isGoods )
                        <div id="view-list-grid" class="list-grid">
                            <div class="row offers-items">
                                @foreach( $offersRg AS $offer )
                                    @include('pages.price.offers.includes.row-grid-list')
                                    @php( ++$i )
                                @endforeach

                                @foreach( $offersNg AS $offer )
                                    @include('pages.price.offers.includes.row-grid-list')
                                    @php( ++$i )
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @else
            <div>
                По вашему запросу ничего не найдено
            </div>
        @endif
    </section>
    <script>
        const TZOffers = function(){
            // Private Function
            let section = '#price-offers';
            let $displayTableGrid, $displayListGrid, $viewTable, $viewList, $viewGrid, $views;
            let common = function(){
                $displayTableGrid = $('#view-table-grid');
                $displayListGrid = $('#view-list-grid');

                $viewTable = $('#tz-view-table');
                $viewList = $('#tz-view-list');
                $viewGrid = $('#tz-view-grid');
                
                $views = $('.tz-view');
            };
            let view = {
                table : function(){
                    if( $viewTable.hasClass('active') ) return;
                    $views.removeClass('active');
                    $viewTable.addClass('active');
                    
                    $displayListGrid.removeClass('show');
                    $displayTableGrid.addClass('show');

                    localStorage.setItem('offers-display-view', 'table');
                },
                list : function(){
                    if( $viewList.hasClass('active') ) return;
                    $views.removeClass('active');
                    $viewList.addClass('active');

                    $displayTableGrid.removeClass('show');
                    $displayListGrid.addClass('show');

                    $(section+' .product-grid > .clearfix').remove();

                    $displayListGrid.find('.product-layout').attr('class', 'row-root product-layout product-items product-list col-12');

                    localStorage.setItem('offers-display-view', 'list');
                },
                grid : function(){
                    if( $viewGrid.hasClass('active') ) return;
                    $views.removeClass('active');
                    $viewGrid.addClass('active');

                    $displayTableGrid.removeClass('show');
                    $displayListGrid.addClass('show');

                    var cols = $('#column-right, #column-left').length;

                    let _class = '';
                    if (cols === 2) {
                        _class = 'row-root product-layout product-items product-grid col-xl-4 col-lg-6 col-md-4 col-sm-4 col-6';
                    } else if (cols === 1) {
                        _class = 'row-root product-layout product-items product-grid col-xl-3 col-lg-4 col-sm-4 col-6';
                    } else {
                        _class = 'row-root product-layout product-items product-grid col-lg-4 col-md-4 col-sm-6 col-12';
                    }
                    $displayListGrid.find('.product-layout').attr('class',_class);

                    localStorage.setItem('offers-display-view', 'grid');
                }
            };
            return {
                // Public Function
                init: function () {
                    common();
                    // Other Init Method;
                }
                ,view : view
            };
        }();
        ready(function(){
            TZOffers.init();
            @if( $isGoods )
                let view = localStorage.getItem('offers-display-view')||'{{ $displayView??NULL }}';
                console.log('offers-display-view: ',view);
                if( view && typeof TZOffers.view[view] === 'function' ) TZOffers.view[view]();
            @endif
        });
    </script>
@endsection
