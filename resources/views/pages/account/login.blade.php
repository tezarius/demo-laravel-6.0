@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="account-login" class="container">
        @if( $errors->any() )<div class="alert alert-danger alert-dismissible fade show" role="alert">
            @foreach( $errors->all() as $error )
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div>{{ $error }}</div>
            @endforeach</div>
        @endif
        <h1 class="h2 mb-4">Вход на сайт</h1>
        <div class="row ">
            <div class="col-12 col-md-6 mb-4">
                <form action="{{ route('login') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="email" value="{{ old('email') }}" placeholder="E-Mail адрес" id="input-email" class="form-control @error('email') is-invalid @enderror" />
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" value="{{ old('password') }}" placeholder="Пароль" id="input-password" class="form-control @error('password') is-invalid @enderror" />
                    </div>
                    <input type="submit" name="btnLogin" value="Войти" class="btn btn-primary px-5">
                    <!--a href="{ route('forgotten') }" class="btn btn-link">Забыли пароль?</a-->
                </form>
            </div>
        </div>
    </section>
@endsection
