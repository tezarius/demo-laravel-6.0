@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="account-password" class="container">
        <h1 class="h2 mb-4">Личный кабинет</h1>
        <form action="{{ route('password') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="">
                <h5 class="mb-3">Пароль</h5>
                <div class="form-group required">
                    <input type="password" value="{{ old('password_old') }}" name="password_old" placeholder="Старый пароль" id="input-password_old"
                           class="form-control @error('password_old') is-invalid @enderror"
                    />
                    @error('password_old')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                </div>

                <div class="form-group required">
                    <input type="password" value="{{ old('password') }}" name="password" placeholder="Новый пароль" id="input-password"
                           class="form-control @error('password') is-invalid @enderror"
                    />
                    @error('password')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                </div>
                <div class="form-group required">
                    <!-- <label for="input-confirm">Подтвердите пароль</label> -->
                    <input type="password" value="{{ old('password_confirmation') }}" name="password_confirmation" placeholder="Подтвердите новый пароль" id="input-password_confirmation"
                           class="form-control @error('password_confirmation') is-invalid @enderror"
                    />
                    @error('password_confirmation')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                </div>
            </div>
            <div class="">
                <a href="{{ route('account') }}" class="btn btn-secondary px-5">Назад</a>
                <input type="submit" name="btnPassword" value="Продолжить" class="btn btn-primary px-4">
            </div>
        </form>
    </section>
@endsection
