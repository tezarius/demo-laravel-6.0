@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="account-account" class="container">
        <h1 class="h2 mb-4">Личный кабинет</h1>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="well card card-body bg-light mb-3">
                    <div class="media">
                        <div class="float-left mr-3"><i class="fa fa-credit-card card-icon"></i></div>
                        <div class="media-body">
                            Внутренний счет
                            <h4 class="media-heading"><a class="text-secondary" href="{{ route('balance') }}">{{ Arr::get($balanceInfo,'onBalance','00.00')*1 }} руб</a></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="well card card-body bg-light mb-3">
                    <div class="media">
                        <div class="float-left mr-3"><i class="fa fa-gem card-icon"></i></div>
                        <div class="media-body">
                            @if( $onBonus = (int)Arr::get($discountInfo,'onBonus') )
                                Бонусных баллов
                                <h4 class="media-heading"><a class="text-secondary" href="{{ route('balance') }}">{{ $onBonus }}</a></h4>
                            @elseif( $prcMax = (int)Arr::get($discountInfo,'percent_max') )
                                Размер скидки
                                <h4 class="media-heading"><a class="text-secondary" href="{{ route('balance') }}">{{ $prcMax }}%</a></h4>
                            @else
                                Бонусны \ Скидка
                                <h4 class="media-heading"><a class="text-secondary" href="{{ route('balance') }}">0</a></h4>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="well card card-body bg-light">
                    <div class="media">
                        <div class="float-left mr-3"><i class="fa fa-cube card-icon"></i></div>
                        <div class="media-body">
                            Всего заказов
                            <h4 class="media-heading"><a class="text-secondary" href="{{ route('orders') }}">49</a></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="well card card-body bg-light">
                    <div class="media">
                        <div class="float-left mr-3"><i class="fa fa-heart card-icon"></i></div>
                        <div class="media-body">
                            Избранное
                            <h4 class="media-heading"><a class="text-secondary" href="{{ route('wishlist') }}wishlist">0</a></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="panel-title">
                            <i class="icon ion-2-person text-secondary mr-3"></i>
                            <span>Моя учетная запись</span>
                        </h4>
                    </div>
                    <div class="list-group list-group-flush">
                        <a class="list-group-item list-group-item-action py-2" href="{{ route('accountEdit') }}">
                            <i class="fa fa-edit list-group-icon mr-2"></i>
                            Изменить профиль
                        </a>
                        <a class="list-group-item list-group-item-action py-2" href="{{ route('password') }}">
                            <i class="fa fa-unlock list-group-icon mr-2"></i>
                            Изменить пароль
                        </a>
                        <a class="list-group-item list-group-item-action py-2" href="{{ route('newsletter') }}">
                            <i class="fa fa-mail-bulk list-group-icon mr-2"></i>
                            Рассылка уведомлений
                        </a>
                        <a class="list-group-item list-group-item-action py-2" href="{{ route('logout') }}">
                            <i class="fa fa-sign-out-alt list-group-icon mr-2"></i>
                            Выйти
                        </a>
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="panel-title">
                            <i class="icon ion-2-ios-briefcase text-secondary mr-3"></i>
                            Мои заказы
                        </h4>
                    </div>
                    <div class="list-group list-group-flush">
                        <a class="list-group-item list-group-item-action py-2" href="{{ route('orders') }}">
                            <i class="fa fa-history list-group-icon mr-2"></i>
                            История заказов
                        </a>
                        <a class="list-group-item list-group-item-action py-2" href="{{ route('reward') }}">
                            <i class="fa fa-gem list-group-icon mr-2"></i>
                            Бонусные баллы
                        </a>
                        <a class="list-group-item list-group-item-action py-2" href="{{ route('balance') }}">
                            <i class="fa fa-credit-card list-group-icon mr-2"></i>
                            История транзакций
                        </a>
                        <a class="list-group-item list-group-item-action py-2" href="{{ route('wishlist') }}">
                            <i class="fa fa-heart list-group-icon mr-2"></i>
                            Посмотреть закладки
                        </a>
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
        </div>
    </section>
@endsection
