@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
    <link rel="stylesheet" href="/css/tempusdominus-bootstrap-4.min.css">
@endsection

@section('jsThisPage')
    <script src="/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="/js/orders.js?1"></script>
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="account-orders" class="container">
        <h1 class="h2 mb-4">Личный кабинет</h1>
        @if( !empty($orders)  || $isButtonPressed )
            <form id="orders-filters" method="post" enctype="multipart/form-data"
                  data-history="{{ route('orderHistory') }}"
                  data-withdraw="{{ route('orderWithdraw') }}"
                  data-delivery="{{ route('orderDelivery') }}"
                  data-towork="{{ route('orderToWork') }}"
            >
                @csrf
                <input type="hidden" name="filter" class="filter-code" value="{{ $filter??'current' }}" />
                <div class="row">
                    <div class="col-sm-12 col-md-8 col-lg-7">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="filter-name">{{ $filterName }}</span>
                                    <i class="icon icon-xs ion-2-chevron-down"></i>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" data-filter="current">Текущие</a>
                                    <a class="dropdown-item" data-filter="period">За период</a>
                                    <a class="dropdown-item" data-filter="number">По номеру</a>
                                    <div role="separator" class="dropdown-divider"></div>
                                    <a class="dropdown-item" data-filter="search">Мультипоиск</a>
                                </div>
                            </div>


                            <input type="text" name="current" class="form-control current" aria-label="Текущие" value="Текущие заказы"
                                   style="display:{{ $filter=='current'?'inline-block':'none' }}" readonly
                            />


                            <input type="text" name="period[dd1]" class="form-control period" aria-label="От"
                                   id="period-dd1" data-target="#period-dd1" data-toggle="datetimepicker" autocomplete="off"
                                   value="{{ $period?Arr::get($period,'dd1'):'' }}" style="display:{{ $filter=='period'?'inline-block':'none' }}"
                            />
                            <input type="text" name="period[dd2]" class="form-control period" aria-label="До"
                                   id="period-dd2" data-target="#period-dd2" data-toggle="datetimepicker" autocomplete="off"
                                   value="{{ $period?Arr::get($period,'dd2'):'' }}" style="display:{{ $filter=='period'?'inline-block':'none' }}"
                            />


                            <input type="text" name="number" class="form-control number" aria-label="По номеру" value="{{ $number }}"
                                   style="display:{{ $filter=='number'?'inline-block':'none' }}"
                            />

                            <input type="text" name="search" class="form-control search" aria-label="Мультипоиск" value="{{ $search }}"
                                   style="display:{{ $filter=='search'?'inline-block':'none' }}"
                            />

                            <div class="input-group-append">
                                <button type="submit" name="btnFilterOut" class="btn btn-outline-secondary">Загрузить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <br>
            <div class="table-grid">
                @if( !empty($orders) )
                    <div class="row row-root">
                        <div class="col-auto">
                            <label class="custom-control material-checkbox checkbox-total" onClick="TZCheckBox.all(this)">
                                <input type="checkbox" class="material-control-input" />
                                <span class="material-control-indicator"></span>
                                <span class="material-control-description"></span>
                            </label>
                        </div>
                        <div class="col">
                            <button class="btn btn-primary btn-all-to-work btn-lg mt-1" onclick="TZOrders.toWork()" disabled="true">В работу по отмеченным</button>
                        </div>
                    </div>
                    @foreach( $orders AS $order )
                        @php( $stateCode = Str::lower(Arr::get($order,'StateCode')) )
                        @php( $orderID = Arr::get($order,'id') )
                        <div id="row-order-id-{{ $orderID }}" class="row row-root align-items-center" style="background-color:{{ Arr::get($order,'color_web') }};color:{{ Arr::get($order,'color_font_web') }};">
                            <div class="col-auto">
                                <label class="custom-control material-checkbox state-{{$stateCode}} {{ 'new'==$stateCode?'visible':'invisible' }}" onClick="TZCheckBox.one(this)">
                                    <input type="checkbox" name="orders[]" class="material-control-input" value="{{ $orderID }}" />
                                    <span class="material-control-indicator"></span>
                                    <span class="material-control-description"></span>
                                </label>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col-6 col-sm-3 order-0 order-sm-0">
                                        <div class="row">
                                            <div class="col-12">
                                                Заказ № {{ Arr::get($order,'doc_number') }}
                                            </div>
                                            <div class="col-12">
                                                {{ $order->art_code }} / {{ $order->art_brand }}
                                            </div>
                                            <div class="col-12">
                                                {{ date('d.m.Y',strtotime(Arr::get($order,'doc_date'))) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 order-2 order-sm-1">
                                        <div class="row">
                                            <div class="col-12">
                                                <span class="ellipsis">{{ Arr::get($order,'name') }}</span>
                                                @if( Arr::get($order,'last_note') )
                                                    <br><b>NOTE:</b> <span>{{ Arr::get($order,'last_note') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-sm-3 order-1 order-sm-2">
                                        <div class="row">
                                            <div class="col-12">
                                                <span>{{ Arr::get($order,'cost')*1 }} р/шт</span>
                                            </div>
                                            <div class="col-6">
                                                <span>{{ Arr::get($order,'qty')*1 }} шт</span>
                                            </div>
                                            <div class="col-6">
                                                <span>{{ Arr::get($order,'total')*1 }} р</span>
                                            </div>
                                            <div class="col-12">
                                                <a class="dropdown-toggle dashed pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <b>{{ Arr::get($order,'StateName') }}</b>
                                                </a>
                                                <div class="dropdown-menu dropdown-13 dropdown-menu-right width-full-ns" style="background-color:{{ Arr::get($order,'color_web') }};color:{{ Arr::get($order,'color_font_web') }};">
                                                    <a class="dropdown-item ellipsis pointer" data-toggle="tooltip" title="Запрос срока доставки" onClick="TZOrders.delivery({{ $orderID }})">
                                                        <i class="icon ion-2-ios-time"></i> Запрос срока
                                                    </a>
                                                    @if( 'WaitingToBeSent'!=Arr::get($order,'StateCode') and 'ReadyToIssue'!=Arr::get($order,'StateCode') and 'RequestToSupplier'!=Arr::get($order,'StateCode') )
                                                        <a class="dropdown-item ellipsis pointer" data-toggle="tooltip" title="Запрос на снятие" onClick="TZOrders.withdraw({{ $orderID }})">
                                                            <i class="fa fa-calendar-alt"></i> Запрос на снятие
                                                        </a>
                                                    @endif
                                                    <a class="dropdown-item ellipsis pointer" data-toggle="tooltip" title="История позиции заказа" onClick="TZOrders.history({{ $orderID }})">
                                                        <i class="fa fa-history"></i> История
                                                    </a>
                                                </div>
                                                @if( 'New'==Arr::get($order,'StateCode') )
                                                    &emsp;<button class="btn btn-primary btn-sm mt-1" onClick="TZOrders.toWork({{ $orderID }})">В работу</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="row row-root">
                        <div class="col-auto">
                            <label class="custom-control material-checkbox checkbox-total" onClick="TZCheckBox.all(this)">
                                <input type="checkbox" class="material-control-input" />
                                <span class="material-control-indicator"></span>
                                <span class="material-control-description"></span>
                            </label>
                        </div>
                        <div class="col">
                            <button class="btn btn-primary btn-all-to-work btn-lg mt-1" onclick="TZOrders.toWork()" disabled="true">В работу по отмеченным</button>
                        </div>
                    </div>
                @else
                    <p>По запросу ничего не найдено</p>
                @endif
            </div>
        @else
            <p>У вас пока нет заказов</p>
        @endif
        <br>
        <br>
        <br>
    </section>
    <script>
        const TZCheckBox = function(){
            // Private Function
            let params = {
                total: '.checkbox-total',
                checkbox: '.state-new',
                several: 'several-selected',
                action: '.btn-all-to-work',
            };
            let common = function(){
                //
            };
            let calculate = function(){
                let $total = $(params.total);
                let $action = $(params.action);
                let checked = $(params.checkbox+' input:checked').length;
                let count = $(params.checkbox+' input').length;
                if( checked===0 ){
                    $total.removeClass(params.several);
                    $(params.total+' input').prop('checked',false);
                    $action.prop('disabled',true);
                }
                else if( checked === count ){
                    $total.removeClass(params.several);
                    $(params.total+' input').prop('checked',true);
                    $action.prop('disabled',false);
                }
                else{
                    $total.addClass(params.several);
                    $(params.total+' input').prop('checked',false);
                    $action.prop('disabled',false);
                }
            };
            let one = function(ths){
                calculate();
            };
            let all = function(ths){
                let state = $(ths).find('input').prop('checked');
                $(params.total+' input').prop('checked',state);
                $(params.checkbox+' input').prop('checked',state);

                calculate();
            };
            return {
                // Public Function
                init: function () {
                    common();
                    // Other Init Method;
                }
                ,one : one
                ,all : all
            };
        }();
        ready(function(){
            TZCheckBox.init();
        });
    </script>
@endsection
