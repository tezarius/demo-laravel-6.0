<h2>История статусов</h2>

<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <td class="text-left">Дата</td>
            <td class="text-left">Статус</td>
            <td class="text-left">Заметка</td>
        </tr>
        </thead>
        <tbody>
        @foreach( $rows AS $row )
            <tr style="background-color:{{ Arr::get($row,'color_web') }};:{{ Arr::get($row,'color_font_web') }}">
                <td class="text-left">{{ date("d.m.Y H:i:s",strtotime(Arr::get($row,'data'))) }}</td>
                <td class="text-left">{{ Arr::get($row,'StateName') }}</td>
                <td class="text-left">{{ Arr::get($row,'note') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>