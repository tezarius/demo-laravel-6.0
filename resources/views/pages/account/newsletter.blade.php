@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="account-newsletter" class="container">
        <small>Мы тут специально оставили правый сайдбар, чтобы знать, что такая возможность есть</small>
        <div class="row">
            <div class="col-12 col-sm col-lg-3 order-lg-3 col-xl-mw260">
                @include('pages.account.pieces.aside')
            </div>
            <div id="content" class="col-12 col-lg order-lg-2 mb-3">
                <h1 class="h2 mb-4">Личный кабинет</h1>
                <form action="{{ route('newsletter') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <fieldset>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <table>
                                    @foreach( $notices AS $n )
                                    <tr>
                                        <td align="right">{{ $n->name }}:&emsp;</td>
                                        <td>
                                            <input type="checkbox" name="notified[]" value="{{ $n->id }}" {{ $n->isOnInform?'checked':'' }}
                                            data-toggle="toggle" data-on="ВКЛ" data-off="ВЫКЛ" data-onstyle="success" data-offstyle="danger"
                                            />
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </fieldset>
                    <div class="">
                        <a href="{{ route('account') }}" class="btn btn-secondary px-5">Назад</a>
                        <input type="submit" name="btnNotified" value="Продолжить" class="btn btn-primary px-4">
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
