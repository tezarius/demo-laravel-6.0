<aside id="column-right" class="column column-right">
    <div class="card mb-3">
        <div class="card-header">Личный кабинет</div>
        <div class="list-group list-group-flush">
            <a href="{{ route('account') }}" class="list-group-item list-group-item-action py-2">Моя информация</a>
            <a href="{{ route('accountEdit') }}" class="list-group-item list-group-item-action py-2">Изменить профиль</a>
            <a href="{{ route('password') }}" class="list-group-item list-group-item-action py-2">Изменить пароль</a>
            <a href="{{ route('wishlist') }}" class="list-group-item list-group-item-action py-2">Закладки</a>
            <a href="{{ route('orders') }}" class="list-group-item list-group-item-action py-2">История заказов</a>
            <a href="{{ route('reward') }}" class="list-group-item list-group-item-action py-2">Бонусные баллы</a>
            <a href="{{ route('balance') }}" class="list-group-item list-group-item-action py-2">Баланс</a>
            <a href="{{ route('newsletter') }}" class="list-group-item list-group-item-action py-2">E-Mail рассылка</a>
            <a href="{{ route('logout') }}" class="list-group-item list-group-item-action py-2">Выход</a>
        </div>
    </div>
</aside>