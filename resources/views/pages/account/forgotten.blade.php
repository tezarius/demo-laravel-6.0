@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="account-forgotten" class="container">
        <h1 class="h2 mb-4">Личный кабинет</h1>
        <p>Введите адрес электронной почты Вашей учетной записи. Нажмите кнопку продолжить, чтобы получить пароль по электронной почте.</p>
        <form action="{{ route('forgotten') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div>
                <h5>Ваш E-Mail</h5>
                <div class="form-group required d-inline-block">
                    <label for="input-email">E-Mail адрес</label>
                    <div class="input-group">
                        <input type="text" name="email" value="" placeholder="E-Mail адрес" id="input-email" class="form-control">
                        <div class="input-group-append">
                            <button type="submit" id="button-coupon" data-loading-text="Загрузка..." class="btn btn-primary" title="Продолжить">
                            <span class="si si-rem">
                                <svg fill="#fff" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"></path></svg>
                            </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <a href="{{ route('login') }}" class="btn btn-primary px-5">Назад</a>
                </div>
            </div>
        </form>
    </section>
@endsection
