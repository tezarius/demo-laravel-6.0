@extends('layouts.base')

@section('cssThisPage')
@endsection

@section('jsThisPage')
    <script src="/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="/js/balance.js?1"></script>
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
	<section id="account-balance" class="container">
		<h1 class="h2 mb-4">Информация о балансе</h1>
		@if( !empty($reports)  || $isButtonPressed )
			<form id="balance-filters" method="post" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="filter" class="filter-code" value="{{ $filter??'current' }}" />
				<div class="row">
					<div class="col-sm-12 col-md-8 col-lg-7">
						<div class="input-group">
							<div class="input-group-prepend">
								<button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="filter-name">{{ $filterName }}</span>
									<i class="icon icon-xs ion-2-chevron-down"></i>
								</button>
								<div class="dropdown-menu">
									<a class="dropdown-item" data-filter="current">Текущие</a>
									<a class="dropdown-item" data-filter="period">За период</a>
									<?php /*/?>
									<a class="dropdown-item" data-filter="number">По номеру</a>
									<div role="separator" class="dropdown-divider"></div>
									<a class="dropdown-item" data-filter="search">Мультипоиск</a>
									<?php //*/?>
								</div>
							</div>


							<input type="text" name="current" class="form-control current" aria-label="Текущие" value="Текущие транзакции"
								   style="display:{{ $filter=='current'?'inline-block':'none' }}" readonly
							/>


							<input type="text" name="period[dd1]" class="form-control period" aria-label="От"
								   id="period-dd1" data-target="#period-dd1" data-toggle="datetimepicker" autocomplete="off"
								   value="{{ $period?Arr::get($period,'dd1'):'' }}" style="display:{{ $filter=='period'?'inline-block':'none' }}"
							/>
							<input type="text" name="period[dd2]" class="form-control period" aria-label="До"
								   id="period-dd2" data-target="#period-dd2" data-toggle="datetimepicker" autocomplete="off"
								   value="{{ $period?Arr::get($period,'dd2'):'' }}" style="display:{{ $filter=='period'?'inline-block':'none' }}"
							/>


							<input type="text" name="number" class="form-control number" aria-label="По номеру" value="{{ $number }}"
								   style="display:{{ $filter=='number'?'inline-block':'none' }}"
							/>

							<input type="text" name="search" class="form-control search" aria-label="Мультипоиск" value="{{ $search }}"
								   style="display:{{ $filter=='search'?'inline-block':'none' }}"
							/>

							<div class="input-group-append">
								<button type="submit" name="btnFilterOut" class="btn btn-outline-secondary">Загрузить</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<br>
			<div class="table-grid">
				@if( !empty($reports) )
					@php( $receipt = $spending = 0 )
					@foreach( $reports AS $item )
						@php( $totalIn = Arr::get($item,'total_in',0) * 1 )
						@php( $totalOut = Arr::get($item,'total_out',0) * 1 )

						@php( $receipt += $totalIn )
						@php( $spending += $totalOut )
						<div id="row-order-id-{{ Arr::get($item,'id') }}" class="row row-root">

                            <div class="col-6 col-sm-3">№ {{ Arr::get($item,'payment_number') }}</div>
                            <div class="col-6 col-sm-3">{{ date('d.m.Y',strtotime(Arr::get($item,'payment_date'))) }}</div>
                            <div class="col-6 col-sm-3">
                                {{ Arr::get($item,'OperationName') }}
                                @if( $note = Arr::get($item,'note') )
                                    <br><small class="font-italic text-secondary">{{ $note }}</small>
                                @endif
                            </div>
                            <div class="col-6 col-sm-3">
                                @if( $totalIn )
                                    <span class="text-success">+{{ $totalIn }}</span>
                                @elseif( $totalOut )
                                    <span class="text-danger">-{{ $totalOut }}</span>
                                @else
                                    <span class="text-dark">*</span>
                                @endif
                            </div>
						</div>
					@endforeach
					<div class="mt-3">
						<div><strong>Приход: <span class="text-success">+{{ $receipt }}</span></strong></div>
						<div><strong>Расход: <span class="text-danger">-{{ $spending }}</span></strong></div>
					</div>
				@else
					<h4>По запросу ничего не найдено</h4>
				@endif
			</div>
		@else
			<h4>У вас пока нет транзакций</h4>
		@endif
		<br>
		<br>
		<br>
	</section>
@endsection
