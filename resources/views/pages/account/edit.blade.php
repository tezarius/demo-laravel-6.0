@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="account-edit" class="container">
        @if( $errors->any() )<div class="alert alert-danger alert-dismissible fade show" role="alert">
            @foreach( $errors->all() as $error )
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div>{{ $error }}</div>
            @endforeach</div>
        @endif
        <h1 class="h2 mb-4">Личный кабинет</h1>
        <form action="{{ route('accountEdit') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <fieldset>
                <legend>Ваша учетная запись</legend>

                <div class="form-row">

                    <div class="col-6">
                        <div class="form-group required">
                            <label for="input-firstname">Имя</label>
                            <input type="text" name="firstname" value="{{ old('firstname',Arr::get($user,'firstname')) }}" id="input-firstname" class="form-control @error('firstname') is-invalid @enderror">
                            @error('firstname')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group required">
                            <label for="input-lastname">Фамилия</label>
                            <input type="text" name="lastname" value="{{ old('lastname',Arr::get($user,'lastname')) }}" id="input-lastname" class="form-control @error('lastname') is-invalid @enderror">
                            @error('lastname')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group required">
                            <label for="input-patronymic">Отчество</label>
                            <input type="text" name="patronymic" value="{{ old('patronymic',Arr::get($user,'patronymic')) }}" id="input-patronymic" class="form-control @error('patronymic') is-invalid @enderror">
                            @error('patronymic')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group required">
                            <label for="input-email">E-Mail</label>
                            <input type="email" name="email" value="{{ old('email',Arr::get($user,'email')) }}" id="input-email" class="form-control @error('email') is-invalid @enderror">
                            @error('email')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group required">
                            <label for="input-telephone">Телефон</label>
                            <input type="tel" name="telephone" value="{{ old('telephone',Arr::get($user,'telephone')) }}" id="input-telephone" class="form-control @error('telephone') is-invalid @enderror">
                            @error('telephone')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                        </div>
                    </div>

                </div>
            </fieldset>
            <hr>
            <fieldset>
                <legend>Карточка организации <small>*(правка доступа только один раз)</small></legend>
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group required">
                            <label for="input-org_name">Организация</label>
                            <input type="text" name="org_name" placeholder="Организация" id="input-org_name" class="form-control {{ ( $errors->has('org_false')&&!old('org_name') )?'is-invalid':''}}"
                                   value="{{ old('org_name',Arr::get($user,'org_name')) }}"
                                    {{ Arr::get($user,'org_name')?' readonly':'' }}
                            />
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group required">
                            <label for="input-org_addr">Адрес</label>
                            <input type="text" name="org_addr" placeholder="Адрес" id="input-org_addr" class="form-control {{ ( $errors->has('org_false')&&!old('org_addr') )?'is-invalid':''}}"
                                   value="{{ old('org_addr',Arr::get($user,'org_addr')) }}"
                                    {{ Arr::get($user,'org_addr')?' readonly':'' }}
                            />
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-4">
                        <div class="form-group required">
                            <label for="input-org_inn">ИНН</label>
                            <input type="text" name="org_inn" placeholder="ИНН" id="input-org_inn" class="form-control {{ ( $errors->has('org_false')&&!old('org_inn') )?'is-invalid':''}}"
                                   value="{{ old('org_inn',Arr::get($user,'org_inn')) }}"
                                    {{ Arr::get($user,'org_inn')?' readonly':'' }}
                            />
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group required">
                            <label for="input-org_kpp">КРР</label>
                            <input type="text" name="org_kpp" placeholder="КРР" id="input-org_kpp" class="form-control {{ ( $errors->has('org_false')&&!old('org_kpp') )?'is-invalid':''}}"
                                   value="{{ old('org_kpp',Arr::get($user,'org_kpp')) }}"
                                    {{ Arr::get($user,'org_kpp')?' readonly':'' }}
                            />
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group required">
                            <label for="input-org_ogrn">ОГРН</label>
                            <input type="text" name="org_ogrn" placeholder="ОГРН" id="input-org_ogrn" class="form-control {{ ( $errors->has('org_false')&&!old('org_ogrn') )?'is-invalid':''}}"
                                   value="{{ old('org_ogrn',Arr::get($user,'org_ogrn')) }}"
                                    {{ Arr::get($user,'org_ogrn')?' readonly':'' }}
                            />
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    @error('org_false')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                </div>
            </fieldset>

            <div class="">
                <a href="{{ route('account') }}" class="btn btn-secondary px-5">Назад</a>
                <input type="submit" name="btnUserEdit" value="Продолжить" class="btn btn-primary px-4">
            </div>
        </form>
    </section>
@endsection
