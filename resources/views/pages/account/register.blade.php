@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="account-register" class="container">
        <h1 class="h2 mb-4">Личный кабинет</h1>
        <p>Если Вы уже зарегистрированы, перейдите на страницу <a href="{{ route('login') }}">входа в систему</a>.</p>
        <form action="{{ route('register') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <div id="account">
                <h5>Офис регистрации</h5>
                <p>
                    {{ Store::singleton()->getCurrentStockName() }}
                    (<a class="pointer dashed" onClick="$('#tzOfficeChooseModal').modal('show');">сменить офис</a>)
                </p>
                <hr>
                <h5>Контактные данные</h5>
                <div class="form-row">
                    <div class="col-12 col-sm-6">
                        <div class="form-group required">
                            <label for="input-firstname"><strong>Имя</strong></label>
                            <input type="text" name="firstname" value="{{ old('firstname') }}" placeholder="Имя" id="input-firstname" class="form-control @error('firstname') is-invalid @enderror">
                            @error('firstname')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group required">
                            <label for="input-lastname"><strong>Фамилия</strong></label>
                            <input type="text" name="lastname" value="{{ old('lastname') }}" placeholder="Фамилия" id="input-lastname" class="form-control @error('lastname') is-invalid @enderror">
                            @error('lastname')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group required">
                            <label for="input-email"><strong>E-Mail</strong></label>
                            <input type="email" name="email" value="{{ old('email') }}" placeholder="E-Mail" id="input-email" class="form-control @error('email') is-invalid @enderror">
                            @error('email')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group required">
                            <label for="input-telephone"><strong>Телефон</strong></label>
                            <input type="tel" name="telephone" value="{{ old('telephone') }}" placeholder="Телефон" id="input-telephone" class="form-control @error('telephone') is-invalid @enderror">
                            @error('telephone')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                        </div>
                    </div>
                </div>
            </div>
            <!--hr-->
            <div>
                <!--h5>Ваш пароль</h5-->
                <div class="form-row">
                    <div class="col-12 col-sm-6">
                        <div class="form-group required">
                            <label for="input-password"><strong>Пароль</strong></label>
                            <input type="password" name="password" value="{{ old('password') }}" placeholder="Пароль" id="input-password" class="form-control @error('password') is-invalid @enderror">
                            @error('password')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group required">
                            <label for="input-password_confirmation"><strong>Подтвердите пароль</strong></label>
                            <input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Подтвердите пароль" id="input-password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror">
                            @error('password_confirmation')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-row">
                <div class="col-12 col-md text-md-right mb-3 mb-md-0">
                    <div class="d-table w-100 h-100">
                        <div class="d-table-cell w-100 h-100 align-middle">
                            <div class="checkbox">
                                <label class="m-0">
                                    <span class="row no-gutters">
                                        <span class="col-auto order-md-2">
                                            <input type="checkbox" class="align-middle mr-2 mr-md-0 ml-md-2 @error('agree') is-invalid @enderror" name="agree" value="1" {{ (old('agree'))?'checked':'' }}>
                                        </span>
                                        <span class="col order-md-1">
                                            Я прочитал и согласен с условиями <a href="{{ route('offer') }}" class="!agree" target="_blank"><b>Политика безопасности</b></a>
                                        </span>
                                    </span>
                                </label>
                                @error('agree')<div class="text-danger small">{{ $message }} </div>@enderror
                                @error('StockID')<div class="text-danger small">{{ $message }} </div>@enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-auto">
                    <input type="submit" name="btnRegister" value="Продолжить" class="btn btn-block btn-primary px-4">
                </div>
            </div>
        </form>
    </section>
@endsection
