@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
    <script src="/js/items.js?1"></script>
@endsection

@section('title',$title)
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="goods-info" class="container" itemscope="" itemtype="http://schema.org/Product">
        <div class="row">
            <div id="content" class="col-12 col-lg order-lg-2">
                <h1 class="h2 mb-4" itemprop="name">{{ $heading }}</h1>
                <div class="metaproduct bg-light border rounded py-2 px-3 mb-4">
                    <div class="row py-1">
                        <div class="col-12 col-sm-6 col-lg-auto mb-1 mb-md-0" itemprop="brand" itemscope="" itemtype="http://schema.org/Brand">
                            <div class="mr-3">Производители <a href="#"><span itemprop="name">{{ $brand }}</span></a></div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-auto mb-1 mb-md-0">
                            <div class="mr-3">
                                Код Товара: <span>{{ $code }}</span>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-auto mb-1 mb-sm-0">
                            <div class="mr-3">Бонусные баллы: {{ (int)Arr::get($offer,'reward') }}</div>
                        </div>

                        <div class="col-12 col-sm-6 col-lg-auto ml-lg-auto">
                            @if( 1==2 && $reviewStatus )
                                <hr class="d-sm-none my-2">
                                <div class="row no-gutters">
                                    <div class="col-auto">
                                        <span class="si d-block float-left">
                                            <svg fill="#ddd" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"></path>
                                            </svg>
                                        </span>
                                        <span class="si d-block float-left">
                                            <svg fill="#ddd" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"></path>
                                            </svg>
                                        </span>
                                        <span class="si d-block float-left">
                                            <svg fill="#ddd" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"></path>
                                            </svg>
                                        </span>
                                        <span class="si d-block float-left">
                                            <svg fill="#ddd" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"></path>
                                            </svg>
                                        </span>
                                        <span class="si d-block float-left">
                                            <svg fill="#ddd" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"></path>
                                            </svg>
                                        </span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="ml-3">
                                            <a onclick="$('a[href=\'#tab-review\']').trigger('click');  $('html, body').animate({ scrollTop: $('a[href=\'#tab-review\']').offset().top - 5}, 250); return false;">
                                                0 отзывов
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-xl-9 col-lg-8 col-md-7 order-lg-1">
                        <div class="mb-4">
                            @if( $image or $images )
                                <div class="product-gallery position-relative">
                                    <div class="owl-carousel">
                                        @if( $image )
                                            <div class="swiper-slide item px-3" data-dot="<button><img width='100%' src='{{ Arr::get($image,'path') }}' alt='{{ Arr::get($image,'name') }} | 0-thumb'></button>">
                                                <meta itemprop="image" content="{{ Arr::get($image,'path') }}">
                                                <a href="{{ Arr::get($image,'path') }}" title="{{ $heading }}" target="_blank" onclick="psw_show(0);return false">
                                                    <div class="position-absolute lazy-spinner"><div class="spinner-border"></div></div>
                                                    <picture>
                                                        <img data-src="{{ Arr::get($image,'path') }}" title="{{ $heading }}" alt="{{ Arr::get($image,'name') }} | 0 | {{ $heading }}" height="200px" class="img-fluid d-block mx-auto w-auto owl-lazy">
                                                    </picture>
                                                </a>
                                            </div>
                                        @endif
                                        @if( $images )
                                            @php( $i = 1 )
                                            @foreach( $images as $img )
                                                <div class="swiper-slide item px-3" data-dot="<button><img width='100%' src='{{ Arr::get($img,'path') }}' alt='{{ Arr::get($img,'name') }} | {{$i}}-thumb'></button>">
                                                    <a href="{{ Arr::get($img,'path') }}" title="{{ $heading }}" target="_blank" onclick="psw_show({{ $i }});return false">
                                                        <div class="position-absolute lazy-spinner"><div class="spinner-border"></div></div>
                                                        <picture>
                                                            <img data-src="{{ Arr::get($img,'path') }}" title="{{ $heading }}" alt="{{ $heading }} | {{ $i }} | {{ $heading }}" height="200px" class="img-fluid d-block mx-auto w-auto owl-lazy">
                                                        </picture>
                                                    </a>
                                                </div>
                                                @php( $i += 1 )
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="product-gallery-pagination"><div class="owl-dots"></div></div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-12 col-xl-3 col-lg-4 col-md-5 order-lg-3">
                        <div class="card mb-3">
                            <div class="card-body p-3">
                                <div class="row form-row flex-nowrap">
                                    <div class="col" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">

                                        <!-- <meta itemprop="url" content="{{ url()->current() }}"> -->
                                        <link itemprop="url" href="{{ url()->current() }}">

                                        <meta itemprop="priceCurrency" content="RUB">
                                        <meta itemprop="priceValidUntil" content="2019-12-25">
                                        @if( Arr::get($offer,'rbg') )
                                            <link itemprop="availability" href="http://schema.org/InStock">
                                        @else
                                            <link itemprop="availability" href="https://schema.org/OutOfStock">
                                        @endif
                                        <div class="d-table w-100 h-100">
                                            <div class="d-table-cell w-100 h-100 align-middle">
                                                @if( Arr::get($offer,'special') )
                                                    <meta itemprop="price" content="{!! Arr::get($offer,'special') !!}">
                                                    <s class="text-danger">{!! Arr::get($offer,'price') !!}</s>
                                                    <div class="h2 m-0 text-nowrap">{!! Arr::get($offer,'special') !!}</div>
                                                @else
                                                    <meta itemprop="price" content="{!! Arr::get($offer,'price') !!}">
                                                    <div class="h2 m-0 text-nowrap">{!! Arr::get($offer,'price') !!}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto ml-auto">
                                        <div class="btn-group btn-group-sm ml-1 ml-sm-2">
                                            <?php /*/?>
                                            <button type="button" data-toggle="tooltip" class="btn btn-light" title="В сравнение" onclick="compare.add('43');">
                                                <svg fill="#aaa" class="d-block" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M10 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h5v2h2V1h-2v2zm0 15H5l5-6v6zm9-15h-5v2h5v13l-5-6v9h5c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z"></path></svg>
                                            </button>
                                            <?php //*/?>
                                            <button type="button" data-toggle="tooltip" class="btn btn-light" title="В закладки" onClick="alert('Добавить в избранное ?')">
                                                <svg fill="#aaa" class="d-block" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"></path></svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                @php( $qty = (int) Arr::get($offer,'qty') )
                                <div class="alert {{ $qty > 0 ? 'alert-success' : 'alert-danger' }} py-2 px-3 mt-3">
                                    {{  $qty > 0 ? "В наличии $qty шт" : 'Под заказ' }}
                                    @if( $qty > 0 && $stockName )
                                        <br>{{ $stockName }}
                                    @endif
                                </div>
                                <div id="product">
                                    <div class="row form-row mt-4">
                                        <div class="col">
                                            @php( $i = 7 )
                                            <div id="counter_{{$i}}"  class="input-group counter">
                                                <div class="input-group-prepend">
                                                    <button class="minus btn btn-light border" type="button" onclick="TZCounter.minus('#counter_{{$i}}',this);">
                                                        <i class="ion ion-2-minus"></i>
                                                    </button>
                                                </div>
                                                @php( $step = $offer->step!=null ? $offer->step : 1 )
                                                <input type="text" name="quantity[{{$i}}]" id="input-quantity" class="form-control border text-center counter"
                                                       value="{{ $step }}"
                                                       data-step="{{ $step }}"
                                                       data-min="{{ $step }}"
                                                       data-max="{{ $offer->qty*1 }}"
                                                       data-rbg="{{ $offer->rbg }}"
                                                       data-tzp="{{ $offer->tzp }}"
                                                       data-std="{{ $offer->std }}"
                                                       data-pld="{{ $offer->pld }}"
                                                       data-row-root="{{$i}}"
                                                       onInput="TZCounter.input('#counter_{{$i}}');"
                                                       size="1"
                                                />
                                                <div class="input-group-append">
                                                    <button class="plus btn btn-light border" type="button" onclick="TZCounter.plus('#counter_{{$i}}',this);">
                                                        <i class="ion ion-2-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="row no-gutters">
                                                <div class="col">
                                                    <button type="button" class="btn btn-danger btn-block" onclick="TZBasket.add('#counter_{{$i}}',this);">
                                                        <b class="btn-text btn-cart-text d-block">Купить</b>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                @if( Arr::get($offer,'step') > 1 )
                                    <div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ $textMinimum }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="">
                            <?php $noActive = TRUE; ?>
                            <ul class="nav nav-tabs mb-4">
                                @if( $settingsLocal and (Arr::get($settingsLocal,'name') or Arr::get($settingsLocal,'caption') ) )
                                    <?php if( $noActive ){ $noActive = FALSE; $class = 'active'; }else{ $class = ''; }?>
                                    <li class="nav-item">
                                        <a class="nav-link pl-sm-2 show {{ $class }}" href="#tab-description" data-toggle="tab">
                                            <span class="si si-rem"><svg fill="#ccc" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M14 2H6c-1.1 0-1.99.9-1.99 2L4 20c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zm2 16H8v-2h8v2zm0-4H8v-2h8v2zm-3-5V3.5L18.5 9H13z"></path></svg></span>
                                            <span class="d-none d-sm-inline ml-1">Описание</span>
                                        </a>
                                    </li>
                                @endif
                                @if( $settingsGlobal or ($settingsLocal and Arr::get($settingsLocal,'table') ) )
                                    <?php if( $noActive ){ $noActive = FALSE; $class = 'active'; }else{ $class = ''; }?>
                                    <li class="nav-item">
                                        <a class="nav-link pl-sm-2 show {{ $class }}" href="#tab-specification" data-toggle="tab">
                                            <span class="si si-rem"><svg fill="#ccc" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M3 9h14V7H3v2zm0 4h14v-2H3v2zm0 4h14v-2H3v2zm16 0h2v-2h-2v2zm0-10v2h2V7h-2zm0 6h2v-2h-2v2z"></path></svg></span>
                                            <span class="d-none d-sm-inline ml-1">Характеристики</span>
                                        </a>
                                    </li>
                                @endif
                                @if( $applyGlobal )
                                    <?php if( $noActive ){ $noActive = FALSE; $class = 'active'; }else{ $class = ''; }?>
                                    <li class="nav-item">
                                        <a class="nav-link pl-sm-2 show {{ $class }}" href="#tab-applicability" data-toggle="tab">
                                            <i class="fa fa-swift"></i>
                                            <span class="d-none d-sm-inline ml-1">Применяемость</span>
                                        </a>
                                    </li>
                                @endif
                                @if( $reviewStatus )
                                    <?php if( $noActive ){ $noActive = FALSE; $class = 'active'; }else{ $class = ''; }?>
                                    <li class="nav-item">
                                        <a class="nav-link pl-sm-2 show {{ $class }}" href="#tab-review" data-toggle="tab">
                                            <span class="si si-rem"><svg fill="#ccc" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18zM18 14H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z"></path></svg></span>
                                            <span class="d-none d-sm-inline ml-1">{{ $tabReview }}</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                            <?php $noActive = TRUE; ?>
                            <div class="tab-content">
                                @if( $settingsLocal and (Arr::get($settingsLocal,'name') or Arr::get($settingsLocal,'caption') ) )
                                    <?php if( $noActive ){ $noActive = FALSE; $class = 'active'; }else{ $class = ''; }?>
                                    <div class="tab-pane show {{ $class }}" id="tab-description" itemprop="description">
                                        <div class="row">
                                            <div class="col">
                                                @if( Arr::get($settingsLocal,'name') ) <p>{{ Arr::get($settingsLocal,'name') }}</p> @endif
                                                @if( Arr::get($settingsLocal,'caption') ) <p>{{ Arr::get($settingsLocal,'caption') }}</p> @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if( $settingsGlobal or ($settingsLocal and Arr::get($settingsLocal,'table') ) )
                                    <?php if( $noActive ){ $noActive = FALSE; $class = 'active'; }else{ $class = ''; }?>
                                    <div class="tab-pane show {{ $class }}" id="tab-specification">
                                        @if( $settingsLocal )
                                            <!--h3>Информация от партнеров</h3-->
                                            <table class="table table-bordered">
                                                <tbody>
                                                @foreach( Arr::get($settingsLocal,'table') as $setting )
                                                    <tr>
                                                        <td>{{ Arr::get($setting,'name') }}</td>
                                                        <td>{{ Arr::get($setting,'value') }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @endif
                                        @if( $settingsGlobal )
                                            <h3>Информация из справочников</h3>
                                            <table class="table table-bordered">
                                                <tbody>
                                                @foreach( $settingsGlobal as $setting )
                                                    <tr>
                                                        <td>{{ Arr::get($setting,'criteriaDescription') }}</td>
                                                        <td>{{ Arr::get($setting,'val') }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @endif
                                    </div>
                                @endif
                                @if( $applyGlobal )
                                    <?php if( $noActive ){ $noActive = FALSE; $class = 'active'; }else{ $class = ''; }?>
                                    <div class="tab-pane show {{ $class }}" id="tab-applicability">
                                        <div class="alert alert-info alert-dismissible">
                                            <i class="fa fa-info-circle"></i>
                                            Выберите марку и\или модель автомобиля
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            @foreach( Arr::get($applyGlobalFilter,'marks') as $key => $value )
                                                <button type="button" class="btn btn-secondary" data-code="{{ $key }}" onClick="TZItems.apply.filter.make(this)">{{ $value }}</button>
                                            @endforeach
                                        </div>
                                        <hr>
                                        @foreach( Arr::get($applyGlobalFilter,'models') as $makeCode => $models )
                                            <div id="apply-filter-models-{{ $makeCode }}" class="btn-group apply-filter-models" role="group" style="display:none">
                                                @foreach( $models as $key => $value )
                                                    <button type="button" class="btn btn-secondary" data-code="{{ $key }}" onClick="TZItems.apply.filter.model(this)">{{ $value }}</button>
                                                @endforeach
                                            </div>
                                        @endforeach
                                        <hr>
                                        <table id="applicability" class="table table-bordered">
                                            <thead style="display:none">
                                            <tr><th>Марка</th><th>Модель</th><th>Год выпуска</th><th>Спецификация</th></tr>
                                            </thead>
                                            <tbody>
                                            @foreach( $applyGlobal as $apply )
                                                <tr data-make-code="{{ Arr::get($apply,'makeCode') }}" data-model-code="{{ Arr::get($apply,'modelCode') }}" style="display:none">
                                                    <td>{{ Arr::get($apply,'makeCode') }}</td>
                                                    <td>{{ Arr::get($apply,'model') }} [{{ Arr::get($apply,'description') }}] {{ Arr::get($apply,'constructionType') }}</td>
                                                    <td>{{ Arr::get($apply,'constructioninterval') }}</td>
                                                    <td>{{ Arr::get($apply,'hp') }} \ {{ Arr::get($apply,'cylinderCapacityCcm') }} \ {{ Arr::get($apply,'motorType') }} </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                                @if( $reviewStatus )
                                    <?php if( $noActive ){ $noActive = FALSE; $class = 'active'; }else{ $class = ''; }?>
                                    <div class="tab-pane show {{ $class }}" id="tab-review">
                                        <button type="button" class="btn btn-lg btn-block border btn-light mb-3 text-muted" data-toggle="collapse" data-target="#collapse-review">
                                            Написать отзыв
                                        </button>
                                        <div class="collapse" id="collapse-review">
                                            <div class="card mb-3">
                                                <div class="card-header">
                                                    <h6 class="card-title m-0">
                                                        <span class="si si-rem mr-1">
                                                            <svg fill="#aaa" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18zM18 14H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z"></path>
                                                            </svg>
                                                        </span>
                                                        Написать отзыв
                                                    </h6>
                                                </div>
                                                <div class="card-body bg-light">
                                                    <form class="form-horizontal" id="form-review">
                                                        <div class="form-group required">
                                                            <input type="text" name="name" value="Роман&nbsp;Трошков" id="input-name" class="form-control" placeholder="Ваше имя *">
                                                        </div>
                                                        <div class="form-group required">
                                                            <textarea name="text" rows="4" id="input-review" class="form-control" placeholder="Ваш отзыв *"></textarea>
                                                            <div class="help-block d-none"><span style="color: #FF0000;">Примечание:</span> HTML разметка не поддерживается! Используйте обычный текст.</div>
                                                        </div>
                                                        <div class="form-group required">
                                                            <label class="d-inline-block align-middle mb-0 mr-1 control-label">Рейтинг</label>
                                                            <div class="prod-rat d-inline-block clearfix align-middle">
                                                                <input id="rat1" type="radio" name="rating" value="1">
                                                                <label class="rat-star d-block float-left m-0" for="rat1">
                                                                    <svg fill="#ddd" class="d-block" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path></svg>
                                                                </label>
                                                                <input id="rat2" type="radio" name="rating" value="2">
                                                                <label class="rat-star d-block float-left m-0" for="rat2">
                                                                    <svg fill="#ddd" class="d-block" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path></svg>
                                                                </label>
                                                                <input id="rat3" type="radio" name="rating" value="3">
                                                                <label class="rat-star d-block float-left m-0" for="rat3">
                                                                    <svg fill="#ddd" class="d-block" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path></svg>
                                                                </label>
                                                                <input id="rat4" type="radio" name="rating" value="4">
                                                                <label class="rat-star d-block float-left m-0" for="rat4">
                                                                    <svg fill="#ddd" class="d-block" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path></svg>
                                                                </label>
                                                                <input id="rat5" type="radio" name="rating" value="5">
                                                                <label class="rat-star d-block float-left m-0" for="rat5">
                                                                    <svg fill="#ddd" class="d-block" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path></svg>
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="">

                                                            <button type="button" id="button-review" data-loading-text="Загрузка..." class="btn btn-primary pr-4">
                                                                    <span class="si si-rem mr-1">
                                                                        <svg fill="#fff" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                            <path d="M2.01 21L23 12 2.01 3 2 10l15 2-15 2z"></path>
                                                                        </svg>
                                                                    </span>
                                                                Продолжить
                                                            </button>

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="review"><p class="text-muted">Нет отзывов об этом товаре.</p></div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>

        function psw_show(index_start) {

            if ($('.pswp').length < 1) {
                pswp_c  = '<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">'
                pswp_c += '	<div class="pswp__bg"></div>'
                pswp_c += '	<div class="pswp__scroll-wrap">'
                pswp_c += '		<div class="pswp__container">'
                pswp_c += '			<div class="pswp__item"></div>'
                pswp_c += '			<div class="pswp__item"></div>'
                pswp_c += '			<div class="pswp__item"></div>'
                pswp_c += '		</div>'
                pswp_c += '		<div class="pswp__ui pswp__ui--hidden">'
                pswp_c += '			<div class="pswp__top-bar">'
                pswp_c += '				<div class="pswp__counter"></div>'
                pswp_c += '				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>'
                pswp_c += '				<button class="pswp__button pswp__button--fs" title="Fullscreen"></button>'
                pswp_c += '				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>'
                pswp_c += '				<div class="pswp__preloader">'
                pswp_c += '					<div class="pswp__preloader__icn">'
                pswp_c += '						<div class="pswp__preloader__cut">'
                pswp_c += '							<div class="pswp__preloader__donut"></div>'
                pswp_c += '						</div>'
                pswp_c += '					</div>'
                pswp_c += '				</div>'
                pswp_c += '			</div>'
                pswp_c += '			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">'
                pswp_c += '				<div class="pswp__share-tooltip"></div>'
                pswp_c += '			</div>'
                pswp_c += '			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>'
                pswp_c += '			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>'
                pswp_c += '			<div class="pswp__caption">'
                pswp_c += '				<div class="pswp__caption__center"></div>'
                pswp_c += '			</div>'
                pswp_c += '		</div>'
                pswp_c += '	</div>'
                pswp_c += '</div>';
                $('body').append(pswp_c);
            }

            var pswpElement = document.querySelectorAll('.pswp')[0];

            var items = [
                @if( $image or $images )
                    @if( $image )
                    {
                        src: '{{ Arr::get($image,'path') }}',
                        w: '{{ $popupWidth }}',
                        h: '{{ $popupHeight }}',
                        t: 'image'
                    },
                    @endif
                    @if( $images )
                        @foreach( $images AS $img )
                        {
                            src: '{{ Arr::get($img,'path') }}',
                            w: '{{ $popupWidth }}',
                            h: '{{ $popupHeight }}',
                            t: 'images'
                        },
                        @endforeach
                    @endif
                @endif
            ];

            var options = {
                index: index_start
            };

            function psw_construct() {

                var psw_gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);

                psw_gallery.init();

                $('body').addClass('psw-open');

                psw_gallery.listen('destroy', function() {
                    setTimeout(function () {
                        $('body').removeClass('psw-open');
                    }, 100);
                });
            }

            if( typeof PhotoSwipe != 'function' ) alert('photo swipe notfound, execute: npm i && npm run production');
            else psw_construct();
        }
        function startSwp() {

            var owl_product_gallery = $('.product-gallery .owl-carousel');

            owl_product_gallery.owlCarousel({
                loop: false,
                margin: 0,
                items: 1,
                nav: false,
                lazyLoad: true,
                dots: true,
                dotsEach: 2,
                dotsContainer: '.product-gallery .product-gallery-pagination .owl-dots',
                autoplay: false,
                dotData: true,
                dotsData: true
            });

            owl_product_gallery.on('loaded.owl.lazy',function(e){
                e.element.parent().parent().find('.lazy-spinner').remove();
            });

            @if( count($images) > $addImagesLimit )
                $('.product-gallery .product-gallery-pagination').append('<div onclick="psw_show({{ $addImagesLimit }});return false" class="more-imgs">Еще {{ count($images) - $addImagesLimit }}</div>');
                $('head').append('<style>.product-gallery .product-gallery-pagination .owl-dot:nth-child(n+{{ $addImagesLimit + 1 }}) {display:none!important}}</style>');
            @endif
        }
        function startRelSwp() {

            var owl_related_products = $('#related-products .owl-carousel');

            owl_related_products.owlCarousel({
                loop: false,
                margin: 0,
                lazyLoad: true,
                responsive:{
                    0:		{items: 2},
                    480:	{items: 3},
                    768:	{items: 4},
                    992:	{items: 5}
                },
                nav: false,
                dots: false
            });

            owl_related_products.on('loaded.owl.lazy',function(e){
                e.element.parent().parent().find('.lazy-spinner').remove();
            });

            $('#related-products .button-prev').click(function() { owl_related_products.trigger('prev.owl.carousel'); });
            $('#related-products .button-next').click(function() { owl_related_products.trigger('next.owl.carousel'); });

        }

        ready(function (){

            $('.rat-star').hover(function(){
                $(this).prevAll('.rat-star').addClass('active');
                $(this).addClass('active');
            },function(){
                $(this).prevAll('.rat-star').removeClass('active');
                $(this).removeClass('active');
            });
            $('.rat-star').click(function(){
                $('.rat-star').each(function(){
                    $(this).removeClass('checked');
                    $(this).prevAll('.rat-star').removeClass('checked');
                });
                $(this).addClass('checked');
                $(this).prevAll('.rat-star').addClass('checked');
            });

            startSwp();
            startRelSwp();
        });
    </script>
@endsection
