@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="account-garage" class="container">
        <h1 class="h2 mb-4">Личный кабинет</h1>
        <div class="row">
            @foreach( $cars AS $car )
                <div class="col-md-6 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <a class="btn btn-primary mr-2" aria-label="Дерево узлов" target="_blank"
                               href="{{ route('amTree',['hash'=>Arr::encryption(['type'=>$car->CarTypeOut,'car'=>$car->id_out,'garage'=>1])]) }}"
                            >
                                <i class="fa fa-search text-white"></i>
                            </a>
                            <span>{{ $car->name }}</span>
                        </div>
                        <div class="card-body">
                            <div><b>VIN:</b> {{ $car->vin }}</div>
                            <div><b>Год:</b>  {{ $car->year }}</div>
                            <br>
                            <div class="text-secondary">{{ $car->note }}</div>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection
