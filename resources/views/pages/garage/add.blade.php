@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu,'breads'=>$breads,'flashers'=>$flashers,])

@section('cssThisPage')
@endsection

@section('jsThisPage')
    <script src="/js/garage.js?1"></script>
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
    <section id="account-garage-add" class="container">
        <h1 class="h2 mb-4">Личный кабинет</h1>
        <form action="{{ route('garageAdd') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <fieldset class="mb-5">
                <legend class="border-bottom pb-1">Выбор по каталогу</legend>
                <div class="row form-group required">
                    <label class="col-sm-3 col-form-label text-right" for="input-type">Тип</label>
                    <div class="col-sm-9">
                        <select name="type" id="input-type" class="form-control" onChange="TZGarage.marks(this)" data-uri="{{ route('amMarks') }}">
                            <option>Выберите тип авто</option>
                            @foreach( $types AS $t )
                            <option value="{{ Arr::get($t,'code') }} {{ Arr::get($t,'code')==$type?'selected':'' }}">{{ Arr::get($t,'name') }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row form-group required">
                    <label class="col-sm-3 col-form-label text-right" for="input-mark">Марка</label>
                    <div class="col-sm-9">
                        <select name="mark" id="input-mark" class="form-control" onChange="TZGarage.models(this)" data-uri="{{ route('amModels') }}" disabled>
                            <option>Выберите марку</option>
                        </select>
                    </div>
                </div>
                <div class="row form-group required">
                    <label class="col-sm-3 col-form-label text-right" for="input-model">Модель</label>
                    <div class="col-sm-9">
                        <select name="model" id="input-model" class="form-control" onChange="TZGarage.vehicles(this)" data-uri="{{ route('amVehicles') }}" disabled>
                            <option>Выберите модель</option>
                        </select>
                    </div>
                </div>
                <div class="row orm-group required">
                    <label class="col-sm-3 col-form-label text-right" for="input-vehicle">Модификация</label>
                    <div class="col-sm-9">
                        <select name="vehicle" id="input-vehicle" class="form-control" onChange="TZGarage.auto(this)" disabled>
                            <option>Выберите модификацию</option>
                        </select>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend class="border-bottom pb-1">Карточка авто/мото</legend>
                <div class="row form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9 required">
                        <label class="col-form-label" for="input-auto">Наименование авто/техники</label>
                        <input type="text" name="auto" value="{{ $auto }}" placeholder="Можно будет редактировать" id="input-auto" class="form-control @error('auto') is-invalid @enderror" minlength="3" disabled required/>
                        @error('auto')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="col-form-label" for="input-engine">Двигатель</label>
                                <input type="text" name="engine" value="{{ $engine }}" placeholder="Объем двигателя в см3" id="input-engine" class="form-control" disabled/>
                            </div>
                            <div class="col-sm-6 required">
                                <label class="col-form-label" for="input-year">Год</label>
                                <input type="number" name="year" value="{{ $year }}" placeholder="Год выпуска" id="input-year"
                                       min="{{ $year_min }}" max="{{ $year_max }}" disabled
                                       class="form-control
                                            @error('year') is-invalid @enderror
                                            @error('year_min') is-invalid @enderror
                                            @error('year_max') is-invalid @enderror"
                                       required/>
                                @error('year')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                                @error('year_min')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                                @error('year_max')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9 required">
                        <label class="col-form-label" for="input-vin">VIN / Номер кузова</label>
                        <input type="text" name="vin" value="{{ $vin }}" placeholder="Если не определился, нужно ввести" id="input-vin" class="form-control @error('vin') is-invalid @enderror" maxlength="17"
                               onKeyUp="TZGarage.vcHintText(this)"
                               onPaste="setTimeout(function(){ TZGarage.vcHintText(this); }.bind(this),1);"
                               onFocus="TZGarage.vcHintShow(this)"
                               onBlur="TZGarage.vcHintHide(this)"
                               disabled required
                        />
                        @error('vin')<div class="invalid-feedback" role="alert">{{ $message }} </div>@enderror
                        <div class="text-success vin-code" style="display:none">Введено символов: 0</div>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-sm-3" for="input-note">Комментарий</label>
                    <div class="col-sm-9">
                        <textarea type="note" name="note" placeholder="Введите при необходимости" id="input-note" class="form-control" rows="7">{{ $note }}</textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9 required">
                        <strong class="required">Поля, помеченные <label class="col-form-label"></label>, обязательные к заполнению</strong>
                    </div>
                </div>
            </fieldset>

            <div class="buttons">
                <div class="pull-right">
                    <input type="submit" id="btnGarageAdd" name="btnGarageAdd" value="Сохранить" class="btn btn-primary" disabled/>
                </div>
            </div>
        </form>
    </section>
@endsection
