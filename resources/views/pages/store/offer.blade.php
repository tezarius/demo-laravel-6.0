@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
	<section id="store-offer" class="container">
		{!! $content !!}
	</section>
@endsection
