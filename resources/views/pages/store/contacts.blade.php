@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
	<section id="store-contacts" class="container">
		@if( $content )
		    <div>{!! $content !!}</div>
		@endif
		@if( $map )
		    <div>{!! $map !!}</div>
		@endif
	</section>
@endsection
