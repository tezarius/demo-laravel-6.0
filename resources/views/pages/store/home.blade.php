@extends('layouts.base',['openedLeftMenu'=>$openedLeftMenu])

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
	<section id="common-home" class="container">
		<div id="content">
			<div class="row flex-nowrap">

				<?php /*/?>
				@include('includes.pages.left-sidebar',['openedLeftMenu'=>$openedLeftMenu])
				<?php //*/?>
				<div class="d-none d-lg-block col-lg-3 col-xl-mw260 order-1">
					@if( $openedLeftMenu>0 )
						<div class="menu-holder d-none d-lg-block mb-3"><!-- LEFT MENU CONTENT --></div>
					@endif
					<!-- NEXT SIDEBAR CONTENT -->
				</div>

				<div class="col-12 col-lg order-2">
					<div class="content-top">
						@if( !empty($sliders) )
							@include('includes.pages.slider',['module' => '0', 'sliders' => $sliders, 'links' => $links])
						@endif
						@include('pages.store.home.blocks-'.Store::singleton()->getType())
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">

				</div>
			</div>

            @include('includes.pages.recommended',['module' => 'offers'])

			@if( count($lastNews)>0 )
				<div id="news" class="row mt-5 mb-5">
					<div class="col-12 mb-2">
						<h4 class="m-0">Новости</h4>
					</div>
					@foreach( $lastNews AS $n )
						<div class="col-12 col-sm-4 mb-2">
							<hr class="d-show">
							<small class="mb-2 d-inline-block text-secondary"><i class="fa fa-calendar"></i>&ensp;{{ date('d.m.Y',strtotime(Arr::get($n,'data_start'))) }}</small>
							<h6>{{ Arr::get($n,'subject') }}</h6>
							<div class="preview">{!! Arr::get($n,'body') !!}</div>
							<a href="{{ route('news',['id'=>Arr::get($n,'id')]) }}">читать далее...</a>
						</div>
					@endforeach
				</div>
			@endif

			<?php /*/ ?>
			<div class="card box-products product-items mb-3">
				<div class="card-header px-3">
					<h6 class="card-title m-0">
						Рекомендуем
					</h6>
				</div>
				<div class="card-body p-0">
					<div class="products-wrapper">
						<div class="row no-gutters">
							<div class="col-6 col-sm-4 col-lg-3 product-col">
								<div class="card h-100 border-0 rounded-0 mx-auto">
									<div class="image position-relative px-2 px-sm-3 pt-2 pb-2">
										<a href="http://demo.tezarius.lo/index.php?route=product/product&amp;product_id=43">
											<img src="./img/demo/macbook_1-200x200.jpg" alt="MacBook" title="MacBook" class="img-fluid d-block mx-auto">
										</a>
									</div>
									<div class="card-body px-2 px-sm-3 pt-2 pb-0">
										<div class="caption">
											<h6 class="card-title"><a href="http://demo.tezarius.lo/index.php?route=product/product&amp;product_id=43">MacBook</a></h6>
										</div>
									</div>
									<div class="card-footer px-2 px-sm-3 pb-2 pb-sm-3 pt-0 bg-white border-0">
										<div class="price">
											<div class="h6 mb-1">500.00р.</div>
											<small class="font-weight-light text-muted d-block">Без НДС: 500.00р.</small>
										</div>
										<div class="btn-group btn-group-cart-add mt-2">
											<button type="button" class="btn btn-light btn-cart-add px-1" onclick="cart.add('43');" aria-label="Купить">
												<span class="si si-rem">
													<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
														<path d="M17.21 9l-4.38-6.56c-.19-.28-.51-.42-.83-.42-.32 0-.64.14-.83.43L6.79 9H2c-.55 0-1 .45-1 1 0 .09.01.18.04.27l2.54 9.27c.23.84 1 1.46 1.92 1.46h13c.92 0 1.69-.62 1.93-1.46l2.54-9.27L23 10c0-.55-.45-1-1-1h-4.79zM9 9l3-4.4L15 9H9zm3 8c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"></path>
													</svg>
												</span>
												<span class="ml-1 mr-2 d-none d-md-inline text-muted">Купить</span>
											</button>
											<button type="button" class="btn btn-light btn-cart-add px-1" title="Быстрый заказ" onclick="tz_fastorder('43')">
												<span class="si si-rem">
													<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
														<path d="M15 1H9v2h6V1zm-4 13h2V8h-2v6zm8.03-6.61l1.42-1.42c-.43-.51-.9-.99-1.41-1.41l-1.42 1.42C16.07 4.74 14.12 4 12 4c-4.97 0-9 4.03-9 9s4.02 9 9 9 9-4.03 9-9c0-2.12-.74-4.07-1.97-5.61zM12 20c-3.87 0-7-3.13-7-7s3.13-7 7-7 7 3.13 7 7-3.13 7-7 7z"></path>
													</svg>
												</span>
											</button>
											<button type="button" class="btn btn-light btn-cart-add px-1" title="Быстрый просмотр" onclick="tz_qview('43')">
												<span class="si si-rem">
													<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
														<path d="M12,6.5c3.79,0,7.17,2.13,8.82,5.5c-1.65,3.37-5.02,5.5-8.82,5.5S4.83,15.37,3.18,12C4.83,8.63,8.21,6.5,12,6.5 M12,4.5 C7,4.5,2.73,7.61,1,12c1.73,4.39,6,7.5,11,7.5s9.27-3.11,11-7.5C21.27,7.61,17,4.5,12,4.5L12,4.5z"></path><path d="M12,9.5c1.38,0,2.5,1.12,2.5,2.5s-1.12,2.5-2.5,2.5S9.5,13.38,9.5,12S10.62,9.5,12,9.5 M12,7.5c-2.48,0-4.5,2.02-4.5,4.5 s2.02,4.5,4.5,4.5s4.5-2.02,4.5-4.5S14.48,7.5,12,7.5L12,7.5z"></path>
													</svg>
												</span>
											</button>
											<div class="btn-group position-static">
												<div class="dropdown-menu">
													<a class="dropdown-item pl-2 pr-3" onclick="wishlist.add('43');return false">
														<span class="si si-rem mr-1">
															<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																<path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"></path>
															</svg>
														</span>
														В закладки
													</a>
													<a class="dropdown-item pl-2 pr-3" onclick="compare.add('43');return false">
														<span class="si si-rem mr-1">
															<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																<path d="M10 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h5v2h2V1h-2v2zm0 15H5l5-6v6zm9-15h-5v2h5v13l-5-6v9h5c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z"></path>
															</svg>
														</span>
														В сравнение
													</a>
												</div>
												<button type="button" class="btn btn-light btn-cart-add px-0" data-toggle="dropdown" data-flip="false">
													<span class="si si-rem">
														<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
															<path d="M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"></path>
														</svg>
													</span>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-6 col-sm-4 col-lg-3 product-col">
								<div class="card h-100 border-0 rounded-0 mx-auto">
									<div class="image position-relative px-2 px-sm-3 pt-2 pb-2">
										<a href="http://demo.tezarius.lo/index.php?route=product/product&amp;product_id=40">
											<img src="./img/demo/iphone_1-200x200.jpg" alt="iPhone" title="iPhone" class="img-fluid d-block mx-auto">
										</a>
									</div>
									<div class="card-body px-2 px-sm-3 pt-2 pb-0">
										<div class="caption">
											<h6 class="card-title"><a href="http://demo.tezarius.lo/index.php?route=product/product&amp;product_id=40">iPhone</a></h6>
										</div>
									</div>
									<div class="card-footer px-2 px-sm-3 pb-2 pb-sm-3 pt-0 bg-white border-0">
										<div class="price">
											<div class="h6 mb-1">101.00р.</div>
											<small class="font-weight-light text-muted d-block">Без НДС: 101.00р.</small>
										</div>
										<div class="btn-group btn-group-cart-add mt-2">
											<button type="button" class="btn btn-light btn-cart-add px-1" onclick="cart.add('40');" aria-label="Купить">
												<span class="si si-rem">
													<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
														<path d="M17.21 9l-4.38-6.56c-.19-.28-.51-.42-.83-.42-.32 0-.64.14-.83.43L6.79 9H2c-.55 0-1 .45-1 1 0 .09.01.18.04.27l2.54 9.27c.23.84 1 1.46 1.92 1.46h13c.92 0 1.69-.62 1.93-1.46l2.54-9.27L23 10c0-.55-.45-1-1-1h-4.79zM9 9l3-4.4L15 9H9zm3 8c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"></path>
													</svg>
												</span>
												<span class="ml-1 mr-2 d-none d-md-inline text-muted">Купить</span>
											</button>
											<div class="btn-group position-static">
												<div class="dropdown-menu">
													<a class="dropdown-item pl-2 pr-3" onclick="tz_fastorder('40');return false">
														<span class="si si-rem">
															<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																<path d="M15 1H9v2h6V1zm-4 13h2V8h-2v6zm8.03-6.61l1.42-1.42c-.43-.51-.9-.99-1.41-1.41l-1.42 1.42C16.07 4.74 14.12 4 12 4c-4.97 0-9 4.03-9 9s4.02 9 9 9 9-4.03 9-9c0-2.12-.74-4.07-1.97-5.61zM12 20c-3.87 0-7-3.13-7-7s3.13-7 7-7 7 3.13 7 7-3.13 7-7 7z"></path>
															</svg>
														</span>
														Быстрый заказа
													</a>
													<a class="dropdown-item pl-2 pr-3" onclick="tz_qview('40');return false">
														<span class="si si-rem">
															<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																<path d="M12,6.5c3.79,0,7.17,2.13,8.82,5.5c-1.65,3.37-5.02,5.5-8.82,5.5S4.83,15.37,3.18,12C4.83,8.63,8.21,6.5,12,6.5 M12,4.5 C7,4.5,2.73,7.61,1,12c1.73,4.39,6,7.5,11,7.5s9.27-3.11,11-7.5C21.27,7.61,17,4.5,12,4.5L12,4.5z"></path><path d="M12,9.5c1.38,0,2.5,1.12,2.5,2.5s-1.12,2.5-2.5,2.5S9.5,13.38,9.5,12S10.62,9.5,12,9.5 M12,7.5c-2.48,0-4.5,2.02-4.5,4.5 s2.02,4.5,4.5,4.5s4.5-2.02,4.5-4.5S14.48,7.5,12,7.5L12,7.5z"></path>
															</svg>
														</span>
														Быстрый просмотр
													</a>
													<a class="dropdown-item pl-2 pr-3" onclick="wishlist.add('40');return false">
														<span class="si si-rem mr-1">
															<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																<path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"></path>
															</svg>
														</span>
														В закладки
													</a>
													<a class="dropdown-item pl-2 pr-3" onclick="compare.add('40');return false">
														<span class="si si-rem mr-1">
															<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																<path d="M10 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h5v2h2V1h-2v2zm0 15H5l5-6v6zm9-15h-5v2h5v13l-5-6v9h5c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z"></path>
															</svg>
														</span>
														В сравнение
													</a>
												</div>
												<button type="button" class="btn btn-light btn-cart-add px-0" data-toggle="dropdown" data-flip="false">
													<span class="si si-rem">
														<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
															<path d="M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"></path>
														</svg>
													</span>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-6 col-sm-4 col-lg-3 product-col">
								<div class="card h-100 border-0 rounded-0 mx-auto">
									<div class="image position-relative px-2 px-sm-3 pt-2 pb-2">
										<a href="http://demo.tezarius.lo/index.php?route=product/product&amp;product_id=42">
											<img src="./img/demo/apple_cinema_30-200x200.jpg" alt="Apple Cinema 30&quot;" title="Apple Cinema 30&quot;" class="img-fluid d-block mx-auto">
										</a>
									</div>
									<div class="card-body px-2 px-sm-3 pt-2 pb-0">
										<div class="caption">
											<h6 class="card-title"><a href="http://demo.tezarius.lo/index.php?route=product/product&amp;product_id=42">Apple Cinema 30"</a></h6>
										</div>
									</div>
									<div class="card-footer px-2 px-sm-3 pb-2 pb-sm-3 pt-0 bg-white border-0">
										<div class="price">
											<div class="h6 mb-1">
												<s class="d-inline-block text-danger font-weight-light">100.00р.</s>
												<span class="d-inline-block">90.00р.</span>
											</div>
											<small class="font-weight-light text-muted d-block">Без НДС: 90.00р.</small>
										</div>
										<div class="btn-group btn-group-cart-add mt-2">
											<button type="button" class="btn btn-light btn-cart-add px-1" onclick="cart.add('42');" aria-label="Купить">
															<span class="si si-rem">
																<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																	<path d="M17.21 9l-4.38-6.56c-.19-.28-.51-.42-.83-.42-.32 0-.64.14-.83.43L6.79 9H2c-.55 0-1 .45-1 1 0 .09.01.18.04.27l2.54 9.27c.23.84 1 1.46 1.92 1.46h13c.92 0 1.69-.62 1.93-1.46l2.54-9.27L23 10c0-.55-.45-1-1-1h-4.79zM9 9l3-4.4L15 9H9zm3 8c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"></path>
																</svg>
															</span>
												<span class="ml-1 mr-2 d-none d-md-inline text-muted">Купить</span>
											</button>
											<button type="button" class="btn btn-light btn-cart-add px-1" title="Быстрый заказ" onclick="tz_fastorder('42')">
															<span class="si si-rem">
																<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																	<path d="M15 1H9v2h6V1zm-4 13h2V8h-2v6zm8.03-6.61l1.42-1.42c-.43-.51-.9-.99-1.41-1.41l-1.42 1.42C16.07 4.74 14.12 4 12 4c-4.97 0-9 4.03-9 9s4.02 9 9 9 9-4.03 9-9c0-2.12-.74-4.07-1.97-5.61zM12 20c-3.87 0-7-3.13-7-7s3.13-7 7-7 7 3.13 7 7-3.13 7-7 7z"></path>
																</svg>
															</span>
											</button>
											<div class="btn-group position-static">
												<div class="dropdown-menu">
													<a class="dropdown-item pl-2 pr-3" onclick="tz_qview('40');return false">
																	<span class="si si-rem">
																		<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																			<path d="M12,6.5c3.79,0,7.17,2.13,8.82,5.5c-1.65,3.37-5.02,5.5-8.82,5.5S4.83,15.37,3.18,12C4.83,8.63,8.21,6.5,12,6.5 M12,4.5 C7,4.5,2.73,7.61,1,12c1.73,4.39,6,7.5,11,7.5s9.27-3.11,11-7.5C21.27,7.61,17,4.5,12,4.5L12,4.5z"></path><path d="M12,9.5c1.38,0,2.5,1.12,2.5,2.5s-1.12,2.5-2.5,2.5S9.5,13.38,9.5,12S10.62,9.5,12,9.5 M12,7.5c-2.48,0-4.5,2.02-4.5,4.5 s2.02,4.5,4.5,4.5s4.5-2.02,4.5-4.5S14.48,7.5,12,7.5L12,7.5z"></path>
																		</svg>
																	</span>
														Быстрый просмотр
													</a>
													<a class="dropdown-item pl-2 pr-3" onclick="wishlist.add('42');return false">
																	<span class="si si-rem mr-1">
																		<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																			<path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"></path>
																		</svg>
																	</span>
														В закладки
													</a>
													<a class="dropdown-item pl-2 pr-3" onclick="compare.add('42');return false">
																	<span class="si si-rem mr-1">
																		<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																			<path d="M10 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h5v2h2V1h-2v2zm0 15H5l5-6v6zm9-15h-5v2h5v13l-5-6v9h5c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z"></path>
																		</svg>
																	</span>
														В сравнение
													</a>
												</div>
												<button type="button" class="btn btn-light btn-cart-add px-0" data-toggle="dropdown" data-flip="false">
																<span class="si si-rem">
																	<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																		<path d="M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"></path>
																	</svg>
																</span>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-6 col-sm-4 col-lg-3 product-col">
								<div class="card h-100 border-0 rounded-0 mx-auto">
									<div class="image position-relative px-2 px-sm-3 pt-2 pb-2">
										<a href="http://demo.tezarius.lo/index.php?route=product/product&amp;product_id=30">
											<img src="./img/demo/canon_eos_5d_1-200x200.jpg" alt="Canon EOS 5D" title="Canon EOS 5D" class="img-fluid d-block mx-auto">
										</a>
									</div>
									<div class="card-body px-2 px-sm-3 pt-2 pb-0">
										<div class="caption">
											<h6 class="card-title"><a href="http://demo.tezarius.lo/index.php?route=product/product&amp;product_id=30">Canon EOS 5D</a></h6>
										</div>
									</div>
									<div class="card-footer px-2 px-sm-3 pb-2 pb-sm-3 pt-0 bg-white border-0">
										<div class="price">
											<div class="h6 mb-1">
												<s class="d-inline-block text-danger font-weight-light">100.00р.</s>
												<span class="d-inline-block">80.00р.</span>
											</div>
											<small class="font-weight-light text-muted d-block">Без НДС: 80.00р.</small>
										</div>
										<div class="btn-group btn-group-cart-add mt-2">
											<button type="button" class="btn btn-light btn-cart-add px-1" onclick="cart.add('30');" aria-label="Купить">
															<span class="si si-rem">
																<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																	<path d="M17.21 9l-4.38-6.56c-.19-.28-.51-.42-.83-.42-.32 0-.64.14-.83.43L6.79 9H2c-.55 0-1 .45-1 1 0 .09.01.18.04.27l2.54 9.27c.23.84 1 1.46 1.92 1.46h13c.92 0 1.69-.62 1.93-1.46l2.54-9.27L23 10c0-.55-.45-1-1-1h-4.79zM9 9l3-4.4L15 9H9zm3 8c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"></path>
																</svg>
															</span>
												<span class="ml-1 mr-2 d-none d-md-inline text-muted">Купить</span>
											</button>
											<button type="button" class="btn btn-light btn-cart-add px-1" title="Быстрый просмотр" onclick="tz_qview('30')">
															<span class="si si-rem">
																<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																	<path d="M12,6.5c3.79,0,7.17,2.13,8.82,5.5c-1.65,3.37-5.02,5.5-8.82,5.5S4.83,15.37,3.18,12C4.83,8.63,8.21,6.5,12,6.5 M12,4.5 C7,4.5,2.73,7.61,1,12c1.73,4.39,6,7.5,11,7.5s9.27-3.11,11-7.5C21.27,7.61,17,4.5,12,4.5L12,4.5z"></path><path d="M12,9.5c1.38,0,2.5,1.12,2.5,2.5s-1.12,2.5-2.5,2.5S9.5,13.38,9.5,12S10.62,9.5,12,9.5 M12,7.5c-2.48,0-4.5,2.02-4.5,4.5 s2.02,4.5,4.5,4.5s4.5-2.02,4.5-4.5S14.48,7.5,12,7.5L12,7.5z"></path>
																</svg>
															</span>
											</button>
											<div class="btn-group position-static">
												<div class="dropdown-menu">
													<a class="dropdown-item pl-2 pr-3" onclick="tz_fastorder('40');return false">
																	<span class="si si-rem">
																		<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																			<path d="M15 1H9v2h6V1zm-4 13h2V8h-2v6zm8.03-6.61l1.42-1.42c-.43-.51-.9-.99-1.41-1.41l-1.42 1.42C16.07 4.74 14.12 4 12 4c-4.97 0-9 4.03-9 9s4.02 9 9 9 9-4.03 9-9c0-2.12-.74-4.07-1.97-5.61zM12 20c-3.87 0-7-3.13-7-7s3.13-7 7-7 7 3.13 7 7-3.13 7-7 7z"></path>
																		</svg>
																	</span>
														Быстрый заказа
													</a>
													<a class="dropdown-item pl-2 pr-3" onclick="wishlist.add('30');return false">
																	<span class="si si-rem mr-1">
																		<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																			<path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"></path>
																		</svg>
																	</span>
														В закладки
													</a>
													<a class="dropdown-item pl-2 pr-3" onclick="compare.add('30');return false">
																	<span class="si si-rem mr-1">
																		<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																			<path d="M10 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h5v2h2V1h-2v2zm0 15H5l5-6v6zm9-15h-5v2h5v13l-5-6v9h5c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z"></path>
																		</svg>
																	</span>
														В сравнение
													</a>
												</div>
												<button type="button" class="btn btn-light btn-cart-add px-0" data-toggle="dropdown" data-flip="false">
																<span class="si si-rem">
																	<svg fill="#bbb" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
																		<path d="M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"></path>
																	</svg>
																</span>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php //*/ ?>

			@include('includes.pages.carousel',['module' => 'brands', 'banners' => $banners])
		</div>
	</section>
@endsection
