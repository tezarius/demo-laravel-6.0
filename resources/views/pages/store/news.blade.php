@extends('layouts.base')

@section('cssThisPage')
@endsection

@section('jsThisPage')
@endsection

@section('title','')
@section('keywords', '')
@section('description', '')

@section('content')
	<section id="store-news" class="container">
		<small class="mb-2 d-inline-block text-secondary"><i class="fa fa-calendar"></i>&ensp;{{ date('d.m.Y',strtotime(Arr::get($news,'data_start'))) }}</small>
		<h1>{{ Arr::get($news,'subject') }}</h1>
		<article>
			@if( $img = Arr::get($news,'img_main_url') )
				<div class="row">
					<div class="col-12 col-sm-5 col-md-4 col-lg-3">
						<img src="{{ $img }}" width="80%" class="d-block m-auto m-sm-0" />
					</div>
					<div class="col-12 col-sm-7 col-md-8 col-lg-9">
						{!! Arr::get($news,'body') !!}
					</div>
				</div>
			@else
				{!! Arr::get($news,'body') !!}
			@endif
		</article>
	</section>
@endsection
