<div class="row">
    <div class="col-12 col-md-4 d-flex">
        <a href="{{ route('amTypes') }}" class="well card card-body bg-light mb-3 text-secondary text-decoration-none">
            <div class="media">
                <div class="float-left mr-3">
                    <i class="icon icon-30 ico-custom-construction text-danger"></i>
                </div>
                <div class="media-body">
                    <h6 class="media-heading break-word overflow-hidden h-1_1rem">Неоригинальные каталоги</h6>
                    <hr class="m-0 p-0">
                    <p class="m-0 line-clamp clamp-2">Производители неоригинальных запчастей</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-12 col-md-4 d-flex"
         onClick="TZAlert.fire('ВНИМАНИЕ!','Оригинальные каталоги подключим по вашему запросу по одному из поставщиков:<br>' +
								  '<a href=\'https://www.laximo.ru\' target=\'_blank\'>https://www.laximo.ru</a><br>' +
								   '<a href=\'https://www.ilcats.ru\' target=\'_blank\'>https://www.ilcats.ru</a><br>Каталоги будут интегрированы в ваш сайт!','info')"
    >
        <a href="#" class="well card card-body bg-light mb-3 text-secondary text-decoration-none">
            <div class="media">
                <div class="float-left mr-3">
                    <i class="icon icon-30 ico-custom-original text-danger"></i>
                </div>
                <div class="media-body">
                    <h6 class="media-heading break-word overflow-hidden h-1_1rem">Оригинальные каталоги</h6>
                    <hr class="m-0 p-0">
                    <p class="m-0 line-clamp clamp-2">Поиск запчастей по схеме автомобиля</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-12 col-md-4 d-flex">
        <a href="{{ route('toMarks') }}" class="well card card-body bg-light mb-3 text-secondary text-decoration-none">
            <div class="media">
                <div class="float-left mr-3">
                    <i class="icon icon-30 ico-custom-repair text-danger"></i>
                </div>
                <div class="media-body">
                    <h6 class="media-heading break-word overflow-hidden h-1_1rem">Каталоги ТО</h6>
                    <hr class="m-0 p-0">
                    <p class="m-0 line-clamp clamp-2">Все для технического обслуживания</p>
                </div>
            </div>
        </a>
    </div>
</div>