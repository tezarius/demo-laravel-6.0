<div class="row">
    <div class="col-12 col-md-4 d-flex">
        <div class="well card card-body bg-light mb-3 text-decoration-none">
            <div class="media">
                <div class="float-left mr-3">
                    <img width="50px" src="/img/icons/shield.svg" />
                </div>
                <div class="media-body">
                    <h6 class="media-heading break-word overflow-hidden h-1_1rem text-secondary">Защита заказов</h6>
                    <hr class="m-0 p-0">
                    <p class="m-0 line-clamp clamp-2 text-secondary">Мы страхуем каждую вашу покупку</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4 d-flex">
        <div class="well card card-body bg-light mb-3 text-decoration-none">
            <div class="media">
                <div class="float-left mr-3">
                    <img width="50px" src="/img/icons/truck.svg" />
                </div>
                <div class="media-body">
                    <h6 class="media-heading break-word overflow-hidden h-1_1rem text-secondary">Доставка</h6>
                    <hr class="m-0 p-0">
                    <p class="m-0 line-clamp clamp-2 text-secondary">Мы отправим ваш заказ в любую точку России</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4 d-flex">
        <div class="well card card-body bg-light mb-3 text-decoration-none">
            <div class="media">
                <div class="float-left mr-3">
                    <img width="50px" src="/img/icons/phone-call.svg" />
                </div>
                <div class="media-body">
                    <h6 class="media-heading break-word overflow-hidden h-1_1rem text-secondary">Поддержка 24/7</h6>
                    <hr class="m-0 p-0">
                    <p class="m-0 line-clamp clamp-2 text-secondary">Есть вопросы? Мы всегда на связи!</p>
                </div>
            </div>
        </div>
    </div>
</div>