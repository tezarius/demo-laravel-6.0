<?php

namespace App\Http\Middleware;

use App\Helpers\Carts\Customer;
use Closure;

class TZAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( !Customer::singleton()->isLogged() )
        {
            return redirect()->route('login');
        }
        return $next($request);
    }
}
