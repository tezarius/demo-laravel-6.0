<?php

namespace App\Http\Middleware;

use App\Helpers\Carts\Customer;
use Closure;

class TZRoomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( !Customer::singleton()->isAdminWebsite() )
        {
            abort(403, 'Access Denied into the TZ Room');
        }
        return $next($request);
    }
}
