<?php

namespace App\Http\Middleware;

use App\Helpers\Carts\Basket;
use App\Helpers\Carts\Customer;
use App\Helpers\Carts\Store;
use Closure;
use Illuminate\Support\Facades\Cookie;

class TZInitMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * Так как middleware вызываются до BaseComposer
     * То некотрые карточки, точнее их инициализацию
     * Вынесем сюда и обернем все маршруты
     */
    public function handle($request, Closure $next)
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///   S I N G L E T O N  C A R T S   ///////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Через родные контейнеры
        /*/
        app()->singleton(Customer::class);
        $oCustomer = app()->make(Customer::class);
        $oCustomer->login();
        //*/
        /// Через Singleton Trait
        Store::singleton()->init();    /// Объект для хранения данных по самому магазину
        Basket::singleton()->init();   /// Объект для хранение данных о корзинах пользователя
        Customer::singleton()->init(); /// Объект для хранения данных о покупателе
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///   S E S S I O N  I D   /////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// MULTI ::: https://divinglaravel.com/understanding-how-laravel-configures-database-connections
        /// Привязываем токен
        // selectedStockID => tz_office_id && SESSION => COOKIE
        $CustomerSID = Cookie::get('tz_sid');
        if( !$CustomerSID )
        {
            $CustomerSID = csrf_token();
            $minutes = 60 * 24 * 30 * 3; /// час * сутки * месяц * сколько
            Cookie::queue('tz_sid', $CustomerSID, $minutes);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        return $next($request);
    }
}
