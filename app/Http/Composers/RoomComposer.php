<?php

namespace App\Http\Composers;

use App\Helpers\Models\AnyParams;
use App\Helpers\Support\Arr;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Route;

class RoomComposer
{
    public function compose(View $view)
    {
        $routeName = Route::currentRouteName();

        $route = Route::current();
        $routePrefix = $route->parameter('action.prefix');

        $action = Arr::get($route,'action');
        $routeScope = Arr::get($action,'scope');
        ///dd($routePrefix,$action,$routeScope);

        $paramInfo = (new AnyParams())->get('component-logo');
        $headLogo = Arr::get($paramInfo,'value','/img/header-logo.svg');
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///   R E N D E R  &&  P R E  R E N D E R   ////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $view
            ->with('headLogo', $headLogo)
            ///
            ->with('routeName', $routeName)
            ->with('routePrefix', $routePrefix)
            ->with('routeScope', $routeScope)
        ;
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
