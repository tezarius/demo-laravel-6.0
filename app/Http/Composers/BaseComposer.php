<?php

namespace App\Http\Composers;

use App\Helpers\Carts\Basket;
use App\Helpers\Carts\Customer;
use App\Helpers\Carts\Store;
use App\Helpers\Models\AnyParams;
use App\Helpers\Models\Category;
use App\Helpers\Support\Arr;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Facades\Agent;

class BaseComposer
{
    public function compose(View $view)
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $routeName = Route::currentRouteName();
        $routePrefix = Route::current()->parameter('prefix');
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///   C O U N T  N E W  M E S S A G E  &&  M A G I C  C O N S T A N T S   //////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $CustomerSID = Customer::singleton()->getID();
        $countNewMsg = DB::connection('tezarius')->select("CALL pMessage('GetMessagesCustomer', JSON_OBJECT('CountNewMess','1','recipientUID','{$CustomerSID}'),'ru')");
        $countNewMsg = current($countNewMsg);
        $countNewMsg = (int) $countNewMsg->qty;
        ///dd($CustomerSID,$countNewMsg);
        ///$countNewMsg = 0;
        $CyID = 1; /// Валюта /// http://www.zerkalov.org.ua/node/1531
        $FirmID = 2;
        $BankAccID = 6;

        Session::put('CyID',$CyID);
        Session::put('FirmID',$FirmID);
        Session::put('BankAccID',$BankAccID);
        ///Session::put('LastChatID',$LastChatID);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///   O F F I C E  S E C T I O N   /////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Office List
        $offices = Store::singleton()->getAccessStocks();
        $officeName = Store::singleton()->getOfficeName();
        $officeID = Store::singleton()->getOfficeID();
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///   F A S T  B A S K E T   ///////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// fb == fast basket
        $fastBasketInfo = Basket::singleton()->fastBasketInfo();
        $fbProducts = Arr::get($fastBasketInfo,'items');
        $fbTotal = Arr::get($fastBasketInfo,'total');
        $fbCount = Arr::get($fastBasketInfo,'count');
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $discountInfo = Customer::singleton()->getDiscountInfo();
        $balanceInfo = Customer::singleton()->getBalanceInfo();
        ///ddd($balanceInfo);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///   C A T E G O R I E S   ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $categories = (new Category())->flat();
        $categories = Arr::buildTree($categories,'id_parent','id');
        if( Agent::isDesktop() )
        {
            $_new = []; $_more = [];
            foreach( $categories AS $k=>$c ) if( $k < 10 ) array_push($_new,$c); else array_push($_more,$c);
            if( !empty($_more) )
            {
                if( count($_more)==1 ) $_new = array_merge($_new,$_more);
                else array_push($_new,[
                    'id' => 'more',
                    'name' => 'Еще',
                    'level' => 'more',
                    'children' => $_more,
                ]);
            }
            $categories = $_new;
        }
        /// one-col & col-12 | two-cols & col-6 | three-cols & col-4
        $secondMenuClass = 'three-cols';
        $secondMenuCols = 'col-md-4';
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $paramInfo = (new AnyParams())->some(['component-logo','component-counters','component-hello-chat','component-schedule']);
        $_new = []; foreach( $paramInfo AS $p ) $_new[Arr::get($p,'code')] = $p; $paramInfo = $_new;
        ///dd($paramInfo);
        $arrHeadLogo = Arr::get($paramInfo,'component-logo');
        $arrCounters = Arr::get($paramInfo,'component-counters');
        $arrHelloChat = Arr::get($paramInfo,'component-hello-chat');
        $arrSchedule = Arr::get($paramInfo,'component-schedule');

        $headLogo = ( $arrHeadLogo ) ? Arr::get($arrHeadLogo,'value') : '/img/header-logo.svg';
        $jsCounters = ( $arrCounters ) ? Arr::get($arrCounters,'value') : '';
        $helloChat = ( $arrHelloChat ) ? Arr::get($arrHelloChat,'value') : '';
        $schedule = ( $arrSchedule ) ? Arr::get($arrSchedule,'value') : '';
        ///dd($jsCounters,$helloChat,$schedule);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///   R E N D E R  &&  P R E  R E N D E R   ////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $view
            ->with('balanceInfo', $balanceInfo)
            ->with('discountInfo', $discountInfo)
            ///
            ->with('routeName', $routeName)
            ->with('routePrefix', $routePrefix)
            /// Store
            ->with('countNewMsg', $countNewMsg)
            ->with('offices', $offices)
            ->with('officeName', $officeName)
            ->with('officeID', $officeID)
            /// Categories
            ->with('categories', $categories)
            ->with('secondMenuClass', $secondMenuClass)
            ->with('secondMenuCols', $secondMenuCols)
            /// Fast Basket
            ->with('fbProducts', $fbProducts)
            ->with('fbTotal', $fbTotal)
            ->with('fbCount', $fbCount)
            ///
            ->with('headLogo', $headLogo)
            ->with('jsCounters', $jsCounters)
            ->with('helloChat', $helloChat)
            ->with('schedule', $schedule)
        ;
        ///View::share('routePrefix',$prefix);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
