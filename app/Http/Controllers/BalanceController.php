<?php

namespace App\Http\Controllers;

use App\Helpers\Models\Account;
use App\Helpers\Support\Arr;
use App\Helpers\Support\Request;

class BalanceController extends Controller
{
    public function main(Request $request)
    {
        $mAccount = new Account();

        $filter = $request->rcv('filter','current');
        $arrFilterNameByCode = [
            'current' => 'Текущие',
            'period' => 'За период',
            'number' => 'По номеру',
            'search' => 'Мультипоиск',
        ];
        $filterName = Arr::get($arrFilterNameByCode,$filter);

        $period = [];
        $number = $search = NULL;
        $isButtonPressed = FALSE;
        if( $this->buttonPressed('btnFilterOut') )
        {
            $isButtonPressed = TRUE;
            ///dd(request()->all());
            switch( $filter )
            {
                case 'period':{
                    $period = $request->rcv('period',[]);
                    $dd1 = date('Y.m.d',strtotime(Arr::get($period,'dd1')));
                    $dd2 = date('Y.m.d',strtotime(Arr::get($period,'dd2')));
                    $reports = $mAccount->getReports($dd1,$dd2);
                }break;
                case 'number':{
                    $number = $request->rcv('number');
                    $reports = [];
                }break;
                case 'search':{
                    $search = $request->rcv('search');
                    $reports = [];
                }break;
                default:{
                    $dd1 = date('Y.m.d',strtotime('-30 days'));
                    $dd2 = date('Y.m.d',time());
                    $reports = $mAccount->getReports($dd1,$dd2);
                }
            }
        }
        else{
            $dd1 = date('Y.m.d',strtotime('-30 days'));
            $dd2 = date('Y.m.d',time());
            $reports = $mAccount->getReports($dd1,$dd2);
        }
        ///ddd($reports);

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Личный кабинет",
            'link' => route('account')
        ],[
            'text' => "Баланс",
        ]];

        return $this->render('pages.account.balance.grid',compact('reports','filterName','filter','period','number','search','isButtonPressed'));
    }
}
