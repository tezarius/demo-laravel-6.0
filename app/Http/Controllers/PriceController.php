<?php

namespace App\Http\Controllers;

use App\Helpers\Carts\Basket;
use App\Helpers\Carts\Customer;
use App\Helpers\Carts\Store;
use App\Helpers\Models\Price;
use App\Helpers\Support\Arr;
use App\Helpers\Support\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Facades\Agent;

class PriceController extends Controller
{
    public function brands(Request $request, $number)
    {
        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Бренды для $number",
            'link' => route('priceBrands',['number'=>$number])
        ],[
            'icon' => '<span class="oi oi-loop-circular" title="Обновить"></span>',
            'text' => 'Обновить',
            'link' => route('priceBrands',['number'=>$number])."?reload=1"
        ]];

        $isReLoad = $request->input('reload');
        ///$isReLoad =  $this->>buttonPressed('btnBrandReload');

        $mPrice = new Price();
        $brands = $mPrice->getBrands($number);

        if( empty($brands) || $isReLoad )
        {
            $row = $mPrice->getBrandsKey($number);

            if( Arr::get($row,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => Arr::get($row,'mess'),
                ];
            }
            else
            {
                $IDSearchCode = Arr::get($row,'IDSearchCode');
                $mPrice->getBrandsRemote($IDSearchCode);
                $brands = $mPrice->getBrands($number);
            }
        }

        return $this->render('pages.price.brands',compact('number','brands'));
    }

    public function offers(Request $request, $number, $brand)
    {
        ini_set('max_execution_time', 60);
        ///ddd( $this->getStockID() );
        $offers = [];
        if( $StockID = $this->officeChoosed() )
        {
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $mPrice = new Price();
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            /// Check Price Cash
            $recentlyRequest = FALSE; /// Недавно был запрос ?
            $cash = Session::get('priceCash');
            ///dd($cash);
            $queryStr = Arr::get($cash,'QUERY_STR');
            if( $queryStr && $queryStr == crc32("{$number}{$brand}") )
            {
                $lastQuery = (int) Arr::get($cash,'LAST_TIME'); /// Первый раз 0
                $timePassedS = time() - $lastQuery; /// Первый раз - Дохера секунд
                $timePassedM = $timePassedS / 60;   /// Первый раз - Дохера минут
                ///dd([$cash,$timePassedS,$timePassedM]);
                if( $timePassedM < 5 ) $recentlyRequest = TRUE; /// Меньше 5-ти минут - значит недавно был запрос
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if( !$recentlyRequest )
            {
                $res = $mPrice->getSearchID($number,$brand);
                if( Arr::get($res,'isError') )
                {
                    $this->flashers[] = [
                        'type' => 'error',
                        'text' => Arr::get($res,'mess'),
                    ];
                    $IDSearchCode = 0;
                } /// $this->session->data['error'] || $this->session->data['success']
                else $IDSearchCode = Arr::get($res,'IDSearchCode','IDSearchCode');
                ///$this->data['IDSearchCode'] = [$IDSearchCode,$res];

                $mPrice->webServicesLoad($IDSearchCode);

                /// Create Price Cash
                Session::put('priceCash',[
                    'SEARCH_ID' => $IDSearchCode,
                    'QUERY_STR' => crc32("{$number}{$brand}"),
                    'LAST_TIME' => time()
                ]);
            }
            else $IDSearchCode = Arr::get($cash,'SEARCH_ID');
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $PriceLevelID = Customer::singleton()->getPriceLID();
            $StockTypeFilter = 'StockAll';
            $SearchFilter = 'ByCodeBrand';
            $SearchValue = $IDSearchCode;
            $SortData = '';
            ///pp([$recentlyRequest,$StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData]);
            $offers = $mPrice->priceLoad($StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData);
            ///dd($offers);
            if( !$offers )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => 'OFFERS: Сервер не вернул результатов, сообщите в чат оператору и прикрепите ссылку',
                ];
                $offers = [];
            }
            if( @Arr::get($offers[0],'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => 'OFFERS: '.@Arr::get($offers[0],'mess'),
                ];
                $offers = [];
            }
            else if( !empty($offers) ) Store::singleton()->fitPriceResult($offers);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Список брендов для $number",
            'link' => route('priceBrands',['number'=>$number])
        ],[
            'text' => "$number / $brand (".count($offers)." всего)",
        ]];

        ///ddd($offers);
        $basketTabID = Basket::singleton()->getTabActive();

        ///ddd($offers);
        $isGoods = 0;
        return $this->render('pages.price.offers',compact('offers','basketTabID','isGoods'));
    }
    public function name(Request $request, $search)
    {
        ///ddd( $this->getStockID() );
        $offers = [];
        if( $StockID = $this->officeChoosed() )
        {
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $mPrice = new Price();
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $PriceLevelID = Customer::singleton()->getPriceLID();
            $StockTypeFilter = 'StockAll';
            $SearchFilter = 'MultiSearch';
            $SearchValue = $search;
            $SortData = '';
            ///pp([$recentlyRequest,$StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData]);
            $offers = $mPrice->priceLoad($StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData);
            ///dd($offers);
            if( !$offers )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => 'OFFERS: Сервер не вернул результатов, сообщите в чат оператору и прикрепите ссылку',
                ];
                $offers = [];
            }
            if( @Arr::get($offers[0],'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => 'OFFERS: '.@Arr::get($offers[0],'mess'),
                ];
                $offers = [];
            }
            else if( !empty($offers) ){
                $params = [];
                $params['back'] = [
                    'text' => $search,
                    'link' => route('priceName',['search'=>$search]),
                ];
                Store::singleton()->fitPriceResult($offers,$params);
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "$search",
        ]];

        ///ddd($offers);
        $basketTabID = Basket::singleton()->getTabActive();

        ///ddd($offers);
        $isGoods = 1;
        $displayView = ( Agent::isDesktop() ) ? 'grid' : 'list';
        return $this->render('pages.price.offers',compact('offers','basketTabID','isGoods','displayView'));
    }
    public function goods($hash)
    {
        $arr = Arr::decryption($hash);
        $goodID = Arr::get($arr,'id');
        $goodName = Arr::get($arr,'name');

        $mPrice = new Price();

        $offers = [];
        if( $StockID = $this->officeChoosed() )
        {
            $PriceLevelID = Customer::singleton()->getPriceLID();
            $StockTypeFilter = 'StockAll';
            $SearchFilter = 'ByWebCategory';
            $SearchValue = $goodID;
            $SortData = '';
            ///pp([$StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData]);
            $offers = $mPrice->priceLoad($StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData);
            ///dd($offers);
            if( !$offers )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => 'GOODS: Сервер не вернул результатов, сообщите в чат оператору и прикрепите ссылку',
                ];
                $offers = [];
            }
            if( @Arr::get($offers[0],'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => 'GOODS: '.@Arr::get($offers[0],'mess'),
                ];
                $offers = [];
            }
            else if( !empty($offers) )
            {
                $params = [];
                $params['back'] = [
                    'text' => $goodName,
                    'link' => route('priceGoods',['hash'=>$hash]),
                ];
                Store::singleton()->fitPriceResult($offers,$params);
            }
        }

        $properties = $mPrice->properties($goodID);
        $_arr = [];
        foreach( $properties as $p )
        {
            $id = Arr::get($p,'id');
            if( !isset($_arr[$id]) ) $_arr[$id] = [
                'id' => $id,
                'name' => Arr::get($p,'name'),
            ];
            $_arr[$id]['items'][] = Arr::get($p,'value');
        }
        $properties = $_arr;
        $offers['filters']['properties'] = $properties;
        ///dd($offers);

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "$goodName",
        ]];

        $basketTabID = Basket::singleton()->getTabActive();

        ///ddd($offers);
        $isGoods = 1;
        $displayView = ( Agent::isDesktop() ) ? 'grid' : 'list';
        return $this->render('pages.price.offers',compact('offers','basketTabID','isGoods','displayView'));
    }

    public function history()
    {
        $json = [];
        $UserID = (int) Customer::singleton()->getID();

        ///$arr = $this->model_account_customer->historySearch($UserID);
        $sql = "call pSearch_ArtCode('MyHistory',JSON_OBJECT('isWebsiteQuery',1,'id_rbCounterparts',{$UserID}))";
        ///$json['sql'] = $sql;
        $arr = (array) DB::connection('tezarius')->select($sql);

        $arrStack = []; $arrHistory = []; $i = 0;
        foreach( $arr AS $h )
        {
            $hash = crc32(Arr::get($h,'code').Arr::get($h,'brand'));
            if( !in_array($hash,$arrStack) )
            {
                array_push($arrStack,$hash);
                array_push($arrHistory,[
                    'id' => $i++,
                    'name' => Arr::get($h,'code').' '.Arr::get($h,'brand'),
                    'code' => Arr::get($h,'code'),
                    'brand' => Arr::get($h,'brand'),
                ]);
            }
        }
        $json['history'] = $arrHistory;

        return ( request()->ajax() )
            ? response()->json($json)
            : ddd($json) ///'' ///Пока только Ajax запрос
            ;
    }
    public function stockInfo(Request $request)
    {
        $json = [];

        $shopID  = (int) Customer::singleton()->getStockIDGlobal();
        $stockID = (int) $request->rcv('stockID');
        $dlvrMin = (int) $request->rcv('dlvrMin');
        $dlvrMax = (int) $request->rcv('dlvrMax');
        $res = (new Price())->stockInfo($stockID,$shopID,$dlvrMin,$dlvrMax);

        $json['info'] = Arr::get($res,'inf');

        return response()->json($json);
    }
}
