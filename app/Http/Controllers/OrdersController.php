<?php

namespace App\Http\Controllers;

use App\Helpers\Carts\Basket;
use App\Helpers\Carts\Customer;
use App\Helpers\Models\Orders;
use App\Helpers\Support\Arr;
use App\Helpers\Support\Request;

class OrdersController extends Controller
{
    public function checkout()
    {
        $json = [];

        $note = Request::rcv('note');

        $data = [
            'ShopID'   => Customer::singleton()->getStockID(),
            'UserID'   => Customer::singleton()->getID(),
            'TabID'    => Basket::singleton()->getTabActive(),
            'FirmID'   => Customer::singleton()->getFirmID(),
            'PriceLID' => Customer::singleton()->getPriceLID(),

            'CarsCustomerID' => 0,
            'CustomersSourceID' => 0,
            'ManagerID' => Customer::singleton()->getManagerID(),

            'isDelivery' => 0,
            'addressDelivery' => '',
            'isDeliveryPartly' => 0,

            'CardID' => Customer::singleton()->getDiscountCardID(),
            'note' => $note,

            '' => '',
        ];

        $res = (new Orders())->create($data);
        if( Arr::get($res,'isError') )
        {
            $json['type'] = 'error';
            $json['text'] = Arr::get($res,'mess');
        }
        elseif( $_id = Arr::get($res,'jrDocsID') )
        {
            $json['type'] = 'success';
            $json['text'] = Arr::get($res,'mess',"Номер заказа: {$_id}",TRUE);
            $json['order_id'] = $_id;
        }
        else
        {
            $json['type'] = 'warning';
            $json['text'] = Arr::get($res,'mess','Что-то пошло не так :(',TRUE);
        }

        return response()->json($json);
    }
    public function main(Request $request)
    {
        $mOrders = new Orders();

        $filter = $request->rcv('filter','current');
        $arrFilterNameByCode = [
            'current' => 'Текущие',
            'period' => 'За период',
            'number' => 'По номеру',
            'search' => 'Мультипоиск',
        ];
        $filterName = Arr::get($arrFilterNameByCode,$filter);

        $period = [];
        $number = $search = NULL;
        $isButtonPressed = FALSE;
        if( $this->buttonPressed('btnFilterOut') )
        {
            $isButtonPressed = TRUE;
            ///dd(request()->all());
            switch( $filter )
            {
                case 'period':{
                    $period = $request->rcv('period',[]);
                    $dd1 = Arr::get($period,'dd1');
                    $dd2 = Arr::get($period,'dd2');
                    $orders = $mOrders->getOrdersByData($dd1,$dd2);
                }break;
                case 'number':{
                    $number = $request->rcv('number');
                    $orders = $mOrders->getOrdersByNumber($number);
                }break;
                case 'search':{
                    $search = $request->rcv('search');
                    $orders = $mOrders->getOrdersBySearch($search);
                }break;
                default: $orders = $mOrders->getOrderList();
            }
        }
        else $orders = $mOrders->getOrderList();
        ///ddd($orders);

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Личный кабинет",
            'link' => route('account')
        ],[
            'text' => "История заказов",
        ]];

        return $this->render('pages.account.orders.grid',compact('orders','filterName','filter','period','number','search','isButtonPressed'));
    }
    public function history()
    {
        $json = [];
        $json['status'] = 0;
        //
        if( $id = (int) Request::rcv('id') )
        {
            $res = (new Orders())->getOrderHistory($id);
            if( Arr::get($res,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => Arr::get($res,'mess')
                ];;
            }
            else
            {
                $json['status'] = 1;
                $json['rows'] = $res;
                $json['html'] = $this->render('pages.account.orders.history',['rows'=>$res])->render();
            }
        }
        else
        {
            $this->flashers[] = [
                'type' => 'error',
                'text' => 'Запрошенный заказ не найден!'
            ];
        }
        $json['flashers'] = $this->flashers;
        //
        return response()->json($json);
    }
    public function withdraw()
    {
        $json = [];

        $id = (int) Request::rcv('id');
        $note = Request::rcv('note');

        $res = (new Orders())->reqOrderWithdraw($id,$note);
        if( Arr::get($res,'isError') )
        {
            $json['type'] = 'error';
            $json['text'] = Arr::get($res,'mess');
        }
        else
        {
            $json['type'] = 'success';
            $json['text'] = Arr::get($res,'mess','Запрос на снятие отправлен',TRUE);
        }
        return response()->json($json);
    }
    public function delivery()
    {
        $json = [];

        $id = (int) Request::rcv('id');
        $note = Request::rcv('note');

        $res = (new Orders())->getOrderDelivery($id,$note);
        if( Arr::get($res,'isError') )
        {
            $json['type'] = 'error';
            $json['text'] = Arr::get($res,'mess');
        }
        else
        {
            $json['type'] = 'success';
            $json['text'] = Arr::get($res,'mess','Запрос срока доставки отправлен',TRUE);
        }

        return response()->json($json);
    }
    public function toWork()
    {
        $json = [];

        $id = (int) Request::rcv('id');
        $ids = Request::rcv('ids');
        $ids = implode(',',$ids);
        $note = Request::rcv('note');

        $res = (new Orders())->getOrderToWork($id,$ids,$note);
        $json['$res'] = $res;
        if( Arr::get($res,'isError') )
        {
            $json['type'] = 'error';
            $json['text'] = Arr::get($res,'mess');
        }
        else
        {
            $_msg = ( $id ) ? 'Позиция принята в работу' : 'Позиции приняты в работу';
            $json['type'] = 'success';
            $json['text'] = Arr::get($res,'mess',$_msg,TRUE);
        }

        return response()->json($json);
    }
}
