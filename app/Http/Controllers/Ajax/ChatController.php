<?php

namespace App\Http\Controllers\Ajax;

use App\Helpers\Carts\Customer;
use App\Helpers\ResizeImage;
use App\Helpers\Support\Arr;
use App\Helpers\Support\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AccessWeb;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

use Intervention\Image\ImageManagerStatic as Image;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){}
    public function new(Request $request)
    {
        $LastChatID = (int) $request->post('lastID');

        $CustomerUID = Customer::singleton()->getID();
        $CustomerSess = Customer::singleton()->getSessionID();

        $_type = ( $CustomerUID ) ? 'CustomerId' : 'CustomerSess';
        $_sender = ( $CustomerUID ) ? $CustomerUID : $CustomerSess;

        $sql1 = "CALL pMessage('GetMessagesCustomer',JSON_OBJECT('chatKind','{$_type}','recipientUID','{$_sender}','LastID',$LastChatID),'ru')";
        /*/
        return response()->json([
            '$lastID'=>$lastID,
            '$CustomerSID'=>$CustomerSID,
            '$sql1'=>$sql1,
        ]);
        //*/
        $arrMessage = DB::connection('tezarius')->select($sql1);
        ///$arrMessage = (array) current($arrMessage);

        $StockID = Customer::singleton()->getStockIDGlobal();
        $sql1 = "CALL pMessage('usersocks', JSON_OBJECT('isWebsite','1','StockID','{$StockID}'),'ru')";
        $obj1 = DB::connection('tezarius')->selectOne($sql1);
        $sockets = Arr::get($obj1,'sockets');

        return response()->json([
            'state'=>'success',
            'messages'=>$arrMessage,

            'event'=>'readed',
            'type'=>$_type,
            'senderId'=>$_sender,
            'sockets'=>$sockets,
        ]);
    }
    public function send(Request $request)
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $input_data = $request->all();

        $validator = Validator::make($input_data,[
            ///'images' => 'required',
            'images.*' => 'mimes:jpg,jpeg,png,bmp|max:10240',
            ///'images.*' => 'mimes:jpg,jpeg,png,bmp',
        ],[
            ///'image_file.*.required' => 'Please upload an image',
            'image_file.*.mimes' => 'Разрешены только изображения jpg, png and bmp',
            'image_file.*.max' => 'Sorry! Максимально допустимый размер изображения 1MB',
        ]);

        if( $validator->fails() )
        {
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ) , 422);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $CustomerUID = Customer::singleton()->getID();
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $images = [];
        if( 1==2 && $request->hasFile('images') ) foreach( $request->file('images') as $file )
        {
            ///$fname = $file->getClientOriginalName();
            ///$f_ext = $file->guessExtension();

            $officeID = Customer::singleton()->getStockIDGlobal();

            $fpath = $file->store("chat/office-{$officeID}/user-{$CustomerUID}");
            ///$filepath = 'uploads/' . Auth::user()->username . '/' . $file_name;
            ///$file->storeAs(('uploads/' . Auth::user()->username), $file_name);

            //Resize image here
            $localPath = storage_path('app/public/'.$fpath);
            $width = Image::make($localPath)->width();
            $height = Image::make($localPath)->height();

            $totalBytes = 1024;
            $fsizeKB = ceil($file->getSize()/1024);
            if( $fsizeKB > $totalBytes )
            {
                $pct = $totalBytes / $fsizeKB;

                $width = $width * $pct;
                $height = $height * $pct;

                $img = Image::make($localPath)->resize($width,$height,function($constraint){
                    ///$constraint->aspectRatio();
                });
                $img->save($localPath);
            }

            $images[] = [
                ///'name'=>$fname,
                ///'path'=>$fpath,
                ///'_ext'=>$f_ext,
                'size'=>[[$width,$height],$fsizeKB],
                ///'public_path'=>public_path(),
                ///'$localPath'=>$localPath,
                ///'root' => storage_path('app/public'),
                'url' => env('APP_URL').'/storage/'.$fpath,
            ];
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $message = $request->post('message');

        $StockID = Customer::singleton()->getStockIDGlobal();

        $sql1 = "CALL pMessage('usersocks', JSON_OBJECT('isWebsite','1','StockID','{$StockID}'),'ru')";
        $obj1 = DB::connection('tezarius')->selectOne($sql1);

        $sockets = Arr::get($obj1,'sockets');
        $tokens = Arr::get($obj1,'tokens');
        $usersID = Arr::get($obj1,'usersID');

        /// $CustomerUID; /// Определен выше, тут что-бы глаза помозолил
        $CustomerSess = Customer::singleton()->getSessionID();

        $_type = ( $CustomerUID ) ? 'CustomerId' : 'CustomerSess';
        $_sender = ( $CustomerUID ) ? $CustomerUID : $CustomerSess;

        if( $images )
        {
            foreach( $images AS $img )
            {
                $image = Arr::get($img,'url');
                $sql2 = "CALL pMessage('add',JSON_OBJECT('recipient','User','recipientUIDs','{$usersID}','sender','{$_type}','senderUID','{$_sender}','msg','','photoPath','{$image}','background_add','1'),'ru')";
                DB::connection('tezarius')->select($sql2);
            }
            sleep(1);
        }

        if( $message )
        {
            $sql2 = "CALL pMessage('add',JSON_OBJECT('recipient','User','recipientUIDs','{$usersID}','sender','{$_type}','senderUID','{$_sender}','msg','{$message}','photoPath','','background_add','1'),'ru')";
            DB::connection('tezarius')->select($sql2);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        return response()->json([
            'state'=>'success',
            'event'=>'newmess',
            'type'=>$_type,
            ///'uid'=>$CustomerUID, /// 2019.01.27
            'senderId'=>$_sender,
            'sockets'=>$sockets,
            'tokens'=>$tokens,
            'images'=>$images,
            ///'sql1'=>$sql1,
            ///'sql2'=>$sql2,
        ]);
    }
    public function regsocket(Request $request)
    {
        $socket = $request->post('socket');
        $CustomerSID = Customer::singleton()->getSID();
        $obj = DB::connection('tezarius')->select("CALL pSocket(JSON_OBJECT('CustomerUID','{$CustomerSID}','socket','{$socket}'))");
        $obj = current($obj);
        return response()->json(['state'=>'success','request'=>$request,'response'=>$obj]);
    }
    public function callback(Request $request)
    {
        /// Подглядеть по параметрам можно в StoreController->callback();
        /// App\Helpers\Carts\Stock::singleton()->callback();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){}
}
