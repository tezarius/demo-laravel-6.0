<?php

namespace App\Http\Controllers;

use App\Helpers\Carts\Customer;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\URL;

class OfficeController extends Controller
{
    /** Пока только по Ajax
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function choose($id)
    {
        $current = Customer::singleton()->getStockID();
        if( $id!==$current ) /// Всегда будем тут пр ивыборе, пока так
        {
            Customer::singleton()->logout();

            $minutes = 60 * 24 * 30 * 3; /// час * сутки * месяц * сколько
            Cookie::queue('tz_office_id', $id, $minutes, '/');

            $redirect = ( $current ) ? route('logout') : URL::previous();

            return ( request()->ajax() )
                ? response()->json(['redirect'=>$redirect,'choose'=>$id,'current'=>$current])
                : redirect($redirect)
            ;
        }
        return ( request()->ajax() )
            ? response()->json(['choose'=>$id,'current'=>$current])
            : redirect()->route('home')
        ;
    }
}
