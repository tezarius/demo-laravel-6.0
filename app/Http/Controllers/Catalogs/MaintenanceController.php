<?php

namespace App\Http\Controllers\Catalogs;

use App\Helpers\Models\Catalogs\Maintenance;
use App\Helpers\Models\Price;
use App\Helpers\Support\Arr;
use App\Helpers\Support\Request;
use App\Helpers\Carts\Customer;
use App\Http\Controllers\Controller;

class MaintenanceController extends Controller
{
    private $model; /// Maintenance Model
    public function __construct()
    {
        $this->model = new Maintenance();
    }

    public function marks()
    {
        $marks = $this->model->marks();
        ///dd($marks);
        if( request()->ajax() )
        {
            $json = [];
            $json['marks'] = $marks;
            return response()->json($json);
        }

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Выберите марку автомобиля",
        ]];

        return $this->render('pages.catalogs.maintenance.marks',compact('marks'));
    }
    public function models($hash=NULL)
    {
        if( $hash )
        {
            $hash = Arr::decryption($hash);
            $markID = Arr::get($hash,'markID');
            $markName = Arr::get($hash,'markName');
        }
        else
        {
            $markID = Request::rcv('markID');
            $markName = Request::rcv('markName');
        }


        $models = $this->model->models($markID);
        ///dd($models);
        if( request()->ajax() )
        {
            $json = [];
            $json['models'] = $models;
            return response()->json($json);
        }

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Каталог ТО: $markName",
            'link' => route('toMarks')
        ],[
            'text' => 'Выберите модель',
        ]];

        /// table || grid
        return $this->render('pages.catalogs.maintenance.models.grid',compact('markID','markName','models'));
    }
    public function vehicles($hash=NULL)
    {
        if( $hash )
        {
            $hash = Arr::decryption($hash);
            $markID = Arr::get($hash,'markID');
            $markName = Arr::get($hash,'markName');
            $modelID = Arr::get($hash,'modelID');
            $modelName = Arr::get($hash,'modelName');
        }
        else
        {
            $markID = Request::rcv('markID');
            $markName = Request::rcv('markName');
            $modelID = Request::rcv('modelID');
            $modelName = Request::rcv('modelName');
        }

        $vehicles = $this->model->vehicles($modelID);
        ///dd($vehicles);
        if( request()->ajax() )
        {
            $json = [];
            $json['vehicles'] = $vehicles;
            return response()->json($json);
        }

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Каталог ТО: $markName",
            'link' => route('toMarks')
        ],[
            ///'text' => "$modelName",
            'text' => "$modelName",
            'link' => route('toModels',['hash'=>Arr::encryption(['markID'=>$markID,'markName'=>$markName])])
        ],[
            'text' => 'Выберите модификацию'
        ]];

        /// table || grid
        return $this->render('pages.catalogs.maintenance.vehicles.grid',compact('markID','markName','modelID','modelName','vehicles'));
    }
    public function details($hash)
    {
        if( $hash )
        {
            $hash = Arr::decryption($hash);
            $markID = Arr::get($hash,'markID');
            $markName = Arr::get($hash,'markName');
            $modelID = Arr::get($hash,'modelID');
            $modelName = Arr::get($hash,'modelName');
            $vehicleID = Arr::get($hash,'vehicleID');
            $vehicleName = Arr::get($hash,'vehicleName');
        }
        else
        {
            $markID = Request::rcv('markID');
            $markName = Request::rcv('markName');
            $modelID = Request::rcv('modelID');
            $modelName = Request::rcv('modelName');
            $vehicleID= Request::rcv('vehicleID');
            $vehicleName = Request::rcv('vehicleName');
        }


        $offers = [];

        $details = $this->model->details($vehicleID);
        ///dd($details);

        $detailsCatalog = view('pages.catalogs.maintenance.details.catalog',['details' => $details])->render();
        $detailsOffers = view('pages.catalogs.maintenance.details.offers',['offers' => $offers])->render();
        if( request()->ajax() )
        {
            if( Request::rcv('inner') )
            {
                return response()->json([
                    'inner' => 1,
                    'details' => $detailsOffers.$detailsCatalog,
                    ///'detailsCatalog' => $detailsCatalog,
                    ///'detailsOffers' => $detailsOffers
                ]);
            }
            else
            {
                return response()->json(['detailsCatalog'=>$details,'detailsOffers'=>$offers]);
            }
        }


        $garage = Request::rcv('garage');
        if( $garage )
        {
            $this->breads = [[
                'text' => 'Главная',
                'link' => route('home'),
                'icon' => '<span class="oi oi-home" title="Главная"></span>',
                'hide' => 1
            ],[
                'text' => 'Гараж',
                'link' => route('garage')
            ],[
                'text' => 'Список автозапчастей',
            ]];
        }
        else
        {
            $this->breads = [[
                'text' => 'Главная',
                'link' => route('home'),
                'icon' => '<span class="oi oi-home" title="Главная"></span>',
                'hide' => 1
            ],[
                'text' => "Каталог ТО: $markName",
                'link' => route('toMarks')
            ],[
                ///'text' => "$modelName",
                'text' => "$modelName",
                'link' => route('toModels',['hash'=>Arr::encryption(['markID'=>$markID,'markName'=>$markName])])
            ],[
                ///'text' => "$vehicleName",
                'text' => "$vehicleName",
                'link' => route('toVehicles',['hash'=>Arr::encryption(['markID'=>$markID,'markName'=>$markName,'modelID'=>$modelID,'modelName'=>$modelName,])]),
            ],[
                'text' => 'Список автозапчастей',
            ]];
        }

        return $this->render('pages.catalogs.maintenance.details',compact('detailsCatalog','detailsOffers'));
    }
}
