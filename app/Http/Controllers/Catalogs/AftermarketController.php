<?php

namespace App\Http\Controllers\Catalogs;

use App\Helpers\Models\Catalogs\Aftermarket;
use App\Helpers\Models\Price;
use App\Helpers\Support\Arr;
use App\Helpers\Support\Request;
use App\Helpers\Carts\Customer;
use App\Http\Controllers\Controller;

class AftermarketController extends Controller
{
    private $types;
    private $model; /// AfterMarket Model
    public function __construct()
    {
        $this->types = [
            'P' => 'Легковые',
            'C' => 'Грузовые',
            'M' => 'Мототехника',
        ];
        $this->model = new Aftermarket();
    }

    public function types()
    {
        $types = $this->model->types();
        //dd($types);
        if( request()->ajax() )
        {
            $json = [];
            $json['types'] = $types;
            return response()->json($json);
        }

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Неоригинальный каталог",
        ]];

        return $this->render('pages.catalogs.aftermarket.types',compact('types'));
    }
    public function marks($hash=NULL)
    {
        /// Вариант без HASH используется при добавление авто в гараж - до модификаций такая схема
        /// Так то не принципиально как данные передавать, HASH для простоты клика, как GET запрос
        if( $hash )
        {
            $hash = Arr::decryption($hash);
            $type = Arr::get($hash,'type');
        }
        else
        {
            $type = Request::rcv('type');
        }

        $marks = $this->model->marks($type);
        ///dd($marks);
        if( request()->ajax() )
        {
            $json = [];
            $json['marks'] = $marks;
            return response()->json($json);
        }

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => Arr::get($this->types,$type),
            'link' => route('amTypes')
        ],[
            'text' => "Выберите марку автомобиля",
        ]];

        return $this->render('pages.catalogs.aftermarket.marks',compact('type','marks'));
    }
    public function models($hash=NULL)
    {
        if( $hash )
        {
            $hash = Arr::decryption($hash);
            $type = Arr::get($hash,'type');
            $mark = Arr::get($hash,'mark');
        }
        else
        {
            $type = Request::rcv('type');
            $mark = Request::rcv('mark');
        }


        $models = $this->model->models($type,$mark);
        if( request()->ajax() )
        {
            $json = [];
            $json['models'] = $models;
            return response()->json($json);
        }

        $_mark = $this->model->mark($type,$mark);
        $markName = Arr::get($_mark,'manuName');

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => Arr::get($this->types,$type),
            'link' => route('amTypes')
        ],[
            'text' => "$markName",
            'link' => route('amMarks',['hash'=>Arr::encryption(['type'=>$type])])
        ],[
            'text' => 'Выберите модификацию',
        ]];

        /// table || grid
        return $this->render('pages.catalogs.aftermarket.models.grid',compact('type','mark','models'));
    }
    public function vehicles($hash=NULL)
    {
        if( $hash )
        {
            $hash = Arr::decryption($hash);
            $type = Arr::get($hash,'type');
            $mark = Arr::get($hash,'mark');
            $model = Arr::get($hash,'model');
        }
        else
        {
            $type = Request::rcv('type');
            $mark = Request::rcv('mark');
            $model = Request::rcv('model');
        }

        $vehicles = $this->model->vehicles($type,$mark,$model);
        if( request()->ajax() )
        {
            $json = [];
            $json['vehicles'] = $vehicles;
            return response()->json($json);
        }

        $_model = $this->model->model($type,$model);
        $markName = Arr::get($_model,'manuName');
        $modelName = Arr::get($_model,'modelName');

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => Arr::get($this->types,$type),
            'link' => route('amTypes')
        ],[
            'text' => "$markName",
            'link' => route('amMarks',['hash'=>Arr::encryption(['type'=>$type])])
        ],[
            'text' => "$modelName",
            'link' => route('amModels',['hash'=>Arr::encryption(['type'=>$type,'mark'=>$mark])])
        ],[
            'text' => 'Выберите модификацию'
        ]];

        /// table || grid
        return $this->render('pages.catalogs.aftermarket.vehicles.grid',compact('type','mark','model','vehicles'));
    }
    public function tree($hash)
    {
        $hash = Arr::decryption($hash);
        $type = Arr::get($hash,'type');
        $mark = Arr::get($hash,'mark');
        $model = Arr::get($hash,'model');
        $car = Arr::get($hash,'car');

        $res = $this->model->tree($type,$car);
        /*/ /// parentNodeId // assemblyGroupNodeId
        usort($tree, function($a,$b) {
            $a = $a['parentNodeId'];
            $b = $b['parentNodeId'];
            return ($a == $b) ? 0 : ( ($a < $b) ? -1 : 1 );
        });
        //*/
        $tree = Arr::buildTree($res,'parentNodeId','assemblyGroupNodeId');
        if( request()->ajax() )
        {
            $json = [];
            $json['tree'] = ( Request::rcv('simple') ) ? $res : $tree;
            return response()->json($json);
        }

        $_vehicle = $this->model->vehicle($type,$car);
        ///dd($_vehicle);
        $title = Arr::get($_vehicle,'fulldescription');

        $garage = Request::rcv('garage');
        if( $garage )
        {
            $this->breads = [[
                'text' => 'Главная',
                'link' => route('home'),
                'icon' => '<span class="oi oi-home" title="Главная"></span>',
                'hide' => 1
            ],[
                'text' => 'Гараж',
                'link' => route('garage')
            ],[
                'text' => 'Дерево узлов',
            ]];
            $paramsURI = ['type'=>$type,'car'=>$car,'garage'=>$garage,'node'=>NULL];
        }
        else
        {
            $markName = Arr::get($_vehicle,'manuName');
            $modelName = Arr::get($_vehicle,'modelName');
            $vehicleName = Arr::get($_vehicle,'description');

            $this->breads = [[
                'text' => 'Главная',
                'link' => route('home'),
                'icon' => '<span class="oi oi-home" title="Главная"></span>',
                'hide' => 1
            ],[
                'text' => Arr::get($this->types,$type),
                'link' => route('amTypes')
            ],[
                'text' => "$markName",
                'link' => route('amMarks',['hash'=>Arr::encryption(['type'=>$type])])
            ],[
                'text' => "$modelName",
                'link' => route('amModels',['hash'=>Arr::encryption(['type'=>$type,'mark'=>$mark])])
            ],[
                'text' => $vehicleName,
                'link' => route('amVehicles',['hash'=>Arr::encryption(['type'=>$type,'mark'=>$mark,'model'=>$model])]),
            ],[
                'text' => 'Дерево узлов',
            ]];
            $paramsURI = ['type'=>$type,'mark'=>$mark,'model'=>$model,'car'=>$car,'node'=>NULL];
        }

        $treeHTML = $this->buildTreeHTML($tree,[
            'params'=>$paramsURI,
            'nid'=>'assemblyGroupNodeId',
            ///
            'type'=>$type,
            'car'=>$car,
        ]);
        ///die($treeHTML);
        /// tree-sidebar || tree
        return $this->render('pages.catalogs.aftermarket.tree-sidebar',compact('type','mark','model','car','title','treeHTML'));
    }
    public function details($hash)
    {
        if( Request::rcv('inner') )
        {
            $type = Request::rcv('type');
            $car = Request::rcv('car');
            $node = Request::rcv('node');
        }
        else
        {
            $hash = Arr::decryption($hash);
            $type = Arr::get($hash,'type');
            $mark = Arr::get($hash,'mark');
            $model = Arr::get($hash,'model');
            $car = Arr::get($hash,'car');
            $node = Arr::get($hash,'node');
        }


        $_vehicle = $this->model->vehicle($type,$car);
        $vehicle = Arr::get($_vehicle,'fulldescription');

        /// 32532;P
        $mPrice = new Price();
        $code = "$node;$type";
        $res = $mPrice->searchByAuto($car,$code,$vehicle);
        ///dd($res);
        $offers = [];
        if( $IDSearchCode = Arr::get($res,'IDSearchCode'))
        {
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $StockID = Customer::singleton()->getStockIDGlobal();
            $PriceLevelID = Customer::singleton()->getPriceLID();
            $StockTypeFilter = 'StockAll';
            $SearchFilter = 'ByCarNode';
            $SearchValue = $IDSearchCode;
            $SortData = '';
            ///pp([$recentlyRequest,$StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData]);
            $offers = $mPrice->priceLoad($StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData);
            ///dd($offers);
            if( @Arr::get($offers[0],'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => @Arr::get($offers[0],'mess'),
                ];
                $offers = [];
            }
            else if( !empty($offers) ) array_walk($offers, function(&$row)
            {
                $row->more = route('goodsInfo',['hash' => Arr::encryption([
                    'artbrID' => (int) Arr::get($row,'id_rbArticles'),
                    'brandID' => (int) Arr::get($row,'id_rbBrands'),
                    'brand'   => Arr::get($row,'brand'),
                    'code'    => Arr::get($row,'code'),
                    'goodsID' => (int) Arr::get($row,'GoodsID'),
                    ///
                    'offer' => [
                        'qty'  => (int) Arr::get($row,'qty')*1,
                        'step' => (int) Arr::get($row,'cost_for_qty'),
                        'rbg'  => (int) Arr::get($row,'GoodsID'),
                        'tzp'  => Arr::get($row,'tzp'),
                        'std'  => (int) Arr::get($row,'id_rbStock'),
                        'pld'  => (int) Arr::get($row,'id_rbStockStoragePlace'),
                        'reward' => Customer::singleton()->getRewardPoints(),
                    ],
                ])
                ]);
            });
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }
        else
        {
            $this->flashers[] = [
                'type' => 'error',
                'text' => Arr::get($res,'mess'),
            ];
        }

        $res = $this->model->details($type,$car,$node);

        $detailsCatalog = view('pages.catalogs.aftermarket.details.catalog',['details' => $res])->render();
        $detailsOffers = view('pages.catalogs.aftermarket.details.offers2',['offers' => $offers])->render();
        if( request()->ajax() )
        {
            if( Request::rcv('inner') )
            {
                return response()->json([
                    'inner' => 1,
                    'details' => $detailsOffers.$detailsCatalog,
                    ///'detailsCatalog' => $detailsCatalog,
                    ///'detailsOffers' => $detailsOffers
                ]);
            }
            else
            {
                return response()->json(['detailsCatalog'=>$res,'detailsOffers'=>$res,]);
            }
        }


        $garage = Request::rcv('garage');
        if( $garage )
        {
            $this->breads = [[
                'text' => 'Главная',
                'link' => route('home'),
                'icon' => '<span class="oi oi-home" title="Главная"></span>',
                'hide' => 1
            ],[
                'text' => 'Гараж',
                'link' => route('garage')
            ],[
                'text' => 'Дерево узлов',
                'link' => route('amTree',['hash'=>Arr::encryption(['type'=>$type,'mark'=>$mark,'model'=>$model,'car'=>$car])])
            ],[
                'text' => 'Список автозапчастей',
            ]];
        }
        else
        {
            $this->breads = [[
                'text' => Arr::get($this->types,$type),
                'link' => route('amTypes')
            ],[
                'text' => Arr::get($this->types,$type),
                'link' => route('amTypes')
            ],[
                'text' => "markName",
                'link' => route('amMarks',['hash'=>Arr::encryption(['type'=>$type])])
            ],[
                'text' => "modelName",
                'link' => route('amModels',['hash'=>Arr::encryption(['type'=>$type,'mark'=>$mark])])
            ],[
                'text' => "vehicleName",
                'link' => route('amVehicles',['hash'=>Arr::encryption(['type'=>$type,'mark'=>$mark,'model'=>$model])]),
            ],[
                'text' => "treeNodeName",
                'link' => route('amTree',['hash'=>Arr::encryption(['type'=>$type,'mark'=>$mark,'model'=>$model,'car'=>$car])]),
            ],[
                'text' => 'Список автозапчастей',
            ]];
        }

        return $this->render('pages.catalogs.aftermarket.details',compact('detailsCatalog','detailsOffers'));
    }

    public function buildTreeHTML($items,$params=[])
    {
        $dom = new \DOMDocument();

        $ul  = $dom->appendChild( new \DOMElement( "ul" ) );
        $ul->setAttribute( "id", "tz-tree" );
        $ul->setAttribute( "class", "am-tree-nodes" );
        ///$ul->setAttribute( "class", "ul-treefree ul-dropfree" );

        $counter = 0; /// Вместо ID
        $placement = function($items,$root,$params,&$counter) use (&$placement)
        {
            ///++$counter;
            foreach( $items AS $f )
            {
                ///++$counter;

                $li = $root->appendChild( new \DOMElement( "li" ) );

                $text = Arr::get($f,'assemblyGroupName');

                if( $children = Arr::get($f,'children') )
                {
                    $dID = ++$counter;
                    ///$dID = md5($text);

                    /// Название
                    $a = $li->appendChild( new \DOMElement( "a" ) );
                    $a->appendChild( new \DOMText( $text ) );
                    $a->setAttribute("id","a{$dID}");
                    $a->setAttribute("data-group","$dID");
                    ///$a->setAttribute("onClick","yulsun.catalogs.tree('$dID',event)");

                    /// Рекурсия
                    $ul = $li->appendChild( new \DOMElement( "ul" ) );
                    $ul->setAttribute('id',$dID);
                    $ul->setAttribute('class',"tree-submenu !subgroup subgroup$dID");

                    ///++$counter;
                    $placement($children,$ul,$params,$counter);
                }
                else
                {
                    /// Название
                    $a = $li->appendChild( new \DOMElement( "a" ) );
                    $a->appendChild( new \DOMText( $text ) );

                    /// Действие
                    $node = Arr::get($f,Arr::get($params,'nid'));
                    $_arr = Arr::get($params,'params');
                    $_arr['node'] = $node;
                    $a->setAttribute("href",route('amDetails',['hash'=>Arr::encryption($_arr)]));

                    /// Накидываем
                    $a->setAttribute("class",'pricing');
                    $a->setAttribute("data-type",Arr::get($params,'type'));
                    $a->setAttribute("data-car",Arr::get($params,'car'));
                    $a->setAttribute("data-node",$node);
                }
            }
        };
        $placement($items,$ul,$params,$counter);

        $html = $dom->saveHTML();
        return $html;
    }
}
