<?php

namespace App\Http\Controllers;

use App\Helpers\Carts\Basket;
use App\Helpers\Models\Goods;
use App\Helpers\Support\Arr;

class GoodsController extends Controller
{
    public function info($hash)
    {
        $arr = Arr::decryption($hash);

        $artbrID = Arr::get($arr,'artbrID');
        $brandID = Arr::get($arr,'brandID');
        $brand   = Arr::get($arr,'brand');
        $code    = Arr::get($arr,'code');
        $GoodsID = Arr::get($arr,'goodsID');
        $offer   = Arr::get($arr,'offer');
        $back    = Arr::get($arr,'back');

        $stockName = Arr::get($arr,'stockName');


        $product_info = (new Goods())->getAll($artbrID,$brandID,$brand,$code,$GoodsID);
        ///ddd($product_info);

        $title = Arr::get($product_info,'meta_title',Arr::get($product_info,'name'));
        ///$this->document->setTitle($title);

        $heading = Arr::get($product_info,'meta_h1',Arr::get($product_info,'name'));
        $tabReview = sprintf("Отзывов (%s)", (int)Arr::get($product_info,'reviews'));

        $image = Arr::get($product_info,'image');
        $images = Arr::get($product_info,'images');

        $settingsGlobal = Arr::get($product_info,'settings_global');
        $settingsLocal = Arr::get($product_info,'settings_local');

        $textMinimum = sprintf('Минимальное количество заказа этого товара %s',Arr::get($offer,'step'));
        $basketTabID = Basket::singleton()->getTabActive();

        $arrApplyGlobal = Arr::get($product_info,'apply_global');
        $arrApplyGlobalFilter = [ 'marks' => [], 'models' => [] ];
        foreach( $arrApplyGlobal AS &$app )
        {
            ///array_push($arrApplyGlobalFilter['marks'],[]);
            ///array_push($arrApplyGlobalFilter['models'],[]);

            $make = Arr::get($app,'make');
            $make = strtoupper($make);
            $makeCode = crc32($make);

            $model = Arr::get($app,'model');
            $model = str_replace(''.Arr::get($app,'constructionType'),'',''.$model);
            $modelCode = crc32($model);

            $arrApplyGlobalFilter['marks'][$makeCode] = $make;
            $arrApplyGlobalFilter['models'][$makeCode][$modelCode] = $model;

            $app->make = $make;
            $app->makeCode = $makeCode;
            $app->model = $model;
            $app->modelCode = $modelCode;
        }
        $applyGlobal = $arrApplyGlobal;
        $applyGlobalFilter = $arrApplyGlobalFilter;

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<i class="oi oi-home" title="Главная"></i>',
            'hide' => 1
        ]];
        if( $back ) array_push($this->breads,$back);
        array_push($this->breads,['text'=>$heading]);

        $reviewStatus = 1;    /// Разрешены отзыва ? | 1-да, 0-нет
        $addImagesLimit = 4;  /// Текущей версткой всего 4 предусмотренно
        $popupWidth = '500';  /// Ширина попап фото
        $popupHeight = '500'; /// Высота попап фото
        return $this->render('pages.goods.info',compact(
            'title','heading','brand','code','tabReview',
            'basketTabID','offer','addImagesLimit','image','images',
            'popupWidth','popupHeight','textMinimum','settingsGlobal','settingsLocal','applyGlobal','applyGlobalFilter','reviewStatus'
            ,'stockName'
        ));
    }
}
