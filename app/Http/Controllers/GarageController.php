<?php

namespace App\Http\Controllers;

use App\Helpers\Carts\Customer;
use App\Helpers\Models\Catalogs\Aftermarket;
use App\Helpers\Models\Garage;
use App\Helpers\Support\Arr;
use App\Helpers\Support\Request;
use App\Helpers\Support\Session;
use Illuminate\Support\Facades\Validator;

class GarageController extends Controller
{
    public function main()
    {
        $add = route('garageAdd');
        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<i class="oi oi-home" title="Главная"></i>',
            'hide' => 1
        ],[
            'text' => "Личный кабинет",
            'link' => route('account'),
        ],[
            'text' => "Гараж",
            'html' => '&nbsp;&nbsp;&nbsp;<a class="btn btn-sm-tz btn-light pl-1" href="'.$add.'" aria-label="Гараж">'
                .'<i class="fa fa-plus"></i><span class="d-none d-md-inline ml-1" id="compare-total">Добавить авто</span></a>',
        ]];

        $cars = (new Garage())->vehicles();
        ///ddd($cars);

        return $this->render('pages.garage.main',compact('cars'));
    }
    public function add()
    {
        $auto = Request::rcv('auto');
        $vin  = Request::rcv('vin');
        $year = Request::rcv('year');
        $year_max = date('Y');
        $year_min = 1950;

        $types = (new Aftermarket())->types();

        $type   = Request::rcv('type');
        $vehicle = (int) Request::rcv('vehicle');
        $engine = Request::rcv('engine');
        $note   = Request::rcv('note');

        if( $this->buttonPressed('btnGarageAdd') )
        {
            ///ddd($data);
            $validator = Validator::make(['auto'=>$auto,'vin'=>$vin,'year'=>$year], [
                'auto' => 'required|min:3',
                'vin'  => 'required|min:5|max:17',
                'year' => 'required|gte:1950|lte:'.date('Y'),
            ]);

            if( $validator->fails() ) return redirect()->route('garageAdd')->withErrors($validator)->withInput();

            ///dd($this->request->post);

            $res = (new Garage())->add($auto,$engine,$vin,$year,$note,$vehicle,$type);
            ///dd($res);
            if( Arr::get($res,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => Arr::get($res,'mess'),
                ];
            }
            else
            {
                $this->flashers[] = [
                    'type' => 'success',
                    'text' => 'Автомобиль успешно добавлен',
                ];
                return redirect()->route('garage')->with('flashers',$this->flashers);
            }
        }

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<i class="oi oi-home" title="Главная"></i>',
            'hide' => 1
        ],[
            'text' => "Личный кабинет",
            'link' => route('account'),
        ],[
            'text' => "Гараж",
            'link' => route('garage'),
        ],[
            'text' => "Добавление автомобиля",
        ]];

        return $this->render('pages.garage.add',compact('types','type','auto','vin','year','year_min','year_max','engine','note'));
    }

}
