<?php

namespace App\Http\Controllers\Room;

use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    public function dashboard()
    {
        return $this->render('room.pages.store.dashboard');
    }
}
