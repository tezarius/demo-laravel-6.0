<?php

namespace App\Http\Controllers\Room;

use App\Helpers\Carts\Store;
use App\Helpers\Models\AnyParams;
use App\Helpers\Models\Category;
use App\Helpers\Models\File;
use App\Helpers\Support\Arr;
use App\Helpers\Support\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    private $mCategory;
    public function __construct()
    {
        $this->mCategory = new Category();
    }

    public function main()
    {
        $categories = $this->mCategory->flat();
        $categories = Arr::buildTree($categories,'id_parent','id');
        ///ddd($categories);
        $treeHTML = Store::singleton()->buildTreeHTMLCategory($categories,[
            'fields' => [
                'nodeID' => 'id',
                'nodeName' => 'name',
                'nodeIcon' => 'icon_url',
                'iconType' => 'path', /// path||class
                'children' => 'children',

                'rootID' => 'tz-tree',
                'rootClass' => 'tz-tree',
                'subMenuClass' => 'tree-submenu',
                'itemClass' => 'exile',
            ],
            'route' => [
                'params' => [],
                'name' => '',
            ],
        ]);

        $paramInfo = (new AnyParams())->get('component-category-recommended');
        $hash = Arr::get($paramInfo,'value');
        $content = Arr::decryption($hash);
        $recommended = Arr::get($content,'nodeName');

        return $this->render('room.pages.categories.main',['treeHTML'=>$treeHTML,'recommended'=>$recommended]);
    }
    public function iconAdd(Request $request)
    {
        $json = [];

        ///$json['hasFile'] = 0;
        if( $request->hasFile('icon') )
        {
            ///$json['hasFile'] = 1;
            ///$json['isValid'] = $request->file('icon')->isValid();

            $file = $request->file('icon');
            $info = File::validSave($file,'icons/categories');

            $nodeID = (int) $request->input('nodeID');
            $iconURI = Arr::get($info,'path');
            $iconURI = "/storage/$iconURI";
            $res = $this->mCategory->iconAdd($nodeID,$iconURI);

            $json['isError'] = Arr::get($res,'isError');
            $json['message'] = Arr::get($res,'mess');


            ///$string = (int) $request->input('string');
            //

            ///$json['info'] = $res;
            ///$json['hasFile'] = 2;
        }

        ///$json['status'] = 1;
        ///$json['$_FILES'] = $_FILES;
        ///$json['$_POST'] = $_POST;

        return response()->json($json);
    }
    public function iconDel(Request $request)
    {
        $json = [];

        $nodeID = (int) $request->input('nodeID');
        $res = $this->mCategory->icondel($nodeID);
        $json['$res'] = $res;

        $json['isError'] = Arr::get($res,'isError');
        $json['message'] = Arr::get($res,'mess');

        $icon = public_path(Arr::get($res,'path'));
        $json['path'] = $icon;
        $json['delete'] = @unlink($icon);

        return response()->json($json);
    }
    public function recommended(Request $request)
    {
        $nodeID = (int) $request->rcv('nodeID');
        $nodeName = $request->rcv('nodeName');

        $hash = Arr::encryption(['nodeID'=>$nodeID,'nodeName'=>$nodeName]);
        $res = (new AnyParams())->put('component-category-recommended',$hash);

        return response()->json([
            ///'$_POST' => $_POST,
            ///'$nodeID' => $nodeID,
            ///'$nodeName' => $nodeName,
            ///'$res' => $res,
            'isGood' => Arr::get($res,'isGood'),
            'isError' => Arr::get($res,'isError'),
            'message' => Arr::get($res,'mess','Данные успешно сохранены!'),
        ]);
    }
}
