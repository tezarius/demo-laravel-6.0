<?php

namespace App\Http\Controllers\Room;

use App\Helpers\Models\AnyParams;
use App\Helpers\Models\File;
use App\Helpers\Support\Arr;
use App\Helpers\Support\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    private $mAnyParams;
    public function __construct()
    {
        $this->mAnyParams = new AnyParams();
    }

    public function refund(Request $request)
    {
        $paramInfo = $this->mAnyParams->get('page-refund-content');
        $content = Arr::get($paramInfo,'value');
        if( $request->ajax() )
        {
            $content = $request->input('content','');
            $res = $this->mAnyParams->put('page-refund-content',$content);
            return response()->json([
                'isGood' => Arr::get($res,'isGood'),
                'isError' => Arr::get($res,'isError'),
                'message' => Arr::get($res,'mess','Данные успешно сохранены!'),
            ]);
        }
        $header = 'Информация о возврате товара';
        $route = 'roomPageRefund';
        return $this->render('room.pages.html.ckeditor',compact('header','content','route'));
    }
    public function delivery(Request $request)
    {
        $paramInfo = $this->mAnyParams->get('page-delivery-content');
        $content = Arr::get($paramInfo,'value');
        if( $request->ajax() )
        {
            $content = $request->input('content','');
            $res = $this->mAnyParams->put('page-delivery-content',$content);
            return response()->json([
                'isGood' => Arr::get($res,'isGood'),
                'isError' => Arr::get($res,'isError'),
                'message' => Arr::get($res,'mess','Данные успешно сохранены!'),
            ]);
        }
        $header = 'Информация о доставке';
        $route = 'roomPageDelivery';
        return $this->render('room.pages.html.ckeditor',compact('header','content','route'));
    }
    public function payment(Request $request)
    {
        $paramInfo = $this->mAnyParams->get('page-payment-content');
        $content = Arr::get($paramInfo,'value');
        if( $request->ajax() )
        {
            $content = $request->input('content','');
            $res = $this->mAnyParams->put('page-payment-content',$content);
            return response()->json([
                'isGood' => Arr::get($res,'isGood'),
                'isError' => Arr::get($res,'isError'),
                'message' => Arr::get($res,'mess','Данные успешно сохранены!'),
            ]);
        }
        $header = 'Оплата';
        $route = 'roomPagePayment';
        return $this->render('room.pages.html.ckeditor',compact('header','content','route'));
    }
    public function contacts(Request $request)
    {
        $paramInfo = $this->mAnyParams->get('page-contacts-content');
        $content = Arr::get($paramInfo,'value');
        if( $request->ajax() )
        {
            $content = $request->input('content','');
            $res = $this->mAnyParams->put('page-contacts-content',$content);
            return response()->json([
                'isGood' => Arr::get($res,'isGood'),
                'isError' => Arr::get($res,'isError'),
                'message' => Arr::get($res,'mess','Данные успешно сохранены!'),
            ]);
        }
        $header = 'Страница с контактами';
        $route = 'roomPageContacts';
        return $this->render('room.pages.html.ckeditor',compact('header','content','route'));
    }
    public function about(Request $request)
    {
        $paramInfo = $this->mAnyParams->get('page-about-content');
        $content = Arr::get($paramInfo,'value');
        if( $request->ajax() )
        {
            $content = $request->input('content','');
            $res = $this->mAnyParams->put('page-about-content',$content);
            return response()->json([
                ///'$content' => $content,
                'isGood' => Arr::get($res,'isGood'),
                'isError' => Arr::get($res,'isError'),
                'message' => Arr::get($res,'mess','Данные успешно сохранены!'),
            ]);
        }
        $header = 'Информация о возврате товара';
        $route = 'roomPageAbout';
        return $this->render('room.pages.html.ckeditor',compact('header','content','route'));
    }
    public function offer(Request $request)
    {
        $paramInfo = $this->mAnyParams->get('page-offer-content');
        $content = Arr::get($paramInfo,'value');
        if( $request->ajax() )
        {
            $content = $request->input('content','');
            $res = $this->mAnyParams->put('page-offer-content',$content);
            return response()->json([
                'isGood' => Arr::get($res,'isGood'),
                'isError' => Arr::get($res,'isError'),
                'message' => Arr::get($res,'mess','Данные успешно сохранены!'),
            ]);
        }
        $header = 'Текст публичной оферты';
        $route = 'roomPageOffer';
        return $this->render('room.pages.html.ckeditor',compact('header','content','route'));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function counters(Request $request)
    {
        $paramInfo = $this->mAnyParams->get('component-counters');
        $content = Arr::get($paramInfo,'value');
        if( $request->ajax() )
        {
            $content = $request->input('content','');
            $res = $this->mAnyParams->put('component-counters',$content);
            return response()->json([
                ///'$content' => $content,
                'isGood' => Arr::get($res,'isGood'),
                'isError' => Arr::get($res,'isError'),
                'message' => Arr::get($res,'mess','Данные успешно сохранены!'),
            ]);
        }
        $header = 'Счетчики и метрика на сайт';
        $route = 'roomPageCounters';
        return $this->render('room.pages.html.textarea',compact('header','content','route'));
    }
    public function map(Request $request)
    {
        $paramInfo = $this->mAnyParams->get('component-map');
        $content = Arr::get($paramInfo,'value');
        if( $request->ajax() )
        {
            $content = $request->input('content','');
            $res = $this->mAnyParams->put('component-map',$content);
            return response()->json([
                ///'$content' => $content,
                ///'content' => @$_POST['content'],
                'isGood' => Arr::get($res,'isGood'),
                'isError' => Arr::get($res,'isError'),
                'message' => Arr::get($res,'mess','Данные успешно сохранены!'),
            ]);
        }
        $header = 'Схема проезда';
        $route = 'roomPageMap';
        return $this->render('room.pages.html.textarea',compact('header','content','route'));
    }
    public function schedule(Request $request)
    {
        $paramInfo = $this->mAnyParams->get('component-schedule');
        $content = Arr::get($paramInfo,'value');
        if( $request->ajax() )
        {
            $content = $request->input('content','');
            $res = $this->mAnyParams->put('component-schedule',$content);
            return response()->json([
                ///'$content' => $content,
                ///'content' => @$_POST['content'],
                'isGood' => Arr::get($res,'isGood'),
                'isError' => Arr::get($res,'isError'),
                'message' => Arr::get($res,'mess','Данные успешно сохранены!'),
            ]);
        }
        $header = 'График работы';
        $route = 'roomSchedule';
        return $this->render('room.pages.html.ckeditor',compact('header','content','route'));
    }
    public function helloChat(Request $request)
    {
        $paramInfo = $this->mAnyParams->get('component-hello-chat');
        $content = Arr::get($paramInfo,'value');
        if( $request->ajax() )
        {
            $content = $request->input('content','');
            $res = $this->mAnyParams->put('component-hello-chat',$content);
            return response()->json([
                ///'$content' => $content,
                ///'content' => @$_POST['content'],
                'isGood' => Arr::get($res,'isGood'),
                'isError' => Arr::get($res,'isError'),
                'message' => Arr::get($res,'mess','Данные успешно сохранены!'),
            ]);
        }
        $header = 'Приветсвтвие в чат';
        $route = 'roomHelloChat';
        return $this->render('room.pages.html.ckeditor',compact('header','content','route'));
    }
    public function logo(Request $request)
    {
        $paramInfo = $this->mAnyParams->get('component-logo');
        $content = Arr::get($paramInfo,'value');

        if( $request->hasFile('logo') || $hash = $request->rcv('hash') )
        {
            $json = [];
            $json['isAjax'] = $request->ajax();

            $logoPath = $msgSuccess = '';
            $isRemoval = FALSE;
            if( $request->hasFile('logo') )
            {
                $json['hasLogo'] = 1;
                $file = $request->file('logo');
                $info = File::validSave($file,'images');

                $logoPath = Arr::get($info,'path');
                $logoPath = "/storage/$logoPath";

                $msgSuccess = 'Логотип успешно сохранен!';
            }
            if( $hash )
            {
                $isRemoval = TRUE;
                $json['hasLogo'] = 0;
                $msgSuccess = 'Логотип успешно удален!';

                $hash = Arr::decryption($hash);
                $path = Arr::get($hash,'path');
                $json['isRemoved'] = @unlink(public_path($path));
            }
            $json['isRemoval'] = $isRemoval;
            $json['logoPath'] = $logoPath;

            $res = $this->mAnyParams->put('component-logo',$logoPath);
            $isGood = Arr::get($res,'isGood');

            $json['isGood'] = $isGood;
            $json['isError'] = Arr::get($res,'isError');
            $json['message'] = Arr::get($res,'mess',$msgSuccess);

            return response()->json($json);
        }

        $code = 'component-logo';
        $header = 'Установить логотип сайта';
        $route = 'roomMainLogo';
        return $this->render('room.pages.html.logo',compact('header','content','code','route'));
    }
    public function slider(Request $request)
    {
        $code = 'component-slider';
        $fieldName = 'slides';
        ///$bundle = TRUE;

        $paramInfo = $this->mAnyParams->get($code);
        $strFilePath = Arr::get($paramInfo,'value');
        $arrFilePath = ( $strFilePath ) ? explode(',',$strFilePath) : [];

        ///
        $paramInfo = $this->mAnyParams->get('component-slide-uri');
        $strLinks = Arr::get($paramInfo,'value');
        $arrLinks = Arr::decryption($strLinks);

        /// Сохранить или удалить слайд
        if( $request->hasFile($fieldName) || $request->rcv('hash') )
        {
            $json = [];
            $json['isAjax'] = $request->ajax();

            $msgSuccess = '';
            $isRemoval = FALSE;

            if( $request->hasFile($fieldName) )
            {
                $json['hasFile'] = 1;
                $files = $request->file($fieldName);

                ///$arrFilePath = [];
                foreach( $files AS $f )
                {
                    $info = File::validSave($f,'images/home/slider/top');
                    $filePath = Arr::get($info,'path');
                    $filePath = "/storage/$filePath";
                    array_push($arrFilePath,$filePath);
                }
                $strFilePath = implode(',',$arrFilePath);
                ///$json['$arrFilePath'] = $arrFilePath;
                ///$json['$strFilePath'] = $strFilePath;
                ///return response()->json($json);

                $msgSuccess = 'Слайды успешно сохранены!';
            }
            if( $hash = $request->rcv('hash') )
            {
                $isRemoval = TRUE;
                $json['hasFile'] = 0;
                $msgSuccess = 'Слайды успешно удалены!';

                $hash = Arr::decryption($hash);
                $path = Arr::get($hash,'path');
                $isRemoved = @unlink(public_path($path));
                $json['isRemoved'] = $isRemoved;

                $arrFilePath = array_diff($arrFilePath,[$path]);
                $strFilePath = implode(',',$arrFilePath);
            }
            $json['isRemoval'] = $isRemoval;
            $json['strFilePath'] = $strFilePath;

            ///return response()->json($json);
            $res = $this->mAnyParams->put($code,$strFilePath);
            $isGood = Arr::get($res,'isGood');

            $json['isGood'] = $isGood;
            $json['isError'] = Arr::get($res,'isError');
            $json['message'] = Arr::get($res,'mess',$msgSuccess);

            return response()->json($json);
        }
        /// Привязать адреса страниц к слайдам
        if( ($links = $request->rcv('links')) || $request->ajax() )
        {
            $json = [];
            ///$json['POST_1'] = $_POST;
            ///$json['POST_2'] = $request->all();
            ///$json['links'] = $links??NULL;
            if( $links )
            {
                $content = Arr::encryption($links);
                $res = $this->mAnyParams->put('component-slide-uri',$content);
                $isGood = Arr::get($res,'isGood');

                $json['isGood'] = $isGood;
                $json['isError'] = Arr::get($res,'isError');
                $json['message'] = Arr::get($res,'mess','Ссылки успешно сохранены!');

                $arrLinks = $links;
            }
            else
            {
                $json['isError'] = 1;
                $json['message'] = 'Не увидели ссылок в запросе :(';
            }
            ///
            return response()->json($json);
        }

        $header = 'Слайдер на главной страницы';
        $route = 'roomMainSlider';
        return $this->render('room.pages.html.slider',compact('header','code','fieldName','arrFilePath','arrLinks','route'));
    }
}
