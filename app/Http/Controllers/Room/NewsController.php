<?php

namespace App\Http\Controllers\Room;

use App\Helpers\Models\News;
use App\Helpers\Support\Arr;
use App\Helpers\Support\HTML;
use App\Helpers\Support\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    private $mNews;
    public function __construct()
    {
        $this->mNews = new News();
    }

    public function main()
    {
        $news = $this->mNews->all();
        return $this->render('room.pages.news.list',['news'=>$news]);
    }
    public function action($id=NULL)
    {
        if( $this->buttonPressed('btnNewsSave') )
        {
            $subject = Request::rcv('subject');
            $img = Request::rcv('img');
            $body = Request::rcv('body');
            $body = HTML::entityDecode($body);
            $body = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $body);
            $dd1 = Request::rcv('dd1');
            $dd2 = Request::rcv('dd2');
            ///dd(HTML::entityDecode($body));
            $res = ( $id )
                ? $this->mNews->upd($subject,$body,$dd1,$dd2,$img,$id)
                : $this->mNews->add($subject,$body,$dd1,$dd2,$img)
            ;
            if( (int) Arr::get($res,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => Arr::get($res,'mess'),
                ];
            }
            else
            {
                if( Arr::get($res,'isGood') ) /// Тут должны оказаться 100%, но перебздим
                {
                    $_msg = ( $id ) ? 'Новость успешно обновленна!' : 'Новость успешно размещена!';
                    return redirect()->route('roomNewsList')->with('success',$_msg);
                }
                else $this->flashers[] = [
                    'type' => 'error',
                    'text' => 'Что-то пошло не так :( [tzn_add]',
                ];
            }
        }
        elseif( $id )
        {
            $news = $this->mNews->get($id); // dd($news);
            $subject = Arr::get($news,'subject');
            $img = Arr::get($news,'img_main_url');
            $body = Arr::get($news,'body');
            ///$body = HTML::entityDecode($body);
            $dd1 = Arr::get($news,'data_start');
            $dd2 = Arr::get($news,'data_end');
        }
        else $subject = $img = $body = $dd1 = $dd2 = '';

        $header = ( $id ) ? 'Редактировать новость' : 'Добавить новость';
        $params = [
            'header'=>$header,
            'subject'=>$subject,
            'img'=>$img,
            'body'=>$body,
            'dd1'=>$dd1,
            'dd2'=>$dd2,
        ]; // dd($params);
        return $this->render('room.pages.news.action',$params);
    }
    public function delete($id)
    {
        $res = $this->mNews->del($id);
        if( (int) Arr::get($res,'isError') )
            $message = ['type'=>'error','text'=>Arr::get($res,'mess')];
        else if( Arr::get($res,'isGood') )
            $message = ['type'=>'success','text'=>'Новость успешно удалена!'];
        else
            $message = ['type'=>'error','text'=>'Что-то пошло не так :( [tzn_del]'];

        return $this->jecho($message);
    }
}
