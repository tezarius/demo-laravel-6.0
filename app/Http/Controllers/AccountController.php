<?php

namespace App\Http\Controllers;

use App\Helpers\Carts\Customer;
use App\Helpers\Support\Arr;
use App\Helpers\Support\Request;
use App\Helpers\Support\Session;
use App\Rules\PhoneNumber;
use App\Rules\TezariusUserUnique;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///   Б Е З  А В Т О Р И З А Ц И И   ///////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function login(Request $request)
    {
        if( Customer::singleton()->isLogged() ) return redirect()->route('account');
        if( !Session::has('referer') )
        {
            $referer = request()->headers->get('referer');
            if( $referer!==route('login') ) Session::put('referer',$referer);
        }

        $this->breads = [[
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Личный кабинет",
            'link' => route('account')
        ],[
            'text' => "Авторизация",
        ]];

        $email = $request->rcv('email');
        $pass = $request->rcv('password');
        if( $this->buttonPressed('btnLogin') )
        {
            $data = [
                'email'    => $email,
                'password' => $pass,
            ];
            $validator = Validator::make($data, [
                'email'    => ['required', 'string', 'email', 'max:255'],
                'password' => 'required|min:8',
            ]);
            $validator->after(function($validator)use($email,$pass){
                if( !Customer::singleton()->login($email,$pass) ) $validator->errors()->add('enter', 'Пользователь не найден');
            });
            if( $validator->fails() ) return redirect()->route('login')->withErrors($validator)->withInput();

            $redirect = Session::get('referer');
            if( $redirect && $redirect!==route('login')  )
            {
                Session::remove('referer');
                return redirect(str_replace('&amp;','&',$redirect));
            }
            else return redirect()->route('home');
        }

        return $this->render('pages.account.login');
    }
    public function logout()
    {
        Customer::singleton()->logout();
        return redirect()->route('home');
    }
    public function register(Request $request)
    {
        if( Customer::singleton()->isLogged() ) return redirect()->route('account');
        if( !Session::get('referer') )
        {
            $referer = request()->headers->get('referer');
            if( $referer!==route('register') ) Session::put('referer',$referer);
        }

        $email = $request->rcv('email');
        $pass = $request->rcv('password');

        ///if( $StockID && get($this->request->server,'REQUEST_METHOD')=='POST' && $this->validateRegister() )
        if( $this->buttonPressed('btnRegister') )
        {
            $data = [
                'firstname' => $request->rcv('firstname'),
                'lastname'  => $request->rcv('lastname'),
                'email'     => $email,
                'password'  => $pass,
                'password_confirmation' => $request->rcv('password_confirmation'),
                'telephone' => $request->rcv('telephone'),
                'agree'     => $request->rcv('agree'),
                'StockID'   => $this->officeChoosed(),
            ];
            ///ddd($data);
            $validator = Validator::make($data, [
                'firstname' => 'required|min:2|max:20',
                'lastname'  => 'required|min:2|max:20',
                'email'     => ['required', 'string', 'email', 'max:255', new TezariusUserUnique],
                'password'  => 'required|min:8|confirmed',
                'password_confirmation' => 'required|min:8',
                'telephone' => ['required', new PhoneNumber, new TezariusUserUnique],
                'agree'     => 'required',
                'StockID'   => 'required|not_in:0',
            ]);

            if( $validator->fails() ) return redirect()->route('register')->withErrors($validator)->withInput();

            ///dd($data);

            $mCustomer = new \App\Helpers\Models\Customer();
            $oReg = $mCustomer->addCustomer($data);
            ///dd($oReg);
            if( (int) Arr::get($oReg,'/room/') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => Arr::get($oReg,'mess'),
                ];
            }
            else
            {
                $CustomerUID = Arr::get($oReg,'newRecId'); /// newRecId
                if( $CustomerUID )
                {
                    Session::put('CustomerUID',$CustomerUID);

                    $susMsg = $request->rcv('firstname')." ".$request->rcv('lastname')."! ".'Congratulations! Registration completed!';
                    $this->flashers[] = [
                        'type' => 'success',
                        'text' => $susMsg,
                    ];

                    if( Customer::singleton()->login($email,$pass) )
                    {
                        $redirect = Session::get('referer');
                        if( $redirect && $redirect!==route('register')  ){
                            Session::remove('referer');
                            return redirect(str_replace('&amp;','&',$redirect));
                        }
                        else return redirect()->route('account');
                    }
                    else $this->flashers[] = [
                        'type' => 'error',
                        'text' => 'Что-то пошло не так :( [tzr_2]',
                    ];
                }
                else $this->flashers[] = [
                    'type' => 'error',
                    'text' => 'Что-то пошло не так :( [tzr_1]'
                        ///.'<pre>'.print_r($oReg,TRUE).'</pre>'
                ];
            }
        }

        $this->breads = [[
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Личный кабинет",
            'link' => route('account')
        ],[
            'text' => "Регистрация",
        ]];

        return $this->render('pages.account.register');
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///   Т О Л Ь К О  А В Т О Р И З О В А Н Н Ы Е   ///////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function main()
    {
        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Личный кабинет",
        ]];

        $balanceInfo = Customer::singleton()->getBalanceInfo(TRUE);
        $discountInfo = Customer::singleton()->getDiscountInfo(TRUE);

        return $this->render('pages.account.main',compact('balanceInfo','discountInfo'));
    }
    public function edit(Request $request)
    {
        if( $this->buttonPressed('btnUserEdit') )
        {
            $data = [
                'firstname'  => $request->rcv('firstname'),
                'lastname'   => $request->rcv('lastname'),
                'patronymic' => $request->rcv('patronymic'),
                'email'      => $request->rcv('email'),
                'telephone'  => $request->rcv('telephone'),
            ];
            ///ddd($data);
            $validator = Validator::make($data, [
                'firstname'  => 'required|min:2|max:20',
                'lastname'   => 'required|min:2|max:20',
                'patronymic' => 'required|min:2|max:20',
                'email'      => ['required', 'string', 'email', 'max:255'],
                'telephone'  => ['required', new PhoneNumber],
            ]);
            //*/
            $validator->after(function($validator)use($request){
                $org_name = $request->rcv('org_name');
                $org_inn  = $request->rcv('org_inn');
                $org_kpp  = $request->rcv('org_kpp');
                $org_ogrn = $request->rcv('org_ogrn');
                $org_addr = $request->rcv('org_addr');
                if( ($org_name||$org_inn||$org_kpp||$org_ogrn||$org_addr) && (!$org_name||!$org_inn||!$org_kpp||!$org_ogrn||!$org_addr) )
                    $validator->errors()->add('org_false', 'Нужно заполнить сразу все поля для организации');
            });
            //*/

            if( $validator->fails() ) return redirect()->route('accountEdit')->withErrors($validator)->withInput();

            $mCustomer = new \App\Helpers\Models\Customer();
            $oReg = $mCustomer->editCustomer(Customer::singleton()->getID(),Customer::singleton()->getStockID(),$data);
            ///dd($oReg);
            if( (int) Arr::get($oReg,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => Arr::get($oReg,'mess'),
                ];
            }
            else
            {
                $this->flashers[] = [
                    'type' => 'success',
                    'text' => 'Данные изменены!',
                ];
                return redirect()->route('account')->with('flashers',$this->flashers);
            }
        }

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Личный кабинет",
            'link' => route('account')
        ],[
            'text' => "Редактировать информацию",
        ]];

        $user = Customer::singleton()->getCustomer();
        ///dd($user);

        return $this->render('pages.account.edit',compact('user'));
    }
    public function password(Request $request)
    {
        if( $this->buttonPressed('btnPassword') )
        {
            $email = Customer::singleton()->getLogin();
            $password = $request->rcv('password');
            $data = [
                'password_old' => $request->rcv('password_old'),
                'password' => $password,
                'password_confirmation' => $request->rcv('password_confirmation'),
            ];
            $validator = Validator::make($data, [
                'password_old'  => 'required|in:'.Customer::singleton()->getPasswd(),
                'password'  => 'required|min:8|confirmed',
                'password_confirmation' => 'required|min:8',
            ]);

            if( $validator->fails() ) return redirect()->route('password')->withErrors($validator)->withInput();

            $mCustomer = new \App\Helpers\Models\Customer();
            $oReg = $mCustomer->editPassword(Customer::singleton()->getID(),$email,$password);
            ///dd($oReg);
            if( (int) Arr::get($oReg,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => Arr::get($oReg,'mess'),
                ];
            }
            else
            {
                if( Customer::singleton()->login($email,$password) )
                {
                    $this->flashers[] = [
                        'type' => 'success',
                        'text' => 'Пароль изменен!',
                    ];
                    return redirect()->route('account')->with('flashers',$this->flashers);
                }
                else $this->flashers[] = [
                    'type' => 'error',
                    'text' => 'Что-то пошло не так :( [tzp_1]',
                ];
            }
        }

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Личный кабинет",
            'link' => route('account')
        ],[
            'text' => "Изменить пароль",
        ]];

        $user = Customer::singleton()->getCustomer();
        ///dd($user);

        return $this->render('pages.account.password',compact('user'));
    }
    public function newsletter(Request $request)
    {
        $mCustomer = new \App\Helpers\Models\Customer();

        if( $this->buttonPressed('btnNotified') )
        {
            $res = $mCustomer->editNewsletter($request->rcv('notified',[]));
            if( Arr::get($res,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => Arr::get($res,'mess'),
                ];
            }
            else
            {
                $this->flashers[] = [
                    'type' => 'success',
                    'text' => 'Настройки сохранены!',
                ];
                return redirect()->route('account')->with('flashers',$this->flashers);
            }
        }

        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => "Личный кабинет",
            'link' => route('account')
        ],[
            'text' => "Изменить пароль",
        ]];

        $notices = $mCustomer->getNewsletter();
        ///dd($user);

        return $this->render('pages.account.newsletter',compact('notices'));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
