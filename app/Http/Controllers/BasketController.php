<?php

namespace App\Http\Controllers;

use App\Helpers\Carts\Basket;
use App\Helpers\Carts\Customer;
use App\Helpers\Support\Arr;
use App\Helpers\Support\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use App\Helpers\Models\Basket AS mBasket;

class BasketController extends Controller
{
    public function main()
    {
        $data = [];
        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<span class="oi oi-home" title="Главная"></span>',
            'hide' => 1
        ],[
            'text' => 'Корзина'
        ]];

        $tabActive = Basket::singleton()->getTabActive();
        $data['tabActive_cookie'] = $tabActive;
        $tabs = Basket::singleton()->getTabs();
        /// Проверям не удалена ли активная корзина
        $isExist = 0; foreach( $tabs AS $t ) if( (int)Arr::get($t,'tabID')===$tabActive ){ $isExist = 1; break; }
        $data['tabActive_isExist'] = $tabActive;
        if( !$isExist )
        {
            /// Делаем активной по последней модифицированной
            $LastUpdTimeSave = 0;
            foreach( $tabs AS $t )
            {
                $LastUpdTime = date('YmdHis',strtotime(Arr::get($t,'LastUpdTime')));
                if( $LastUpdTime > $LastUpdTimeSave )
                {
                    $LastUpdTimeSave = $LastUpdTime;
                    $tabActive = Arr::get($t,'tabID');
                }
            }
            Basket::singleton()->setTabActive($tabActive);
        }
        $data['tabsList'] = $tabs;
        $data['tabActive'] = $tabActive;

        $products = Basket::singleton()->getProducts();
        ///ddd($products[0]);
        $data['products'] = $products;
        $data['reward'] = Customer::singleton()->getRewardPoints();
        $data['coupon'] = Session::get('coupon');
        ///ddd($data);

        $view = 'table'; /// list || table
        return $this->render("pages.basket.{$view}",$data);
    }
    public function add(Request $request)
    {
        $json = [];

        ///dd([$this->request->post]);
        $ShopID = (int) Customer::singleton()->getStockIDGlobal();
        $UserID = (int) Customer::singleton()->getID();
        $SessID = (string) Cookie::get('tz_sid');
        $PriceLID = (int) Customer::singleton()->getPriceLID();

        $tabActive = Basket::singleton()->getTabActive();
        $tabs = Basket::singleton()->getTabs();
        $LastUpdTimeSave = $LastUpdTabID = 0; $TabExist = FALSE;
        foreach( $tabs AS $t )
        {
            $LastUpdTime = date('YmdHis',strtotime(Arr::get($t,'LastUpdTime')));
            if( $LastUpdTime > $LastUpdTimeSave )
            {
                $LastUpdTimeSave = $LastUpdTime;
                $LastUpdTabID = Arr::get($t,'tabID');
            }
            $TabID = Arr::get($t,'tabID');
            if( $TabID==$tabActive ) $TabExist = TRUE;
        }
        $tabActive = ( !$TabExist ) ? $tabActive : $LastUpdTabID;

        $json['tabActive'] = $tabActive;
        $json['tabs'] = $tabs;
        Basket::singleton()->setTabActive($tabActive);

        $rbg = (int) $request->input('rbg');
        $tzp = $request->input('tzp');
        $qty = (int) $request->input('qty');
        $std = (int) $request->input('std');
        $pld = (int) $request->input('pld');

        $res = Basket::singleton()->add($rbg,$tzp,$qty,$std,$pld,$ShopID,$UserID,$PriceLID,$SessID,$tabActive);
        /// $res['isError'] = '1'; $res['mess'] = 'test error message';

        if( Arr::get($res,'isError') )
        {
            $this->flashers[] = [
                'type' => 'error',
                'text' => Arr::get($res,'mess')
            ];
        }
        if( $_id = Arr::get($res,'AddCartID') )
        {
            $json['cartID'] = $_id;
            $this->flashers[] = [
                'type' => 'success',
                'text' => sprintf('Позиция добавлена в <a href="%s" onClick="TZBasket.change(this,true); return false;">корзину покупок</a>!',route('basketList'))
            ];
            /// Получаем данные для сквозной корзины
            if( $request->input('withFBI') )
            {
                $fastBasketInfo = Basket::singleton()->fastBasketInfo();

                $fbProducts = Arr::get($fastBasketInfo,'items');
                $fbCount = Arr::get($fastBasketInfo,'count');
                $fbTotal = Arr::get($fastBasketInfo,'total');

                $routeName = Route::currentRouteName();
                $fastBasketInfo['view'] = view('includes.header.basket', compact('fbProducts','fbCount','fbTotal','routeName'))->render();

                $json['basket'] = $fastBasketInfo;
            }
        }
        $json['flashers'] = $this->flashers;

        return response()->json($json);
    }
    public function update(Request $request)
    {
        $json = [];

        $id = (int) $request->input('id');
        $qty = (int) $request->input('qty');
        $note = $request->input('note');

        $ShopID = (int) Customer::singleton()->getStockIDGlobal();
        $UserID = (int) Customer::singleton()->getID();
        $SessID = (string) Cookie::get('tz_sid');
        $PriceLID = (int) Customer::singleton()->getPriceLID();

        $TabID = Basket::singleton()->getTabActive();

        // Update
        $res = Basket::singleton()->update($id,$ShopID,$TabID,$UserID,$SessID,$qty,$note,$PriceLID);
        $json['$res'] = $res;
        $redirect = '';
        if( Arr::get($res,'isError') )
        {
            $message = [
                'type' => 'error',
                'text' => Arr::get($res,'mess')
            ];
            $status = 0;
        }
        else
        {
            $message = [
                'type' => 'success',
                'text' => 'Корзина обновленна'
            ];
            $status = 1;
            /// Если не существует заметки, значит меняли кол-во, значит сбрасываем скидку
            if( !$note ){
                Session::remove('reward');
                $redirect = route('basketList');
            }
            /// Получаем данные для сквозной корзины
            if( $request->input('withFBI') ) $json['basket'] = Basket::singleton()->fastBasketInfo();
        }
        $json['status'] = $status;
        $json['message'] = $message;
        $json['redirect'] = $redirect;

        $this->flashers[] = $message;
        $json['flashers'] = $this->flashers;

        return response()->json($json) ;
    }
    public function remove(Request $request)
    {
        $json = [];

        // Remove
        $key = $request->input('key');
        if( $key )
        {
            $res = Basket::singleton()->remove($key);
            if( Arr::get($res,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => Arr::get($res,'mess')
                ];
                $json['status'] = 0;
            }
            else
            {
                $this->flashers[] = [
                    'type' => 'success',
                    'text' => 'Корзина покупок обновлена!'
                ];
                $json['status'] = 1;
                $json['redirect'] = route('basketList');;
                /// Получаем данные для сквозной корзины
                if( $request->input('withFBI') ) $json['basket'] = Basket::singleton()->fastBasketInfo();
                Session::remove('reward');
            }
            $json['flashers'] = $this->flashers;
        }

        return response()->json($json);
    }
    public function create()
    {
        $status = 0;

        $tabActive = Basket::singleton()->getTabActive();

        $ShopID = (int) Customer::singleton()->getStockIDGlobal();
        $UserID = (int) Customer::singleton()->getID();
        $SessID = (string) Cookie::get('tz_sid');
        $tabs = Basket::singleton()->loadTabs($ShopID,$UserID,$SessID);

        foreach( $tabs AS $t )
        {
            if( (int) Arr::get($t,'tabID')===$tabActive )
            {
                ++$tabActive;
                $status = 1;
                break;
            }
        }
        Basket::singleton()->setTabActive($tabActive);

        return response()->json(['status'=>$status,'TabID'=>$tabActive,'message'=>'Корзина пока пустая!']);
    }
    public function change(Request $request)
    {
        $status = 0;
        $redirect = '';

        $tabChange = (int) $request->input('key');

        $ShopID = (int) Customer::singleton()->getStockIDGlobal();
        $UserID = (int) Customer::singleton()->getID();
        $SessID = (string) Cookie::get('tz_sid');
        $tabs = Basket::singleton()->loadTabs($ShopID,$UserID,$SessID);

        foreach( $tabs AS $t )
        {
            if( (int) Arr::get($t,'tabID')===$tabChange )
            {
                $status = 1;
                Basket::singleton()->setTabActive($tabChange);
                $redirect = route('basketList');
                break;
            }
        }

        return response()->json(['status'=>$status,'change'=>[$_POST,$_GET],'redirect'=>$redirect]);
    }
    public function clear()
    {
        $json = [];

        $status = 0;
        $redirect = '';

        $res = Basket::singleton()->clear();
        $json['$res'] = $res;
        if( Arr::get($res,'isError') )
        {
            $this->flashers[] = [
                'type' => 'error',
                'text' => Arr::get($res,'mess')
            ];
        }
        else
        {
            $this->flashers[] = [
                'type' => 'success',
                'text' => 'Текущая корзина очищена'
            ];
            $status = 1;
            $redirect = route('basketList');
        }
        $json['status'] = $status;
        $json['redirect'] = $redirect;

        $json['flashers'] = $this->flashers;

        return response()->json($json);
    }
    public function coupon(Request $request)
    {
        $json = [];

        $PromoName = $request->input('coupon');
        $ShopID    = (int) Customer::singleton()->getStockIDGlobal();
        $TabID     = Basket::singleton()->getTabActive();
        $UserID    = (int) Customer::singleton()->getID();
        $PriceLID  = (int) Customer::singleton()->getPriceLID();
        $CardID    = (int) Customer::singleton()->getDiscountCardID();

        $mBasket = new mBasket();
        $res = $mBasket->applyCoupon($PromoName,$CardID,$ShopID,$TabID,$UserID,$PriceLID);
        $json['$inn'] = [$PromoName,$CardID,$ShopID,$TabID,$UserID,$PriceLID];
        $json['$res'] = $res;

        if( Arr::get($res,'isError') )
        {
            $this->flashers[] = [
                'type' => 'error',
                'text' => Arr::get($res,'mess')
            ];
            $status = 0;
            $redirect = '';
            Session::remove('coupon');
        }
        else
        {
            $this->flashers[] = [
                'type' => 'success',
                'text' => 'Промокод успешно активирован!'
            ];
            $status = 1;
            $redirect = route('basketList');
            Session::put('coupon',$PromoName);
        }
        $json['status'] = $status;
        $json['flashers'] = $this->flashers;
        $json['redirect'] = $redirect;

        return response()->json($json);
    }
    public function reward() /// Бонусные Рубли COPY ONLY (from demo1)
    {
        $this->load->language('extension/total/reward');
        $json = [];
        ///
        $ShopID = (int) $this->getStockID();
        $TabID = $this->getBasketTabActive();
        $UserID = (int) $this->customer->getId();
        $PriceLID = (int) $this->customer->getPriceLID();
        $CardID = (int) $this->customer->getDiscountCardID();
        ///
        $this->load->model('checkout/cart');
        $res = $this->model_checkout_cart->applyRewardPoints($CardID,$ShopID,$TabID,$UserID,$PriceLID);
        ///
        ///$json['$res'] = $res;
        if( get($res,'isError') )
        {
            $message = [
                'type' => 'error',
                'text' => get($res,'mess')
            ];
            $status = 0;
            $redirect = '';
        }
        else
        {
            $message = [
                'type' => 'success',
                'text' => $this->language->get('text_success')
            ];
            $status = 1;
            $redirect = $this->url->link('checkout/cart');

            $reward = rcv('reward');
            $this->session->data['reward'] = abs($reward);
        }
        $json['status'] = $status;
        $json['message'] = $message;
        $json['redirect'] = $redirect;
        ///
        ///$this->flashers[] = $message;
        ///$json['flash'] = $this->load->controller('common/flash',['flashers'=>$this->flashers]);
        ///
        $this->jEcho($json);
    }
    public function cancel() /// Отсменить скидки COPY ONLY (from demo1)
    {
        $this->load->language('extension/total/cancel');
        $json = [];
        ///
        $ShopID = (int) $this->getStockID();
        $TabID = $this->getBasketTabActive();
        $UserID = (int) $this->customer->getId();
        $PriceLID = (int) $this->customer->getPriceLID();
        ///
        $this->load->model('checkout/cart');
        $res = $this->model_checkout_cart->removeBasketDiscounts($ShopID,$TabID,$UserID,$PriceLID);
        ///
        ///$json['$res'] = $res;
        if( get($res,'isError') )
        {
            $message = [
                'type' => 'error',
                'text' => get($res,'mess')
            ];
            $status = 0;
            $redirect = '';
        }
        else
        {
            $message = [
                'type' => 'success',
                'text' => $this->language->get('text_success')
            ];
            $status = 1;
            $redirect = $this->url->link('checkout/cart');

            unset($this->session->data['reward']);
            unset($this->session->data['coupon']);
        }
        $json['status'] = $status;
        $json['message'] = $message;
        $json['redirect'] = $redirect;
        ///
        ///$this->flashers[] = $message;
        ///$json['flash'] = $this->load->controller('common/flash',['flashers'=>$this->flashers]);
        ///
        $this->jEcho($json);
    }
}
