<?php

namespace App\Http\Controllers;

use App\Helpers\Carts\Customer;
use App\Helpers\Models\AnyParams;
use App\Helpers\Models\News;
use App\Helpers\Models\Price;
use App\Helpers\Support\Arr;
use App\Helpers\Support\Request;
use App\Helpers\Carts\Store;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class StoreController extends Controller
{
    private $mAnyParams;
    public function __construct()
    {
        $this->mAnyParams = new AnyParams();
    }

    public function home()
    {
        $this->openedLeftMenu = 1;
        $banners = NULL;
        $siteType = Store::singleton()->getType();
        if( 'auto'==$siteType ) $banners = [
            [
                'image' => '/img/brands-auto/Akyoto.png',
                'title' => '',
            ],[
                'image' => '/img/brands-auto/alkar.jpg',
                'title' => '',
            ],[
                'image' => '/img/brands-auto/bosch.jpg',
                'title' => '',
            ],[
                'image' => '/img/brands-auto/IBERIS.png',
                'title' => '',
            ],[
                'image' => '/img/brands-auto/K&N-Filters.jpg',
                'title' => '',
            ],[
                'image' => '/img/brands-auto/kayaba.jpg',
                'title' => '',
            ],[
                'image' => '/img/brands-auto/Koyo.jpg',
                'title' => '',
            ],[
                'image' => '/img/brands-auto/mann-logo.jpg',
                'title' => '',
            ],[
                'image' => '/img/brands-auto/NEVSKY-FILTER.jpg',
                'title' => '',
            ],[
                'image' => '/img/brands-auto/NSK_logo.jpg',
                'title' => '',
            ]];
        if( 'base'==$siteType ) $banners = [
            [
                'image' => '/img/brands-base/1.svg',
                'title' => '',
            ],[
                'image' => '/img/brands-base/2.svg',
                'title' => '',
            ],[
                'image' => '/img/brands-base/3.svg',
                'title' => '',
            ],[
                'image' => '/img/brands-base/4.svg',
                'title' => '',
            ],[
                'image' => '/img/brands-base/5.svg',
                'title' => '',
            ],[
                'image' => '/img/brands-base/6.svg',
                'title' => '',
            ],[
                'image' => '/img/brands-base/7.svg',
                'title' => '',
            ],[
                'image' => '/img/brands-base/8.svg',
                'title' => '',
            ],[
                'image' => '/img/brands-base/9.svg',
                'title' => '',
            ],[
                'image' => '/img/brands-base/11.svg',
                'title' => '',
            ]];
        $lastNews = (new News())->last(3);

        $paramInfo = $this->mAnyParams->some(['component-slider','component-slide-uri','component-category-recommended']);
        $_new = []; foreach( $paramInfo AS $p ) $_new[Arr::get($p,'code')] = $p; $paramInfo = $_new; /// dd($paramInfo);

        $arrSliders = Arr::get($paramInfo,'component-slider');
        $arrLinks = Arr::get($paramInfo,'component-slide-uri');
        $arrGoods = Arr::get($paramInfo,'component-category-recommended');

        $strFilePath = ( $arrSliders ) ? Arr::get($arrSliders,'value') : NULL;
        $strLinks = ( $arrLinks ) ? Arr::get($arrLinks,'value') : NULL;
        $strGoods = ( $arrGoods ) ? Arr::get($arrGoods,'value') : NULL;

        $sliders = ( $strFilePath ) ? explode(',',$strFilePath) : [];
        $links = ( $strLinks ) ? Arr::decryption($strLinks) : [];
        $goods = ( $strGoods ) ? Arr::decryption($strGoods) : [];

        $goodID = Arr::get($goods,'nodeID');

        $offers = [];
        if( $goodID && ($StockID = $this->officeChoosed()) )
        {
            $PriceLevelID = Customer::singleton()->getPriceLID();
            $StockTypeFilter = 'StockAll';
            $SearchFilter = 'ByWebCategory';
            $SearchValue = $goodID;
            $SortData = '';
            $offers = (new Price())->priceLoad($StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData);
            ///dd($offers);
            if( !$offers )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => 'GOODS: Сервер не вернул результатов, сообщите в чат оператору и прикрепите ссылку',
                ];
            }
            if( @Arr::get($offers[0],'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => 'GOODS: '.@Arr::get($offers[0],'mess'),
                ];
                $offers = [];
            }
            else if( !empty($offers) ) Store::singleton()->fitPriceResult($offers);
            ///dd($offers);
        }
        ///dd($offers);

        return $this->render('pages.store.home',compact('banners','sliders','links','lastNews','offers'));
    }
    public function news($id)
    {
        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<i class="oi oi-home" title="Главная"></i>',
            'hide' => 1
        ],[
            'text' => "Новости",
        ]];

        $news = (new News())->get($id);
        return $this->render('pages.store.news',compact('news'));
    }
    public function offer()
    {
        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<i class="oi oi-home" title="Главная"></i>',
            'hide' => 1
        ],[
            'text' => "Договор публичной оферты",
        ]];
        $paramInfo = $this->mAnyParams->get('page-offer-content');
        $content = Arr::get($paramInfo,'value');
        return $this->render('pages.store.offer',compact('content'));
    }
    public function about()
    {
        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<i class="oi oi-home" title="Главная"></i>',
            'hide' => 1
        ],[
            'text' => "О нас",
        ]];
        $paramInfo = $this->mAnyParams->get('page-about-content');
        $content = Arr::get($paramInfo,'value');
        return $this->render('pages.store.about',compact('content'));
    }
    public function contacts()
    {
        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<i class="oi oi-home" title="Главная"></i>',
            'hide' => 1
        ],[
            'text' => "Контакты",
        ]];
        $paramInfo = $this->mAnyParams->some(['page-contacts-content','component-map']);
        ///dd($paramInfo);
        $objContacts = Arr::get($paramInfo,0,[]);
        $objMap = Arr::get($paramInfo,1,[]);
        $content = Arr::get($objContacts,'value');
        $map = Arr::get($objMap,'value');
        return $this->render('pages.store.contacts',compact('content','map'));
    }
    public function delivery()
    {
        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<i class="oi oi-home" title="Главная"></i>',
            'hide' => 1
        ],[
            'text' => "Доставка",
        ]];
        $paramInfo = $this->mAnyParams->get('page-delivery-content');
        $content = Arr::get($paramInfo,'value');
        return $this->render('pages.store.delivery',compact('content'));
    }
    public function payment()
    {
        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<i class="oi oi-home" title="Главная"></i>',
            'hide' => 1
        ],[
            'text' => "Оплата",
        ]];
        $paramInfo = $this->mAnyParams->get('page-payment-content');
        $content = Arr::get($paramInfo,'value');
        return $this->render('pages.store.payment',compact('content'));
    }
    public function refund()
    {
        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<i class="oi oi-home" title="Главная"></i>',
            'hide' => 1
        ],[
            'text' => "Возврат",
        ]];
        $paramInfo = $this->mAnyParams->get('page-refund-content');
        $content = Arr::get($paramInfo,'value');
        return $this->render('pages.store.refund',compact('content'));
    }
    public function certificate()
    {
        $this->breads = [[
            'text' => 'Главная',
            'link' => route('home'),
            'icon' => '<i class="oi oi-home" title="Главная"></i>',
            'hide' => 1
        ],[
            'text' => "Возврат",
        ]];
        return $this->render('pages.store.certificate');
    }
    public function callback(Request $request)
    {
        $name = $request->rcv('name','NoName');
        $phone = $request->rcv('phone','NoPhone');
        $note = $request->rcv('note','NoComment');
        ///$ip = $request->ip();
        $ip = FALSE;

        $subject = 'Обратный звоок';
        $body = "Комметарий пользователя: $note";
        $data = [
            'name' => $name,
            'phone' => $phone,
            'ip' => $ip,
        ];

        $json = Store::singleton()->callback($subject,$body,$data);

        return response()->json($json);
    }
    public function stockInfo(Request $request)
    {
        //
    }
}
