<?php

namespace App\Http\Controllers;

use App\Helpers\Carts\Customer;
use App\Helpers\Support\Session;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $breads = [];
    protected $flashers = [];
    protected $openedLeftMenu = 0;
    protected function render($view,$compact=[])
    {
        $this->flashers = array_merge((array)$this->flashers,(array)Session::get('flashers'));

        $breads = $this->breads;
        $flashers = $this->flashers;
        $openedLeftMenu = $this->openedLeftMenu; /// Используется только на frontend, но не хочется создавать клон контроллера с почти таким же функционалом
        /// View::share('flashers',$flashers); /// Можно и без этого, странно, возможно в 6-й версии не нужно в рутовый пробрассывать
        /// Тогда нужно frontend почистить !!
        return view($view,compact('breads','flashers','openedLeftMenu'),$compact);
    }
    protected function jecho($json=[])
    {
        return response()->json($json);
    }
    /**
     * Офис выбран ?
     * Пока только добавление flash alert
     * Возвращает либо StockID или FALSE
     * StockID в приоритете берется от привязки пользователя, только потом из сессии
     */
    protected function officeChoosed()
    {
        if( $current = Customer::singleton()->getStockIDGlobal() )
        {
            $isChoose = $current;
        }
        else
        {
            $this->flashers[] = [
                'type' => 'error',
                'text' => 'Для продолжения нужно выбрать офис!'
            ];
            $isChoose = FALSE;
        }
        return $isChoose;
    }
    /**
     * метод проверяет, была ли отправлена форма по наличию имени кнопки в массиве $_POST
     * кроме того происходит проверка referrer
     * Возвращает TRUE при выполнении условий и значение, если оно есть, в объект
     */
    protected function buttonPressed($buttonName){
        $matches = NULL;
        if( !isset($_POST[$buttonName]) ) return FALSE;
        if( !isset($_SERVER['HTTP_REFERER']) ) return TRUE; // проверка referrer
        if( !preg_match('/^https?\:\/\/([^\/]+)\/.*$/i',$_SERVER['HTTP_REFERER'],$matches) ) return FALSE;
        if( $matches[1] != $_SERVER['HTTP_HOST'] ) return FALSE;
        ///$this->bttn = $_POST[$buttonName];
        return TRUE;
    }
}
