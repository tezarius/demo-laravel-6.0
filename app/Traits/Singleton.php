<?php
namespace App\Traits;
trait Singleton
{
    protected static $instance = null;
    /** Получить объект класса можно толкьо через этот метод */
    public static function singleton(){
        if (static::$instance === null){
            static::$instance = new static();
        }
        return static::$instance;
    }
    /** Замена конструктора */
    abstract function init();
    /** Что бы не клонировали, что даст в втрой такой же объек */
    protected function __clone(){}
    /** И что бы через метод new не плодили */
    protected function __construct(){}
}