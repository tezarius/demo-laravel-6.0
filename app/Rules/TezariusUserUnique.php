<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Helpers\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class TezariusUserUnique implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $sql = "call pANY_www('CustomerExistChecking',JSON_OBJECT('phone','{$value}','login','{$value}','email','{$value}'),'ru')";
        $obj = DB::connection('tezarius')->selectOne($sql);
        $this->message = Arr::get($obj,'mess','Success!');
        ///$this->message = Lang::getFromJson( $this->message ); /// Не завелось, потом выяснить
        return ( !Arr::get($obj,'isError') );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    private $message = 'Error :(';
    public function message()
    {
        return $this->message;
    }
}
