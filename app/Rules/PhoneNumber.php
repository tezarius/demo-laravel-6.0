<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Lang;

class PhoneNumber implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        ///return preg_match('%^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$%i', $value) && strlen($value) >= 10;
        $isValid = TRUE;
        $value = preg_replace("/[^0-9]/", '', $value);
        if( strlen($value) < 11 )
        {
            $isValid = FALSE;
            $this->message = 'Телефон должен содержать 11 или более цифр';
        }
        if( $value && !in_array($value[0],[7,8,9]) )
        {
            $isValid = FALSE;
            $this->message = 'Телефон должен начинаться с 7,8 или 9';
        }
        return $isValid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    private $message = 'Incorrect phone number';
    public function message()
    {
        return $this->message;
    }
}
