<?php
namespace App\Helpers\Support;
class Arr extends \Illuminate\Support\Arr
{
    /** Получение данных из массива или объекта */
    public static function get($array, $key, $default = '', $checkEmpty = FALSE)
    {
        /// Сперва заюзаем родной метод от Laravel
        $res = parent::get($array,$key,NULL);                   /// Выставим NULL хоть это и дефолт, вдруг потом поменяют в хелпере дефолт
        if( !$res && $checkEmpty && is_array($array) ) $res = $default; /// is_array обязательно, так как с объекта не будет результата и отработает дальше уже наш код
        if( $res!==NULL ) return $res;                                  /// Если есть отдаем, что лара вернет + проверка на пустое от нас, которое дальше тоже есть

        /// Потом наш проверенный гадами со времен Kohana
        /// $checkEmpty принудительно возвращает значение из $default если по $key есть значание, но оно отдает FALSE в IF
        if( gettype($array)=='object' ) return (isset($array->{$key}) && ( !$checkEmpty || ($checkEmpty&&!empty($array->{$key})) )) ? $array->{$key} : $default;
        if( $array instanceof \ArrayObject ) return $array->offsetExists($key) ? $array->offsetGet($key) : $default;
        else return ( isset($array[$key]) && ( !$checkEmpty || ($checkEmpty&&!empty($array[$key])) ) ) ? $array[$key] : $default;
    }
    /**
     * Cut Symbols from String on the Array with Positions
     *
     *      $oHash = Core::cutSymbol($hash,[5,13]);
     *
     * @param   string  $str    input string
     * @param   array   $pos    array with symbol position
     * @return  object
     */
    public static function cutSymbol($str=NULL,$pos=NULL)
    {
        if( $str===NULL || $pos===NULL ) return json_decode(json_encode(["symbol"=>'',"symbols"=>'',"string"=>'']));
        $symbols = array(); $symbol = FALSE;
        if( is_array($pos) )
        {
            $i = -1; foreach( $pos AS $p ){ ++$i;
            $p = $p-$i;
            if( $p>strlen($str)+1 ) return json_decode(json_encode(["symbol"=>'',"symbols"=>'',"string"=>'']));
            $symbol .= $str{$p};
            $symbols[] = $str{$p};
            $str = substr_replace($str,'',$p,1);
        }
            $string = $str;
        }
        else{
            if( $pos>strlen($str)+1 ) return json_decode(json_encode(["symbol"=>'',"symbols"=>'',"string"=>'']));
            $symbol .= $str{$pos};
            $symbols[] = $symbol;
            $string = substr_replace($str,'',$pos,1);
        }
        return json_decode( json_encode( array("symbol"=>$symbol,"symbols"=>$symbols,"string"=>$string) ) );
    }
    /** Encryption */
    public static function encryption($array,$method=0){
        $method = (int) $method;
        switch($method){
            case 1:
                $jArr  = json_encode($array);
                $rjArr = strrev($jArr);
                $sArr  = serialize($rjArr);
                $rsArr = strrev($sArr);
                $bArr  = base64_encode($rsArr);
                $rbArr = strrev($bArr);
                $enArr = str_replace("=","$",$rbArr);
                break;
            default:
                $jArr  = json_encode($array);
                $rjArr = strrev($jArr);
                $sArr  = serialize($rjArr);
                $rsArr = strrev($sArr);
                $bArr  = base64_encode($rsArr);
                $rbArr = strrev($bArr);
                $enArr = str_replace("=","@",$rbArr);
                $enArr = substr($enArr,0,5).'0'.substr($enArr,5,strlen($enArr));
                $enArr = substr($enArr,0,16).'1'.substr($enArr,16,strlen($enArr));
        }
        return $enArr;
    }
    /** Decryption */
    public static function decryption($string,$method=0){
        $method = (int) $method;
        switch($method){
            case 1:
                $rbArr = str_replace("$","=",$string);
                $bArr  = strrev($rbArr);
                $rsArr = base64_decode($bArr);
                $sArr  = strrev($rsArr);
                $rjArr = unserialize($sArr);
                $jArr  = strrev($rjArr);
                $deArr = json_decode($jArr);
                break;
            default:
                $string = static::cutSymbol($string,[5,16])->string;
                $rbArr = str_replace("@","=",$string);
                $bArr  = strrev($rbArr);
                $rsArr = base64_decode($bArr);
                $sArr  = strrev($rsArr);
                $rjArr = unserialize($sArr);
                $jArr  = strrev($rjArr);
                $deArr = json_decode($jArr);
        }
        return $deArr;
    }

    /**
     * @param array $flat Array Items
     * @param string $pidKey Field Parent ID Item
     * @param string $idKey Field ID Item
     * @param string $childName Set Child Group Name
     * @param string $levelName Set Level Field Name
     * @return
     */
    public static function buildTree($flat, $pidKey, $idKey, $childName = 'children', $levelName = 'level')
    {
        $grouped = array();
        foreach( $flat as $sub )
        {
            $sub = (array) $sub;
            $grouped[$sub[$pidKey]][] = $sub;
        }
        $fnBuilder = function($siblings,$level=0) use (&$fnBuilder, $grouped, $idKey, $childName, $levelName)
        {
            ++$level;
            foreach( $siblings as $k => $sibling )
            {
                $sibling[$levelName] = $level;
                $id = @$sibling[$idKey];
                if( $_items = @$grouped[$id] ) $sibling[$childName] = $fnBuilder($_items,$level);
                $siblings[$k] = $sibling;
            }
            return $siblings;
        };
        $tree = $fnBuilder($grouped[0]);
        return $tree;
    }
}