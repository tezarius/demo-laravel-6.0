<?php
namespace App\Helpers\Support;
class HTML
{
    /** Обертки над родными методами, малоли потом значения по умолчанию сменят - было уже такое */
    public static function entityDecode($str='',$style=ENT_QUOTES,$charset='UTF-8')
    {
        return html_entity_decode($str,$style,$charset);
    }
    public static function entityEncode($str='',$style=ENT_QUOTES,$charset='UTF-8')
    {
        return htmlentities($str,$style,$charset);
    }
    public static function addSlashes($string)
    {
        return addslashes($string);
    }
    /** https://www.php.net/manual/ru/function.strip-tags.php */
    public static function stripTags($string,$allowable=NULL)
    {
        return strip_tags($string,$allowable);
    }
}