<?php
namespace App\Helpers\Support;
class Str extends \Illuminate\Support\Str
{
    /** Ex: Str::declension(3,'месяц','месяца','месяцев') */
    public static function declension($number,$form1,$form2,$form3,$pn=TRUE){
        if(($number == "0") or (($number >= "5") and ($number <= "20")) or preg_match("|[056789]$|",$number)){
            return ($pn)?"$number $form3":"$form3";
        }
        if(preg_match("|[1]$|",$number)){
            return ($pn)?"$number $form1":"$form1";
        }
        if(preg_match("|[234]$|",$number)){
            return ($pn)?"$number $form2":"$form2";
        }
        return FALSE;
    }
}