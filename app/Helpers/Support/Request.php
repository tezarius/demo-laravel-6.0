<?php
namespace App\Helpers\Support;
///class Request extends \Illuminate\Support\Facades\Request
class Request extends \Illuminate\Http\Request
{
    ///use \Illuminate\Http\Request;
    /** Получение переменной из POST или GET */
    public static function rcv($name,$df="",$entity=TRUE){
        $dt = @$_POST[$name];
        if( !$dt ){
            $POST = file_get_contents("php://input");
            $POST = json_decode($POST);
            $dt = @$POST->{$name};
        }
        if( !$dt ) $dt = @$_GET[$name];

        if( gettype($dt)=='string' ){
            if( $entity ) $dt = HTML::entityEncode($dt,ENT_QUOTES,"UTF-8");
            $dt = trim($dt);
        }
        else if( gettype($dt)=='array' ){
            array_walk_recursive($dt,function(&$value)use($entity){
                if( $entity ) $value = HTML::entityEncode($value,ENT_QUOTES,"UTF-8");
                $value = trim($value);
            });
        }
        return ( $dt ) ? $dt : $df;
    }
    public static function hasButton($buttonName)
    {
        $matches = NULL;
        if( !isset($_POST[$buttonName]) ) return FALSE;
        if( !isset($_SERVER['HTTP_REFERER']) ) return TRUE; // проверка referrer
        if( !preg_match('/^https?\:\/\/([^\/]+)\/.*$/i',$_SERVER['HTTP_REFERER'],$matches) ) return FALSE;
        if( $matches[1] != $_SERVER['HTTP_HOST'] ) return FALSE;
        ///$this->bttn = $_POST[$buttonName];
        return TRUE;
    }
}