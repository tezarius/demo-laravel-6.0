<?php
namespace App\Helpers\Models;
use App\Helpers\Carts\Customer;
use Illuminate\Support\Facades\DB;

class Price extends Model
{
    /** STEP 1 */
    public function getBrands($number)
    {
        return (array) DB::connection('tezarius')->select("CALL pBrands_by_code('$number')");
    }
    /** STEP 1.2 */
    public function getBrandsKey($code)
    {
        $StockID = (int) Customer::singleton()->getStockID();
        $CounterpartID = (int) Customer::singleton()->getID();
        $sql = "call pSearch_ArtCode('brandNew',JSON_OBJECT('code','{$code}','id_rbStock',{$StockID},'id_rbCounterparts',{$CounterpartID}))";
        return $this->row($sql);
    }
    /** STEP 1.3 */
    public function getBrandsRemote($LogSearchID)
    {
        $ans = [];

        $host = env('DB_HOST', '127.0.0.1');
        $project = env('TEZ_PROJECT_ID', 0);
        $target_url = "https://{$host}/services/in/suppliers/search.php?BrandsRequest=1&IDProjects={$project}&IDSearchCode={$LogSearchID}";
        $ans['uri'] = $target_url;

        $ch = curl_init($target_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($ch);
        curl_close($ch);

        $ans['res'] = $result;
        return $ans;
    }



    /** STEP 2.1 */
    public function getSearchID($code,$brand)
    {
        $StockID = (int) Customer::singleton()->getStockID();
        $CounterpartID = (int) Customer::singleton()->getID();
        $ip = request()->ip();

        $sql = "call pSearch_ArtCode('new',JSON_OBJECT('isWebsiteQuery',1,'ip','$ip','code','$code','brand','$brand','source','website','id_rbStock',$StockID,'id_rbCounterparts',$CounterpartID))";
        return $this->row($sql);
    }
    /** STEP 2.2 */
    public function webServicesLoad($LogSearchID)
    {
        $ans = [];

        $host = env('DB_HOST', '127.0.0.1');
        $project = env('TEZ_PROJECT_ID', 0);
        $target_url = "https://{$host}/services/in/suppliers/search.php?IDProjects={$project}&IDSearchCode={$LogSearchID}";
        $ans['uri'] = $target_url;

        $ch = curl_init($target_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($ch);
        curl_close($ch);

        $ans['res'] = $result;
        return $ans;
    }
    /** STEP 2.3 */
    public function priceLoad($StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData)
    {
        $StockID = (int) $StockID;
        $sql = "call pPrice(JSON_OBJECT(
            'isWebSearch',1,
            'StockDestinationID',$StockID,
            'PriceLevelID',$PriceLevelID,
            'StockTypeFilter','$StockTypeFilter',
            'SearchFilter','$SearchFilter',
            'SearchFilterValue','$SearchValue',
            'SortData','$SortData',
            'limit',1000,
            'limitPos',1
        ),'ru')";
        return $this->rows($sql);
    }


    public function searchByAuto($car,$code,$vehicle)
    {
        $UserUD = Customer::singleton()->getID();
        $ShopID = Customer::singleton()->getStockID();
        $ip = request()->ip();
        $sql = "call pSearch_ArtCode('new',JSON_OBJECT('isWebsiteQuery',1,'ip','{$ip}','code','{$code}','brandID',{$car},'brand','{$vehicle}',"
            ."'TypeData','ByCarNode','source','website','id_rbStock',{$ShopID},'id_rbCounterparts',{$UserUD}))";
        return $this->row($sql);
    }

    public function properties($id)
    {
        $sql = "call pANY_www('GoodsPropertiesList',JSON_OBJECT('by','category','id',{$id}),'ru')";
        return $this->rows($sql);
    }

    public function stockInfo($stockID,$shopID,$dlvrMin,$dlvrMax)
    {
        $sql = "call pStock_info(?,?,?,?,'ru')";
        return $this->row($sql,[$stockID,$shopID,$dlvrMin,$dlvrMax]);

        $sql = "call pStock_info($stockID,$shopID,$dlvrMin,$dlvrMax,'ru')";
        $arr = $this->row($sql);
        $arr->sql = $sql;
        return $arr;
    }
}