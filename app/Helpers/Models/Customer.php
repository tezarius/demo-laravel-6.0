<?php
namespace App\Helpers\Models;
use App\Helpers\Support\Arr;
use Illuminate\Support\Facades\DB;

class Customer extends Model
{
    public function verifyExistence($login,$phone,$email)
    {
        $sql = "call pANY_www('CustomerExistChecking',JSON_OBJECT('phone','{$phone}','login','{$login}','email','{$email}'),'ru')";
        return $this->row($sql);
    }
    public function addCustomer($data)
    {
        $StockID = (int) $data['StockID'];
        $name_i = Arr::get($data,'firstname');
        $name_f = Arr::get($data,'lastname');
        $login  = Arr::get($data,'email');
        $pass   = Arr::get($data,'password');
        $phone  = Arr::get($data,'telephone');

        $jsRegData = "'name_f','{$name_f}','name_i','{$name_i}','email','{$login}','phone','{$phone}','id_rbStock','{$StockID}','login','{$login}','pass','{$pass}'";
        $sql = "CALL `pRB_change`('ins','rbCustomers',0,JSON_OBJECT('isWebsiteQuery',1,$jsRegData),'ru',1)";
        return $this->row($sql);
    }
    public function editPassword($CounterpartsID,$login,$password)
    {
        $sql = "call `pRB_change`('upd','rbCustomers',0,JSON_OBJECT('isWebsiteQuery',1,'isAuthorizationDataUpd',1,'login','$login','pass','{$password}','CntptsId',{$CounterpartsID}),'ru',1)";
        return $this->row($sql);
    }
    public function editCustomer($CustomerUID, $StockID, $data)
    {
        $name_i = Arr::get($data,'firstname');
        $name_f = Arr::get($data,'lastname');
        $name_o = Arr::get($data,'patronymic');
        $email  = Arr::get($data,'email');
        $phone  = Arr::get($data,'telephone');

        $org  = Arr::get($data,'org_name');
        $inn  = Arr::get($data,'org_inn');
        $kpp  = Arr::get($data,'org_kpp');
        $ogrn = Arr::get($data,'org_ogrn');
        $addr = Arr::get($data,'org_addr');


        $sql = "call `pRB_change`('upd','rbCustomers',0,JSON_OBJECT('isWebsiteQuery',1,
            /* Контактные данные */
            'name_f','{$name_f}', 'name_i','{$name_i}','name_o','{$name_o}', 'email','{$email}','phone','{$phone}','id_rbStock',{$StockID},
            /* Карточка организации, правка доступа только один раз */
            'name_full','{$org}','inn','{$inn}','kpp','{$kpp}','ogrn','{$ogrn}','address','{$addr}',
            /* Код клиента */
            'CntptsId',{$CustomerUID}
            ),'ru',1)";
        return $this->row($sql);
    }
    public function historySearch($UserID)
    {
        $sql = "call pSearch_ArtCode('MyHistory',JSON_OBJECT('isWebsiteQuery',1,'id_rbCounterparts',{$UserID}))";
        return $this->rows($sql);
    }
    public function getNewsletter()
    {
        $UserID = \App\Helpers\Carts\Customer::singleton()->getID();
        $sql = "CALL pRB_get('rbOrderStates',JSON_OBJECT('isWebsiteQuery',1,'id_rbCounterparts',{$UserID}),'ru',0,1,0,'')";
        return $this->rows($sql);
    }
    public function editNewsletter($newsletter)
    {
        $UserID = \App\Helpers\Carts\Customer::singleton()->getID();
        $ArrayStates = implode(',',(array)$newsletter);
        $sql = "CALL pANY_www('SetOrderStatesInform',JSON_OBJECT('CouterpartsID',{$UserID},'ArrayStates','{$ArrayStates}'),'ru')";
        return $this->row($sql);
    }
}