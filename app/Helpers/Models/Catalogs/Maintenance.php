<?php
namespace App\Helpers\Models\Catalogs;
use App\Helpers\Models\Model;
class Maintenance extends Model
{
    public function marks()
    {
        $sql = "call pANY_www('CarCatalogTO',JSON_OBJECT('type','manufacture'),'ru')";
        return $this->rows($sql);
    }
    public function models($mark)
    {
        $sql = "call pANY_www('CarCatalogTO',JSON_OBJECT('type','model','id',?),'ru')";
        return $this->rows($sql,[$mark]);
    }
    public function vehicles($model)
    {
        $sql = "call pANY_www('CarCatalogTO',JSON_OBJECT('type','modif','id',?),'ru')";
        return $this->rows($sql,[$model]);
    }
    public function details($vehicle)
    {
        $sql = "call pANY_www('CarCatalogTO',JSON_OBJECT('type','autoparts','id',?),'ru')";
        return $this->rows($sql,[$vehicle]);
    }
}