<?php

namespace App\Helpers\Models\Catalogs;

use App\Helpers\Models\Model;
use App\Helpers\Support\Arr;

class Aftermarket extends Model
{
    public function types()
    {
        return [[
            'code' => 'P',
            'name' => 'Легковые',
            'image' => '/img/catalogs/aftermarket/type/P.svg',
            'href' => route('amMarks',['hash'=>Arr::encryption(['type'=>'P'])]),
        ],[
            'code' => 'C',
            'name' => 'Грузовые',
            'image' => '/img/catalogs/aftermarket/type/C.svg',
            'href' => route('amMarks',['hash'=>Arr::encryption(['type'=>'C'])]),
        ],[
            'code' => 'M',
            'name' => 'Мототехника',
            'image' => '/img/catalogs/aftermarket/type/M.svg',
            'href' => route('amMarks',['hash'=>Arr::encryption(['type'=>'M'])]),
        ]];
    }
    public function marks($type)
    {
        $sql = "CALL pCars('getMakes',JSON_OBJECT('Type','{$type}'),@Code,@Error)";
        return $this->rows($sql);
    }
    public function mark($type,$mark)
    {
        $sql = "CALL pCars('getCarsInfo',JSON_OBJECT('Step','marka','Type','$type','id',{$mark}),@Code,@Error)";
        return $this->row($sql);
    }
    public function models($type,$mark)
    {
        $sql = "CALL pCars('getModels',JSON_OBJECT('Type','{$type}','ManuID',{$mark}),@Code,@Error)";
        return $this->rows($sql);
    }
    public function model($type,$model)
    {
        $sql = "CALL pCars('getCarsInfo',JSON_OBJECT('Step','model','Type','$type','id',$model),@Code,@Error)";
        return $this->row($sql);
    }
    public function vehicles($type,$mark,$model)
    {
        $sql = "CALL pCars('getCars',JSON_OBJECT('Type','{$type}','ManuID',{$mark},'ModelID',{$model}),@Code,@Error)";
        return $this->rows($sql);
    }
    public function vehicle($type,$car)
    {
        $sql = "CALL pCars('getCarsInfo',JSON_OBJECT('Step','car','Type','$type','id',$car),@Code,@Error)";
        return $this->row($sql);
    }
    public function tree($type,$car)
    {
        $sql = "CALL pCars('getTree',JSON_OBJECT('Type','{$type}','CarID',{$car}),@Code,@Error)";
        return $this->rows($sql);
    }
    public function details($type,$car,$node)
    {
        $sql = "call pCars('getArticlesByNode',JSON_OBJECT('Type','{$type}','CarID',{$car},'NodeID',{$node}),@Code,@Error)";
        return $this->rows($sql);
    }
}