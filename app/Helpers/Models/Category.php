<?php
namespace App\Helpers\Models;
class Category extends Model
{
    public function flat()
    {
        $sql = "call `pRB_get`('rbGroups',JSON_OBJECT('filter','ForObject','value','rbGoodsWebCategory'),'ru',0,1,0,'')";
        return $this->rows($sql);
    }
    public function iconAdd($nodeID,$iconURI)
    {
        $sql = "call pANY_www('CategoryIcons',JSON_OBJECT('do','set','id_category',?,'icon_url',?),'ru')";
        return $this->row($sql,[$nodeID,$iconURI]);
    }
    public function iconDel($nodeID)
    {
        $sql = "call pANY_www('CategoryIcons',JSON_OBJECT('do','del','id_category',?),'ru')";
        return $this->row($sql,[$nodeID]);
    }
}