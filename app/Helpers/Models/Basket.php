<?php
namespace App\Helpers\Models;
use App\Helpers\Carts\Customer;
use Illuminate\Support\Facades\DB;

class Basket extends Model
{
    /** Сколько бонусов по клиенту */
    public function getRewardPoints()
    {
        /// Пока метод тут:
        /// app/Helpers/Carts/Customer.php
    }
    /** Испорльзовать бонусные рубли */
    public function applyRewardPoints($CardID,$ShopID,$TabID,$UserID,$PriceLID)
    {
        $sql = "call pCart('DiscountSet',JSON_OBJECT('DiscontType','bonus','DiscontValue','','DiscontCardID',{$CardID},'isWebCart',1,'ShopID',{$ShopID},
        'CounterPartsID',{$UserID},'tabID',{$TabID},'PriceLevelID',{$PriceLID}))";
        return $this->row($sql);
    }
    /** Промокод == Купон */
    public function applyCoupon($PromoName,$CardID,$ShopID,$TabID,$UserID,$PriceLID)
    {
        $sql = "call pCart('DiscountSet',JSON_OBJECT('DiscontType','promo','DiscontValue','{$PromoName}','DiscontCardID',{$CardID},'isWebCart',1,'ShopID',{$ShopID},
        'CounterPartsID',{$UserID},'tabID',{$TabID},'PriceLevelID',{$PriceLID}))";
        return $this->row($sql);
    }
    /** Удалить все скидки по корзине */
    public function removeBasketDiscounts($ShopID,$TabID,$UserID,$PriceLID)
    {
        $sql = "call pCart('DiscountSet',JSON_OBJECT('DiscontType','skip','isWebCart',1,'ShopID',{$ShopID},'CounterPartsID',{$UserID},'tabID',{$TabID},'PriceLevelID',{$PriceLID}))";
        return $this->row($sql);
    }
}