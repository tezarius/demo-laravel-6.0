<?php
namespace App\Helpers\Models;
use App\Helpers\Support\Arr;
use \App\Helpers\Carts\Store;

class Goods extends Model
{
    public function getAll($artbrID,$brandID,$brand,$code,$GoodsID)
    {
        $images = [];
        $images_global = $this->imagesGlobal($brandID,$brand,$code);
        $images_local = $this->imagesLocal($artbrID);
        foreach( $images_global AS $img )
        {
            array_push($images,[
                'name' => Arr::get($img,'DataSupplierArticleNumber').' / '.Arr::get($img,'description'),
                'path' => Arr::get($img,'url'),
                'note' => Arr::get($img,'Description'),
            ]);
        }
        foreach( $images_local AS $img )
        {
            array_push($images,[
                'name' => Arr::get($img,'id_rbArticles').' / '.Arr::get($img,'rbBrands_id_rbBrands'),
                'path' => Arr::get($img,'name'),
                'note' => Arr::get($img,'note'),
            ]);
        }
        $image = array_shift($images);

        $settingsLocal = (object) $this->settingsLocal($GoodsID);
        $settingsLocal->table = $this->settingsLocalTable($GoodsID);

        $ans = [
            'images_global' => $images_global,
            'images_local'  => $images_local,
            'images'        => $images,
            'image'         => $image,
            'name'          => ( 'auto'==Store::singleton()->getType() ) ? "$code / $brand" : ( ($_n=Arr::get($settingsLocal,'name'))?"$_n":"$brand \ $code" ),

            'settings_global' => $this->settingsGlobal($brandID,$brand,$code),
            'settings_local'  => $settingsLocal,

            'apply_global' => $this->applyGlobal($brandID,$brand,$code),
            'apply_local'  => $this->applyLocal(),

            'brand_info' => $this->brandInfo($brandID),
        ];
        return $ans;
    }

    /**
     * Получить фото детали из глобального автосправочника
     * https://lk.tezarius.ru/ru/public-api/show/36
     * @param $brandID
     * @param $brand
     * @param $code
     * @return array
     */
    public function imagesGlobal($brandID,$brand,$code)
    {
        $sql = "call pCars('getArticlePhoto',JSON_OBJECT('BrandID',{$brandID},'BrandName','{$brand}','code','{$code}'),@Code,@Error)";
        return $this->rows($sql);
    }
    /**
     * Получить список путей фото товара из вашей базы данных
     * https://lk.tezarius.ru/ru/public-api/show/34
     * @param $artbrID
     * @return array
     */
    public function imagesLocal($artbrID)
    {
        $sql = "CALL pRB_get('rbPhoto',JSON_OBJECT('filter','PhotoByArtId','id',{$artbrID}),'ru',1,0,1,'')";
        return $this->rows($sql);
    }

    /**
     * Получить информацию о детали из глобального автосправочника, размеры, свойства и тд
     * https://lk.tezarius.ru/ru/public-api/show/35
     * @param $brandID
     * @param $brand
     * @param $code
     * @return array
     */
    public function settingsGlobal($brandID,$brand,$code)
    {
        $sql = "call pCars('getArticleInfo',JSON_OBJECT('brandID',{$brandID},'brand','{$brand}','code','{$code}'),@Code,@Error)";
        return $this->rows($sql);
    }
    /**
     * Получить карточку товара вашего наличия, свойства, примечание и тд
     * https://lk.tezarius.ru/ru/public-api/show/38
     * @param $GoodsID
     * @return array
     */
    public function settingsLocal($GoodsID)
    {
        $sql = "CALL pRB_get('rbGoods',JSON_OBJECT('filter','id','id',{$GoodsID}),'ru',1,0,1,'')";
        return $this->row($sql);
    }
    public function settingsLocalTable($GoodsID)
    {
        $sql = "CALL pRB_get('rbGoodsProperties',JSON_OBJECT('filter','ByGoodsId','id',{$GoodsID},'isWebsite',1),'ru',1,0,1,'')";
        return $this->rows($sql);
    }

    /**
     * Получить инфо о применяемости запчасти к авто из глобального автосправочника
     * https://lk.tezarius.ru/ru/public-api/show/37
     * @param $brandID
     * @param $brand
     * @param $code
     * @return array
     */
    public function applyGlobal($brandID,$brand,$code)
    {
        $sql = "call pCars('getArticleApplicability',JSON_OBJECT('brandID',{$brandID},'brand','{$brand}','code','{$code}'),@Code,@Error)";
        return $this->rows($sql);
    }
    public function applyLocal(){ return NULL; }

    /**
     * Информация о бренде
     * https://lk.tezarius.ru/ru/public-api/show/39
     * @param $brandID
     * @return array
     */
    public function brandInfo($brandID)
    {
        $sql = "CALL pANY('BrandInfo',JSON_OBJECT('ID',{$brandID}),'ru')";
        return $this->row($sql);
    }
}