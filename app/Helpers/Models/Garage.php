<?php
namespace App\Helpers\Models;
use App\Helpers\Carts\Customer;
use Illuminate\Support\Facades\DB;

class Garage extends Model
{
    public function vehicles()
    {
        $UserID = Customer::singleton()->getID();
        $sql = "call `pRB_get`('rbCarsCustomer',JSON_OBJECT('filter','ByCounterpartsID','id',{$UserID}),'ru',1,0,1,'')";
        return $this->rows($sql);
    }
    public function add($auto,$engine,$vin,$year,$note,$carID,$type)
    {
        $UserID = Customer::singleton()->getID();
        $sql = "call `pRB_change`('ins','rbCarsCustomer',0,JSON_OBJECT("
            ."'isWebsiteQuery',1,'id_rbCounterparts',{$UserID},'name','{$auto}','engine','{$engine}','vin','{$vin}',"
            ."'year','{$year}','note','{$note}','id_out',{$carID},'CarTypeOut','{$type}'),'ru',1)"
        ;
        return $this->row($sql);
    }
    public function del()
    {
        $sql = "";
        return $this->row($sql);
    }
}