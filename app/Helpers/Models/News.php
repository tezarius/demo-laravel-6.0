<?php
namespace App\Helpers\Models;
class News extends Model
{
    public function add($subject,$body,$dd1,$dd2,$img)
    {
        $sql = "call pANY_www('news',JSON_OBJECT('do','add','subject','$subject','body','$body','data_start','$dd1','data_end','$dd2','img_main_url','$img'),'ru')";
        return $this->row($sql);
    }
    public function upd($subject,$body,$dd1,$dd2,$img,$id)
    {
        $sql = "call pANY_www('news',JSON_OBJECT('do','upd','subject','$subject','body','$body','data_start','$dd1','data_end','$dd2','img_main_url','$img','id',$id),'ru')";
        return $this->row($sql);
    }
    public function del($id)
    {
        $sql = "call pANY_www('news',JSON_OBJECT('do','del','id',$id),'ru')";
        return $this->row($sql);
    }
    public function active()
    {
        $sql = "call pANY_www('news',JSON_OBJECT('do','get','isActive',1),'ru')";
        return $this->rows($sql);
    }
    public function last($count)
    {
        $sql = "call pANY_www('news',JSON_OBJECT('do','get','isActive',1,'count',$count),'ru')";
        return $this->rows($sql);
    }
    public function all()
    {
        $sql = "call pANY_www('news',JSON_OBJECT('do','get'),'ru')";
        return $this->rows($sql);
    }
    public function get($id)
    {
        $sql = "call pANY_www('news',JSON_OBJECT('do','get','id',$id),'ru')";
        return $this->row($sql);
    }
}