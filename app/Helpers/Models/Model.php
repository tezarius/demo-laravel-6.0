<?php
namespace App\Helpers\Models;
use Illuminate\Support\Facades\DB;

class Model
{
    protected function rows($sql,$arr=[])
    {
        return DB::connection('tezarius')->select($sql,$arr);
    }
    protected function row($sql,$arr=[])
    {
        return DB::connection('tezarius')->selectOne($sql,$arr);
    }
    protected function insert($sql,$arr=[])
    {
        return DB::connection('tezarius')->insert($sql,$arr);
    }
}