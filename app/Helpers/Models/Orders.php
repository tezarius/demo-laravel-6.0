<?php
namespace App\Helpers\Models;
use App\Helpers\Carts\Customer;
use App\Helpers\Support\Arr;

class Orders extends Model
{
    public function create($data)
    {
        $ShopID   = (int) Arr::get($data,'ShopID',0);
        $UserID   = (int) Arr::get($data,'UserID',0);
        $TabID    = (int) Arr::get($data,'TabID',0);
        $FirmID   = (int) Arr::get($data,'FirmID',0);
        $PriceLID = (int) Arr::get($data,'PriceLID',0);

        $CarsCustomerID = (int) Arr::get($data,'CarsCustomerID',0);
        $CustomersSourceID = (int) Arr::get($data,'CustomersSourceID',0);
        $ManagerID = (int) Arr::get($data,'ManagerID',0);

        $isDelivery       = (int) Arr::get($data,'isDelivery',0);
        $addressDelivery  = Arr::get($data,'addressDelivery');
        $isDeliveryPartly = (int) Arr::get($data,'isDeliveryPartly',0);

        $CardID = (int) Arr::get($data,'CardID',0);
        $note   = Arr::get($data,'note');


        $sql = "call pCart('OrderFast',JSON_OBJECT('isWebCart',1,'clearBasket',1,
        'ShopID',{$ShopID},'CounterPartsID',{$UserID},'tabID',{$TabID},'id_rbFirms',{$FirmID},'PriceLevelID',{$PriceLID},
        'id_rbCarsCustomer',{$CarsCustomerID},'id_rbCustomersSource',{$CustomersSourceID},'id_rbUsers_manager',{$ManagerID},
        'isDelivery',{$isDelivery},'address_delivery','{$addressDelivery}','isDeliveryPartly',{$isDeliveryPartly},
        'CardID',{$CardID},'note','$note','LanguageCode','ru'))"
        ;
        return $this->row($sql);
    }

    public function getOrderList()
    {
        $UserID = Customer::singleton()->getID();
        $sql = "CALL pOrder('GetOrderListDetails',0,JSON_OBJECT('isWebsiteQuery',1,'ByCustID',{$UserID},'ShowArh',0),'ru')";
        return $this->rows($sql);
    }
    public function getOrdersByData($dd1,$dd2)
    {
        $UserID = Customer::singleton()->getID();
        $dd1 = date('Y.m.d',strtotime($dd1));
        $dd2 = date('Y.m.d',strtotime($dd2));
        $sql = "call pOrder('GetOrderListDetails',0,JSON_OBJECT('isWebsiteQuery',1,'ByCustID',{$UserID},'ByDate',1,'date1','{$dd1}','date2','{$dd2}','ShowArh',1),'ru')";
        return $this->rows($sql);
    }
    public function getOrdersByNumber($number)
    {
        $UserID = Customer::singleton()->getID();
        $sql = "call pOrder('GetOrderListDetails',0,JSON_OBJECT('isWebsiteQuery',1,'ByCustID',{$UserID},'ByOrderNumber','{$number}','ShowArh',1),'ru')";
        return $this->rows($sql);
    }
    public function getOrdersBySearch($str)
    {
        $UserID = Customer::singleton()->getID();
        $sql = "call pOrder('GetOrderListDetails',0,JSON_OBJECT('isWebsiteQuery',1,'ByCustID',{$UserID},'MultiSearch','{$str}','ShowArh',1),'ru')";
        return $this->rows($sql);
    }
    public function getOrderHistory($id)
    {
        $UserID = Customer::singleton()->getID();
        $sql = "call pHistory(JSON_OBJECT( 'isWebsiteQuery',1,'ObjID',33,'ByCustID',{$UserID},'ID',{$id},'LanguageCode','ru'))";
        return $this->rows($sql);
    }

    public function reqOrderWithdraw($id,$note)
    {
        $UserID = Customer::singleton()->getID();
        $sql = "call pOrder('ToCancelRequest',{$id},JSON_OBJECT('isWebsiteQuery',1,'ByCustID',{$UserID},'note','{$note}'),'ru')";
        return $this->row($sql);
    }
    public function getOrderDelivery($id,$note)
    {
        $UserID = Customer::singleton()->getID();
        $sql = "call pOrder('ToRequestDelivery',{$id},JSON_OBJECT('isWebsiteQuery',1,'ByCustID',{$UserID},'note','{$note}'),'ru')";
        return $this->row($sql);
    }
    public function getOrderToWork($id,$ids,$note)
    {
        $UserID = Customer::singleton()->getID();
        $sql = "call pOrder('ToWork',{$id},JSON_OBJECT('isWebsiteQuery',1,'ArrayID','{$ids}','ByCustID',{$UserID},'note','{$note}'),'ru')";
        $res = $this->row($sql);
        $res->sql = $sql;
        return $res;
    }
}