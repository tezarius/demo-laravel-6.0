<?php

namespace App\Helpers\Models;

use App\Helpers\Carts\Customer;
use App\Helpers\Support\Arr;
use App\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class File extends Model
{
    protected $fillable = ['user_id','name','hash','type','mime','ext_mime','ext_file','size','path'];
    /**
     * A files can have many tickets
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /** Access all extensions array */
    public static $image_ext = ['jpg','jpeg','png','gif','bmp'];
    public static $audio_ext = ['mp3','ogg','mpga'];
    public static $video_ext = ['mp4','mpeg'];
    public static $document_ext = ['doc','docx','xls','xlsx','odt','odf','pdf','txt'];
    public static $web_ext = ['html','htm','js','css','sass','scss'];
    public static $script_ext = ['sh','bash','bat','class','json','lua','php','pl','py','sql','ts','vbs'];
    public static $archive_ext = ['zip','rar','7я','gz','gzip','tar','tgz'];

    /**
     * Get maximum file size
     * @return int maximum file size in kilobites
     */
    public static function getMaxSize()
    {
        return (int)ini_get('upload_max_filesize') * 1000;
    }
    /**
     * Get all extensions
     * @return array Extensions of all file types
     */
    public static function getAllExtensions()
    {
        $merged_arr = array_merge(self::$image_ext, self::$audio_ext, self::$video_ext, self::$document_ext);
        return implode(',', $merged_arr);
    }
    /**
     * Get type by extension
     * @param  string $ext Specific extension
     * @return string      Type
     */
    public static function getType($ext)
    {
        if (in_array($ext, self::$image_ext)) {
            return 'image';
        }

        if (in_array($ext, self::$audio_ext)) {
            return 'audio';
        }

        if (in_array($ext, self::$video_ext)) {
            return 'video';
        }

        if (in_array($ext, self::$document_ext)) {
            return 'document';
        }

        if (in_array($ext, self::$web_ext)) {
            return 'web';
        }

        if (in_array($ext, self::$script_ext)) {
            return 'script';
        }

        if (in_array($ext, self::$archive_ext)) {
            return 'archive';
        }

        return 'unknown';
    }

    /**
     * Upload file to the server and save database
     * @param $file
     * @param $folder
     * @param $model
     * @return array
     */
    public static function validSave($file,$folder,$model=NULL)
    {
        $fname = $file->getClientOriginalName();
        $fname = explode('.',$fname)[0];
        $fhash = Str::random(40);

        $ext_file = $file->getClientOriginalExtension();
        $ext_mime = $file->guessExtension() ?? $ext_file; /// Скорее всего нулевого размера файл

        $type = self::getType($ext_mime);
        ///$fpath = $file->store($folder); /// Ниже клон этого, просто что бы хэш не вытягивать
        $fpath = $file->storeAs($folder, "{$fhash}.{$ext_mime}");

        /// Если картинка
        if( 'image'==$type)
        {
            //Resize image here
            $localPath = storage_path('app/public/'.$fpath);

            $width = Image::make($localPath)->width();
            $height = Image::make($localPath)->height();

            $totalBytes = 1024;
            $fsizeKB = ceil($file->getSize()/1024);
            if( $fsizeKB > $totalBytes )
            {
                $pct = $totalBytes / $fsizeKB;

                $width = $width * $pct;
                $height = $height * $pct;

                $img = Image::make($localPath)->resize($width,$height,function($constraint){
                    ///$constraint->aspectRatio();
                });
                $img->save($localPath);
            }
        }

        $url = Storage::url($fpath);
        $url = Str::replaceFirst('https:','',$url);
        $url = Str::replaceFirst('http:','',$url);

        $data = [
            'name' => $fname,
            'hash' => $fhash,
            'type' => $type,
            'mime' => $file->getClientMimeType(),
            'ext_mime' => $ext_mime,
            'ext_file' => $ext_file,
            'size' => $file->getSize(),
            'path' => $fpath,
            'url' => $url,
            ///'user_id' => Customer::singleton()->getID(),
        ];
        ///self::create($data);
        /// Если нужно сохранить в смежную табличу, в случае работы с laravel напрямую
        ///$fSave = self::create($data);
        ///if( $model ) $model->files()->save($fSave);
        return $data;
    }
}
