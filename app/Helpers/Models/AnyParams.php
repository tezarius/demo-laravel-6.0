<?php
namespace App\Helpers\Models;
class AnyParams extends Model
{
    public function put($code,$value)
    {
        $sql = "call pANY_www('AnyParams',JSON_OBJECT('do','set','code',?,'value',?),'ru')";
        return $this->row($sql,[$code,$value]);
    }
    public function get($code)
    {
        $sql = "call pANY_www('AnyParams',JSON_OBJECT('do','get','by','code','code',?),'ru')";
        return $this->row($sql,[$code]);
    }
    public function all()
    {
        $sql = "call pANY_www('AnyParams',JSON_OBJECT('do','get','by','all'),'ru')";
        return $this->rows($sql);
    }
    public function some($arr)
    {
        $str = implode(',',$arr);
        $sql = "call pANY_www('AnyParams',JSON_OBJECT('do','get','by','ArrayCode','ArrayCode',?),'ru')";
        return $this->rows($sql,[$str]);
    }
}