<?php
namespace App\Helpers\Models;
use App\Helpers\Carts\Customer;

class Account extends Model
{
    public function getReports($dd1,$dd2)
    {
        $UserID = (int) Customer::singleton()->getID();
        $FirmID = (int) Customer::singleton()->getFirmID();
        $sql = "call pReports('98',JSON_OBJECT('FirmID',?,'CounterpartsID',?,'d1',?,'d2',?),'ru')";
        ///dd($sql,[$FirmID,$UserID,$dd1,$dd2]);
        return $this->rows($sql,[$FirmID,$UserID,$dd1,$dd2]);
    }
}