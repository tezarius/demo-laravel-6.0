<?php
namespace App\Helpers\Carts;
use App\Helpers\Models\Model;
use App\Traits\Singleton;
use App\Helpers\Support\Arr;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class Basket extends Model
{
    use Singleton;

    public $isTezarius = TRUE;

    private $tabs = [];
    private $products = [];
    private $tabActive = 0;

    public function init()
    {
        $this->tabActive = (int) Cookie::get('basketTabActive',0);
    }

    public function loadTabs($ShopID,$CounterPartsID,$SessionId)
    {
        $sql = "CALL pCart('TabsInfo',JSON_OBJECT('isWebCart',1,'ShopID',{$ShopID},'CounterPartsID',{$CounterPartsID},'SessionId','{$SessionId}','LanguageCode','ru'))";
        $this->tabs = DB::connection('tezarius')->select($sql);
        return $this->tabs;
    }
    public function getTabs()
    {
        if( !$this->tabs )
        {
            $ShopID = (int) Customer::singleton()->getStockIDGlobal();
            $UserID = (int) Customer::singleton()->getID();
            $SessID = (string) Cookie::get('tz_sid');
            $this->tabs = $this->loadTabs($ShopID,$UserID,$SessID);
        }
        return $this->tabs;

    }
    public function getTabActive()
    {
        return $this->tabActive;
    }
    public function setTabActive($tabActive)
    {
        $minutes = 60 * 24 * 30 * 3; /// час * сутки * месяц * сколько
        Cookie::queue('basketTabActive', $tabActive, $minutes);
        /// Так как куки отработают только при следующей загрузки, используем переменную
        $this->tabActive = $tabActive;
    }
    public function hasProducts()
    {
        if( !$this->tabs )
        {
            $ShopID = (int) Customer::singleton()->getStockIDGlobal();
            $UserID = (int) Customer::singleton()->getID();
            $SessID = (string) Cookie::get('tz_sid');
            $this->tabs = $this->loadTabs($ShopID,$UserID,$SessID);
        }
        return (bool)($this->tabs);
    }
    public function loadBasket($ShopID,$TabID,$UserID,$SessID,$ByRecord)
    {
        $sql = "call pCart('Get',JSON_OBJECT('isWebCart',1,'ShopID',{$ShopID},'tabID',{$TabID},'CounterPartsID',{$UserID},'SessionId','$SessID','ByRecord',{$ByRecord},'LanguageCode','ru'))";
        $this->products = DB::connection('tezarius')->select($sql);
        return $this->products;
    }
    public function getProducts()
    {
        if( $this->products ) return $this->products;
        $ShopID = (int) Customer::singleton()->getStockIDGlobal();
        $UserID = (int) Customer::singleton()->getID();
        $SessID = (string) Cookie::get('tz_sid');
        $TabID = (int) $this->getTabActive();
        $ByRecord = 0;
        $arr = $this->loadBasket($ShopID,$TabID,$UserID,$SessID,$ByRecord);
        return $arr;
    }
    public function add($rbg,$tzp,$qty,$std,$pld,$ShopID,$UserID,$PriceLID,$SessID,$TabID)
    {
        $GoodsTempID = 0;
        if( !$rbg )
        {
            $sql = "CALL pGoodsSupplierAdd(JSON_OBJECT('Source','price','StockID',{$std},'TZP','{$tzp}'),@id,1)";
            $rows = DB::connection('tezarius')->select($sql);
            $row = current((array)$rows);
            if( Arr::get($row,'isError ') ) return $row;
            ///$row['sql'] = $sql;
            ///return $row;
            $GoodsTempID = Arr::get($row,'id');
        }
        $sql = "call pCart('Add',JSON_OBJECT(
                         'isWebCart',1,
                         'ShopID',{$ShopID},
                         'tabID',{$TabID},
                         'CounterPartsID',{$UserID},
                         'SessionId','{$SessID}',
                         'id_rbGoods',{$rbg},
                         'GoodsTempID',{$GoodsTempID},
                         'qty',{$qty},
                         'PriceLevelID',{$PriceLID},
                         'StockID',{$std},
                         'PlaceID',{$pld},
                         'note','',
                         'isHideArticleCode',0,
                        'LanguageCode','ru'))"
        ;
        $rows = DB::connection('tezarius')->select($sql);
        $row = current((array)$rows);
        return $row;
    }

    public function update($id,$ShopID,$TabID,$UserID,$SessID,$qty,$note,$PriceLID)
    {
        $sql = "call pCart('Upd',JSON_OBJECT('isWebCart',1,'id',{$id},'ShopID',{$ShopID},'tabID',{$TabID},'CounterPartsID',{$UserID},'SessionId','$SessID',
        'qty',{$qty},'note','{$note}','PriceLevelID',{$PriceLID},'LanguageCode','ru'));";
        $rows = DB::connection('tezarius')->select($sql);
        $row = current((array)$rows);
        return $row;
    }

    public function remove($cart_id)
    {
        $sql = "call pCart('Upd',JSON_OBJECT('qty',0,'id',{$cart_id},'LanguageCode','ru'))";
        $rows = DB::connection('tezarius')->select($sql);
        $row = current((array)$rows);
        return $row;
    }

    public function clear()
    {
        $ShopID = (int) Customer::singleton()->getStockIDGlobal();
        $UserID = (int) Customer::singleton()->getID();
        $SessID = (string) Cookie::get('tz_sid');
        $TabID = (int) $this->getTabActive();

        $sql = "call pCart('TabDelete',JSON_OBJECT( 'isWebCart',1,'ShopID',{$ShopID},'CounterPartsID',{$UserID},'SessionId','{$SessID}','tabID',{$TabID},'LanguageCode','ru'))";
        $rows = DB::connection('tezarius')->select($sql);
        $row = current((array)$rows);
        return $row;
    }

    public function getRecurringProducts()
    {
        $product_data = [];
        foreach( $this->getProducts() as $value ) if( Arr::get($value,'recurring') ) $product_data[] = $value;
        return $product_data;
    }

    public function countProducts()
    {
        $product_total = 0;
        $products = $this->getProducts();
        foreach( $products as $product ) $product_total += $product['qty'];
        return $product_total;
    }

    public function hasRecurringProducts() {
        return count($this->getRecurringProducts());
    }

    public function fastBasketInfo()
    {
        $ans = [];

        $ShopID = (int) Customer::singleton()->getStockIDGlobal();
        $UserID = (int) Customer::singleton()->getID();
        $SessID = (string) Cookie::get('tz_sid');
        $tabActive = $this->getTabActive();
        $ans['items'] = $this->getProducts(); /// Basket::singleton()->loadBasket($ShopID,$tabActive,$UserID,$SessID,$ByRecord);

        $tabs = $this->loadTabs($ShopID,$UserID,$SessID);
        $tabInfo = [];
        foreach( $tabs AS $t ) if( (int)Arr::get($t,'tabID')===(int)$tabActive ){ $tabInfo = $t; break; }
        $ans['total'] = (int) Arr::get($tabInfo,'total');
        $ans['count'] = (int) Arr::get($tabInfo,'count');

        return $ans;
    }
}
/*
name : "Стойка амортизационная - Excel-G | перед прав | 333114"
properties : " "
code : "333114"
code_display : "333114"
brand : "KAYABA"
id_rbArticles : null
id_rbBrands : null
id : "780"
data_ins : "2019-09-25 16:17:19"
data : "2019-09-25 16:17:19"
id_rbUsers : "3"
tabID : "0"
id_rbCounterparts : null
isWebCart : "1"
id_rbStock_shop : "5"
id_rbStock : "6"
id_rbStockStoragePlace : null
id_rbGoods : null
id_rbGoods_Temp : "308"
qty : "1.000"
cost : "3540.00"
total : "3540.00"
cost_first : "3540.00"
discount : "0.00"
discount_detail : null
cost_in : "2281.90"
note : ""
isHideArticleCode : "0"
sess_id : "da81c316c44e46cfa657c8da69"
margin : "1258.10000"
unitName : ""
StorPlace : null
StockName : "Башавтоком"
qty_free : "100.000"
GoodsType : null
dscntGift : null
dscntBonus : null
dscntCard : null
dscntPromo : null
TaxSystemCode : ""
dlMin : "2019-10-02"
dlMax : "2019-10-09"
dlvr_period : "02 Октябрь- 09 Октябрь "
StockLogo : "DRM"
part : "1"
*/
