<?php
namespace App\Helpers\Carts;
use App\Helpers\Models\Model;
use App\Traits\Singleton;
use App\Helpers\Support\Arr;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class Store extends Model
{
    use Singleton;
	private $data ;
	public function init() {
	    /// https://stackoverflow.com/questions/18640607/what-is-better-stdclass-or-object-array-to-store-related-data#18640881
        ///$this->data = new \stdClass();
        $this->data = (object)[];

        $this->data->type = env('STORE_TYPE','auto');

        $offices = (array) DB::connection('tezarius')->select("CALL `pRB_get`('rbStock',JSON_OBJECT('filter','login'),'ru',1,0,1,'')");
        $this->setAccessStocks($offices);
        /// Default Choose
        $officeName = 'Офис не выбран';
        if( $officeID = Customer::singleton()->getStockID() )
        {
            $officeName = Customer::singleton()->getStockAddr();
            foreach( $offices AS $office ) if( Arr::get($office,'id')==$officeID )
            {
                ///$officeName = Arr::get($office,'address'); /// Хоть это тоде самое, что и выше, но пусть будет как дано
                $this->setCurrentStock($office);
                break;
            }
        }
        elseif( $officeID = Cookie::get('tz_office_id') )
        {
            $isCoincidence = FALSE;
            foreach( $offices AS $office ) if( Arr::get($office,'id')==$officeID )
            {
                $officeName = Arr::get($office,'address');
                $isCoincidence = TRUE;
                $this->setCurrentStock($office);
                break;
            }
            if( !$isCoincidence )
            {
                if( count($offices)==1 ) /// Не авторизован, не выбран офис и офис всего один
                {
                    $office_1 = Arr::get($offices,0);
                    $this->setCurrentStock($office_1);
                    $officeID = Arr::get($office_1,'id');
                    $officeName = Arr::get($office_1,'address');
                }
            }
        }
        elseif( count($offices)==1 ) /// Не авторизован, не выбран офис и офис всего один
        {
            $office_1 = Arr::get($offices,0);
            $this->setCurrentStock($office_1);
            $officeID = Arr::get($office_1,'id');
            $officeName = Arr::get($office_1,'address');
        }
        else $officeID = 0;
        $officeID = (int) $officeID;
        if( $officeID )
        {
            $minutes = 60 * 24 * 30 * 3; /// час * сутки * месяц * сколько
            Cookie::queue('tz_office_id', $officeID, $minutes);
        }
        $this->setOfficeName($officeName);
        $this->setOfficeID($officeID);
	}
    public function getType() {
        return $this->data->type;
    }
    public function isBase() {
        return $this->data->isBase;
    }
    public function isAuto() {
        return $this->data->isAuto;
    }
	public function setAccessStocks($offices) {
        $this->data->offices = $offices;
    }
	public function getAccessStocks() {
        return Arr::get($this->data,'offices',[]);
    }
	public function setCurrentStock($office) {
        $this->data->office = $office;
    }
	public function getCurrentStock() {
        return Arr::get($this->data,'office',[]);
    }
	public function getCurrentStockName() {
        $office = $this->getCurrentStock();
        return Arr::get($office,'address','');
    }
    public function getStockPhones($withMain = TRUE) {
        $_phones = [];
        $_office = $this->getCurrentStock();
        if( $_phone = Arr::get($_office,'phone1') ) array_push($_phones,Arr::get($_office,'phone1_code').' '.$_phone);
        if( $_phone = Arr::get($_office,'phone2') ) array_push($_phones,Arr::get($_office,'phone2_code').' '.$_phone);
        if( !$withMain ) array_shift($_phones);
        return $_phones;
    }
    public function getMainPhone(){
        $_phones = $this->getStockPhones();
        return Arr::get($_phones,0,'');
    }
    public function setOfficeName($name) {
        $this->data->officeName = $name;
    }
    public function getOfficeName() {
        return Arr::get($this->data,'officeName');
    }
    public function setOfficeID($name) {
        $this->data->officeID = $name;
    }
    public function getOfficeID() {
        return Arr::get($this->data,'officeID');
    }
    public function callback($subject,$body,$data)
    {
        $json = [];

        $isError = FALSE;

        $titleSuccess = 'Ваша заявка успешно отправлена!';
        $titleError = 'Не удалось отправить заявку';

        $ShopID = Customer::singleton()->getStockID();
        $CustomerUID = Customer::singleton()->getID();
        $car = Arr::get($data,'car');

        $name = Arr::get($data,'name');
        $phone = Arr::get($data,'phone');
        $email = Arr::get($data,'email');
        $ip = Arr::get($data,'ip');

        if( !$CustomerUID )
        {
            $sql = "call pANY_www('CustomerExistChecking',JSON_OBJECT('phone',?,'email',?),'ru')";
            $obj2 = $this->row($sql,[$phone,$email]);
            if( Arr::get($obj2,'isError') )
            {
                $CustomerUID = Arr::get($obj2,'CounterpartsID');
            }
            else
            {

                $sql = "CALL pRB_change('ins','rbCustomers',0,JSON_OBJECT('isWebsiteQuery',1,'isRequestCustomer',1,'name_i',?,'phone',?,'email',?,'id_rbStock',?),'ru',1)";
                $obj3 = $this->row($sql,[$name,$phone,$email,$ShopID]);
                if( Arr::get($obj3,'isError') )
                {
                    $json = ['state'=>'error','title'=>$titleError,'text'=>Arr::get($obj3,'mess')];
                    $isError = TRUE;
                }
                else
                {
                    $CustomerUID = Arr::get($obj3,'AnyID');
                }
            }
        }

        if( $CustomerUID )
        {
            $sql = "call `pANY`('RequestAdd',JSON_OBJECT(
                    'id_rbStock',?,
                    'id_rbCounterparts',?,
                    'subject',?,
                    'body',?,
                    'carId_out',?,
                    'ip',?
                 ),'ru')"
            ;
            $obj4 = $this->row($sql,[$ShopID,$CustomerUID,$subject,$body,$car,$ip]);
            if( Arr::get($obj4,'isError') )
            {
                $json = ['state'=>'error','title'=>$titleError,'text'=>Arr::get($obj4,'mess')];
                $isError = TRUE;
            }
        }

        if( !$isError )
        {
            $json = ['state'=>'success','title'=>$titleSuccess,'text'=>'Мы перезвоним Вам в ближайшее время и ответим на все вопросы.'];
        }

        $json['CustomerUID'] = $CustomerUID;
        return $json;
    }
    public function buildTreeHTMLCategory($items,$params=[])
    {
        $dom = new \DOMDocument();

        $ul  = $dom->appendChild( new \DOMElement( "ul" ) );
        $ul->setAttribute( "id", ''.(string)Arr::get($params,'fields.rootID','tree-root') );
        $ul->setAttribute( "class", ''.(string)Arr::get($params,'fields.rootID','tree-root') );

        $counter = 0; /// Вместо ID
        $placement = function($items,$root,$params,&$counter) use (&$placement)
        {
            ///++$counter;
            foreach( $items AS $f )
            {
                ///++$counter;

                $li = $root->appendChild( new \DOMElement( "li" ) );

                $node = Arr::get($f,''.(string)Arr::get($params,'fields.nodeID','id'));
                $text = Arr::get($f,''.(string)Arr::get($params,'fields.nodeName','name'));

                /// Ссылка или наимеование подмею
                $a = $li->appendChild( new \DOMElement( "a" ) );
                /// Иконка
                if( $icon = Arr::get($f,''.(string)Arr::get($params,'fields.nodeIcon','icon')) )
                {
                    $iconType = Arr::get($params,'fields.iconType');
                    if( 'class'==$iconType )
                    {
                        $i = $a->appendChild( new \DOMElement( "i" ) );
                        $i->setAttribute('class',$icon);
                        $a->appendChild( new \DOMText( ' ' ) );
                        $a->setAttribute("data-info",$node);
                    }
                    if( 'path'==$iconType )
                    {
                        $img = $a->appendChild( new \DOMElement( "img" ) );
                        $img->setAttribute('src',$icon);
                        $img->setAttribute('width',20);
                        $a->appendChild( new \DOMText( ' ' ) );
                        $a->setAttribute("data-info",$node);
                    }
                }

                if( $children = Arr::get($f,''.(string)Arr::get($params,'fields.children','children')) )
                {
                    $dID = ++$counter;
                    ///$dID = md5($text);

                    /// Название
                    $a->appendChild( new \DOMText( $text ) );
                    $a->setAttribute("id","a{$dID}");
                    $a->setAttribute("data-group","$dID");
                    $a->setAttribute("data-node",$node);
                    $a->setAttribute("data-name",$text);
                    $a->setAttribute('class','node-link');
                    ///$a->setAttribute("onClick","yulsun.catalogs.tree('$dID',event)");


                    /// Рекурсия
                    $ul = $li->appendChild( new \DOMElement( "ul" ) );
                    $ul->setAttribute('id',"ul{$dID}");
                    $ul->setAttribute('class',"submenu submenu_{$dID} ".(string)Arr::get($params,'fields.subMenuClass','submenu'));

                    ///++$counter;
                    $placement($children,$ul,$params,$counter);
                }
                else
                {
                    /// Название
                    $a->appendChild( new \DOMText( $text ) );

                    /// Действие
                    if(  $name = Arr::get($params,'route.name') )
                    {
                        $__ = Arr::get($params,'route.params');
                        $_arr = ( is_callable($__) ) ? call_user_func($__,$f) : $__;
                        $_arr = (array) $_arr;
                        $a->setAttribute("href",route($name,['hash'=>Arr::encryption($_arr)]));
                    }

                    /// Накидываем
                    $a->setAttribute("class",'node-link '.(string)Arr::get($params,'fields.itemClass','link'));
                    $a->setAttribute("data-node",$node);
                    $a->setAttribute("data-name",$text);
                }
            }
        };
        $placement($items,$ul,$params,$counter);

        $html = $dom->saveHTML();
        return $html;
    }
    public function fitPriceResult(&$offers,$params=[])
    {
        $arrOrigin = $arrAnalog = [];
        $arrBrands = [];
        $arrPrices = [];
        $arrDLVMin = [];
        $arrDLVMax = [];
        $priceMax = 0;
        $deliveryMax = 0;
        $qtyMax = 0;
        foreach( $offers AS &$row )
        {
            $_brand = Arr::get($row,'brand','unknown');
            $_price = Arr::get($row,'cost',0) * 1;
            $_count = Arr::get($row,'qty',0) * 1;
            $_d_min = Arr::get($row,'dlvrWeb_min',0) * 1;
            $_d_max = Arr::get($row,'dlvrWeb_max',0) * 1;

            if( $_price > $priceMax ) $priceMax = $_price;
            if( $_d_min > $deliveryMax ) $deliveryMax = $_d_min;
            if( $_count > $qtyMax ) $qtyMax = $_count;

            if( !in_array($_brand,$arrBrands) ) array_push($arrBrands,$_brand);
            if( !in_array($_price,$arrPrices) ) array_push($arrPrices,$_price);
            if( !in_array($_d_min,$arrDLVMin) ) array_push($arrDLVMin,$_d_min);
            if( !in_array($_d_max,$arrDLVMax) ) array_push($arrDLVMax,$_d_max);

            $row->more = route('goodsInfo',['hash' => Arr::encryption([
                'artbrID' => (int) Arr::get($row,'id_rbArticles'),
                'brandID' => (int) Arr::get($row,'id_rbBrands'),
                'brand'   => $_brand,
                'code'    => Arr::get($row,'code'),
                'goodsID' => (int) Arr::get($row,'GoodsID'),
                'stockName' => Arr::get($row,'rbStock_name'),
                ///
                'back' => Arr::get($params,'back'),
                'offer' => [
                    'price' => nl2br(Arr::get($row,'cost_display')),
                    'cost' => $_price,
                    'qty'  => $_count,
                    'step' => (int) Arr::get($row,'cost_for_qty'),
                    'rbg'  => (int) Arr::get($row,'GoodsID'),
                    'tzp'  => Arr::get($row,'tzp'),
                    'std'  => (int) Arr::get($row,'id_rbStock'),
                    'pld'  => (int) Arr::get($row,'id_rbStockStoragePlace'),
                    'reward' => Customer::singleton()->getRewardPoints(),
                ],
            ])]);

            if( !Arr::get($row,'SortPrty2') ) array_push($arrOrigin,$row);
            else array_push($arrAnalog,$row);
        }
        $offers = [
            'origin'=>[
                'items'=>$arrOrigin,
                'count'=>count($arrOrigin),
                'limit'=>5,
            ],
            'analog'=>[
                'items'=>$arrAnalog,
                'count'=>count($arrAnalog),
                'limit'=>15,
            ],
            'filters' => [
                'arrBrands' => $arrBrands,
                'arrPrices' => $arrPrices,
                'arrDLVMin' => $arrDLVMin,
                'arrDLVMax' => $arrDLVMax,
                'priceMax' => $priceMax,
                'deliveryMax' => $deliveryMax,
                'qtyMax' => $qtyMax,
            ]
        ];
        ///ddd($offers);
    }
}