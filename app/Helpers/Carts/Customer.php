<?php
namespace App\Helpers\Carts;
use App\Helpers\Models\Model;
use App\Traits\Singleton;
use App\Helpers\Support\Arr;
use \Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Customer extends Model
{
    use Singleton;

    public $isTezarius = TRUE;

    private $data = []; /// Общий массив
    private $customer_id;
    private $discountCardID;
    private $isAdminWebsite;

    private $price_lid; /// Уровень цены клиента
    private $org_name;
    private $org_inn;
    private $org_addr;
    private $org_kpp;
    private $org_ogrn;

    private $manager_id;
    private $source_id;
    private $stock_id;
    private $stock_addr;
    private $stock_name;
    private $firm_id;
    private $stock_phone_1;
    private $stock_phone_2;

    private $firstname;
    private $lastname;
    private $patronymic;
    private $group_id; /// customer_group_id => group_id
    private $email;
    private $telephone;
    private $newsletter;
    private $address_id;
    private $country_id;

    /// Внешние объекты
    private $session;
    private $store;
    public function init()
    {
        ///$this->store = Store::singleton(); /// 2020.01.30
        $this->data = Session::get('customer',[]);

        if( $this->data )
        {
            $hashAuth = Arr::get($this->data,'hashAuth'); /// - Всегда есть (ДОЛЖЕН БЫТЬ) когда есть объект CUSTOMER в SESSION как и CounterpartsID
            $hashAuth = Arr::decryption($hashAuth);
            if( $hashAuth && is_object($hashAuth) )
            {
                $login = Arr::get($hashAuth,'login');
                $passwd = Arr::get($hashAuth,'passwd');
                if( !$this->login($login,$passwd) ) $this->logout();
            }
        }
    }

    public function login($login,$passwd)
    {
        $row = DB::connection('tezarius')->selectOne("CALL pRB_get('rbCustomers',JSON_OBJECT('filter','Authorization','login','$login','pass','$passwd'),'ru',1,0,1,'')");
        if( !empty($row) )
        {
            $this->data = $row;
            $this->data->hashAuth = Arr::encryption([
                'login' => $login,
                'passwd' => $passwd,
            ]);
            session(['customer'=>$this->data]);

            $this->customer_id = (int) Arr::get($row,'id_rbCounterparts');
            $this->discountCardID = (int) Arr::get($row,'DiscountCardID');
            $this->isAdminWebsite = (int) Arr::get($row,'isAdminWebsite');

            $this->price_lid = (int) Arr::get($row,'id_rbPriceLevels');
            $this->org_name = Arr::get($row,'name_full');
            $this->org_inn  = Arr::get($row,'inn');
            $this->org_addr = Arr::get($row,'address');
            $this->org_kpp  = Arr::get($row,'kpp');
            $this->org_ogrn = Arr::get($row,'ogrn');

            $this->manager_id = (int) Arr::get($row,'id_rbUsers_manager');
            $this->source_id  = (int) Arr::get($row,'id_rbCustomersSource');
            $this->stock_id   = (int) Arr::get($row,'id_rbStock');
            $this->stock_addr = Arr::get($row,'StockAddress');
            $this->stock_name = Arr::get($row,'rbStock_id_rbStock');

            ///session(['selectedStockID'=>$this->stock_id]);
            $minutes = 60 * 24 * 30 * 3; /// час * сутки * месяц * сколько
            Cookie::queue('tz_office_id', $this->stock_id, $minutes);

            $this->firm_id    = (int) Arr::get($row,'id_rbFirms',env('DEFAULT_FIRM',777),TRUE);
            $this->stock_phone_1 = Arr::get($row,'StockPhone1');
            $this->stock_phone_2 = Arr::get($row,'StockPhone2');

            $this->firstname   = Arr::get($row,'name_i');
            $this->lastname    = Arr::get($row,'name_f');
            $this->patronymic  = Arr::get($row,'name_o');
            $this->group_id    = Arr::get($row,'group_id');
            $this->email       = Arr::get($row,'email');
            $this->telephone   = Arr::get($row,'phone_formated');
            $this->newsletter  = Arr::get($row,'newsletter');
            $this->address_id  = Arr::get($row,'rbStock_id_rbStock');
            $this->country_id  = 176;

            return TRUE;
        }
        return FALSE;
    }

    public function logout()
    {
        Session::remove('customer');

        $this->data        = NULL;
        $this->customer_id = NULL;
        $this->discountCardID = NULL;
        $this->isAdminWebsite = NULL;

        $this->price_lid  = NULL;
        $this->manager_id = NULL;
        $this->source_id  = NULL;
        $this->stock_id   = NULL;
        $this->firm_id    = NULL;
        $this->org_name   = NULL;
        $this->org_inn    = NULL;
        $this->org_addr   = NULL;

        $this->firstname   = NULL;
        $this->lastname    = NULL;
        $this->group_id    = NULL;
        $this->email       = NULL;
        $this->telephone   = NULL;
        $this->newsletter  = NULL;
        $this->address_id  = NULL;
        $this->country_id  = NULL;

        $this->stock_phone_1 = NULL;
        $this->stock_phone_2 = NULL;
    }

    public function isLogged() {
        return ($this->customer_id);
    }

    public function getID() {
        return (int) $this->customer_id;
    }
    public function getSessionID() {
        ///return Session::getId();
        return (string) Cookie::get('tz_sid');
    }
    public function getSID(){
        return ( $uid = $this->getID() ) ? $uid : $this->getSessionID();
    }

    public function getManagerID() {
        return (int) $this->manager_id;
    }
    public function getSourceID() {
        return (int) $this->source_id;
    }
    public function getStockID() {
        return (int) $this->stock_id;
    }
    public function getStockIDGlobal() {
        $officeID =  (int) ( $this->stock_id ? $this->stock_id : Cookie::get('tz_office_id') ) ?? Store::singleton()->getOfficeID();
        if( !$officeID ) $officeID = (int) Store::singleton()->getOfficeID();
        return $officeID;
    }
    public function getStockAddr() {
        return $this->stock_addr;
    }
    public function getStockName() {
        return $this->stock_name;
    }
    public function getStockPhones() {
        $_phones = [];
        if( $this->stock_phone_1 ) array_push($_phones,$this->stock_phone_1); /// С базы 1
        if( $this->stock_phone_2 ) array_push($_phones,$this->stock_phone_2); /// С базы 2
        if( empty($_phones) ) Store::singleton()->getStockPhones();           /// Если пользователь не авторизован берем телефоны активного склада
        return $_phones;
    }
    public function getFirmID() {
        return (int) $this->firm_id;
    }


    public function getPriceLID() {
        return (int) $this->price_lid;
    }
    public function getDiscountCardID() {
        return (int) $this->discountCardID;
    }

    private $discountInfo = NULL;
    public function getDiscountInfo($fromCache = FALSE)
    {
        if( $fromCache && $this->discountInfo ) return $this->discountInfo;
        $discountCardID = $this->getDiscountCardID();
        if( !$discountCardID ) return NULL;
        $sql = "call `pRB_get`('rbDiscount',JSON_OBJECT('filter','id','id',{$discountCardID}),'ru',1,0,1,'')";
        $discountInfo = (object) $this->row($sql);
        $discountInfo->status = (Arr::get($discountInfo,'onBonus')||Arr::get($discountInfo,'percent_max'));
        $this->discountInfo = $discountInfo;
        return $discountInfo;
    }
    public function getRewardPoints()
    {
        $row = $this->getDiscountInfo();
        return Arr::get($row,'onBonus');
    }

    public function isAdminWebsite() {
        return (bool) $this->isAdminWebsite;
    }

    public function getFirstName() {
        return $this->firstname;
    }

    public function getLastName() {
        return $this->lastname;
    }

    public function getPatronymic() {
        return $this->patronymic;
    }

    public function getFullName() {
        return "{$this->lastname} {$this->firstname} {$this->patronymic}";
    }

    public function getGroupId() {
        return $this->group_id;
    }

    public function getEmail() {
        return $this->email;
    }

    /**
     * Альтернатива из public/catalog/model/account/customer.php
     * Нет смыслда бегать в базу, если тут есть все данные
     * Лучше добавить пару полей, чем добзапрос
    */
    public function getCustomer()
    {
        return [
            'firstname'  => $this->firstname,
            'lastname'   => $this->lastname,
            'patronymic' => $this->patronymic,
            'email'      => $this->email,
            'telephone'  => $this->telephone,
            'org_name'   => $this->org_name,
            'org_inn'    => $this->org_inn,
            'org_addr'   => $this->org_addr,
            'org_kpp'    => $this->org_kpp,
        ];
    }

    public function getData() {
        return $this->data;
    }

    public function setHashAuth($login,$passwd)
    {
        $hashAuth = Arr::encryption([
            'login' => $login,
            'passwd' => $passwd,
        ]);
        $this->data['hashAuth'] = $hashAuth;
        Session::put('customer',$this->data);
        return $hashAuth;
    }
    public function getHashAuth() {
        $hashAuth = Arr::get($this->data,'hashAuth');
        return $hashAuth;
    }
    public function getLogin() {
        $hashAuth = $this->getHashAuth();
        $hashAuth = Arr::decryption($hashAuth);
        $login = Arr::get($hashAuth,'login');
        return $login;
    }
    public function getPasswd() {
        $hashAuth = $this->getHashAuth();
        $hashAuth = Arr::decryption($hashAuth);
        $passwd = Arr::get($hashAuth,'passwd');
        return $passwd;
    }

    public function getTelephone() {
        return $this->telephone;
    }

    /*/ /// Поидеи, нужно не всегда, перенесено пока в public/catalog/model/account/customer.php
    public function getNewsletter() {
        return $this->newsletter;
    }
    //*/

    public function getAddressId() {
        return $this->address_id;
    }

    public function getProperty($name) {
        return Arr::get($this->data,$name);
    }

    private $balanceInfo = NULL;
    public function getBalanceInfo($fromCache = FALSE)
    {
        /**
         * onBalance - на балансе по выданным
         * onActiveOrders - активных, невыданных заказов
         * onDebtOverdue - долг просрочено
         * onCredit - сумма отсрочки по договору поставки
        */
        if( $fromCache && $this->balanceInfo ) return $this->balanceInfo;
        $customerID = $this->getID();
        if( !$customerID ) return NULL;
        $sql = "CALL pFinanceCounterpartsInfo(JSON_OBJECT('type','Balance','id_rbCounterparts',?))";
        $row = $this->row($sql,[$customerID]);
        $this->balanceInfo = $row;
        return $row;
    }
}
/*
{#366 ▼
  +"id": "10153"
  +"id_rbCounterparts": "10326"
  +"id_rbUsers": "7"
  +"data_ins": "2020-01-06 17:11:39"
  +"data": "2020-01-06 17:11:39"
  +"isMarked": "N"
  +"name": "Трошков Роман"
  +"name_f": "Трошков"
  +"name_i": "Роман"
  +"name_o": null
  +"name_full": null
  +"inn": null
  +"kpp": null
  +"ogrn": null
  +"id_rbCounterparts_group": null
  +"id_rbGroups": "44"
  +"id_rbPriceLevels": "5"
  +"email": "borodatych@demka.org"
  +"id_rbCountry_phone": "1725"
  +"phone": "9505530373"
  +"phone_formated": "+79505530373"
  +"phones": null
  +"comment": null
  +"address_delivery": null
  +"address": null
  +"sms_allow": null
  +"birthDay": null
  +"id_rbStock": "13"
  +"id_rbFirms": null
  +"id_rbUsers_manager": null
  +"isNeedFullPayOrderWork": null
  +"isDelivery": null
  +"login": "borodatych@demka.org"
  +"pass": "55557777"
  +"id_rbCustomersSource": null
  +"isWholesaler": "0"
  +"isDeliveryNoBell": null
  +"PercentPrepayToWorkOrder": "50"
  +"CommunicationLevel": null
  +"CallingLastState": null
  +"CallingLastDate": null
  +"rbStock_id_rbStock": "Магазин на Южном"
  +"StockAddress": "г.Москва, ул. Южная, д.333, к.2 "
  +"rbCounterparts_group": null
  +"isSendPriceOn": "0"
  +"rbCounterparts_id_rbCounterparts_group": null
  +"rbCountry_id_rbCountry_phone": "РОССИЯ"
  +"rbCustomersSource_id_rbCustomersSource": null
  +"rbFirms_id_rbFirms": null
  +"rbGroups_id_rbGroups": "3. ИНТЕРНЕТ Новые !!!"
  +"rbPriceLevels_id_rbPriceLevels": "Базовая розница"
  +"rbUsers_id_rbUsers": "Давыдов Артем Павлович"
  +"rbUsers_id_rbUsers_manager": null
  +"DiscountCard": null
  +"DiscountCardID": null
  +"DiscountCardType": null
  +"StockPhone1": "(917) 557-78-99"
  +"StockPhone2": "(917) 557-78-99"
  +"isAdminWebsite": "0"
}
*/
