<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if( App::environment('production') )
        {
            URL::forceScheme('https');
        }
        view()->composer('layouts.base', 'App\Http\Composers\BaseComposer');
        view()->composer('layouts.room', 'App\Http\Composers\RoomComposer');

        /** @used @nl2br($text)  */
        Blade::directive('nl2br', function ($text) {
            return nl2br($text);
        });
        Blade::setEchoFormat('nl2br(e(%s))');
    }
}
