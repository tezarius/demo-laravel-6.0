"use strict";
// Class definition

const TZNews = function() {
    // Private functions

    let newsDelete = function(ths){
        TZAlert.fire({
            title: 'Удалить новость?',
            ///text: "Укажите причину!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Продолжить',
            cancelButtonText: 'Отмена',
            showLoaderOnConfirm: true,
            /*/
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            //*/
            preConfirm: (data) => {
                ///console.log('|.:: TZNews newsDelete preConfirm ::.|',data);
                /// https://learn.javascript.ru/fetch
                return fetch( $(ths).data('href'), {
                    method: 'POST',
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    ///body: JSON.stringify({note:data}),
                }).then(response => {
                    ///console.log('| .:: SWEET ALERT PRE ::. |',response);
                    if( !response.ok ) throw new Error(response.statusText);
                    return response.json();
                }).catch(error => {
                    TZAlert.showValidationMessage(`Request failed: ${error}`);
                })
            },
            allowOutsideClick: () => !TZAlert.isLoading()
        }).then((body) => {
            console.log('| .:: SWEET ALERT THN ::. |',body);
            if( body.value ){
                TZAlert.fire({
                    type: body.value.type,
                    title: body.value.text,
                    ///text: json.message.text,
                }).then(response => {
                    location.reload();
                });
            }
        });
        return false;
    };
    // news table initializer
    let common = function() {
        let $table = $('.kt-datatable');
        let urlEdit = $table.data('url-edit');
        let urlDelete = $table.data('url-delete');
        let datatable = $table.KTDatatable({
            data: {
                saveState: {
                    cookie: false
                },
                pageSize: 100,
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false,
            },
            layout: {
                scroll: true,
                height: 550,
                footer: false,
            },
            search: {
                input: $('#generalSearch'),
            },
            columns: [
                {
                field: 'Анонс',
                type: 'text',
                overflow: 'visible',
                autoHide: false,
            }, {
                field: 'Начало',
                type: 'date',
                format: 'DD.MM.YYYY',
                responsive: {
                    visible: 'md',
                    hidden: 'sm'
                }
            }, {
                field: 'Окончание',
                type: 'date',
                format: 'DD.MM.YYYY',
                responsive: {
                    visible: 'md',
                    hidden: 'sm'
                }
            }, {
                field: 'Статус',
                title: '#Статус',
                autoHide: false,
                responsive: {
                    visible: 'md',
                    hidden: 'sm'
                },
                // callback function support for column rendering
                template: function(row) {
                    let status = {
                        1: {
                            'title': 'В архиве',
                            'class': ' kt-badge--metal'
                        },
                        0: {
                            'title': 'Активная',
                            'class': ' kt-badge--success'
                        },
                    };
                    let isArchive = row.isArchive||0;
                    return '<span class="kt-badge ' + status[isArchive].class + ' kt-badge--inline kt-badge--pill">' + status[isArchive].title + '</span>';
                },
            }, {
                field: 'Действие',
                title: 'Actions',
                sortable: false,
                width: 70,
                overflow: 'visible',
                autoHide: false,
                template: function(row) {
                    return '\
						<a href="'+urlEdit.replace(/@ID/gi,row.id)+'" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Редактировать">\
							<i class="flaticon2-edit"></i>\
						</a>\
						<a data-href="'+urlDelete.replace(/@ID/gi,row.id)+'" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Удалить!!" id="btnNewsDelete">\
							<i class="flaticon2-delete"></i>\
						</a>\
					';
                },
            },{
                field: 'id',
                title: '#',
                type: 'number',
                width: 30,
                sortable: false,
                selector: false,
                textAlign: 'center',
                visible: false,
            },{
                field: 'body',
                title: '#',
                type: 'text',
                width: 30,
                sortable: false,
                selector: false,
                visible: false,
            }, ],
        });

        $('#kt_form_status').on('change', function() {
            datatable.search($(this).val(), 'Статус');
        }).selectpicker();

        $table.on('click', '#btnNewsDelete', function () {
            newsDelete(this);
        });
    };

    return {
        // Public functions
        init: function() {
            // init common
            common();
        }
    };
}();

jQuery(document).ready(function() {
    TZNews.init();
});