"use strict";
// Class definition

const TZPage = function(){
    let $editor;
    let currentEditor;
    let currentContent;
    let editors = [];
    // Private functions
    editors['ckeditor'] = function(selector){
        BalloonEditor
            .create( $(selector)[0] )
            .then( editor => {
                ///console.log( editor );
                $editor = editor;
                currentContent = $editor.getData();
            } )
            .catch( error => {
                console.error( error );
            })
        ;
    };
    editors['none'] = function(selector){
        console.log('|.:: TZPage save ::.|',currentEditor);
        let field = $(selector).prop("tagName" ).toLowerCase();
        if( 'textarea'===field ){
            currentEditor = field;
            $editor = $(selector);
            currentContent = $editor.val();
        }
    };
    editors['form'] = function(selector){
        ///console.log('|.:: TZPage save ::.|',currentEditor);
        $editor = $(selector);
        currentContent = $editor.serialize();
    };
    let getData = function(){
        let data = '';
        if( 'ckeditor'===currentEditor ) data = 'content=' + encodeURIComponent($editor.getData());
        if( 'textarea'===currentEditor ) data = 'content=' + encodeURIComponent($editor.val());
        if( 'form'===currentEditor ) data = $editor.serialize();
        return data;
    };

    let editor = function(editor,place){
        currentEditor = editor;
        if( editors[editor] instanceof Function ) editors[editor](place);
    };

    /// https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/saving-data.html
    let save = function(ths){
        let data = getData();
        console.log('|.:: TZPage save ::.|',currentEditor,data);
        $.ajax({
            url: $(ths).data('route'),
            type: 'post',
            data: data,
            dataType: 'json',
            success: function(json){
                console.log('|.:: TZPage save ::.|',json);
                if( json.isGood ) TZAlert.fire('Успешно',json.message,'success');
                else if( json.isError ) TZAlert.fire('Ошибка',json.message,'error');
                else TZAlert.fire('Что-то пошло не так :(','Обратитесь в службу поддержки','error');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let reset = function(){
        if( 'ckeditor'===currentEditor ) $editor.setData(currentContent);
        if( 'textarea'===currentEditor ) $editor.val(currentContent);
    };

    return {
        // public functions
        init: function() {
            //
        }
        ,editor: editor
        ,save: save
        ,reset: reset

    };
}();

// Initialization
document.ready(function() {
    TZPage.init();
});