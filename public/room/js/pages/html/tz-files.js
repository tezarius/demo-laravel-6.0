const TZFiles = function(){
    // Private Function
    let params = {};
    let common = function(params){
        //
    };
    let settings = function(_params){
        params = _params || {};
        initUppy();
    };

    let uppyMin;

    ///const Tus = Uppy.Tus;
    const XHRUpload = Uppy.XHRUpload;
    const StatusBar = Uppy.StatusBar;
    const FileInput = Uppy.FileInput;
    const Informer = Uppy.Informer;
    let initUppy = function(){
        ///console.log('| .:: INIT UPPY PARAMS ::. |',params);
        // Uppy variables
        // For more info refer: https://uppy.io/
        let elemId = params.elemId || 'kt_uppy';
        let id = '#' + elemId;
        let $statusBar = $(id + ' .kt-uppy__status');
        let $uploadedList = $(id + ' .kt-uppy__list');
        let timeout;

        uppyMin = Uppy.Core({
            debug: true,
            autoProceed: false,
            locale: Uppy.locales.ru_RU, /// https://uppy.io/docs/locales/#List-of-locale-packs
            showProgressDetails: true,
            allowMultipleFiles: true,///params.allowMultipleFiles || false,
            restrictions: {
                maxFileSize: params.maxFileSize || 1000000, /// 1Mb /// 1 000 000 == 1mb ; 1 000 == 1kb ;
                maxNumberOfFiles: params.maxFiles || 1,
                minNumberOfFiles: params.minFiles || 1,
                allowedFileTypes: params.allowedFileTypes
            }
        });

        uppyMin.use(FileInput, { target: id + ' .kt-uppy__wrapper', pretty: false });
        uppyMin.use(Informer, { target: id + ' .kt-uppy__informer'  });

        // demo file upload server
        ///uppyMin.use(Tus, { endpoint: 'https://master.tus.io/files/' });
        uppyMin.use(XHRUpload, {
            fieldName: params.fieldName + ( params.bundle?'[]':'') || 'files',
            formData: params.formData || true,
            bundle: params.bundle || false,
            method: 'post',
            endpoint: params.uploadURI || 'https://master.tus.io/files/',
            headers:{'X-CSRF-TOKEN':document.head.querySelector('meta[name="csrf-token"]').content},
        });
        uppyMin.use(StatusBar, {
            target: id + ' .kt-uppy__status',
            hideUploadButton: params.hideUploadButton || true,
            hideAfterFinish: false
        });
        $(id + ' .uppy-FileInput-input').addClass('kt-uppy__input-control').attr('id', elemId + '_input_control');

        $(id + ' .uppy-FileInput-container').append('<label class="kt-uppy__input-label btn btn-label-brand btn-bold btn-font-sm" for="' + (elemId + '_input_control') + '">Выбрать</label>');
        let $fileLabel = $(id + ' .kt-uppy__input-label');

        uppyMin.on('upload', function(data) {
            $fileLabel.text("Uploading...");
            $statusBar.addClass('kt-uppy__status--ongoing');
            $statusBar.removeClass('kt-uppy__status--hidden');
            clearTimeout( timeout );
            console.log('| .:: UPLOAD 1 ICON ::. |',data);
        });

        uppyMin.on('upload-success', function(file,response) {
            let status = response.status;
            let body = response.body;
            // do something with file and response
            //*/
            console.log('| .:: UPLOAD 2 ICON ::. |',{
                file:file,
                status:status,
                body:body,
            });
            //*/
            if( body.isError ){
                TZAlert.fire( 'Ошибка :(', body.message,'error');
            }
            else{
                TZAlert.fire('Успешно :)',body.message,'success' ).then(() => location.reload() );
            }
        });

        uppyMin.on('complete', function(file) {
            $.each(file.successful, function(index, value){
                let sizeLabel = "bytes";
                let filesize = value.size;
                if (filesize > 1024){
                    filesize = filesize / 1024;
                    sizeLabel = "kb";

                    if(filesize > 1024){
                        filesize = filesize / 1024;
                        sizeLabel = "MB";
                    }
                }
                let uploadListHtml = '' +
                    '<div class="kt-uppy__list-item" data-id="'+value.id+'">' +
                    '<div class="kt-uppy__list-label">'+value.name+' ('+ Math.round(filesize, 2) +' '+sizeLabel+')</div>' +
                    '<span class="kt-uppy__list-remove" data-id="'+value.id+'">' +
                    '<i class="flaticon2-cancel-music"></i>' +
                    '</span>' +
                    '</div>';
                $uploadedList.append(uploadListHtml);
            });

            $fileLabel.text("Добавить файл");

            $statusBar.addClass('kt-uppy__status--hidden');
            $statusBar.removeClass('kt-uppy__status--ongoing');
        });

        $(document).on('click', id + ' .kt-uppy__list .kt-uppy__list-remove', function(){
            let itemId = $(this).attr('data-id');
            uppyMin.removeFile(itemId);
            $(id + ' .kt-uppy__list-item[data-id="'+itemId+'"').remove();
        });
    };

    let upload = function(){
        uppyMin.upload();
    };
    let cancel = function(){
        uppyMin.reset();
        /// Пока не нашел как форму очистить, так быстрее было
        location.reload();
    };

    let remove = function(ths){
        let $ths = $(ths);
        TZAlert.fire({
            title: params.removeMSG || 'Удалить изображение ?',
            ///text: "Вы всегда сможете узнать актуальные цены и повторно добавить позицию!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет'
        }).then((result) => {
            if( result.value ){
                $.ajax({
                    url: params.removeURI || 'https://master.tus.io/files/',
                    type: 'post',
                    data: 'hash=' + $ths.data('hash'),
                    dataType: 'json',
                    success: function(json){
                        console.log('|.:: TZFiles delIcon ::.|',json);
                        if( json.isError ){
                            TZAlert.fire( 'Ошибка :(', json.message,'error');
                        }
                        else{
                            TZAlert.fire('Успешно :)',json.message,'success' ).then(() => location.reload() );
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        });
    };

    return {
        // Public Function
        init: function () {
            common();
        }
        ,settings : settings
        ,upload : upload
        ,cancel : cancel
        ,remove : remove
    };
}();
document.ready(function(){
    TZFiles.init();
});