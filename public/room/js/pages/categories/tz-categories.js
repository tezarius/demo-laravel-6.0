const TZCategory = function(){
    // Private Function
    let $categorySetIconBtn;
    let common = function(){
        let $tree = $('#tz-tree');
        $tree.treeBuilder({
            iconOpened:'fa fa-folder-open',
            iconClosed:'fa fa-folder',
            subMenuRoot:'tree-submenu',
            collapseMenuItems: true,
            hideClass: 'kt-hide',
            showClass: 'kt-show',
            openClass: 'tz-menu__item--open',
            closeClass: 'tz-menu__item--close',
            /// Добавляем действия, не обязательно
            actions:{
                icon:'flaticon2-settings',
                type:'dropdown', /// Пока только этот вариант
                items:[
                    {icon:'flaticon2-image-file',text:'Установить иконку',action:'TZCategory.setIcon(this)'},
                    {icon:'flaticon2-trash',text:'Удалить иконку',action:'TZCategory.delIcon(this)'},
                    {icon:'flaticon-medal',text:'Рекомендуемые товары',action:'TZCategory.recommended(this)'},
                ],
            },
        });
        $tree.treeFilter({
            menuParentRoot: '#tz-tree',
            menuParentShow: 'kt-show',
            menuParentHide: 'kt-hide',

            menuTitleRoot: '.item__branch',
            menuTitleText: '.item__branch > a',
            menuTitleOpen: 'active',
            menuTitleClose: '',

            menuItemsRoot: '.tree-submenu',
            menuItemsShow: 'kt-show',
            menuItemsHide: 'kt-hide',

            menuItemRoot: '.item__link',
            menuItemText: '.item__link > .pricing',
            menuItemTags: '.item__link > .pricing > .keywords-tags',
            menuItemShow: 'active',
            menuItemHide: 'kt-hide',

            ///searchWithMenuText: true,
            searcher: $('#input-search-tree'),
            reset: $('#btn-search-reset')
        });

        $categorySetIconBtn = $('#categorySetIconBtn');
    };

    let nodeID;
    let uppyMin;

    ///const Tus = Uppy.Tus;
    const XHRUpload = Uppy.XHRUpload;
    const StatusBar = Uppy.StatusBar;
    const FileInput = Uppy.FileInput;
    const Informer = Uppy.Informer;
    let initUppy5 = function(){
        // Uppy variables
        // For more info refer: https://uppy.io/
        let elemId = 'kt_uppy_5';
        let id = '#' + elemId;
        let $statusBar = $(id + ' .kt-uppy__status');
        let $uploadedList = $(id + ' .kt-uppy__list');
        let timeout;

        uppyMin = Uppy.Core({
            debug: true,
            autoProceed: false,
            locale: Uppy.locales.ru_RU, /// https://uppy.io/docs/locales/#List-of-locale-packs
            showProgressDetails: true,
            allowMultipleFiles: false,
            restrictions: {
                maxFileSize: 50000, /// 50kb /// 1 000 000 == 1mb ; 1 000 == 1kb ;
                maxNumberOfFiles: 1,
                minNumberOfFiles: 1
            }
        });

        uppyMin.use(FileInput, { target: id + ' .kt-uppy__wrapper', pretty: false });
        uppyMin.use(Informer, { target: id + ' .kt-uppy__informer'  });

        // demo file upload server
        ///uppyMin.use(Tus, { endpoint: 'https://master.tus.io/files/' });
        uppyMin.use(XHRUpload, {
            fieldName: 'icon',
            formData: true,
            method: 'post',
            endpoint: $categorySetIconBtn.data('icon-add'),
            headers:{'X-CSRF-TOKEN':document.head.querySelector('meta[name="csrf-token"]').content},
        });
        uppyMin.use(StatusBar, {
            target: id + ' .kt-uppy__status',
            hideUploadButton: true,
            hideAfterFinish: false
        });
        $(id + ' .uppy-FileInput-input').addClass('kt-uppy__input-control').attr('id', elemId + '_input_control');

        $(id + ' .uppy-FileInput-container').append('<label class="kt-uppy__input-label btn btn-label-brand btn-bold btn-font-sm" for="' + (elemId + '_input_control') + '">Выбрать</label>');
        let $fileLabel = $(id + ' .kt-uppy__input-label');

        uppyMin.on('upload', function(data) {
            $fileLabel.text("Uploading...");
            $statusBar.addClass('kt-uppy__status--ongoing');
            $statusBar.removeClass('kt-uppy__status--hidden');
            clearTimeout( timeout );
            ///console.log('| .:: UPLOAD 1 ICON ::. |',data);
        });

        uppyMin.on('upload-success', function(file,response) {
            let status = response.status;
            let body = response.body;
            // do something with file and response
            /*/
            console.log('| .:: UPLOAD 2 ICON ::. |',{
                file:file,
                status:status,
                body:body,
                nodeID:nodeID,
            });
            //*/
        });

        uppyMin.on('complete', function(file) {
            $.each(file.successful, function(index, value){
                let sizeLabel = "bytes";
                let filesize = value.size;
                if (filesize > 1024){
                    filesize = filesize / 1024;
                    sizeLabel = "kb";

                    if(filesize > 1024){
                        filesize = filesize / 1024;
                        sizeLabel = "MB";
                    }
                }
                let uploadListHtml = '' +
                    '<div class="kt-uppy__list-item" data-id="'+value.id+'">' +
                    '<div class="kt-uppy__list-label">'+value.name+' ('+ Math.round(filesize, 2) +' '+sizeLabel+')</div>' +
                    '<span class="kt-uppy__list-remove" data-id="'+value.id+'">' +
                    '<i class="flaticon2-cancel-music"></i>' +
                    '</span>' +
                    '</div>';
                $uploadedList.append(uploadListHtml);
            });

            $fileLabel.text("Добавить файл");

            $statusBar.addClass('kt-uppy__status--hidden');
            $statusBar.removeClass('kt-uppy__status--ongoing');
        });

        $(document).on('click', id + ' .kt-uppy__list .kt-uppy__list-remove', function(){
            let itemId = $(this).attr('data-id');
            uppyMin.removeFile(itemId);
            $(id + ' .kt-uppy__list-item[data-id="'+itemId+'"').remove();
        });
    };

    let setIcon = function(ths){
        let $ths = $(ths);
        nodeID = $ths.parents('li').eq(0).find('.node-link').eq(0).data('node');
        ///alert('TZCategories => setIcon => ' + nodeID);
        uppyMin.setMeta({ nodeID: nodeID });
        $categorySetIconBtn.data('node-id',nodeID).trigger('click');
    };

    let uploadIcon = function(){
        uppyMin.upload();
    };
    let clearForm = function(){
        uppyMin.reset();
        /// Пока не нашел как форму очистить, так быстрее было
        location.reload();
    };

    let delIcon = function(ths){
        let $ths = $(ths);
        nodeID = $ths.parents('li').eq(0).find('.node-link').eq(0).data('node');
        $categorySetIconBtn.data('node-id',null);
        TZAlert.fire({
            title: 'Удалить иконку ?',
            ///text: "Вы всегда сможете узнать актуальные цены и повторно добавить позицию!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет'
        }).then((result) => {
            if( result.value ){
                $.ajax({
                    url: $categorySetIconBtn.data('icon-del'),
                    type: 'post',
                    data: 'nodeID=' + nodeID,
                    dataType: 'json',
                    success: function(json){
                        ///console.log('|.:: TZCategory delIcon ::.|',json);
                        if( json.isError ){
                            TZAlert.fire( 'Ошибка :(', json.message,'error');
                        }
                        else{
                            TZAlert.fire('Успешно :)','Вы всегда можете установить новую','success' ).then(() => location.reload() );
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        });
    };

    let recommended = function(ths){
        let $ths = $(ths), $node = $ths.parents('li').eq(0).find('.node-link').eq(0);
        nodeID = $node.data('node');
        let name = $node.data('name');
        $categorySetIconBtn.data('node-id',null);
        TZAlert.fire({
            title: 'Сделать рекомендованной ?',
            text: "Все позции из выбранной категории будут отображаться в рекомендуемых товарах!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет'
        }).then((result) => {
            if( result.value ){
                $.ajax({
                    url: $categorySetIconBtn.data('set-recommended'),
                    type: 'post',
                    data: 'nodeID=' + nodeID + '&nodeName=' + encodeURIComponent(name),
                    dataType: 'json',
                    success: function(json){
                        ///console.log('|.:: TZCategory recommended ::.|',json,'nodeID=' + nodeID + 'nodeName=' + encodeURIComponent(name));
                        if( json.isError ){
                            TZAlert.fire( 'Ошибка :(', json.message,'error');
                        }
                        else{
                            TZAlert.fire('Успешно :)',json.message,'success' ).then(() => location.reload() );
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        });
    };

    return {
        // Public Function
        init: function () {
            common();
            initUppy5();
        }
        ,setIcon : setIcon
        ,uploadIcon : uploadIcon
        ,clearForm : clearForm
        ,delIcon : delIcon
        ,recommended : recommended
    };
}();
document.ready(function(){
    TZCategory.init();
});