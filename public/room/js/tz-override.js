/// https://keenthemes.com/keen/?page=docs&section=datatable
/// https://github.com/rmariuzzo/Laravel-JS-Localization
/// php artisan lang:js public/room/js/lang.dist.js
$.fn.KTDatatable.defaults.data.pageSize = 50;
$.fn.KTDatatable.defaults.translate = {
    records: {
        processing: Lang.get('messages.please___wait'),
        noRecords: Lang.get('messages.no_records_found'),
    },
    toolbar: {
        pagination: {
            items: {
                default: {
                    first: Lang.get('messages.first_page'),
                    prev: Lang.get('messages.prev_page'),
                    next: Lang.get('messages.next_page'),
                    last: Lang.get('messages.last_page'),
                    more: Lang.get('messages.more_pages'),
                    input: Lang.get('messages.page_number'),
                    select: Lang.get('messages.select_page_size'),
                    all: Lang.get('messages.all'),
                },
                info: Lang.get('messages.showing_of',{start:'{{start}}',end:'{{end}}',total:'{{total}}'}), ///'Showing {{start}} - {{end}} of {{total}}',
            },
        },
    },
};

/// https://sweetalert2.github.io/
const TZAlert = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-primary btn-lg px-4 mr-2',
        cancelButton: 'btn btn-secondary btn-lg px-4 mr-2'
    },
    buttonsStyling: false
});