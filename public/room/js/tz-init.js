JSON.check = function(str){ try { return (JSON.parse(str) && !!str); } catch (e) { return false; } };
JSON.decode = function(str){ return ( JSON.check(str) ) ? JSON.parse(str) : {err:1,str:str}; };
JSON.encode = JSON.stringify;
String.prototype.trimChars = function(c){ var re = new RegExp("^[" + c + "]+|[" + c + "]+$", "g"); return this.replace(re,""); };
Object.filter = function(obj,predicate){ let result = {}, key; for (key in obj) if (obj.hasOwnProperty(key) && !predicate(obj[key],key)) result[key] = obj[key]; return result; };
document.readCookie = function( name ){ return (name = new RegExp( '(?:^|;\\s*)'+(''+name).replace( /[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&' )+'=([^;]*)' ).exec( document.cookie )) && name[1]; };
document.setCookie = function( name, value, options ){
    options = options || {};

    var expires = options.expires;
    if( typeof expires == "number" && expires ){
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    /// Если не выставленно, то Старый костыль на 60 месяцев
    else{ /// Это что бы поведение на компе было === на мобильном так как у него по умолчанию быстро дохнут куки
        var d = new Date();
        ///d.setTime( d.getTime()+(86400 * 30 * 12) );
        d.setMonth(d.getMonth() + 60);
        expires = d;
    }

    if( expires && expires.toUTCString ){
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent( value );

    var updatedCookie = name+"="+value;

    for( var propName in options ){
        updatedCookie += "; "+propName;
        var propValue = options[propName];
        if( propValue!==true ){
            updatedCookie += "="+propValue;
        }
    }

    document.cookie = updatedCookie;
};
document.ready = function ( fn ) {
    if ( typeof fn !== 'function' ) return;
    if ( document.readyState === 'complete'  ) return fn();
    document.addEventListener( 'interactive', fn, false );
    document.addEventListener( 'complete', fn, false );
    window.addEventListener( 'load', fn, false );
};
document.ready(function(){
    /// 'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
    $.ajaxSetup({
        headers:{'X-CSRF-TOKEN':document.head.querySelector('meta[name="csrf-token"]').content},
        ///beforeSend:function(){TZMain.loader.show();},
        ///complete:function(){TZMain.loader.hide();} /// Если не нужно отключать лоадер, либо метод переопределяем, либо через setTimeout вешаем новый !!
    });
    $.extend($.expr[":"],{
        "containsIN" : function(elem, i, match, array) {
            return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
        },
        "contains" : function(elem, i, match, array) {
            return $(elem).text().toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            ///return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        }
    });
});