"use strict";

console.log("%cОстановитесь!", "font-family:Whitney,Helvetica,Arial,sans-serif;font-size:50px;font-weight:bold;text-transform:uppercase;color:#ffa834;-webkit-text-stroke:2px #26b;");
console.log("\n");
console.log("%cЭта функция браузера предназначена для разработчиков. Если кто-то сказал вам скопировать и вставить что-то здесь, " +
    "чтобы увеличить вдвое ваш баланс, или взломать чужой аккаунт, это мошенники. Выполнив эти действия, вы предоставите им доступ только к своему аккакнту.",
    "font-family:Whitney,Helvetica,Arial,sans-serif;font-size:25px;");
console.log("\n");

// Tezarius Global Methods
let TZMain = function(){
    // Private Function
    let $loader;
    let _isAuth;
    let common = function(){
        $loader = $('#loader-box');
        _isAuth = document.head.querySelector('meta[name="is-auth"]').content;
    };
    let flash = function(flashers,params){
        params = params || {};
        let state = {
            success: 'alert-success',
            error: 'alert-danger',
            info: 'alert-info',
        };
        let $row = $('<div>').addClass('row');
        flashers.forEach(function(flash) {
            let
                $alert = $('#alertTemplate').clone()
                ,$alertTitle = $alert.find('strong.title')
                ,$alertText = $alert.find('span.message')
            ;
            if( flash.title ) $alertTitle.html(flash.title);
            if( flash.text ) $alertText.html(flash.text);

            $row.append($alert.addClass(state[flash.type]).addClass('col').show());
        });
        if( params.to ) $(params.to).append($row);
        if( params.return ) return $row;
    };
    let loader = {
        show: function(callback,time){
            $('body').addClass('blured').addClass('noscroll');
            $loader.show();
            if( 'function'==typeof callback ) setTimeout(callback,time);
        },
        hide: function(callback,time){
            $loader.hide();
            $('body').removeClass('blured').removeClass('noscroll');
            if( 'function'==typeof callback ) setTimeout(callback,time);
        },
    };
    let isAuth = function(){
        return _isAuth;
    };
    return {
        // Public Function
        init: function () {
            common();
        }
        ,flash : flash
        ,loader : loader
        ,isAuth : isAuth
    };
}();
// Tezarius Request Methods
let TZCallBack = function(){
    // Private Function
    let common = function(){
        //
    };
    let init = function(){
        $('#tz-header-contacts-comment').on('shown.bs.collapse', function () {
            $(this).find('textarea').focus();
        });
        $('#tz-header-contacts').on('hidden.bs.modal', function (e) {
            $('#tz-callback-form input').removeClass('border-danger');
            $('#tz-callback-form .text-danger').remove();
        });
        $('#tz-header-contacts').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var whatever = button.data('whatever');
            if (whatever == 'callback') {
                $('#tz-header-contacts a[href=\'#tab-callback\']').trigger('click');
            } else if (whatever == 'contacts') {
                $('#tz-header-contacts a[href=\'#tab-contacts\']').trigger('click');
            }
        });
    };
    let open = function(id,ths){
        init();
        let $ths = $(ths), from = $ths.data('from'), $modal = $(id);
        if( 'fab'==from ) $modal.addClass('from-fab');
        else $modal.removeClass('from-fab');
        $modal.modal('show');
    };
    let send = function(ths){
        $('#tz-callback-form input').removeClass('border-danger');
        $('#tz-callback-form .text-danger').remove();
        let data = $('#tz-callback-form').serialize();
        $.ajax({
            url: $(ths).data('callback'),
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $('#tz-callback-send').addClass('loading').attr('disabled','diasbled');
            },
            complete: function() {
                $('#tz-callback-send').removeClass('loading').removeAttr('disabled');
            },
            success: function(json) {
                ///console.log(data,json);
                /// https://getbootstrap.com/docs/4.0/components/modal/
                $('#tz-header-contacts').modal('hide');
                if( json.state ){
                    Swal.fire(json.title,json.text,json.state);
                    document.getElementById('tz-callback-form').reset();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    return {
        // Public Function
        init: function () {
            ///common();
        }
        ,open : open
        ,send : send
    };
}();
// Common Offices Function
let TZOffices = function(){
    let topMenuID; /// ID меню выбора офиса
    let $TopMenu;  /// Объект меню
    let CurrentID; /// Текущий выбранный офис
    /// Private Function
    let common = function(){
        topMenuID = '#top-offices-links';
        ///console.log('| .:: TZOffices common ::. |');
        $TopMenu = $(topMenuID);
        CurrentID = $TopMenu.data('current')*1;
        ///
        ///console.log('| .:: O F F I C E  I D ::. |');
        if( !$('#tzOfficeChooseModal').data('office-id')*1 ) modalChoose();

    };
    let current = function(){
        return CurrentID;
    };
    let selected = function(ths){
        let $ths = $(ths);
        ///console.log('| .:: TZOffices selected ::. |',ths);
        let remote = function(){
            let officeID = $ths.data('office-id');
            let _data = new FormData();
            ///_data.append('office',officeID);
            $.ajax({
                url: '/office/choose/' + officeID,
                type: 'post',
                dataType: 'json',
                data: _data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(json) {
                    ///console.log('|.:: TZOffices add JSON ::.|',json);
                    if( json.redirect ){
                        setTimeout(function () { /// Можно было через переопределение complete отключить, но тогда придется в error-е отключать и в success если !status - ну его нахер, через setTimeout в стек бросим
                            TZMain.loader.show();
                            location.href = json.redirect;
                        });
                    }
                    /// Скорее всего ниже провалиться не должны, так как всегда будет редирект - ДОЛЖЕН БЫТЬ
                    if( json.error ){}
                    if( json.success ){}
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        };
        if( current() ) TZAlert.fire({
            title: 'Сменить офис ?',
            text: "Вы автоматически разлогинитесь в текущем!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет'
        }).then((result) => {
            if( result.value ) remote();
        });
        else remote();
    };
    let noSelected = function(stopWork){
        TZAlert.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Для продолжения выберите офис!',
            footer: '<a href>Вам нужна помощь ?</a>'
        });
        if( stopWork ) return false;
    };
    let modalChoose = function(){
        setTimeout(function() {
            $('#tzOfficeChooseModal').modal('show');
        }, 100);
    };
    /// Public Function
    return {
        init: function(){
            common();
        }
        ,current: current
        ,selected: selected
        ,noSelected: noSelected
        ,modalChoose: modalChoose
    }
}();
// Common Search Functions
let TZSearch = function(){
    // Private Function
    let common = function(){
        let
            $tzSearch = $('#tz_search')
            ,$tzSearchStr = $tzSearch.find('input[name=\'search\']')
            ,$tzSearchBtn = $tzSearch.find('button.search-button')
            ,tzSearchUri = $tzSearchStr.data('search-uri')
        ;
        $tzSearchBtn.on('click', function() {
            if( !TZOffices.current() ) TZOffices.noSelected(true);
            let value = $tzSearchStr.val();
            if( !value || value.length===0 ) TZSearch.emptySearch(true);
            else{
                TZMain.loader.show();
                location.href = tzSearchUri.replace(/#RPL#/g, encodeURIComponent(value));
            }
        });
        $tzSearchStr.on('keydown', function(e) {
            if( !TZOffices.current() ) TZOffices.noSelected(true);
            if( e.keyCode === 13 ) $tzSearchBtn.trigger('click');
        });
        $('#tz_search .select-list a').click(function(){
            $('.select-list > a').removeClass('active');
            $(this).addClass('active');
            $('.select-button .select-text').text($(this).text());
            $('.selected-category').val($(this).attr('data-category'));
        });
        /// https://github.com/bassjobsen/Bootstrap-3-Typeahead
        setTimeout(function(){
            $.ajax({
                url: '/price/history',
                type: 'get',
                dataType: 'json',
                beforeSend: function(){/* Скроем лоадер, переопределив метод по умолчанию */},
                complete: function(){/* Ну и тут соответственно */},
                success: function(json){
                    var $input = $('#tz_search input');
                    $input.typeahead({
                        source: json.history,
                        autoSelect: false,
                        selectOnBlur: false
                    });
                    $input.change(function() {
                        var current = $input.typeahead("getActive");
                        ///console.log('|.:: TZSearch common change ::.|',current);
                        if( current ){
                            // Some item from your model is active!
                            if (current.name === $input.val()) {
                                // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
                                TZMain.loader.show();
                                location.href = '/price/offers/' + current.code + '/' + current.brand;
                            } else {
                                // This means it is only a partial match, you can either add a new item
                                // or take the active if you don't want new items
                            }
                        } else {
                            // Nothing is active so it is a new value (or maybe empty value)
                        }
                    });
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        },500);
    };
    let emptySearch = function(stopWork){
        TZAlert.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Введите номер!',
            footer: '<a href>Вам нужна помощь ?</a>'
        });
        if( stopWork ) return false;
    };
    ///let history = function(){};
    return {
        // Public Function
        init: function () {
            common();
        }
        ,emptySearch: emptySearch
        ///,history: history
    };
}();
// Counter Plus Minus Functions
let TZCounter = function(){
    // Private Function
    ///let $input;
    let common = function(){
        console.log('| .:: TZCounter common ::. |');
    };
    let disable = function($btn){
        $btn.prop('disabled',true);
        $btn.attr('disabled',true);
        $btn.addClass('disabled');
    };
    let enable = function($btn){
        $btn.prop('disabled',false);
        $btn.attr('disabled',false);
        $btn.removeClass('disabled');
    };
    let plus = function(counterID){
        ///console.log('| .:: TSCounter plus ::. |');
        let $input = $(counterID).find('input.counter');
        let value = $input.val() * 1;
        let step = $input.data('step') * 1;
        ///let min = $input.data('min') * 1;
        let max = $input.data('max') * 1;

        let $btnPlus = $(counterID).find('.plus');
        let $btnMinus = $(counterID).find('.minus');

        enable($btnPlus);
        enable($btnMinus);

        value += step;
        if( value >= max ){
            value = max;
            disable($btnPlus);
        }
        $input.val(value).trigger('change');
    };
    let minus = function(counterID){
        ///console.log('| .:: TSCounter minus ::. |');
        let $input = $(counterID).find('input.counter');
        let value = $input.val() * 1;
        let step = $input.data('step') * 1;
        let min = $input.data('min') * 1;
        ///let max = $input.data('max') * 1;

        let $btnPlus = $(counterID).find('.plus');
        let $btnMinus = $(counterID).find('.minus');

        enable($btnPlus);
        enable($btnMinus);

        value -= step;
        if( value <= min ){
            value = min;
            disable($btnMinus)
        }
        $input.val(value).trigger('change');
    };
    let inputTm;
    let input = function(counterID){
        ///console.log('| .:: TSCounter input ::. |');
        if( inputTm ) clearTimeout(inputTm);
        inputTm = setTimeout(function(){
            let $input = $(counterID).find('input.counter');
            let value = $input.val() * 1;
            ///let step = $input.data('step') * 1;
            let min = $input.data('min') * 1;
            let max = $input.data('max') * 1;
            console.log('| .:: TSCounter input ::. |',counterID,value,min,max);

            let $btnPlus = $(counterID).find('.plus');
            let $btnMinus = $(counterID).find('.minus');

            enable($btnPlus);
            enable($btnMinus);

            if( value >= max ){
                value = max;
                $input.val(value);
                disable($btnPlus)
            }

            if( value <= min ){
                value = min;
                $input.val(value);
                disable($btnMinus)
            }

            $input.trigger('change');
        },1000);
    };
    let get = function(counterID){
        return $(counterID).find('input.counter').val() * 1;
    };
    let calculate = function(place,params,ths){
        params = params || {};
        let
            $ths = $(ths)
            ,$place = $(place)
            ,total = $place.data('default') * 1
            ,$inputs
            ;
        if( params.root ) /// Если есть рутовая, значит собираем со всех input-ов
        {
            $inputs = $ths.parents(params.root).find('input.counter')
        }
        else $inputs = $ths;
        $inputs.each((i,input) => {
            let
                $input = $(input)
                ,qty = $input.val() * 1
                ,free = $input.data('free') * 1
                ,rates = $input.data('rates')
                ;
            let found = Object.filter(rates,(price,id) => id > qty );
            let price = found[Object.keys(found).pop()] * 1;
            if( price ) total += price * ( qty - free );
            /*/
             console.log('| .:: TZCounter calculate ::. |',{
             total:total,
             qty:qty,
             rates:rates,
             found:found,
             key:Object.keys(found).pop(),
             price:price,
             free:free
             });
             //*/
        });

        $place.text(total);
    };
    // Public Function
    return {
        init: function(){
            common();
        }
        ,plus: plus
        ,minus: minus
        ,input: input
        ,get: get
        ,calculate: calculate
    }
}();
// Basket Add Remove Functions
let TZBasket = function(){
    /**
     * Кратенько `что-куда-зачем`
     * isfb - is fast basket change
     */
    let $fbRoot; /// - Fast Basket Root
    // Private Function
    let common = function(){
        $fbRoot = $('#cart');
    };
    let add = function(counterID,btn){
        let $btn = $(btn);
        let $input = $(counterID).find('input.counter');
        let qty = $input.val() * 1;   /// Quantity
        let rbg = $input.data('rbg'); /// GoodsID
        let tzp = $input.data('tzp'); /// TZP
        let std = $input.data('std'); /// StockID
        let pld = $input.data('pld'); /// PlaceID
        /*/
        console.log('| .:: TZBasket add ::. |',{
            counterID: counterID,
            input: $input,
            qty: qty,
            tzp: tzp
        });
        //*/
        let _data = new FormData();
        _data.append('rbg',rbg);
        _data.append('tzp',tzp);
        _data.append('qty',qty);
        _data.append('std',std);
        _data.append('pld',pld);
        _data.append('withFBI',1); /// - with fast basket info
        $.ajax({
            url: $fbRoot.data('route-basket-add'),
            type: 'post',
            dataType: 'json',
            data: _data,
            cache: false,
            contentType: false,
            processData: false,
            success: function(json) {
                ///console.log('|.:: TZBasket add JSON ::.|',json);
                if( json['cartID'] ){
                    $fbRoot.removeClass('empty-basket');
                    /// Пока тупо классы проверим, по идеи через data атрибут лучше сделать
                    if( $btn.hasClass('btn-danger') ) $btn.removeClass('btn-danger').addClass('btn-success');
                    if( $btn.hasClass('btn-primary') ) $btn.removeClass('btn-primary').addClass('btn-success');
                    if( $btn.hasClass('text-primary') ) $btn.removeClass('text-primary').addClass('text-success');
                    $btn.attr('onclick','TZBasket.change(this,true)');

                    let $btnText = $btn.find('.btn-text');
                    if( $btnText ) $btnText.text('Перейти');

                    if( json['basket'] ) fastBasketUpdate(json['basket']);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let quantity = function(ths){
        let $ths = $(ths);
        let id = $ths.data('id');
        let qty = $ths.val();
        update(id,qty,'');
    };
    let fastBasketUpdate = function(info){
        if( info.view ) $('#cart').replaceWith(info.view);
        else{
            let $totalCart = $('#cart-total');
            $totalCart.find('.products b').text(info.count);
            $totalCart.find('.prices b').text(info.total);
            /// Ход конем, пока так
            $totalCart = $('#cart .modal .cart-totals').show();
            $totalCart.find('.products b').text(info.count);
            $totalCart.find('.prices b').text(info.total);
            $totalCart.find('#totals-details .prices strong').text(info.total);
            /// НАДО ЕЩЕ ПЕРЕСТРАИВАТЬ СПИСОК СОДЕРЖИМОГО
            /// Обыграл пока генерацией цельного view
            /// Там где колво строк не меняется, будем это юзать, иначе рендерить заного и отдавать шаблоном
            ///console.log('| .:: FAST BASKET DATA UPDATE ONLY ::. |');
            if( info.count*1==0 ) $fbRoot.addClass('empty-basket'); /// Это не должно отработать, в 0 убавить нельзя, а после удаления идет перезагрузка страницы
        }
    };
    let timerNote;
    let note = function(counterID,ths){
        if( timerNote ) clearTimeout(timerNote);
        timerNote = setTimeout(function(){
            let $ths = $(ths);
            let id = $ths.data('id');
            let qty = $(counterID).find('input.counter').val();
            let note = $ths.val();
            update(id,qty,note);
        },1000);
    };
    let update = function(id,qty,note){
        ///console.log('| .:: BASKET CHANGE ::. |');
        let isfb = $('#tz-popup-cart').is(':visible'); /// Попробуем так
        let URL = 'id=' + id + '&qty=' + qty + '&note=' + note + '&withFBI=' + isfb;
        $.ajax({
            url: $fbRoot.data('route-basket-update'),
            type: 'post',
            data: URL,
            dataType: 'json',
            success: function(json) {
                console.log('|.:: TZBasket update JSON ::.|',URL,json);
                if( json.status ){
                    if( !isfb ){
                        if( json.redirect ){
                            setTimeout(function () { /// Можно было через переопределение complete отключить, но тогда придется в error-е отключать и в success если !status - ну его нахер, через setTimeout в стек бросим
                                TZMain.loader.show();
                                location.href = json.redirect;
                            });
                        }
                    }
                    else{
                        if( json['basket'] ) fastBasketUpdate(json['basket']);
                        if( json['flashers'] ) TZMain.flash(json['flashers'],{to:'#flashers'});
                    }
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
            ///,complete:function(){/* По дефолту отключаем лоадер, но сейчас нужно оставить для редиректа */}
        });
    };
    let remove = function(key,rowID){
        TZAlert.fire({
            title: 'Удалить позицию из корзины ?',
            text: "Вы всегда сможете узнать актуальные цены и повторно добавить позицию!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет'
        }).then((result) => {
            if( result.value ){
                let isfb = $('#tz-popup-cart').is(':visible'); /// Попробуем так
                $.ajax({
                    url: $fbRoot.data('route-basket-remove'),
                    type: 'post',
                    data: 'key=' + key + '&withFBI=' + isfb,
                    dataType: 'json',
                    success: function(json){
                        ///console.log('|.:: TZBasket remove JSON ::.|',json);
                        if( json.status ){
                            if( !isfb ){
                                if( json.redirect ){
                                    setTimeout(function () { /// Можно было через переопределение complete отключить, но тогда придется в error-е отключать и в success если !status - ну его нахер, через setTimeout в стек бросим
                                        TZMain.loader.show();
                                        location.href = json.redirect;
                                    });
                                }
                            }
                            else{
                                if( json['basket'] ) fastBasketUpdate(json['basket']);
                                if( json['flashers'] ) TZMain.flash(json['flashers'],{to:'#flashers'});
                                $(rowID).remove();
                            }
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        });
    };
    let create = function(ths){
        $.ajax({
            url: $fbRoot.data('route-basket-create'),
            type: 'post',
            dataType: 'json',
            success: function(json) {
                console.log('|.:: TZBasket create JSON ::.|',json);
                if( json.status ){
                    let $ths = $(ths);

                    let $root = $ths.parents('.tz-basket-tabs');
                    let $btns = $root.find('.tz-basket-tab');
                    let $last = $btns.last();
                    let $tpl = $root.find('.tz-basket-tab-tpl');

                    let classAcv = $tpl.data('class-active');
                    let classDef = $tpl.data('class-default');

                    $btns.removeClass(classAcv).addClass(classDef);

                    let $new = $tpl.clone().addClass(classAcv).data('tab-id',json['TabID']).show();
                    $last.after($new);

                    $('#basket-active-content').html('<h1 class="text-secondary">'+json.message+'</h1>');
                    $('#btn-clear-basket').remove();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let change = function(ths,askme){
        let $ths = $(ths);
        let $root = $ths.parents('.tz-basket-tabs');
        let $tpl = $root.find('.tz-basket-tab-tpl');
        let classAcv = $tpl.data('class-current');
        if( $ths.hasClass(classAcv) ) return false;
        ///
        let remote = function(){
            $.ajax({
                url: $fbRoot.data('route-basket-change'),
                type: 'post',
                dataType: 'json',
                data: 'key=' + $ths.data('tab-id'),
                success: function(json) {
                    ///console.log('|.:: TZBasket create JSON ::.|',json);
                    if( json.status ){
                        setTimeout(function () { /// Можно было через переопределение complete отключить, но тогда придется в error-е отключать и в success если !status - ну его нахер, через setTimeout в стек бросим
                            TZMain.loader.show();
                            location.href = json.redirect;
                        });
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
                ///,complete:function(){/* По дефолту отключаем лоадер, но тут не нужно */}
            });
        };
        if( askme ) TZAlert.fire({
            title: 'Перейти в корзину ?',
            text: 'При переходе вы потеряете текущий результат и выставленные фильтры',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет'
        }).then((result) => {
            if( result.value ) remote();
        });
        else remote();

    };
    let clear = function(key){
        TZAlert.fire({
            title: 'Удалить текущую корзину ?',
            text: "Все позиции из корзины будут удалены!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет'
        }).then((result) => {
            if( result.value ){
            $.ajax({
                url: $fbRoot.data('route-basket-clear'),
                type: 'post',
                data: 'key=' + key,
                dataType: 'json',
                success: function(json){
                    ///console.log('|.:: TZBasket clear JSON ::.|',json);
                    if( json.status ){
                        setTimeout(function () { /// Можно было через переопределение complete отключить, но тогда придется в error-е отключать и в success если !status - ну его нахер, через setTimeout в стек бросим
                            TZMain.loader.show();
                            location.href = json.redirect;
                        });
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
    };
    let coupon = function(input,btn){
        if( !TZMain.isAuth() ){
            TZAlert.fire({
                title: 'Что-бы использовать купон !',
                html: '' +
                    '<a href="/account/register">ЗАРЕГИСТРИРУЙТЕСЬ</a> или <a href="/account/login">АВТОРИЗУЙТЕСЬ</a> <br>' +
                    'И после вернитесь к оформлению заказа' +
                    '' +
                    '',
                icon: 'warning',
                showConfirmButton: false,
                showCancelButton: true,
                cancelButtonText: 'Позже',
            });
            return false;
        }
        let
            $btn = $(btn) ,$inp = $(input)
            ,btnDefault = $btn.data('state-default')
            ,btnSuccess = $btn.data('state-success')
            ,btnError = $btn.data('state-error')

            ,inpDefault = $inp.data('state-default')
            ,inpSuccess = $inp.data('state-success')
            ,inpError = $inp.data('state-error')

            ,coupon = $inp.val()
            ;
        $.ajax({
            url: $fbRoot.data('route-basket-coupon'),
            type: 'post',
            data: 'coupon=' + coupon,
            dataType: 'json',
            success: function(json){
                console.log('|.:: TZBasket coupon JSON ::.|',coupon,json);
                ///if( json.status ) location.href = json.redirect;
                $btn.removeClass(btnDefault).removeClass(btnSuccess).removeClass(btnError);
                $inp.removeClass(inpDefault).removeClass(inpSuccess).removeClass(inpError);
                if( json.status ){
                    ///location.href = json.redirect;
                    $btn.addClass(btnSuccess);
                    $inp.addClass(inpSuccess);

                    setTimeout(function(){
                        TZMain.loader.show();
                        location.reload();
                    });
                }
                else{
                    $btn.addClass(btnError);
                    $inp.addClass(inpError);
                    ///if( json['flashers'] ) TZMain.flash(json['flashers'],{to:'#flashers'});
                }

                ///if( json['basket'] ) fastBasketUpdate(json['basket']);
                ///if( json['flashers'] ) TZMain.flash(json['flashers'],{to:'#flashers'});
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let reward = function(input,btn){
        //
    };
    let checkout = function(url,redirect){
        let isfb = $('#tz-popup-cart').is(':visible');
        if( isfb ) $('#tz-popup-cart #fb-btn-close').trigger('click');
        if( TZMain.isAuth() ) TZAlert.fire({
            title: 'Сформировать заказ ?',
            text: "Можете оставить ваш комментарий",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Продолжить',
            cancelButtonText: 'Отмена',
            showLoaderOnConfirm: true,
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            preConfirm: (data) => {
                ///console.log('|.:: TZBasket checkout preConfirm ::.|',data);
                /// https://learn.javascript.ru/fetch
                return fetch( url, {
                    method: 'POST',
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    body: JSON.stringify({note:data}),
                }).then(response => {
                    ///console.log('| .:: SWEET ALERT PRE ::. |',response);
                    if( !response.ok ) throw new Error(response.statusText);
                    return response.json();
                }).catch(error => {
                    TZAlert.showValidationMessage(`Request failed: ${error}`);
                })
            },
            allowOutsideClick: () => !TZAlert.isLoading()
        }).then((body) => {
            ///console.log('| .:: SWEET ALERT THN ::. |',body);
            if( body.value ){
                TZAlert.fire({
                    icon: body.value.type,
                    title: body.value.text,
                    ///text: json.message.text,
                    onClose: () => {
                        if( 'success'===body.value.type ){
                            TZMain.loader.show();
                            location.href = redirect;
                        }
                    }
                });
            }
        });
        else TZAlert.fire({
            title: 'Для оформления заказа !',
            html: '' +
            '<a href="/account/register">ЗАРЕГИСТРИРУЙТЕСЬ</a> или <a href="/account/login">АВТОРИЗУЙТЕСЬ</a> <br>' +
            'И после вернитесь к оформлению заказа' +
            '' +
            '',
            icon: 'warning',
            showConfirmButton: false,
            showCancelButton: true,
            cancelButtonText: 'Позже',
        });
    };
    // Public Function
    return {
        init: function(){
            common();
        }
        ,add: add
        ,update: update
        ,remove: remove
        ,create: create
        ,change: change
        ,clear: clear
        ,quantity: quantity
        ,note: note
        ,coupon: coupon
        ,reward: reward
        ,checkout: checkout
    }
}();

// Ready && Init Section
$(function(){
    TZMain.init();
    TZCallBack.init();
    TZOffices.init();
    TZSearch.init();
    TZCounter.init();
    TZBasket.init();
});
///ready(function(){});