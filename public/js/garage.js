//"use strict";
// Class definition
// Common Search Functions
let TZGarage = function(){
    let type,mark,model,vehicle;
    // Private Function
    let common = function(){
        ///
    };
    let marksClear = function(){
        let $input = $('#input-mark');
        let options = $input.find('option')[0].outerHTML;
        $input.attr('disabled',true).html(options);
        modelsClear();
    };
    let marks = function(ths){
        marksClear();
        let $ths = $(ths);
        type = $ths.val();
        $.ajax({
            url: $ths.data('uri'),
            type: 'post',
            data: 'type=' + type,
            dataType: 'json',
            beforeSend: function() {
                ///$(ths).button('loading');
            },
            complete: function() {
                ///$(ths).button('reset');
            },
            success: function(json) {
                ///console.log('|.:: TZGarage marks JSON ::.|',getURLVar('route'),json);
                ///if( json.status ) location.href = json.redirect;
                if( json['flash'] ) $('.breadcrumb').after(json['flash']);
                if( json.message ){
                    TZAlert.fire({
                        type: json.message.type,
                        title: json.message.text,
                        ///text: json.message.text,
                    });
                }
                if( json['marks'] ){
                    let $input = $('#input-mark');
                    let options = $input.find('option')[0].outerHTML;
                    $.each(json['marks'],function(i,obj){
                        ///console.log('|.:: TZGarage mark JSON ::.|',obj);
                        options += '<option value="'+obj.manuId+'">'+obj.manuName+'</option>';
                    });
                    $input.removeAttr('disabled').html(options);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let modelsClear = function(){
        let $input = $('#input-model');
        let options = $input.find('option')[0].outerHTML;
        $input.attr('disabled',true).html(options);
        vehiclesClear();
    };
    let models = function(ths){
        modelsClear();
        let $ths = $(ths);
        mark = $ths.val();
        $.ajax({
            url: $ths.data('uri'),
            type: 'post',
            data: 'type=' + type + '&mark=' + mark,
            dataType: 'json',
            beforeSend: function() {
                ///$(ths).button('loading');
            },
            complete: function() {
                ///$(ths).button('reset');
            },
            success: function(json) {
                ///console.log('|.:: TZGarage models JSON ::.|',getURLVar('route'),json);
                ///if( json.status ) location.href = json.redirect;
                if( json['flash'] ) $('.breadcrumb').after(json['flash']);
                if( json.message ){
                    TZAlert.fire({
                        type: json.message.type,
                        title: json.message.text,
                        ///text: json.message.text,
                    });
                }
                if( json['models'] ){
                    let $input = $('#input-model');
                    let options = $input.find('option')[0].outerHTML;
                    $.each(json['models'],function(i,obj){
                        ///console.log('|.:: TZGarage mark JSON ::.|',obj);
                        options += '<option value="'+obj.modelId+'" data-name="'+obj.modelName+'" data-date="'+obj.constructioninterval+'">'
                            + obj.modelName + ' / ' + obj.constructioninterval + '' +
                            '</option>';
                    });
                    $input.removeAttr('disabled').html(options);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let vehiclesClear = function(){
        let $input = $('#input-vehicle');
        let options = $input.find('option')[0].outerHTML;
        $input.attr('disabled',true).html(options);
        autoClear();
    };
    let vehicles = function(ths){
        vehiclesClear();
        let $ths = $(ths);
        model = $ths.val();
        $.ajax({
            url: $ths.data('uri'),
            type: 'post',
            data: 'type=' + type + '&mark=' + mark + '&model=' + model,
            dataType: 'json',
            beforeSend: function() {
                ///$(ths).button('loading');
            },
            complete: function() {
                ///$(ths).button('reset');
            },
            success: function(json) {
                ///console.log('|.:: TZGarage vehicles JSON ::.|',getURLVar('route'),json);
                ///if( json.status ) location.href = json.redirect;
                if( json['flash'] ) $('.breadcrumb').after(json['flash']);
                if( json.message ){
                    TZAlert.fire({
                        type: json.message.type,
                        title: json.message.text,
                        ///text: json.message.text,
                    });
                }
                if( json['vehicles'] ){
                    let $input = $('#input-vehicle');
                    let options = $input.find('option')[0].outerHTML;
                    $.each(json['vehicles'],function(i,obj){
                        ///console.log('|.:: TZGarage mark JSON ::.|',obj);
                        options += '<option value="'+obj.carId+'" data-name="'+obj.typeName+'" data-hp="'+obj.hp+'" data-v3="'+obj.cylinderCapacityCcm+'">'
                            + obj.typeName + ' / ' + obj.hp + ' / ' + obj.impulsionType +
                            '</option>';
                    });
                    $input.removeAttr('disabled').html(options);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let autoClear = function(){
        $('#input-auto').attr('disabled',true).val('');
        $('#input-engine').attr('disabled',true).val('');
        $('#input-year').attr('disabled',true).val('');
        $('#input-vin').attr('disabled',true).val('');
    };
    let auto = function(ths){
        autoClear();

        vehicle = $(ths).val();

        let $option = $("option:selected",ths);
        ///console.log('|.:: TZGarage vehicle JSON ::.|',$option);
        let name = $option.data('name');
        let hp = $option.data('hp');
        let v3 = $option.data('v3');

        $('#input-auto').removeAttr('disabled').val(name + ', ' + hp);
        $('#input-engine').removeAttr('disabled').val(v3);
        $('#input-year').removeAttr('disabled').val('');
        $('#input-vin').removeAttr('disabled').val('');
    };
    let vcHintShow = function(ths){
        $(ths).parents('.form-group').find('.vin-code.text-success').show();
    };
    let vcHintHide = function(ths){
        $(ths).parents('.form-group').find('.vin-code.text-success').hide();
    };
    let vcHintText = function(ths){
        let count = ths.value.replace(/^\s+|\s+$/gm, '').length;
        let message = 'Введено символов: ' + count;
        let success = false;
        if( count >= 5 && count <= 7 ){
            message += " - это может быть номер кузова";
            success = true;
        }
        if( count === 17 ){
            message += " - это может быть VIN код";
            success = true;
        }

        let $btn = $('#btnGarageAdd');

        $(ths).parents('.form-group').find('.vin-code.text-success').text(message);
        if( success ) $btn.removeAttr('disabled');
        else $btn.attr('disabled',true);
    };

    let remove = function(id){
        TZAlert.fire({
            title: 'Вы действительно хотите удалить ТС ?',
            text: "Можете оставить ваш комментарий",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Продолжить',
            cancelButtonText: 'Отмена',
            showLoaderOnConfirm: true,
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            preConfirm: (data) => {
                ///console.log('|.:: TZGarage remove preConfirm ::.|',getURLVar('route'),data);
                /// https://learn.javascript.ru/fetch
                return fetch('index.php?route=account/garage/del', {
                    method: 'POST',
                    ///headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    body: JSON.stringify({id:id,note:data}),
                }).then(response => {
                    ///console.log('| .:: SWEET ALERT PRE ::. |',response);
                    if( !response.ok ) throw new Error(response.statusText);
                    return response.json();
                }).catch(error => {
                    TZAlert.showValidationMessage(`Request failed: ${error}`);
                })
            },
            allowOutsideClick: () => !TZAlert.isLoading()
        }).then((body) => {
            ///console.log('| .:: SWEET ALERT THN ::. |',body);
            if( body.value ){
                TZAlert.fire({
                    type: body.value.type,
                    title: body.value.text,
                    ///text: json.message.text,
                });
            }
        });
    };
    ///let history = function(){};
    return {
        // Public Function
        init: function () {
            common();
        }
        ,marks: marks
        ,models: models
        ,vehicles: vehicles
        ,auto: auto
        ,vcHintShow: vcHintShow
        ,vcHintHide: vcHintHide
        ,vcHintText: vcHintText

        ,remove: remove
        ///,history: history
    };
}();

// Ready && Init Scetion
jQuery(document).ready(function(){
    TZGarage.init();
});
///ready(function(){});