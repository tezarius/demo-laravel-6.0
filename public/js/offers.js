//"use strict";
// Class definition
// Common Search Functions
let TZOffer = function(){
    // Private Function
    let common = function(){
        //
    };
    let stockInfo = function($ths,callBack){
        let $dropdown = $ths.next('.dropdown-menu');

        let svgPie;
        let percent =  $ths.data('delivery-prc');
        ///console.log('| .:: STOCK INFO PERCENT 1 ::. |',percent,typeof percent);
        let _prcPie = ( percent.length===0 ) ? -1 : ( (100-percent*1) * 31.4 / 100 ).toFixed(1);
        ///console.log('| .:: STOCK INFO PERCENT 2 ::. |',percent);

        if( _prcPie >= 0 ) svgPie = '&ensp;<svg height="5rem" width="5rem" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><circle r="10" cx="10" cy="10" fill="green" /><circle r="5" cx="10" cy="10" fill="transparent" stroke="red" stroke-width="10" stroke-dasharray="'+_prcPie+' 31.4" transform="rotate(-90) translate(-20)" /></svg>';

        $.ajax({
            url: $ths.parents('#price-offers').data('stock-info'),
            type: 'post',
            data: 'stockID=' + $ths.data('stock-id') + '&dlvrMin=' + $ths.data('delivery-min') + '&dlvrMax=' + $ths.data('delivery-max'),
            dataType: 'json',
            beforeSend: false,
            success: function(json) {
                ///console.log('|.:: TZOffer stockInfo ::.|',json);
                let info = json.info.replace(/(?:\r\n|\r|\n)/g, '<br>');
                if( svgPie ){
                    info += '<hr>'+
                        '<span class="row no-gutters">' +
                        '<span class="col-auto mr-1">Процент поставки: <br><b>' + percent + '%</b></span>' +
                        '<span class="col">' + svgPie + '</span>' +
                        '</span>'
                    ;
                }
                $dropdown.html(info);
                if( 'function'===typeof callBack) callBack();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let arrStockTimer = [];
    let stockInfoLeave = function(ths){
        ///console.log('| .:: STOCK INFO MOUSE LEAVE ::. |');
        let $ths = $(ths), stockID = $ths.data('stock-id');
        if( arrStockTimer[stockID] ){
            clearTimeout(arrStockTimer[stockID]);
            arrStockTimer[stockID] = null;
            ///$(ths).trigger('click');
        }
        else $(ths).dropdown('hide');
    };
    let stockInfoEnter = function(ths){
        ///console.log('| .:: STOCK INFO MOUSE OVER ENTER ::. |');
        let $ths = $(ths), stockID = $ths.data('stock-id');
        if( !$ths.data('stock-info-load') ){
            if( arrStockTimer[stockID] ) return false;
            arrStockTimer[stockID] = setTimeout(function(){
                stockInfo($ths,function(){
                    $ths.data('stock-info-load',true);
                    $(ths).trigger('click');
                });
            },100);
        }
        else $(ths).dropdown('show');
    };
    let stockInfoClick = function(ths){
        let $ths = $(ths);
        console.log('| .:: STOCK INFO ON CLICK ::. |',$ths.data('stock-info-load'));
        if( !$ths.data('stock-info-load') ){
            stockInfo($ths,function(){
                $ths.data('stock-info-load',true);
                $(ths).dropdown('show');
            });
        }
        else $(ths).dropdown('update');
    };
    return {
        // Public Function
        init: function () {
            common();
        }
        ,stockInfoClick: stockInfoClick
        ,stockInfoEnter: stockInfoEnter
        ,stockInfoLeave: stockInfoLeave
    };
}();

// Ready && Init Section
ready(function(){
    TZOffer.init();
});