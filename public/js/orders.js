//"use strict";
// Class definition
// Common Search Functions
let TZOrders = function(){
    // Private Function
    let filterRootID = '#orders-filters';
    let common = function(){
        ///console.log('| .:: DATE TIME PICKER ::. |',$(filterRootID+' .form-control.period'));
        $(filterRootID+' .form-control.period').datetimepicker({
            format: 'L',
            todayBtn: "linked",
            language: "ru",
            autoclose: true,
            todayHighlight: true,
            dateFormat: 'yyyy.mm.dd'
        });
        $(filterRootID+' .dropdown-menu .dropdown-item').click(function(){
            let $this = $(this);
            let code = $this.data('filter');
            $this.parents(filterRootID).find('.form-control').hide();
            $this.parents(filterRootID).find('.form-control.'+code).show();

            $this.parents(filterRootID).find('.filter-name').text($this.text());
            $this.parents(filterRootID).find('.filter-name').val($this.text());
            $this.parents(filterRootID).find('.filter-code').val(code);
        });
        $('#datetimepicker4').datetimepicker({
            format: 'L'
        });
    };
    let history = function(id){
        $.ajax({
            url: $(filterRootID).data('history'),
            type: 'post',
            data: 'id=' + id,
            dataType: 'json',
            beforeSend: function() {
                ///$(ths).button('loading');
            },
            complete: function() {
                ///$(ths).button('reset');
            },
            success: function(json) {
                console.log('|.:: TZOrders history JSON ::.|',json);
                if( json['flashers'] ) TZMain.flash(json['flashers'],{to:'#flashers'});
                if( json['status'] ) TZAlert.fire({html: json.html });

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let withdraw = function(id){
        TZAlert.fire({
            title: 'Отправить запросна снятие ?',
            text: "Укажите причину!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Продолжить',
            cancelButtonText: 'Отмена',
            showLoaderOnConfirm: true,
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            preConfirm: (data) => {
                console.log('|.:: TZOrders withdraw preConfirm ::.|',data);
                /// https://learn.javascript.ru/fetch
                return fetch( $(filterRootID).data('withdraw'), {
                    method: 'POST',
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    body: JSON.stringify({id:id,note:data}),
                }).then(response => {
                    ///console.log('| .:: SWEET ALERT PRE ::. |',response);
                    if( !response.ok ) throw new Error(response.statusText);
                    return response.json();
                }).catch(error => {
                    TZAlert.showValidationMessage(`Request failed: ${error}`);
                })
            },
            allowOutsideClick: () => !TZAlert.isLoading()
        }).then((body) => {
            console.log('| .:: SWEET ALERT THN ::. |',body);
            if( body.value ){
                TZAlert.fire({
                    type: body.value.type,
                    title: body.value.text,
                    ///text: json.message.text,
                });
            }
        });
    };
    let delivery = function(id){
        TZAlert.fire({
            title: 'Запросить сроки доставки ?',
            text: "Можете оставить ваш комментарий",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Продолжить',
            cancelButtonText: 'Отмена',
            showLoaderOnConfirm: true,
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            preConfirm: (data) => {
                console.log('|.:: TZOrders delivery preConfirm ::.|',data);
                /// https://learn.javascript.ru/fetch
                return fetch( $(filterRootID).data('delivery'), {
                    method: 'POST',
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    body: JSON.stringify({id:id,note:data}),
                }).then(response => {
                    ///console.log('| .:: SWEET ALERT PRE ::. |',response);
                    if( !response.ok ) throw new Error(response.statusText);
                    return response.json();
                }).catch(error => {
                    TZAlert.showValidationMessage(`Request failed: ${error}`);
                })
            },
            allowOutsideClick: () => !TZAlert.isLoading()
        }).then((body) => {
            console.log('| .:: SWEET ALERT THN ::. |',body);
            if( body.value ){
                TZAlert.fire({
                    icon: body.value.type,
                    title: body.value.text,
                    ///text: json.message.text,
                });
            }
        });
    };
    let toWork = function(id){
        id = id || 0;
        let ids = (!id) ? $('input[type="checkbox"][name="orders\\[\\]"]:checked').map(function() { return this.value; }).get() : false;
        TZAlert.fire({
            title: 'Отправка позиции заказа в работу ?',
            text: "Можете оставить ваш комментарий",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Продолжить',
            cancelButtonText: 'Отмена',
            showLoaderOnConfirm: true,
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            preConfirm: (data) => {
                console.log('|.:: TZOrders towork preConfirm ::.|',data,ids);
                /// https://learn.javascript.ru/fetch
                return fetch( $(filterRootID).data('towork'), {
                    method: 'POST',
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    body: JSON.stringify({id:id,ids:ids,note:data}),
                }).then(response => {
                    ///console.log('| .:: SWEET ALERT PRE ::. |',response);
                    if( !response.ok ) throw new Error(response.statusText);
                    return response.json();
                }).catch(error => {
                    TZAlert.showValidationMessage(`Request failed: ${error}`);
                })
            },
            allowOutsideClick: () => !TZAlert.isLoading()
        }).then((body) => {
            console.log('| .:: SWEET ALERT THN ::. |',body);
            if( body.value ){
                TZAlert.fire({
                    icon: body.value.type,
                    title: body.value.text,
                    ///text: json.message.text,
                }).then(response => {
                    TZMain.loader.show();
                    location.reload();
                });
            }
        });
    };
    ///let history = function(){};
    return {
        // Public Function
        init: function () {
            common();
        }
        ,history: history
        ,withdraw: withdraw
        ,delivery: delivery
        ,toWork: toWork
        ///,history: history
    };
}();

// Ready && Init Scetion
jQuery(document).ready(function(){
    TZOrders.init();
});
///ready(function(){});