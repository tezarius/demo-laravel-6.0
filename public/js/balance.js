//"use strict";
// Class definition
// Common Search Functions
let TZBalance = function(){
    // Private Function
    let filterRootID = '#balance-filters';
    let common = function(){
        ///console.log('| .:: DATE TIME PICKER ::. |',$(filterRootID+' .form-control.period'));
        $(filterRootID+' .form-control.period').datetimepicker({
            format: 'L',
            todayBtn: "linked",
            language: "ru",
            autoclose: true,
            todayHighlight: true,
            dateFormat: 'yyyy.mm.dd'
        });
        $(filterRootID+' .dropdown-menu .dropdown-item').click(function(){
            let $this = $(this);
            let code = $this.data('filter');
            $this.parents(filterRootID).find('.form-control').hide();
            $this.parents(filterRootID).find('.form-control.'+code).show();

            $this.parents(filterRootID).find('.filter-name').text($this.text());
            $this.parents(filterRootID).find('.filter-name').val($this.text());
            $this.parents(filterRootID).find('.filter-code').val(code);
        });
    };
    ///let history = function(){};
    return {
        // Public Function
        init: function () {
            common();
        }
        ///,history: history
    };
}();

// Ready && Init Scetion
jQuery(document).ready(function(){
    TZBalance.init();
});
///ready(function(){});