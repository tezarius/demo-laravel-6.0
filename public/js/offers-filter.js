const TZFilter = function(){
    // Private Function
    let initial = { /// Для сортировки важны значения по умолчанию
        price : { sort: 'asc', range: null, cbox: null },
        delivery : { sort: 'asc', range: null, cbox: null },
    };
    let throughStack = false;
    let $stickyBtnApply;
    let stack = [];
    let common = function(){
        /// through the stack
        let $actionBtn = $('#filter-actions');
        throughStack = ( $actionBtn && $actionBtn.is(':visible') );
        if( throughStack ) $stickyBtnApply = $('#offersFilterApply').scrollFollow({
            speed: 100
            ,offset: 0
            ,killSwitch: 'endOffersFilter'
            ///,debug: 1
            ///,onText: 'Перестань за мной ходить!!!' // Этот текст будет написан когда скролл включен
            ///,offText: 'Скользи за мной!' //Этот текст будет написан когда скролл выключен
        });
    };
    let stickyBtnApplyFlashT;
    let stickyBtnApplyFlash = function(delay){
        /*/
        $stickyBtnApply.removeClass('d-none').delay(delay).queue(function(next){
            $(this).addClass('d-none');
            next();
        });
        //*/
        if( stickyBtnApplyFlashT ) clearTimeout(stickyBtnApplyFlashT);

        $stickyBtnApply.removeClass('d-none');
        stickyBtnApplyFlashT = setTimeout(function(){
            $stickyBtnApply.addClass('d-none');
        },delay);
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let filterApply = function($items){
        /// Скрываем все с метками на скрытие
        $items.filter(function(){ return this.className.match(/filter-.*-hide/); }).addClass('d-none');

        /// Скрываем под кномкой more если она есть
        let $offers = $items.parents('.offers-items');
        let $more = $offers.next('.row-more');
        let $show = $offers.prev('.row-more' ).find('.show-more');
        let limit = $offers.data('limit');
        let $itemVisible = $items.not('.d-none');

        let count = $itemVisible.length;
        let more = count*1 - limit*1;
        let needHide = $more.data('need-hide');
        let needShow = $show.data('need-show');

        if( more > 0 ){
            if( needHide ){
                $itemVisible.each(function(inx,item){
                    if( inx > limit ) $(item).addClass('d-none');
                });
                $more.removeClass('d-none').find('a').text('Показать еще '.declension(more,'позиция','позиции','позиций',true));
            }
            if( needShow ){
                $show.removeClass('d-none');
            }
        }
        else{
            $more.addClass('d-none');
            $show.addClass('d-none');
        }

        /// Возвращаем итемсы
        $items.parent('.offers-items').html($items);
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let _sort = function(field){
        let action = function(){
            let $offers = $('.offers-items');
            $offers.each(function(i,offer){
                let $items = $(offer).find('.row-root').removeClass('d-none');
                $items.sort(function(a,b) {
                    let $a = $(a), $b = $(b);
                    let aPrice = $a.data('filter-price') * 1;
                    let bPrice = $b.data('filter-price') * 1;
                    let aDelivery = $a.data('filter-delivery') * 1;
                    let bDelivery = $b.data('filter-delivery') * 1;
                    let _return;
                    switch(field){
                        case 'price':
                            _return = 'asc'===initial.price.sort
                                ? aPrice - bPrice
                                : bPrice - aPrice
                            ;
                            break;
                        case 'delivery':
                            _return = 'asc'===initial.delivery.sort
                                ? aDelivery - bDelivery
                                : bDelivery - aDelivery
                            ;
                            break;
                        default:
                            _return = (
                                'asc'===initial.price.sort
                                    ? aPrice - bPrice
                                    : bPrice - aPrice
                            )||(
                                'asc'===initial.delivery.sort
                                    ? aDelivery - bDelivery
                                    : bDelivery - aDelivery
                            );
                    }
                    /*/
                    console.log('| .:: SORT ::. |',{
                        return:_return,
                        sortPrice:initial.price.sort,
                        sortDelivery:initial.delivery.sort,
                        aPrice:aPrice,
                        bPrice:bPrice,
                        aDelivery:aDelivery,
                        bDelivery:bDelivery,
                    });
                    //*/
                    return _return;
                });
                $items.parent('.offers-items').html($items);
                $items = $(offer).find('.row-root'); /// Возможно это лишнее, но логически вроде как правильно, просто перебздим
                /// Скрываем по всем меткам и лимитам
                filterApply($items);
            });
        };
        if( 1===2 && throughStack ){
            if( $.inArray(filter,stack) === -1 ) stack[filter] = function(){action();};
            stickyBtnApplyFlash(3000);
            return false;
        }
        TZMain.loader.show(function(){
            action();
            TZMain.loader.hide();
        });
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let _rangeTimer = [];
    let _range = function (min,max,filter){
        let action = function(){
            let $offers = $('.offers-items');
            $offers.each(function(i,offer){
                /// Показываем все и ставим метку по фильтру
                let $items = $(offer).find('.row-root').addClass('filter-'+filter+'-hide').removeClass('d-none');
                $items.each(function(_,item){
                    let $item = $(item ), value = $item.data('filter-'+filter);
                    if( value >= min && value <=max ){
                        $item.removeClass('filter-'+filter+'-hide');
                    }
                });
                /// Скрываем по всем меткам и лимитам
                filterApply($items);
            });
        };
        if( throughStack ){
            if( $.inArray(filter,stack) === -1 ) stack[filter] = function(){action();};
            stickyBtnApplyFlash(3000);
            return false;
        }
        if( _rangeTimer[filter] ) clearTimeout(_rangeTimer[filter]);
        _rangeTimer[filter] = setTimeout(function(){
            TZMain.loader.show(function(){
                action();
                TZMain.loader.hide();
            });
        },1000);
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let price = function(type,as,field){
        if( 'sort'===type ){
            initial.price.sort = as ? as : 'asc'===initial.price.sort ? 'desc' : 'asc';
            _sort(field);
        }
        if( 'range'===type || 'input'===type ){
            let $input = $(field);
            let max = $input.val();
            if( !max ){
                max = $input.attr('max');
                $input.val(max);
            }
            $(as).val(max);
            _range(0,max,'price')
        }
    };
    let delivery = function(type,as,field){
        if( 'sort'===type ){
            initial.delivery.sort = as ? as : 'asc'===initial.delivery.sort ? 'desc' : 'asc';
            _sort(field);
        }
        if( 'range'===type || 'input'===type ){
            let $input = $(field);
            let max = $input.val();
            if( !max ){
                max = $input.attr('max');
                $input.val(max);
            }
            $(as).val(max);
            _range(0,max,'delivery')
        }
    };
    let count = function(type,as,field){
        if( 'range'===type || 'input'===type ){
            let $input = $(field);
            let min = $input.val();
            let max = $input.attr('max');
            _range(min,max,'count');
        }
    };
    let sort = function(select){
        let value = select.options[select.selectedIndex].value;
        switch(value){
            case 'sort-asc':
                price('sort','asc','price');
                break;
            case 'sort-desc':
                price('sort','desc','price');
                break;
            case 'delivery-asc':
                delivery('sort','asc','delivery');
                break;
        }
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let brandsTimer;
    let brands = function(){
        let filter = 'brand';
        let action = function(){
            let checked = $('.filter-'+filter+':checked').map(function(_,el){
                return $(el).val();
            }).get();
            let $offers = $('.offers-items');
            $offers.each(function(i,offer){
                /// Показываем все и ставим метку по фильтру
                let $items = $(offer).find('.row-root').addClass('filter-'+filter+'-hide').removeClass('d-none');
                $items.each(function(_,item){
                    let $item = $(item ), value = $item.data('filter-'+filter);
                    if( (checked.length>0 && $.inArray(value,checked) !== -1) || checked.length===0 ){
                        $item.removeClass('filter-'+filter+'-hide');
                    }
                });
                /// Скрываем по всем меткам и лимитам
                filterApply($items);
            });
        };
        if( throughStack ){
            if( $.inArray(filter,stack) === -1 ) stack[filter] = function(){action();};
            stickyBtnApplyFlash(3000);
            return false;
        }
        if( brandsTimer ) clearTimeout(brandsTimer);
        brandsTimer = setTimeout(function(){
            TZMain.loader.show(function(){
                action();
                TZMain.loader.hide();
            });
        },2000);
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let more = function(ths){
        let $ths = $(ths);
        TZMain.loader.show(function(){

            if( $ths.data('show') ){
                $(ths)
                    .parents('.row-more').addClass('d-none').data('need-hide',0)
                    .prev().find('.row-root').removeClass('d-none')
                    .parents('.offers-items').prev().find('.show-more').removeClass('d-none').data('need-show',1)
                ;
            }
            else{
                $ths
                    .parents('.show-more').addClass('d-none' ).data('need-show',0)
                    .parents('.row-more').next().next().data('need-hide',1)
                ;
            }

            let $offers = $('.offers-items');
            $offers.each(function(i,offer){

                /// Показываем все и ставим метку по фильтру
                let $items = $(offer).find('.row-root');

                /// Скрываем по всем меткам и лимитам
                filterApply($items);
            });
            TZMain.loader.hide();
        });
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let propTimers = [];
    let properties = function(filterID){
        let filter = 'properties';
        let action = function(filterID){
            let checked = $('.filter-'+filter+'-'+filterID+':checked').map(function(_,el){
                return $(el).val();
            }).get();
            let $offers = $('.offers-items');
            $offers.each(function(i,offer){

                /// Показываем все и ставим метку по фильтру
                let $items = $(offer).find('.row-root').addClass('filter-'+filter+'-'+filterID+'-hide').removeClass('d-none');
                $items.each(function(_,item){
                    let $item = $(item ), properties = Object.values($item.data('filter-'+filter));
                    ///console.log('| F I L T E R  P R O P E R T I E S |',properties, typeof properties);

                    if( checked.length>0 ){
                        $.each(checked,function(_,value){
                            if( $.inArray(value,properties) !== -1 ) $item.removeClass('filter-'+filter+'-'+filterID+'-hide');
                        });
                    }
                    else $item.removeClass('filter-'+filter+'-'+filterID+'-hide');

                });

                /// Скрываем по всем меткам и лимитам
                filterApply($items);
            });
        };
        if( throughStack ){
            if( $.inArray(filter+filterID,stack) === -1 ) stack[filter+filterID] = function(){action(filterID);};
            stickyBtnApplyFlash(3000);
            return false;
        }
        if( propTimers[filterID] ) clearTimeout(propTimers[filterID]);
        propTimers[filterID] = setTimeout(function(){
            TZMain.loader.show(function(){
                action();
                TZMain.loader.hide();
            });
        },2000);
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    let apply = function(){
        TZMain.loader.show(function(){
            Object.keys(stack).map(function(key) {
                let callback = stack[key];
                if( 'function'===typeof callback) callback();
            });
            $stickyBtnApply.addClass('d-none');
            $('#offers-filter').removeClass('active');
            TZMain.loader.hide();
        },200);
    };
    let reset = function(){
        /// Пока без динамики, некогда
        let input, $input;

        input = '#inputPrice';
        $input = $(input);
        if( $input && $input.length > 0 ){
            $input.val('');
            price('input','#rangePrice',input);
        }

        input = '#inputDelivery';
        $input = $(input);
        if( $input && $input.length > 0 ){
            $input.val('');
            delivery('input','#rangeDelivery',input);
        }

        input = '.filter-brand';
        $input = $(input);
        if( $input && $input.length > 0 ){
            $input.prop('checked', false);
            brands();
        }

        input = '.filter-properties-all';
        $input = $(input);
        if( $input && $input.length > 0 ){
            $input.prop('checked', false);
            properties('all');
        }

        apply();
    };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    return {
        // Public Function
        init: function () {
            common();
            // Other Init Method;
        }
        ,price : price
        ,delivery : delivery
        ,count : count
        ,sort : sort
        ,brands : brands
        ,properties : properties
        ,more : more

        ,apply : apply
        ,reset  : reset
    };
}();
ready(function(){
    TZFilter.init();
    ///TZSearch.otherMethod(param1,param2);
});
